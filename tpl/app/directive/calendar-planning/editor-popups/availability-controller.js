/**
 * In questo file vanno inseriti tutti i controllers custom per la finestra di edit/creazione dei meetings. Il controller da utilizzare viene passato come
 * parametro del meeting-template alla directive
 */
'use strict'
app.controller('AvailabilityController',
		function($rootScope, $scope, $timeout, $http, UserService, MessageService, FormsService, AvailabilityService, $modal,$filter,FormUtilService,REGEXP) {

		console.log($rootScope.timeUnitMap)
		//Inizializza 
		$scope.init = function() {
			$scope.timeUnits = [];
			$scope.days = [];
			$scope.availabilityOperationApplicationTypes = [];
			
			//Recupera tutte le costanti
			//$rootScope.constants = {};
			$scope.constants = angular.copy($rootScope.constants);
			$scope.dayUnitMap = angular.copy($scope.constants['DayOfWeek']);
			$scope.timeUnitMap = angular.copy($scope.constants['TimeUnitAvailability']);
			

			
			$scope.timeUnitMap.sort(function(a, b) {
			    return a.order - b.order;
			})
					
			
				$scope.availabilityOperationApplicationTypesMap = angular.copy($scope.constants['UserAvailabilityOperationApplicationType']);

			
				
				//debugger;
				for(var k=0; k<$scope.timeUnitMap.length; k++) {
					$scope.timeUnits.push( {id: $scope.timeUnitMap[k].id, name: $scope.timeUnitMap[k].label });
				}
				
				for(var k=0; k<$scope.dayUnitMap.length; k++) {
					$scope.days.push( {id: $scope.dayUnitMap[k].id, name: $scope.dayUnitMap[k].label });
				}
				
				for(var k=0; k<$scope.availabilityOperationApplicationTypesMap.length; k++) {
					$scope.availabilityOperationApplicationTypes.push( {id: $scope.availabilityOperationApplicationTypesMap[k].id, name: $scope.availabilityOperationApplicationTypesMap[k].label });
				}
				
				
				
				
				
				$scope.user = $rootScope.user;

				$scope.errors = {};
				
				$scope.users = $scope.users || [];

				
				$scope.availability = $scope.availability !== undefined && $scope.availability != null ? angular.copy($scope.availability) : {};
				$scope.availability.recursiveOriginal = $scope.availability.recursive;
				if($scope.availability.user === undefined) {
					$scope.availability.user = {};
					if($scope.users.length == 1) {
						$scope.availability.user.id = $scope.users[0].id;
					}
				}
		
			
			
			
		};

		$scope.save = function() {
			
			var form = {
				formProperties : [
					{ id:'availability.user.id',value : $scope.availability.user.id, required : true },
					{ id:'availability.startDate',value : $scope.availability.startDate, required : true },
					{ id:'availability.startTime',value : $scope.availability.startTime, required : true },
					{ id:'availability.endTime', value : $scope.availability.endTime, required: true },
					{ id:'availability.available', value : $scope.availability.available, required: true },
					{ id:'availability.recursive', value : $scope.availability.recursive, required: true }
				]
			};
			if($scope.availability.recursive !== undefined && $scope.availability.recursive) {
				form.formProperties.push({ id:'availability.repeatValue', value : $scope.availability.repeatValue, required: true });
				form.formProperties.push({ id:'availability.repeatUnit', value : $scope.availability.repeatUnit, required: true });
				if($scope.availability.id !== undefined && $scope.availability.recursiveOriginal !== undefined && $scope.availability.recursiveOriginal) {
					form.formProperties.push({ id:'availability.operationType', value : $scope.availability.operationType, required: true });
				}
			}
			
			$scope.errors = FormUtilService.validateForm(form);
			
			if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
				
				if(!$scope.availability.recursive) {
					$scope.availability.repeatValue = undefined;
					$scope.availability.repeatUnit = undefined;
					$scope.availability.repeatDays = undefined;
					$scope.availability.repeatEnd = undefined;
					$scope.availability.operationType = undefined;
				}
				else {
					if($scope.availability.repeatUnit !== 'WEEKS') {
						$scope.availability.repeatDays = undefined;
					}
				}
				
				//debugger
				if($scope.availability.id !== undefined) {
					var availabilityUpdate = angular.copy($scope.availability);
					availabilityUpdate.search = {};
					delete availabilityUpdate.user;
					delete availabilityUpdate.timeCreated;
					delete availabilityUpdate.timeModified;
					delete availabilityUpdate.recursiveOriginal;
					var pars = {
	 					'filters': {}
					}
					AvailabilityService.update({ userId: $scope.availability.user.id }, availabilityUpdate,
						function(result) {
							$scope.closePopup(true);
						},
						function(error) {
							MessageService.showError("Errore", "Impossibile aggiornare le disponibilità");
						}
					);
				}
				else {
					var availabilityUpdate = angular.copy($scope.availability);
					delete availabilityUpdate.user;
					delete availabilityUpdate.recursiveOriginal;
					var pars = {
	 					'filters': {}
					}
					{ userId: $scope.availability.user.id };
					AvailabilityService.create({ userId: $scope.availability.user.id }, availabilityUpdate,
						function(result) {
							$scope.closePopup(true);
						},function(error) {
							MessageService.showError("Errore", "Impossibile creare le disponibilità");
						}
					);
				}
			}
			else {
				MessageService.showError('Dati errati','I dati evidenziati in rosso non sono corretti');
			}
		}
		
		$scope.undo = function() {
			$scope.closePopup(false);
		}
		
		$scope.delete = function() {
			console.log( $scope.availability);
			if($scope.availability.id !== undefined) {
				var form = {
					formProperties : [
						{ id:'availability.startDate',value : $scope.availability.startDate, required : true }
					]
				};
				//debugger;
				if($scope.availability.recursive !== undefined && $scope.availability.recursive) {
					if($scope.availability.id !== undefined) {
						form.formProperties.push({ id:'availability.operationType', value : $scope.availability.operationType, required: true });
					}
				}
				
				$scope.errors = FormUtilService.validateForm(form);
				
				if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
					var availabilityUpdate = {
						'id' : $scope.availability.id,
						'startDate' : $scope.availability.startDate,
						'recursiveId' : $scope.availability.recursiveId,
						'operationType' : $scope.availability.operationType,
						'search' : {}
					} 
					var pars = {
	 					'filters': {}
					}
					
					
					AvailabilityService.delete({ userId: $scope.availability.user.id }, availabilityUpdate,
						function(result) {
							$scope.closePopup(true);
							MessageService.showSuccess("Oeprazione avvenuta con sucesso", "");
						},
						function(error) {
							MessageService.showError("Errore", "Impossibile aggiornare le disponibilità");
						}
					);
				}
				else {
					MessageService.showError('Dati errati','I dati evidenziati in rosso non sono corretti');
				}
			}
			else {
				$scope.undo();
			}
		}
		
		$scope.init();
		
	}
);
