'use strict'
app.controller('StatsController', function(
		$rootScope,
		$scope,
		$state, 
		TherapyService,
		TerritoryService,
		DrugService,
		$http,
		$sce,
		$modal,
		RESTURL) {
	
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.control = {};
		$scope.selects = {};
		$scope.initFilters();
		
		TherapyService.search({}, {}, function(result) {
			$scope.selects['Therapies'] = result.data;
		}, function(error) {
			$scope.selects['Therapies'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
		
		TerritoryService.searchRegions({}, {}, function(result) {
			$scope.selects['Regions'] = result.data;
		}, function(error) {
			$scope.selects['Regions'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
		
		DrugService.search({}, {}, function(result) {
			$scope.selects['Drugs'] = result.data;
		}, function(error) {
			$scope.selects['Drugs'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.filters = $scope.searchFilters.filters;
		$scope.refreshCharts();
	}
	
	$scope.initFilters = function() {
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		$scope.filters = $scope.searchFilters.filters;
	}
	
	$scope.refreshCharts = function() {
		$scope.filters = $scope.searchFilters.filters;
		$scope.refresh = true;
	}
	
	$scope.init();
});