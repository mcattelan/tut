'use strict'

app.controller('ProgramController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state, 
		ProgramService,
		ServiceConfService,
		CustomerService) {

		//Inizializza sia la lista che la scheda
		$scope.init = function() {
			$scope.loggedUser = $rootScope.loggedUser;
			
			$scope.errors = {};
			$scope.initDetails();
		};
	
		$scope.initDetails = function() {
			ProgramService.get(function(result) {
				$scope.detail = {};
				for(var i=0; i<result.data.length; i++) {
					if(result.data[i].id == 'NAME') $scope.detail['name'] = result.data[i].value;
					if(result.data[i].id == 'DESCRIPTION') $scope.detail['description'] = result.data[i].value;
					if(result.data[i].id == 'INTERNAL_NOTES') $scope.detail['internalNotes'] = result.data[i].value;
					if(result.data[i].id == 'CUSTOMER_ID') {
						CustomerService.get({ id: result.data[i].value }, function(res) {
							$scope.detail.customer = res.data;
						});
					}
				}
			}, function(error) {
            	$scope.detail = {};
            	if(error.status == 404) {
            		$state.go('access.not-found');
            	}
        	});
			
			ServiceConfService.get(function(result) {
				$scope.list = result.data;
			}, function(error) {
            	$scope.list = {};
            	if(error.status == 404) {
            		$state.go('access.not-found');
            	}
        	});
		};
		
		$scope.showServiceConfDetail = function(id) {
			$state.go('app.service', {id: id});
		}
		
		$scope.edit = function(id) {
			$state.go('app.service', {id: id});
		}
			
		$scope.init();
	}
);
