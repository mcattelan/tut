'use strict'

app.controller('CustomerController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		CustomerService,
		PermissionService,
		RESTURL,
		SMART_TABLE_CONFIG,
		$sce) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		
		$scope.errors = {};
		$scope.selects = {};
		
		if($scope.id === undefined || $scope.id == null) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id) && $scope.id !== '') {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'CUSTOMER' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		
		if (!$scope.isNew()) {
			CustomerService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
				type: 'COMPANY',
			};
			
			PermissionService.getAll({ entityType : 'CUSTOMER' }, function(result) {
				$scope.initPermission(result.data);
				PermissionService.getAllFields({ entityType : 'CUSTOMER' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});

		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		CustomerService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {

			} else {
				$scope.update();
			}
		}
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest;
		CustomerService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			
		});	
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.name', value : $scope.detail.name, type: 'string', required: true },
					{ id: 'detail.sex', value: $scope.detail.sex, required:true, type:'string' },
					{ id: 'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.vatNumber', value : $scope.detail.vatNumber, type: 'string',  dependRequired: 'detail.fiscalCode', pattern: REGEXP.VAT_NUMBER,  customMessage:'Il campo deve rispettare il formato della partita iva' },
					{ id: 'detail.type' , value : $scope.detail.type, type: 'string',  required: true},
					{ id: 'detail.representativePhoneNumber', value : $scope.detail.representativePhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeCellPhoneNumber', value : $scope.detail.representativeCellPhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeFaxNumber', value : $scope.detail.representativeFaxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeEmail', value : $scope.detail.representativeEmail, type: 'string', pattern: REGEXP.EMAIL,  customMessage: "L'indirizzo mail deve essere valido" }
					]
		};
		if($scope.detail.type === 'PERSON') {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale' });
		}
		else {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.VAT_NUMBER_OR_FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale o della partita iva' });
		}
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};
		
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.customers");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.loadRequest = function() {
		return {
			type : $scope.detail.type,
			name: $scope.detail.name,
			description: $scope.detail.description,
			fiscalCode: $scope.detail.fiscalCode,
			vatNumber: $scope.detail.vatNumber,
			phoneNumber: $scope.detail.phoneNumber,
			faxNumber: $scope.detail.faxNumber,
			address: $scope.detail.address,
			internalNotes: $scope.detail.internalNotes,
			representativeFirstName: $scope.detail.representativeFirstName, 
			representativeLastName: $scope.detail.representativeLastName,
			representativePhoneNumber: $scope.detail.representativePhoneNumber,
			representativeFaxNumber: $scope.detail.representativeFaxNumber,
			representativeCellPhoneNumber: $scope.detail.representativeCellPhoneNumber,
			representativeEmail: $scope.detail.representativeEmail
		}
	}
	
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			$scope.list = $scope.list || [];
			
			angular.extend({filters:{"CUSTOMER_ID":$scope.id}}, $scope.pagination);
			//delete $scope.searchFilters.total;
			
			var exportFilter = angular.copy({filters:{"CUSTOMER_ID":$scope.id}});
			if(allData !== undefined && allData != null && allData == true) {
				exportFilter.start = 0;
				delete exportFilter.size;
			} 
	
			$http.post(RESTURL.CUSTOMER_USER + '/search/export-xls', exportFilter, {responseType:'arraybuffer'})
			.success(function (response, status, xhr) {
				var file = new Blob([response], {type: 'application/vnd.ms-excel'});
				var fileURL = URL.createObjectURL(file);
				$scope.pdf = $sce.trustAsResourceUrl(fileURL);
				var link = angular.element('<a/>');
				link.attr({
					href : fileURL,
					target: '_blank',
					download: 'export.xls'
				})[0].click();
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione pdf non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}

	$scope.init();
});
