'use strict'

app.controller('PatientUserController', function($rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state, 
		PatientService,
		PatientUserService,
		FormUtilService,
		MessageService,
		UserService,
		PermissionService,
		NationalityService,
		REGEXP,
		SMART_TABLE_CONFIG) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		
		$scope.selects = {
				'Nationalities' : []
		};
		
		NationalityService.getAll({ }, function(result) {
			$scope.selects['Nationalities'] = result.data;
		}, function(error) {
			$scope.selects['Nationalities'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
		
		if($scope.patientId === undefined || $scope.patientId == null) {
			$scope.patientId = $stateParams.patientId;
		}
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'PATIENT_USER' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		if (!$scope.isNew()) {
			PatientUserService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.initPermission(result.permissions);
				$scope.fieldPermissions = result.fieldPermissions;
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			$scope.detail = {
					departments : []
			};
			
			PermissionService.getAll({ entityType : 'PATIENT_USER' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'PATIENT_USER' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		
		PatientUserService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				if ($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.list.splice(i, 1);
				} else {
					$scope.list[i].selected = false;
				}
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
			
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		request.patientId = $scope.patientId;
		PatientUserService.insert({ }, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {

			}
		});
	};
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PatientUserService.update({ id : $scope.id },request,function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PatientUserService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.patients");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.lastName', value: $scope.detail.lastName, required:true, type:'string' },
					{ id:'detail.firstName', value: $scope.detail.firstName, required:true, type:'string' },
					{ id:'detail.sex', value: $scope.detail.sex, required:true, type:'string' }
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters); 
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.patients");	
		}
		else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.patient", {id: id});
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = [];
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}				
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}
	
	$scope.loadRequest = function() {
		return {
			title : $scope.detail.title,	
			lastName: $scope.detail.lastName,
			firstName: $scope.detail.firstName,
			delegateType : $scope.detail.delegateType,
			sex: $scope.detail.sex,
			nationality: $scope.detail.nationality,
			phoneNumber: $scope.detail.phoneNumber,
			faxNumber: $scope.detail.faxNumber,
			cellPhoneNumber: $scope.detail.cellPhoneNumber,
			email: $scope.detail.email,
			internalNotes: $scope.detail.internalNotes,
			address: $scope.detail.address,
			skypeId: $scope.detail.skypeId
		}
	}

	$scope.init();
});