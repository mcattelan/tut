'use strict'

app.controller('PortalsController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		MessageService,
		UserService) {
	
	
	$scope.programs = [];
		
	
	$scope.onlyOneProgram = function(){
		
		var nonAdminPortal = 0;
		for(var i = 0; i < $scope.programs.length; i++){
			
			if($scope.programs[i].id != 'admin'){
				nonAdminPortal ++;
			}
		}
		
		if(nonAdminPortal == 1){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.init = function(){
		
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.qualifications = [];

		if($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			UserService.getLoggedUserPortals({},
				function(result) {
					$scope.programs = result.data;
					
					
					if($scope.programs.length == 1 && $scope.loggedUser.originalUserId === undefined){
						
						window.location.href = $scope.programs[0].url;
						
					}
					/*else if($scope.onlyOneProgram() && $scope.loggedUser.originalUserId === undefined){
						
						var faund = false;
						var portal = null;
						
						for(var i = 0; i < $scope.programs.length && !faund; i++){
							
							if($scope.programs[i].id != 'admin'){
								faund  =true;
								portal = $scope.programs[i].url;
							}
						}
						if(portal != null){
							window.location.href = portal;
						}
					}*/
				
				}, function(error) {
					MessageService.showError('Errore in fase di salvataggio', error.data !== undefined ? error.data.message : '');
				}
			);
		}
		else {
			$state.go('access.login');
		}
	}
	
	
	$scope.redirectTo = function(url){
		
		window.location.href=url;
	}
	
	$scope.unimpersonate = function(impUserId) {
		UserService.unimpersonate(
			{},
			function(response) {
				$state.go('access.login');
			},
			function(error) {
				MessageService.showError('Impossibile procedere con l\'operazione', 'Errore imprevisto');
			}
		);
	};
	
	$scope.init();
});