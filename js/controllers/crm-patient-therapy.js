'use strict'

app.controller('PatientTherapyController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		PatientService,
		PatientTherapyService,
		TherapyService,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}

		$scope.selects = {
				'Therapy' : $rootScope.therapies
		};

		if($scope.id !== undefined && $scope.id != null) {
			PatientService.get({ id : $scope.id }, function(result) {
				$scope.patient = result.data;
				$scope.initList();
			}, function(error) {
				$scope.program = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			$state.go('access.not-found');
		}
	};
	
	$scope.initList = function() {
		$scope.selection = $scope.selection || false;
		$scope.selected = $scope.selected || [];
		$scope.initPaginationAndFilter();
		$scope.search();
	};
	
	$scope.search = function(tableState) {
		$scope.patientTherapies =  [];
		if($scope.selection) {
			// Sono in aggiunta/modifica/cancellazione utente dal programma

			$scope.pagination.start = tableState !== undefined ? tableState.pagination.start : 0;
			angular.extend($scope.searchFilters, $scope.pagination);
			delete $scope.searchFilters.total;
			delete $scope.searchFilters.size;

			$scope.selectedIds = []
			for(var i=0; i<$scope.selected.length; i++) {
				$scope.selectedIds[i] = $scope.selected[i].id;
			}

			// La ricerca è sugli utenti
			TherapyService.search({}, $scope.searchFilters, function(result) {
				for(var i=0; i<result.data.length; i++) {
					$scope.patientTherapies[i] = {
							'id': 'id-' + i,
							'patient': $scope.patient,
							'therapy': result.data[i],
					}
				}
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;

				var therapySearchFilters = {
						filters: { 'PATIENT_ID' : $scope.id }
				}

				PatientTherapyService.search({}, therapySearchFilters, function(result) {
					$scope.pUserList = {};
					for(var i=0; i<result.data.length; i++) {
						$scope.pUserList[result.data[i].id] = result.data[i];
					}
					for(var i=0; i<$scope.patientTherapies.length; i++) {
						var pUser = $scope.pUserList[$scope.patientTherapies[i].therapy.id];
						if(pUser !== undefined) {
							$scope.patientTherapies[i].id = pUser.id;
							$scope.patientTherapies[i].therapy = pUser.therapy;
						}
					}
					for(var i=0; i<$scope.patientTherapies.length; i++) {
						$scope.patientTherapies[i].selected = $scope.selectedIds.indexOf($scope.patientTherapies[i].id) >= 0;
					}
				}, function(error) {
					$scope.patientTherapies = [];
					$scope.initPaginationAndFilter();
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.patientTherapies = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			// Sono in visualizzazione e non ho la ricerca, ma sono tutti quelli del programma
			// TODO da controllare
			ProgramService.getAllProgramTherapy({ id : $scope.id }, function(result) {
				$scope.patientTherapies = result.data;
			}, function(error) {
				$scope.patientTherapies = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.patientTherapies.length; i++) {
			$scope.patientTherapies[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.patientTherapies.length; i++) {
			$scope.patientTherapies[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		if($scope.selection) {
			$scope.selectedIds = []
			for(var i=0; i<$scope.selected.length; i++) {
				$scope.selectedIds.push($scope.selected[i].id);
			}
			for(var i=0; i<$scope.patientTherapies.length; i++) {
				if($scope.patientTherapies[i].selected) {
					if($scope.selectedIds.indexOf($scope.patientTherapies[i].id) < 0) {
						$scope.selected.push($scope.patientTherapies[i]);
						$scope.selectedIds.push($scope.patientTherapies[i].id);
					}
					else {
						// Già presente nella lista, aggiorno ruolo e qualifiche
						$scope.selected[$scope.selectedIds.indexOf($scope.patientTherapies[i].id)] = $scope.patientTherapies[i];
					}
				}
				else {
					if($scope.selectedIds.indexOf($scope.patientTherapies[i].id) >= 0) {
						$scope.selected.splice($scope.selectedIds.indexOf($scope.patientTherapies[i].id), 1);
						$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.patientTherapies[i].id), 1);
					}
				}
			}
			
			if($scope.modal) {
				$scope.modal.close();
			}
		}
	}
	
	$scope.undoSelection = function() {
		$scope.initList();
		$scope.modal.dismiss();
	}

	$scope.init();
});