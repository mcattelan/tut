'use strict'

app.controller('SupplierListCtrl', function(
		$scope,
		$controller,
		SupplierService) {
	
	$scope.preInit = function() {
		SupplierService.isMedicalServiceSupport(function(result){
			$scope.ismedicalSupportService = result.data != undefined ? result.data : false;
		});
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('SUPPLIER', SupplierService);
});