'use strict'

app.controller('DrugPickUpListCtrl', function(
		$scope,
		$controller,
		DrugPickUpService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DRUG_PICK_UP', DrugPickUpService);
});

app.controller('DrugPickUpDetailController', function(
		$scope,
		$state,
		$rootScope,
		$modal,
		DrugPickUpService,
		DrugPickUpItemService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.complementaryId)) {
			$scope.initDetails();
		}
	}


	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		
		if(angular.isDefined($scope.complementaryType) && $scope.complementaryType == 'DRUG_PICK_UP_DELIVERY') {
			DrugPickUpService.getByUserComplementaryId({ complementaryId : $scope.complementaryId }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			//tramite l'id della complementare cerco di capire se l'oggetto DrugPickUp è stato già salvato oppure no
			DrugPickUpService.getWithDefaults({ complementaryId : $scope.complementaryId }, function(result) {
				$scope.detail = result.data;
	
				//Sovrascrivo con dati della form che passo alla direttiva
				if(angular.isDefined($scope.activitiForm)) {
					//il patient non lo sovrascrivo perchè non può essere cambiato
					if(angular.isDefined($scope.activitiForm.locationType)) {
						$scope.detail.locationType	= $scope.activitiForm.locationType;
					}
					if(angular.isDefined($scope.activitiForm.address)) {
						$scope.detail.address = $scope.activitiForm.address;
					}
					if(angular.isDefined($scope.activitiForm.date)) {
						$scope.detail.pickUpDate = $scope.activitiForm.date;
					}
					if(angular.isDefined($scope.activitiForm.owner) && angular.isDefined($scope.activitiForm.owner.id)) {
						$scope.detail.owner	= $scope.activitiForm.owner;
					}
					if(angular.isDefined($scope.activitiForm.pharmacy) && angular.isDefined($scope.activitiForm.pharmacy.id)) {
						$scope.detail.pharmacy	= $scope.activitiForm.pharmacy;
					}
				} 
	
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		DrugPickUpService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		DrugPickUpService.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	};

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					DrugPickUpService.delete({ id : id, }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};

	$scope.removeItem = function(index) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.detail.items.splice(index, 1);
				}
		);
	};

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.patient' , value : $scope.detail.patient, type: 'string',  required: true},
					{ id: 'detail.pharmacy' , value : $scope.detail.pharmacy, type: 'string',  required: true},
					{ id: 'detail.owner' , value : $scope.detail.owner, type: 'string',  required: true},
					{ id: 'detail.pickUpDate' , value : $scope.detail.pickUpDate, type: 'string',  required: true},
					{ id: 'detail.locationType' , value : $scope.detail.locationType, type: 'string',  required: true}
					]
		};

		// Se l'address è valorizzato controllo l'obbligatorietà di tutti i campi necessari per la spedizione
		if (angular.isDefined($scope.detail.address) && $scope.detail.address) {
			form.formProperties.push({ id:'address.country', value : $scope.detail.address.country, required: true });
			form.formProperties.push({ id:'address.county', value : $scope.detail.address.county, required: true });
			form.formProperties.push({ id:'address.province', value : $scope.detail.address.province, required: true });
			form.formProperties.push({ id:'address.city', value : $scope.detail.address.city, required: true });
			form.formProperties.push({ id:'address.address', value : $scope.detail.address.address, required: true });
		}
		errors = FormUtilService.validateForm(form);
		if(!angular.isDefined($scope.detail.items) || $scope.detail.items.length <= 0) {
			errors['detail.items'] = "E' necessario aggiungere almeno un farmaco in lista";
		}
		if(angular.isDefined($scope.detail.patient) && !angular.isDefined($scope.detail.patient.id)) {
			errors['detail.patient'] = "E' obbligatorio inserire il paziente";
		}
		if(angular.isDefined($scope.detail.pharmacy) && !angular.isDefined($scope.detail.pharmacy.id)) {
			errors['detail.pharmacy'] = "E' obbligatorio inserire la farmacia di riferimento del paziente";
		}
		if(angular.isDefined($scope.detail.owner) && !angular.isDefined($scope.detail.owner.id)) {
			errors['detail.owner'] = "E' obbligatorio inserire il responsabile dell'attività prima di confermare i dati di ritiro!";
		}
		
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.isNew = function() {
		return ($scope.detail.id == undefined || $scope.detail.id == '');
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.loadRequest = function() {
		var items = [];
		for(var i = 0; i < $scope.detail.items.length; i++) {
			var item = $scope.detail.items[i];
			if(angular.isDefined(item)) {
				items.push({
					drugId: item.drug.id,
					collectedQuantity: item.collectedQuantity,
					batchNumber: item.batchNumber,
					drugExpiringDate: item.drugExpiringDate,
					internalNotes: item.internalNotes,
					measurementUnit: item.measurementUnit
				})
			}
		}
		return {
			patientId : $scope.detail.patient.id,
			pharmacyId : $scope.detail.pharmacy.id,
			ownerId : $scope.detail.owner.id,
			pickUpDate : $scope.detail.pickUpDate,
			code : $scope.detail.code,
			internalNotes : $scope.detail.internalNotes,
			address : $scope.detail.address,
			locationType : $scope.detail.locationType,
			complementaryActivityId : $scope.complementaryId,
			items : items
		}
	}
	
	$scope.init();

});