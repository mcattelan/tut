'use strict'

app.controller('PatientMaterialHandlingListCtrl', function(
		$scope,
		$controller,
		PatientMaterialHandlingService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_MATERIAL_HANDLING', PatientMaterialHandlingService);
});

app.controller('PatientMaterialHandlingDetailCtrl', function(
		$scope,
		$controller,
		PatientMaterialHandlingService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_MATERIAL_HANDLING', PatientMaterialHandlingService);
	
});