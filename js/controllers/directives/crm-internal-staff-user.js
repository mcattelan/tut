'use strict'

app.controller('InternalStaffUserListCtrl', function(
		$scope,
		$controller,
		$state,
		MessageService,
		UserService,
		InternalStaffUserService) {
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					UserService.impersonate({ 'nickName' : nickName }, function(response) {
						$state.go('access.login');
					}, function(error) {
						MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
					});
				}
		);
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('INTERNAL_STAFF_USER', InternalStaffUserService);
});