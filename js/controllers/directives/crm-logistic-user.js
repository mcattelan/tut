'use strict'

app.controller('LogisticUserListCtrl', function(
		$scope,
		$controller,
		LogisticUserService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('LOGISTIC_USER', LogisticUserService);
});