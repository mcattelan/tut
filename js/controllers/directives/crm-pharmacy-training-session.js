'use strict'

app.controller('PharmacyTrainingSessionListCtrl', function(
		$scope,
		$controller,
		PharmacyTrainingSessionService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PHARMACY_TRAINING_SESSION', PharmacyTrainingSessionService);
});

app.controller('PharmacyTrainingSessionDetailCtrl', function(
		$scope,
		$controller,
		ProgramService,
		TrainingSubjectService,
		PharmacyTrainingSessionService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.postInit = function() {
//		$scope.selects['Programs'] = [];
//		ProgramService.search({}, {}, function(result) {
//			$scope.selects['Programs'] = result.data;
//		}, function(error) {
//			$scope.selects['Programs'] = [];
//			if(error.status == 404) {
//				$state.go('access.not-found');
//			}
//		});
		
		$scope.selects['TrainingSubjects'] = [];
		TrainingSubjectService.search({}, {filters:{'TRAINING_SESSION_TYPE': 'PHARMACY'}}, function(result) {
			$scope.selects['TrainingSubjects'] = result.data;
		}, function(error) {
			$scope.selects['TrainingSubjects'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.init('PHARMACY_TRAINING_SESSION', PharmacyTrainingSessionService);
});