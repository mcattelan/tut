'use strict'
app.controller('PatientPharmacyListCtrl', function(
		$scope,
		$state,
		$rootScope,
		$controller,
		$q,
		PatientPharmacyService,
		MessageService,
		FormUtilService,
		PermissionService) {

	$scope.preInit = function() {
		$scope.isInternalStaff = $rootScope.isInternalStaff();
		$scope.selectedCopy = [];
	}

	$controller('BaseListCtrl', { $scope: $scope });

	$scope.init('PATIENT_PHARMACY', PatientPharmacyService);

	$scope.saveSelection = function() {
		$scope.request = {};
		$scope.pharmacies = [];
		$scope.referencePharmacy = false;
		
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.pharmacies.push($scope.selected[i].id);
		}
		$scope.request = {
				pharmacyIds: $scope.pharmacies
		};

		for(var i=0; i<$scope.list.length; i++){
			if($scope.referencePharmacy==false){
				if($scope.list[i].reference){
					$scope.referencePharmacy = true;
				}else{
					$scope.referencePharmacy = false;
				}	
			}
		}

		PatientPharmacyService.updateAllPatientPharmacy({ patientId : $scope.id }, $scope.request, function(result) {
			$scope.search();
		}, function(error) {
			MessageService.showError('Salvataggio non riuscito.');
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		if($scope.referencePharmacy == false){
			MessageService.showWarning('Salvataggio riuscito , ma manca la farmacia di riferimento.')
		}
	}
	
	$scope.setReference = function(patientPharmacyId){
		PatientPharmacyService.updateReferencePatientPharmacy({ patientPharmacyId : patientPharmacyId},  function(result) {
			$scope.search();
		}, function(error) {
			MessageService.showError('Errore nel rendere la farmacia di riferimento.');
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		MessageService.showSuccess('Farmacia di riferimento impostata correttamente.');
	}
	
	$scope.confirmReference = function() { 
		var validationError = {};
		if($scope.selection) {
			$scope.mergeList();

			for(var i=0; i<$scope.list.length; i++) {
				if ($scope.list[i].selected) {
					console.log($rootScope);
					validationError = $rootScope.extend(validationError,$scope.validate($scope.list[i], i));
				}
			}

			if ($scope.modal && (validationError == undefined || angular.equals(validationError,{}))) {
				$scope.selected.length = 0;
				for(var i=0; i<$scope.selectedCopy.length; i++) {
					$scope.selected.push($scope.selectedCopy[i]);
				}
				$scope.modal.close();
			} else{
				$scope.errors = validationError;
				console.log("err",$scope.errors);
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		}
	}
	
	$scope.searchAllPharmacies = function() {
		var d = $q.defer();
		PatientPharmacyService.search({}, {filters: {PATIENT_ID: $scope.id}}, function(result) {
			$scope.selected = $scope.selected || [];
			for(var i=0; i<result.data.length; i++) {
				$scope.selected[i] = result.data[i].pharmacy;	
			}
			d.resolve(result.data);
		}, function(error) {
			$scope.selected = [];
			d.resolve([]);
		});
		return d.promise;
	}
	
	$scope.mergeList = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selectedCopy.length; i++) {
			$scope.selectedIds.push($scope.selectedCopy[i].id);
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selectedCopy.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista, aggiorno ruolo e qualifiche
					$scope.selectedCopy[$scope.selectedIds.indexOf($scope.list[i].id)] = $scope.list[i];
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selectedCopy.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
	}

//	$scope.confirmSelection = function() { 
//		var validationError = {};
//		if($scope.selection) {
//			$scope.mergeList();
//
//			for(var i=0; i<$scope.list.length; i++) {
//				if ($scope.list[i].selected) {
//					console.log($rootScope);
//					validationError = $rootScope.extend(validationError,$scope.validate($scope.list[i], i));
//				}
//			}
//
//			if ($scope.modal && (validationError == undefined || angular.equals(validationError,{}))) {
//				$scope.selected.length = 0;
//				for(var i=0; i<$scope.selectedCopy.length; i++) {
//					$scope.selected.push($scope.selectedCopy[i]);
//				}
//				$scope.modal.close();
//			} else{
//				$scope.errors = validationError;
//				console.log("err",$scope.errors);
//				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
//			}
//		}
//	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}
	
	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

});

app.controller('PatientPharmacyDetailCtrl', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$q,
		$state,
		PatientPharmacyService,
		PharmacyService,
		FormUtilService,
		PermissionService,
		MessageService,
		RESTURL,
		$sce) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}

		if($scope.id !== undefined && $scope.id != null) {
			PatientPharmacyService.get({ id : $scope.id }, function(result) {
				$scope.program = result.data;
				$scope.initList();
			}, function(error) {
				$scope.program = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			$state.go('access.not-found');
		}
	};

	$scope.initList = function() {
		$scope.showList = false;

		PermissionService.getAll({ entityType : 'PATIENT_PHARMACY' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = true;
			$scope.selected = $scope.selected || [];
			$scope.selectedCopy = [];
			for(var i=0; i<$scope.selected.length; i++) {
				$scope.selectedCopy.push($scope.selected[i]);
			}
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};


	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;

			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];

		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		$scope.mergeList();

		PharmacyService.search({}, $scope.searchFilters, function(result) {
			$scope.list = [];
			for(var i=0; i<result.data.length; i++) {
				$scope.list[i] = {
						'id': 'id-' + result.data[i].id,
						'pharmacy': result.data[i],
				}
				$scope.selectedIds = []
				for(var x=0; x<$scope.selectedCopy.length; x++) {
					$scope.selectedCopy[x].id = 'id-' + $scope.selectedCopy[x].pharmacy.id;
					$scope.selectedIds.push($scope.selectedCopy[x].id);
				}
				var index = $scope.selectedIds.indexOf($scope.list[i].id);
				if (index >= 0) {
					$scope.list[i].selected = true;
				}
			}
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}

		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.validate = function(toValidate, index) {
		var errors = {};
		var form = {
				formProperties : [

					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.search = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canExport = permissions.EXPORT;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters = angular.copy($scope.initialSearchFilters);
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.mergeList = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selectedCopy.length; i++) {
			$scope.selectedIds.push($scope.selectedCopy[i].id);
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selectedCopy.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista, aggiorno ruolo e qualifiche
					$scope.selectedCopy[$scope.selectedIds.indexOf($scope.list[i].id)] = $scope.list[i];
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selectedCopy.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
	}

	$scope.confirmSelection = function() { 
		var validationError = {};
		if($scope.selection) {
			$scope.mergeList();

			for(var i=0; i<$scope.list.length; i++) {
				if ($scope.list[i].selected) {
					console.log($rootScope);
					validationError = $rootScope.extend(validationError,$scope.validate($scope.list[i], i));
				}
			}

			if ($scope.modal && (validationError == undefined || angular.equals(validationError,{}))) {
				$scope.selected.length = 0;
				for(var i=0; i<$scope.selectedCopy.length; i++) {
					$scope.selected.push($scope.selectedCopy[i]);
				}
				$scope.modal.close();
			} else{
				$scope.errors = validationError;
				console.log("err",$scope.errors);
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		}
	}

	$scope.undoSelection = function() {
		$scope.initList();
		$scope.modal.dismiss();
	}
});