'use strict'

app.controller('PatientAssumptionListCtrl', function(
		$scope,
		$controller,
		$rootScope,
		$state,
		$window,
		PatientAssumptionService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.search = function(tableState, smCtrl) {
		if(!$scope.showList) return;
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		if($scope.preSearch !== undefined){
			$scope.preSearch();
		}
		
		$scope.service.search({ 'patientId': $scope.patientId}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for (var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
			
			if($scope.filterStoreKey) {
				try {
					$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
					$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
				}
				catch(e) {
					console.log(e);
				}
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
//				$state.go('access.not-found');
			}
		});
	}
	
	$scope.init('PATIENT_ASSUMPTION', PatientAssumptionService);
	
});