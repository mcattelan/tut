'use strict'

app.controller('TutorDiaryController', function(
		$scope,
		$rootScope,
		$controller,
		$timeout,
		PatientService) {
	
	$scope.init = function() {
		$scope.list = [];
		$scope.therapyList = [];
		$scope.showList = true;
		$scope.detail = {};
		$scope.assumptionsFilters = {};
		$scope.showAssumptions = false;
		$scope.isHcp = $rootScope.isHcp;
		
		PatientService.diaryDetails({ id : $scope.patientId }, {}, function(result) {  
			$scope.therapyList = result.data;
			if($scope.therapyList && $scope.therapyList.length > 0) {
				$scope.detail = $scope.therapyList[0];
				$scope.loadInjections($scope.detail.drug.id);
			}
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
		
	$scope.onDrugChange = function() {
		var drugId = '';
		if($scope.detail != undefined && $scope.detail != null && $scope.detail.drug != undefined){
			drugId = $scope.detail.drug.id;
		}
		$scope.loadInjections(drugId);
	}
	
	$scope.loadInjections = function(drugId){
		var filters = {'TUTOR_DIARY_DRUG_ID': drugId};
		$scope.showAssumptions = undefined;
		PatientService.homeInjections({ id : $scope.patientId }, {filters}, function(result) {  
			$scope.list = result.data;
			$timeout(function() { 	$scope.showAssumptions = true; }, 1000);
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.init();
});