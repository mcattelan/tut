'use strict'

app.controller('PatientMaterialListCtrl', function(
		$scope,
		$controller,
		PatientMaterialService,
		MaterialService,
		$q) {

	$scope.preInit = function() {
		var d = $q.defer();
		MaterialService.search({}, {}, function(result) {
			$scope.selects['Material'] = result.data;
		}, function(error) {
			$scope.selects['Material'] = undefined;
			d.resolve(undefined);
		});
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_MATERIAL', PatientMaterialService);
	
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// GiÃ  presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
			if($scope.list[i].userMaterials) {
				for(var j=0; j<$scope.list[i].userMaterials.length; j++) {
					if($scope.list[i].userMaterials[j].selected) {
						if($scope.selectedIds.indexOf($scope.list[i].userMaterials[j].id) < 0) {
							$scope.selected.push($scope.list[i].userMaterials[j]);
							$scope.selectedIds.push($scope.list[i].userMaterials[j].id);
						}
						else {
							// GiÃ  presente nella lista
						}
					}
					else {
						if($scope.selectedIds.indexOf($scope.list[i].userMaterials[j].id) >= 0) {
							$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].userMaterials[j].id), 1);
							$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].userMaterials[j].id), 1);
						}
					}
				}
			}
		}
		if($scope.postConfirmSelectionFn !== undefined) {
			$scope.postConfirmSelectionFn();
		}
		if($scope.modal !== undefined) {
			$scope.modal.close();
		}
	}
	
	$scope.unselectUserMaterials = function(materialKit) {
		if(materialKit && materialKit.userMaterials) {
			for(var j=0; j<materialKit.userMaterials.length; j++) {
				materialKit.userMaterials[j].selected = undefined;
			}
		}
	}
});

app.controller('PatientMaterialDetailCtrl', function(
		$rootScope,
		$scope,
		$controller,
		PatientMaterialService,
		MaterialService,
		$q) {
	
	$scope.postInit = $scope.postInit || function() {
		var d = $q.defer();
		MaterialService.search({}, {}, function(result) {
			$scope.selects['Material'] = result.data;
		}, function(error) {
			$scope.selects['Material'] = undefined;
			d.resolve(undefined);
		});
	}
	
	$scope.materialChange = function() {
		$scope.detail.userMaterials = [];
		if(angular.isDefined($scope.detail.material) && angular.isDefined($scope.detail.material.materials)) {
			var j = 0;
			for(var i = 0; i < $scope.detail.material.materials.length; i++) {
				var material = $scope.detail.material.materials[i];
				var x = 0;
				while(x < material.quantity) {
					$scope.detail.userMaterials[j] = {
							'userId': $scope.patientId,
							'material': material.material,
							'status': 'ADMITTED_TO_PATIENT'
					}
					x++;
					j++;
				}
			}
		}
	}
	
	$scope.initDetailsConfirm = function() {
		$scope.confirmDetails = true;
		$scope.newDefaultData = $scope.detail;
		$scope.initDetails();
	}
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	//Inizializza sia la lista che la scheda
	$scope.init = function(entityType, service) {
		if(entityType !== undefined && service !== undefined) {
			$scope.entityType = entityType;
			$scope.service = service;
		
			$scope.loggedUser = $rootScope.loggedUser;
	
			$scope.errors = {};
			$scope.warnings = {};
			$scope.selects = {};
			
			if(angular.isObject($scope.detail)) {
				$scope.initDetailsConfirm();
			} else {
				$scope.newDefaultData = {
						'userId': $scope.patientId,
						'status': 'ADMITTED_TO_PATIENT'
				}
				$scope.initDetails();
			}
		}
		else {
			MessageService.showError('Errore imprevisto', '');
			$state.go('access.not-found');
		}
	};
	
	$scope.confirm = function() {
		angular.merge($scope.newDefaultData, $scope.detail);
		$scope.detail = $scope.newDefaultData;
		$scope.modal.close();
	}
	
	$scope.init('PATIENT_MATERIAL', PatientMaterialService);
	
});