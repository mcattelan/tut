'use strict'

app.controller('UserQuestionnaireListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		UserService,
		UserQuestionnaireService,
		QuestionnaireService,
		MessageService,
		RESTURL,
		$sce,
		$modal,
		$state,
		$http,
		$filter,
		$ocLazyLoad) {
	
	$scope.preInit = function() {
		$scope.searchFilters = {
			filters: {	
				'USER_ID' : $scope.userId	
			}
		}
		if($scope.userId) {
			UserService.getUser({ userId: $scope.userId }, function(response) {
				$scope.user = response.data;
			}, function(error) {
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		
		//nuovi questionari esterni alle attività  
		$scope.listUserQuest = [];
		$scope.questSearchFilters = { 
			filters : {
				'CAN_CREATE_FOR_USER': $scope.userId
			}
		};
		
		$scope.canCreateUserQuestionnaire = false;
		QuestionnaireService.search({}, $scope.questSearchFilters, function(result) {
			$scope.listUserQuest = result.data;
			if($scope.listUserQuest.length > 0) {
				$scope.canCreateUserQuestionnaire = true;
			}
			
		}, function(error) {
		
		});
	}
	
	$scope.draw = function(plot, canvascontext) {
		if(plot.getData().length > 0 && plot.getData()[0]['data'] != undefined) {
			$("#" + $(plot.getPlaceholder()).attr('id') + " > .data-point-label").remove()
			$.each(plot.getData()[0].data, function(i, el) {
				var o = plot.pointOffset({x: el[0], y: el[1]});
				$('<div class="data-point-label">' + $filter('truncateNumber')(el[1], 1) + '</div>').css({
					position: 'absolute',
					left: o.left - 5,
					top: o.top - 20,
					display: 'none'
				}).appendTo(plot.getPlaceholder()).fadeIn('slow');
			});
		}
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.createNewUserQuest = function(questId, userId) {
		var title = 'Conferma la creazione nuovo questionario';
		var text = 'Si desidera procedere con la creazione di un nuovo questionario?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style);
		modalInstance.result.then(
			function(confirm) {
				var request = {
					    'questionnaireId': questId,
					    'userId': userId
					}
				UserQuestionnaireService.insert({}, request, function(result) {
					$scope.search(undefined, undefined);
					
					if(result && result.data && result.data.userQuestionnaire && result.data.userQuestionnaire.id) {
						var questScope = $scope.$new(true);
						questScope.id = result.data.userQuestionnaire.id;
						
						$scope.modal =  $modal.open({
							scope: questScope,
							templateUrl: 'tpl/app/directive/questionnaire/user-questionnaire-modal.html',
							windowClass: 'largeModal',
							size: 'lg',
							controller: 'UserQuestionnaireDetailCtrl',
							resolve: {
								deps: function($ocLazyLoad) {
									return $ocLazyLoad.load(['ui.select','smart-table','rzModule']).then(
										function() {
											return $ocLazyLoad.load( ['js/controllers/directives/user-questionnaire.js'] );
										}
									)
								}
							}
						});
						
						$scope.modal.result.then(function(result) {
							
						}, function () {
							
						});
					}
				}, function(error) {
					MessageService.showError('Inserimento non riuscito');
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		);
	}
	
	$scope.init('USER_QUESTIONNAIRE', UserQuestionnaireService);
	
});

app.controller('UserQuestionnaireDetailCtrl', function(
		$scope,
		UserQuestionnaireService,
		$rootScope,
		$timeout,
		$filter,
		$modal,
		MessageService,
		SupplierUserService,
		$sce) {
	
	
	$scope.canViewQuest = function(userQuest) {
		if(!$scope.permissions['VIEW']) return false;
		
		return userQuest.canView;
	}
	
	$scope.canCompileQuest = function(userQuest) {
		if(!$scope.permissions['UPDATE']) return false;
		
		return userQuest.canCompile;
	}
	
	$scope.canApproveQuest = function(userQuest) {
		if(!$scope.permissions['UPDATE']) return false;
		
		return userQuest.canApprove;
	}
	
	$scope.canViewQuestScore = function(userQuest) {
		if(!$scope.permissions['VIEW']) return false;
		
		return userQuest.canViewScore;
	}
	
	$scope.canDeleteQuest = function(userQuest) {
		return $scope.permissions['DELETE'];
		
		
	}
	
	
	$scope.loadUserQuestionnaire = function(userQuestionnaireId, activityId) {
		
		
		UserQuestionnaireService.get({ id: userQuestionnaireId, activityId: activityId }, function(result) {
			$scope.permissions = result.permissions;
			$scope.detail = $scope.completeUserQuestionnaire(result.data);
			var pageNumber = 1;
			$scope.detail.currentPage = pageNumber;
			for(var q=0; q<$scope.detail.userQuestionnaire.questionnaireVersion.questions.length; q++) {
				$scope.detail.userQuestionnaire.questionnaireVersion.questions[q].page = pageNumber;
				if ($scope.detail.userQuestionnaire.questionnaireVersion.questions[q].type == 'PAGE_JUMPS') {
					pageNumber++;
				}
			}
			$scope.detail.lastPage = pageNumber;
			$rootScope.$broadcast('rzSliderForceRender'); //Force refresh sliders on render. Otherwise bullets are aligned at left side.
		}, function(error) {
			MessageService.showError('Recupero questionario fallito','Non è possibile recuperare il questionario');
		});
	};
	
	$scope.completeUserQuestionnaire = function(cq) {
		cq.choices = {};
		cq.userRepliesId = {};
		cq.userRepliesText = {};
		cq.sliderValues = {};
		
		cq.questionIndexes = {};
		var questionIndex = 1;
		for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
			var question = cq.userQuestionnaire.questionnaireVersion.questions[q];
			question.originalDisabled = question.disabled;
			question.text = $sce.trustAsHtml(question.text);
			$scope.registerEnableQuestionWatch(cq, question);
			
			if(question.type === 'SCORE' || question.type === 'SCORE_SLIDER') {
				question.min = undefined;
				question.max = undefined;
				question.step = undefined;
				if(question.replies.length > 0) {
					question.min = parseFloat(question.replies[0].text);
					question.max = parseFloat(question.replies[question.replies.length - 1].text);
					question.step = parseFloat(question.replies[1].text) - question.min;
				}
				else if(question.replies.length == 1) {
					question.min = parseFloat(question.replies[0].text);
					question.max = parseFloat(question.replies[0].text);
					question.step = 0;
				}
			}
			
			if(question.type!='TEXT_BLOCK' && question.type!='PAGE_JUMPS') {
				cq.questionIndexes[question.id] = parseInt(questionIndex);
				questionIndex++;
			}
			cq.choices[question.id] = [];
			for(var r=0; r<question.replies.length; r++) {
				cq.choices[question.id][r] = { "id": question.replies[r].id, "text": question.replies[r].text };
			}
			cq.userRepliesId[question.id] = undefined;
			cq.userRepliesText[question.id] = undefined;
			for(var r=0; r<cq.replies.length; r++) {
				if(cq.replies[r].questionId == question.id) {
					switch(question.type) {
					case 'SCORE':
					case 'SINGLE_CHOICE':
						cq.userRepliesId[question.id] = undefined;
						cq.userRepliesText[question.id] = undefined;
						for(var i=0; i<cq.replies[r].replies.length; i++) {
							cq.userRepliesId[question.id] = cq.replies[r].replies[i].id;
							cq.userRepliesText[question.id] = cq.replies[r].replies[i].text;
						}
						break;
					case 'SCORE_SLIDER':
						cq.userRepliesId[question.id] = undefined;
						cq.userRepliesText[question.id] = undefined;
						for(var i=0; i<cq.replies[r].replies.length; i++) {
							cq.userRepliesId[question.id] = cq.replies[r].replies[i].id;
							cq.userRepliesText[question.id] = cq.replies[r].replies[i].text;
							cq.sliderValues[question.id] = cq.replies[r].replies[i].text;
						}
						break;
					case 'MULTIPLE_CHOICE':
						cq.userRepliesId[question.id] = [];
						cq.userRepliesText[question.id] = [];
						for(var i=0; i<cq.replies[r].replies.length; i++) {
							cq.userRepliesId[question.id][i] = cq.replies[r].replies[i].id;
							cq.userRepliesText[question.id][i] = cq.replies[r].replies[i].text;
						}
						break;
					case 'FREE':
					case 'NUMERIC_VALUE':
					case 'DATE':
					case 'DATETIME':
						cq.userRepliesId[question.id] = undefined;
						cq.userRepliesText[question.id] = undefined;
						for(var i=0; i<cq.replies[r].replies.length; i++) {
							cq.userRepliesId[question.id] = cq.replies[r].replies[i].id;
							cq.userRepliesText[question.id] = cq.replies[r].replies[i].text;
							break;
						}
						break;
					}
				}
			}
		}
		console.log('Completed questionnaire');
		console.log(cq);
		
		$scope.canView = $scope.canViewQuest(cq);
		$scope.canCompile = $scope.canCompileQuest(cq);
		$scope.canApprove = $scope.canApproveQuest(cq);
		$scope.canViewScore = $scope.canViewQuestScore(cq);
		$scope.canDelete = $scope.canDeleteQuest(cq);
		
		return cq;
	}
	
	$scope.registerEnableQuestionWatch = function(cq, question) {
		var enableWatch = false;
		if(cq !== undefined && question !== undefined) {
			if(question.enableQuestions !== undefined && question.enableQuestions.length > 0) {
				enableWatch = true;
			}
			else {
				for(var r=0; r<question.replies.length; r++) {
					if(question.replies[r].enableQuestions !== undefined && question.replies[r].enableQuestions.length > 0) {
						enableWatch = true;
						break;
					}
				}
			}
		}
		if(enableWatch) {
			$scope.$watch(
				function() { 
					if($scope.hasAnswers(cq, question)) {
						return cq.userRepliesId[question.id] + '_' + (cq.userRepliesText[question.id] !== undefined && cq.userRepliesText[question.id] !== '');
					}
					return '';
				},
				function(nv, ov) {
					$scope.enableQuestions(cq);
				}, true);
		}
	}
	
	$scope.hasAnswers = function(cq, question) {
		cq.userRepliesId[question.id]
		switch(question.type) {
		case 'SCORE':
		case 'SINGLE_CHOICE':
			return cq.userRepliesId[question.id] !== undefined && cq.userRepliesId[question.id] != null;
		case 'SCORE_SLIDER':
			return cq.sliderValues[question.id] != undefined && cq.sliderValues[question.id] != null;
		case 'MULTIPLE_CHOICE':
			return cq.userRepliesId[question.id] !== undefined && cq.userRepliesId[question.id] != null && !angular.equals(cq.userRepliesId[question.id], []);
		case 'FREE':
		case 'NUMERIC_VALUE':
		case 'DATE':
		case 'DATETIME':
			return cq.userRepliesId[question.id] !== undefined && cq.userRepliesId[question.id] != null && cq.userRepliesText[question.id] !== undefined && cq.userRepliesText[question.id] != null && cq.userRepliesText[question.id] !== '';
		}
	}
	
	$scope.hasAnswer = function(cq, question, replyId) {
		cq.userRepliesId[question.id]
		switch(question.type) {
		case 'SCORE':
		case 'SINGLE_CHOICE':
			return cq.userRepliesId[question.id] !== undefined && cq.userRepliesId[question.id] === replyId;
		case 'SCORE_SLIDER':
			//Da verificare
			if(cq.sliderValues[question.id] != undefined) {
				for( var i=0; i<question.replies.length; i++ ) {
					if(question.replies[i].text == cq.sliderValues[question.id] && question.replies[i].id == replyId)
						return true;
				}
				return false;
			}
		case 'MULTIPLE_CHOICE':
			if(cq.userRepliesId[question.id] !== undefined) {
				for(var i=0; i<cq.userRepliesId[question.id].length; i++) {
					if(cq.userRepliesId[question.id][i] === replyId) {
						return true;
					}
				}
			}
			return false;
		case 'FREE':
		case 'NUMERIC_VALUE':
		case 'DATE':
		case 'DATETIME':
			return cq.userRepliesId[question.id] !== undefined && cq.userRepliesId[question.id] === replyId && cq.userRepliesText[question.id] !== undefined && cq.userRepliesText[question.id] != null && cq.userRepliesText[question.id] !== '';
		}
	}
	
	$scope.enableQuestions = function(cq) {
		for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
			var qr = cq.userQuestionnaire.questionnaireVersion.questions[q];
			qr.disabled = qr.originalDisabled;
		}
		for(var qId in cq.userRepliesId) {
			for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
				if(qId == cq.userQuestionnaire.questionnaireVersion.questions[q].id) {
					var question = cq.userQuestionnaire.questionnaireVersion.questions[q];
					if($scope.hasAnswers(cq, question)) {
						if(question.enableQuestions !== undefined) {
							for(var i=0; i<question.enableQuestions.length; i++) {
								for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
									var qr = cq.userQuestionnaire.questionnaireVersion.questions[q];
									if(qr.position === question.enableQuestions[i].position) {
										qr.disabled = false;
									}
								}
							}
						}
					}
					for(var r=0; r<question.replies.length; r++) {
						if($scope.hasAnswer(cq, question, question.replies[r].id) && question.replies[r].enableQuestions !== undefined) {
							for(var i=0; i<question.replies[r].enableQuestions.length; i++) {
								for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
									var qr = cq.userQuestionnaire.questionnaireVersion.questions[q];
									if(qr.position === question.replies[r].enableQuestions[i].position) {
										qr.disabled = false;
									}
								}
							}
						}
					}
				}
			}
		}
		for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
			var qr = cq.userQuestionnaire.questionnaireVersion.questions[q];
			if(qr.disabled) {
				if($scope.hasAnswers(cq, qr)) {
					if(angular.isArray(cq.userRepliesId[qr.id])) {
						cq.userRepliesId[qr.id] = [];
						cq.userRepliesText[qr.id] = [];
						if(qr.type=='SCORE_SLIDER') cq.sliderValues[qr.id] = [];
					}
					else {
						cq.userRepliesId[qr.id] = undefined;
						cq.userRepliesText[qr.id] = undefined;
						if(qr.type=='SCORE_SLIDER') cq.sliderValues[qr.id] = undefined;
					}
				}
			}
		}
	}
	
	$scope.updateUserQuestionnaire = function(permanent) {
		var cq = $scope.detail;
		if(permanent == undefined || permanent == null) permanent = false;
		
		
		var replies = [];
		
		var modalInstance = undefined;
		
		for(var q=0; q<cq.userQuestionnaire.questionnaireVersion.questions.length; q++) {
			var question = cq.userQuestionnaire.questionnaireVersion.questions[q];
			var userReplyId = cq.userRepliesId[question.id];
			var userReplyText = cq.userRepliesText[question.id];
			if(question.type == 'SCORE_SLIDER') {
				userReplyText = cq.sliderValues[question.id];
				if(userReplyText==undefined) {
					console.log('Undefined dallo slider!');
					userReplyId = undefined;
				}
				else {
					for(var i=0; i<question.replies.length; i++) {
						if(question.replies[i].text == userReplyText) 
							userReplyId = question.replies[i].id;
					}
				}
			}
			if(question.type == 'DATE' || question.type=='DATETIME'){
				if(userReplyId == undefined){
					userReplyId = question.type;
				}
			}
			if(userReplyId !== undefined && userReplyId != null) {
				var questionReplies = { "questionId": question.id, "replies": []};
				switch(question.type) {
				case 'SCORE':
				case 'SCORE_SLIDER':
				case 'SINGLE_CHOICE':
					questionReplies.replies.push({ "id": userReplyId, "text": userReplyText });
					break;
				case 'MULTIPLE_CHOICE':
					if(userReplyId.length > 0) {
						for(var i=0; i<userReplyId.length; i++) {
							questionReplies.replies.push({ "id": userReplyId[i], "text": userReplyText[i] });
						}
					}
					break;
				case 'FREE':
				case 'NUMERIC_VALUE':
				case 'DATE':
				case 'DATETIME':
					if(userReplyText !== undefined && userReplyText != null && userReplyText !== '') {
						questionReplies.replies.push({ "id": userReplyId, "text": userReplyText });
					}
					break;
				}
				if(questionReplies.replies.length > 0) {
					replies.push(questionReplies);
				}
			}
		}
		
		if(permanent) {
			modalInstance = $modal.open({
				backdrop: 'static',
				keyboard: false,
				templateUrl: 'tpl/common/alertModal.html',
				controller: function ($scope, $modalInstance, $sce) {
					$scope.dialogTitle = "Attendere";
					$scope.dialogStyle = "info";
					$scope.dialogText = "<i class=\"fa fa-spinner fa-spin\"></i><span>&nbsp;&nbsp;Sto elaborando il questionario...</span>";
					$scope.dialogText =  $sce.trustAsHtml($scope.dialogText);
				}
			});
		}
		var qq = $scope.detail;
		UserQuestionnaireService.update({ "id": $scope.detail.userQuestionnaire.id, "activityId": $scope.activityId }, { "replies": replies, "permanent": permanent, "submissionDate":  $scope.submissionDate},
			function(result) {
				
				$scope.detail = $scope.completeUserQuestionnaire(result.data);
				var pageNumber = 1;
				$scope.detail.currentPage = pageNumber;
				for(var q=0; q<$scope.detail.userQuestionnaire.questionnaireVersion.questions.length; q++) {
					$scope.detail.userQuestionnaire.questionnaireVersion.questions[q].page = pageNumber;
					if ($scope.detail.userQuestionnaire.questionnaireVersion.questions[q].type == 'PAGE_JUMPS') {
						pageNumber++;
					}
				}
				$scope.detail.lastPage = pageNumber;
				if(modalInstance !== undefined) {
					modalInstance.close();
				}
				if($scope.detail.warnings !== undefined && $scope.detail.warnings.length > 0) {
					var msg = '';
					for(var i=0;i<$scope.detail.warnings.length; i++) {
						msg = msg + $scope.detail.warnings[i].errorMessage + '<br />';
					}
					MessageService.showWarning('Attenzione!', msg, true);
					
				}
				else {
					if(permanent) {
						MessageService.showSuccess('Questionario correttamente confermato ed inviato', '');
					} else {
						MessageService.showSuccess('Questionario salvato correttamente', '');
					}
					//Fa un broadcast di notifica conferma questionario
					if($scope.detail.userQuestionnaire.permanent) {
						console.log('Broadcasting questionnaire confirmation: '+$scope.detail.userQuestionnaire.id);
						$rootScope.$broadcast("userQuestionnairePermanent",{ 'detail': $scope.detail });
					}
					if($scope.popup && $scope.modal!=undefined && permanent) {
						$scope.modal.close($scope.detail);
					}
				}
			},
			function(error) {
				if(modalInstance!=undefined) modalInstance.close();
				
				if(permanent) {
					MessageService.showError('Errore','Problema durante la conferma delle risposte al questionario');
				}
				else {
					MessageService.showSuccess('Errore','Problema durante il salvataggio delle risposte al questionario');
				}
			}
		);
	};
	
	$scope.confirmUserQuestionnaire = function() {
		var templateUrl =  'tpl/common/confirmModal.html';
		
		var fn = $scope.updateUserQuestionnaire;
		var modalInstance = $modal.open({
			keyboard: false,
			templateUrl: templateUrl,
			controller: function ($scope, $modalInstance) {
				$scope.dialogTitle = "Conferma Risposte Questionario";
				$scope.dialogText = "Confermi le risposte al questionario? Se si prosegue le risposte non saranno ulteriormente modificabili.";
				$scope.dialogStyle='btn-info';
				$scope.fn = fn;
				$scope.confirm = function() {
					$scope.fn(true);
					$modalInstance.close($scope.detail);
				};
				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};
			}
		});
		modalInstance.result.then( function(quest) {
		
		});
	};

	//Annulla modifiche tab questionario
	$scope.resetQuestionnaireData = function() {
		$scope.loadUserQuestionnaire($scope.detail.userQuestionnaire.id, $scope.activityId);
	};
	
	$scope.prevQuestionnairePage = function() {
		$scope.detail.currentPage--;
		$timeout(function() {
			$rootScope.$broadcast('rzSliderForceRender'); 
			$rootScope.$broadcast('reCalcViewDimensions'); 
		},100);
	}
	$scope.nextQuestionnairePage = function() {
		$scope.detail.currentPage++;
		$timeout(function() {
			$rootScope.$broadcast('rzSliderForceRender'); 
			$rootScope.$broadcast('reCalcViewDimensions'); 
		},100);
	}
	
	//Annulla modifiche tab questionario
	$scope.deleteQuestionnaire = function() {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					UserQuestionnaireService.delete({ id : id, activityId : $scope.activityId }, {}, function(result) {
						modalInstance.close();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};
	
	
	$scope.showHelp = function() {
		
		if($scope.detail.userQuestionnaire.questionnaireVersion.help!=undefined) {
			MessageService.simpleAlert($scope.detail.questionnaireName,$scope.detail.userQuestionnaire.questionnaireVersion.help,"info",true);
		}
	}
	
	$scope.init = function() {
		
		
		$scope.loadUserQuestionnaire($scope.id, $scope.activityId);
		
	}
	
	$scope.init();
});
