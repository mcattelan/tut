'use strict'

app.controller('DocumentCategoryListCtrl', function(
		$scope,
		$controller,
		DocumentCategoryConfService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DOCUMENT_CATEGORY_CONF', DocumentCategoryConfService);
});

app.controller('DocumentCategoryDetailCtrl', function(
		$rootScope,
		$scope,
		$controller,
		$state,
		$modal,
		DocumentCategoryConfService,
		DocumentCategoryPermissionService,
		PermissionService,
		FormUtilService,
		MessageService) {
	
	$scope.refresh = function() {
		$scope.initDetails();
	};
	
	// Funzione che seleziona/deseleziona i checkbox
	$scope.changeEntityType = function(entityType) {
		var index = $scope.entityTypes.indexOf(entityType);
		if (index > -1) {
			$scope.entityTypes.splice(index, 1);
			$scope.entityTypesEnabled[entityType] = false;
		} else {
			$scope.entityTypes.push(entityType);
			$scope.entityTypesEnabled[entityType] = true;
		}
	}
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.entityType = 'DOCUMENT_CATEGORY_CONF';
	
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.entityTypes = [];

		$scope.errors = {};
		$scope.warnings = {};
		$scope.selects = {};
		
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.detail = undefined;
		$scope.fieldPermissions = {};
		$scope.enabledDocumentTypes = [];
		$scope.enabledRecipientTypes = {};
		for(var i = 0; i < $scope.constants.DocumentCategoryRecipientType.length; i++){
			for(var x = 0; x < $scope.constants['DocumentCategoryRecipientType'][i].properties['enableDocumentType'].length; x++){
				var key = $scope.constants['DocumentCategoryRecipientType'][i].properties['enableDocumentType'][x];
				$scope.enabledDocumentTypes.push(key);
				$scope.enabledRecipientTypes[key] = $scope.enabledRecipientTypes[key] || [];
				$scope.enabledRecipientTypes[key].push($scope.constants['DocumentCategoryRecipientType'][i].id);
			}
		}
		if ($scope.isNew()) {
			PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
				$scope.permissions = result.data;
				if($scope.permissions['INSERT']) {
					PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
						$scope.fieldPermissions = result.data;
						if($scope.newDefaultData) {
							$scope.detail = $scope.newDefaultData;
						}
						else {
							$scope.detail = {};
						}
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});	
				}
				else {
					MessageService.showError('Utente non abilitato', '');
				}
				
			}, function(error) {
				$scope.detail = undefined;
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			DocumentCategoryConfService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.permissions = result.permissions;
				$scope.fieldPermissions = result.fieldPermissions;
				// Precarico la lista completa di permessi a false
				$scope.detailPermission = [];
				for (var i = 0; i < $scope.detail.documentTypes.length; i++) {
					var entityType = $scope.detail.documentTypes[i];
					if ($scope.detailPermission.indexOf(entityType) == -1) {
						$scope.detailPermission[entityType] = [];
					}
					for (var x = 0; x < $scope.constants.UserType.length; x++) {
						var userType = $scope.constants.UserType[x].id;
						if ($scope.detailPermission[entityType].indexOf(userType) == -1) {
							$scope.detailPermission[entityType][userType] = [];
						}
						for (var y = 0; y < $scope.constants['DocumentCategoryPermission'].length; y++) {
							var operation = $scope.constants['DocumentCategoryPermission'][y].id;
							if ($scope.detailPermission[entityType][userType].indexOf(operation) == -1) {
								$scope.detailPermission[entityType][userType][operation] = false;
							}
						}
						if($scope.constants.UserType[x].properties && $scope.constants.UserType[x].properties['roles']) {
							for (var r = 0; r < $scope.constants.UserType[x].properties['roles'].length; r++) {
								var role = $scope.constants.UserType[x].properties['roles'][r];
								if ($scope.detailPermission[entityType][userType].indexOf(role) == -1) {
									$scope.detailPermission[entityType][userType][role] = [];
								}
								
								for (var y = 0; y < $scope.constants['DocumentCategoryPermission'].length; y++) {
									var operation = $scope.constants['DocumentCategoryPermission'][y].id;
									if ($scope.detailPermission[entityType][userType][role].indexOf(operation) == -1) {
										$scope.detailPermission[entityType][userType][role][operation] = false;
									}
								}
							}
						}
					}
				}
				var documentCategoryPermissionFilters = {
						filters : {
							'CATEGORY_ID' : $scope.id
						}
				};
				DocumentCategoryPermissionService.search({}, documentCategoryPermissionFilters, function(result) {
					// Sostituisco i permessi precaricati di default con quelli appena letti e preseleziono i checkbox
					// se presenti valori
					$scope.entityTypesEnabled = [];
					$scope.entityTypes = [];
					if (angular.isDefined(result.data) && result.data) {
						for (var i = 0; i < result.data.length; i++) {
							var dcp = result.data[i];
							if(dcp.role === undefined) {
								$scope.detailPermission[dcp.entityType][dcp.userType][dcp.operation] = dcp.enable;
								var index = $scope.entityTypes.indexOf(dcp.entityType);
								if (index == -1) {
									$scope.entityTypes.push(dcp.entityType);
									$scope.entityTypesEnabled[dcp.entityType] = true;
								}
							}
							else {
								$scope.detailPermission[dcp.entityType][dcp.userType][dcp.role][dcp.operation] = dcp.enable;
								var index = $scope.entityTypes.indexOf(dcp.entityType);
								if (index == -1) {
									$scope.entityTypes.push(dcp.entityType);
									$scope.entityTypesEnabled[dcp.entityType] = true;
								}
							}
						}
					}
				}, function(error) {
					$scope.detail = undefined;
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
			}, function(error) {
				$scope.detail = undefined;
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});	
		}
	};
	
	$scope.save = function() {	
		$scope.errors = $scope.validate !== undefined ? $scope.validate() : {};
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		$scope.warnings = {};//reset 
		var request = $scope.prepareInsertRequest !== undefined ? $scope.prepareInsertRequest() : $scope.prepareStandardRequest();
		DocumentCategoryConfService.insert({}, request, function(result) {
			$scope.initDetails();
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
			if(!$scope.isModal) {
				$state.go($state.current, { id: result.data.id }, true);
			}
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	}
	
	$scope.update = function() {
		$scope.warnings = {};//reset
		var request = $scope.prepareUpdateRequest !== undefined ? $scope.prepareUpdateRequest() : $scope.prepareStandardRequest();
		DocumentCategoryConfService.update({ id : $scope.id }, request, function(result) {
			$scope.initDetails();
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	}
	
	$scope.prepareStandardRequest = function() {
		// Costruisco gli oggetti permessi da passare alla chiamata in base alle entity type selezionate
		$scope.documentCategoryConfPermissions = [];
		for (var i = 0; i < $scope.entityTypes.length; i++) {
			var entityType = $scope.entityTypes[i];
			for (var x = 0; x < $scope.constants.UserType.length; x++) {
				var userType = $scope.constants.UserType[x].id;
				for (var y = 0; y <  $scope.constants['DocumentCategoryPermission'].length; y++) {
					var operation = $scope.constants['DocumentCategoryPermission'][y].id;
					if($scope.detailPermission[entityType][userType][operation]) {
						var documentCategoryPermission = {
								'entityType' : entityType,
								'userType' : userType,
								'operation' : operation,
								'enable' : $scope.detailPermission[entityType][userType][operation]
						}
						$scope.documentCategoryConfPermissions.push(documentCategoryPermission);
					}
				}
				
				if($scope.constants.UserType[x].properties['roles']) {
					for (var r = 0; r < $scope.constants.UserType[x].properties['roles'].length; r++) {
						var role = $scope.constants.UserType[x].properties['roles'][r];
						for (var y = 0; y <  $scope.constants['DocumentCategoryPermission'].length; y++) {
							var operation = $scope.constants['DocumentCategoryPermission'][y].id;
							if($scope.detailPermission[entityType][userType][role][operation]) {
								var documentCategoryPermission = {
										'entityType' : entityType,
										'userType' : userType,
										'role': role,
										'operation' : operation,
										'enable' : $scope.detailPermission[entityType][userType][role][operation]
								}
								$scope.documentCategoryConfPermissions.push(documentCategoryPermission);
							}
						}
					}
				}
			}
		}
		var request = angular.copy($scope.detail);
		request['documentCategoryConfPermissions'] = $scope.documentCategoryConfPermissions;
		return request;
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.isNew = function() {
		return !$scope.id;
	}
	
	$scope.deleteNotification = function(index){
		if (index > -1 && $scope.detail.notificationConfs && $scope.detail.notificationConfs.length > index) {
			$scope.detail.notificationConfs.splice(index, 1);
		}
	}
	
	$scope.addNotification = function(){
		$scope.newNotification = $scope.newNotification || {};
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/documentcategory/document-category-notification-conf.html',
			size: 'md',
		});
	}
	
	$scope.confirmNewNotification = function(){
		$scope.errors = $scope.validateNewNotification();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			$scope.detail['notificationConfs'].push($scope.newNotification);
			$scope.newNotification = {};
			$scope.$modalInstance.close();
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Tutti i campi devono essere compilati correttamente');
		}
	}
	
	$scope.undoNewNotification = function(){
		$scope.newNotification = {};
		$scope.$modalInstance.dismiss('cancel');
	}
	
	$scope.validateNewNotification = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'newNotification.entityType', value : $scope.newNotification.entityType, type: 'string', required : true },
					{ id:'newNotification.type', value : $scope.newNotification.type, type: 'string', required : true },
					{ id:'newNotification.time', value : $scope.newNotification.time, type: 'string', required : true },
					{ id:'newNotification.timeUnit', value : $scope.newNotification.timeUnit, type: 'string', required : true },
					{ id:'newNotification.code', value : $scope.newNotification.code, type: 'string', required : true },
					{ id:'newNotification.recipient', value : $scope.newNotification.recipient, type: 'string', required : true },
					]
		};
		errors = FormUtilService.validateForm(form);
		return errors;
	}
	
	$scope.init();
});