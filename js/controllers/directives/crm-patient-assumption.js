'use strict'

app.controller('PatientAssumptionController', function(
		$scope,
		$state,
		PatientAssumptionService,
		PatientTherapyService,
		PatientTherapyDrugService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {
	
	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};
	
		if (angular.isDefined($scope.patientId) && $scope.patientId) {
			if(angular.isDefined($scope.id)) {
				$scope.initDetails();
			} else {
				$scope.initList();
			}
		}
	}
	
	$scope.initList = function() {
		$scope.showList = false;
		$scope.patientTherapyDrugs = [];
		$scope.selects = {
			'Drugs' : []
		};
		
		//recupero la lista di terapie paziente, (sia attive che concluse) per visualizzare il filtro sui patient therapy drug 
		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.patientId }
		}
		PatientTherapyService.search({ }, therapySearchFilters, function(result) {
			$scope.therapies = result.data;
			
			if(angular.isDefined($scope.therapies)) {
				for(var i = 0; i < $scope.therapies.length; i++) {
					if(angular.isDefined($scope.therapies[i].drugs)) {
						for(var j = 0; j < $scope.therapies[i].drugs.length; j++) {
							var patientDrug = $scope.therapies[i].drugs[j];
							if($scope.therapies[i].start !== undefined) {
								$scope.patientTherapyDrugs.push(patientDrug);
								$scope.selects['Drugs'].push(patientDrug);
							}
						}
					}
				}
			}
			
			PermissionService.getAll({ entityType : 'PATIENT_ASSUMPTION' }, function(result) {
				$scope.initPermission(result.data);
				$scope.initPaginationAndFilter();
				$scope.showList = true;
			}, function(error) {
				$scope.list = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		
		$scope.selects = {
			'Drugs' : []
		};
		
		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.patientId },
				sort: ['-o.start']
		}
		PatientTherapyService.search({ }, therapySearchFilters, function(result) {
			$scope.therapies = result.data;
			for(var i = 0; i <$scope.therapies.length; i++) {
				for(var x = 0; x < $scope.therapies[i].drugs.length; x++) {
					var therapyDrug = $scope.therapies[i].drugs[x];
					therapyDrug.therapy = $scope.therapies[i];
					$scope.selects['Drugs'].push(therapyDrug);
				}
			}
			if ($scope.isNew() && $scope.selects['Drugs'].length >= 1) {
				$scope.detail.drug = $scope.selects['Drugs'][0];
				$scope.detail.dosage = $scope.selects['Drugs'][0].expectedDosage;
				$scope.detail.dosageUnit = $scope.selects['Drugs'][0].expectedDosageUnit;
				$scope.detail.dosageMethod = $scope.selects['Drugs'][0].expectedDosageMethod;
			}
			
			if (!$scope.isNew()) {
				PatientAssumptionService.get({ id : $scope.id, patientId : $scope.patientId }, function(result) {
					$scope.detail = result.data;
					$scope.fieldPermissions = result.fieldPermissions;
					$scope.initPermission(result.permissions);
					
				}, function(error) {
					$scope.detail = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			} else {
				$scope.detail.status = 'TAKEN';
				PermissionService.getAll({ entityType : 'PATIENT_ASSUMPTION' }, function(result) {
					$scope.initPermission(result.data);
					
					PermissionService.getAllFields({ entityType : 'PATIENT_ASSUMPTION' }, function(result) {
						$scope.fieldPermissions = result.data;
					}, function(error) {
						$scope.detail = {};
						$scope.fieldPermissions = {};
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
			
		}, function(error) {
			if (error == '404') {
				$state.go('access.404');
			}
		})
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		
		PatientAssumptionService.search({ patientId : $scope.patientId }, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}	
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		if (angular.isUndefined($scope.validate()) || angular.equals($scope.errors, {})) {
			PatientAssumptionService.insert({ patientId : $scope.patientId }, request, function(result) {
				$scope.detail = result.data;
				$scope.initPermission(result.permissions);
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.id = $scope.detail.id;
				MessageService.showSuccess('Inserimento completato con successo');
			}, function(error) {
				$scope.errors = error;
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					var title = 'Errore in fase di inserimento';
					var message = 'Alcuni dati inseriti non sono corretti';
					if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
						message = $scope.errors.data.message;
					}
					MessageService.showError(title, message);
				}
			});
		}
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PatientAssumptionService.update({ id : $scope.id, patientId : $scope.patientId }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				var title = 'Errore in fase di aggiornamento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PatientAssumptionService.delete({ id : id, patientId : $scope.patientId }, {}, function(result) {
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.search();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.status', value : $scope.detail.status, type: 'string', required: true },
					{ id: 'detail.drug' , value : $scope.detail.drug, type: 'string',  required: true},
					{ id: 'detail.date' , value : $scope.detail.date, type: 'number',  required: true},
					{ id: 'detail.hour' , value : $scope.detail.hour, type: 'number',  required: false}
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT && $scope.selects['Drugs'] !== undefined && $scope.selects['Drugs'].length >= 1;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.$watch('detail.drug.id', function(newValue, oldValue) {
		if(newValue && newValue !== oldValue) {
			PatientTherapyDrugService.getInjectionAreaGroup({patientId: $scope.patientId, id: $scope.detail.drug.id, assumptionId: $scope.detail.id}, {} , function(result) {
				$scope.injectionAreaGroup = result.data;
				if($scope.injectionAreaGroup) {
					$scope.injectionAreaGroup.vboxValues = "0 0 " + $scope.injectionAreaGroup.imageWidth + " " + $scope.injectionAreaGroup.imageHeight;
					$scope.getPolygonPoints($scope.injectionAreaGroup.areas);
				}
			}, function(error) {
				$scope.injectionAreaGroup = undefined;
			});
		}
		else {
			if(newValue == undefined) {
				$scope.injectionAreaGroup = undefined;
			}
		}
	}, true);
	
	$scope.getPolygonPoints = function(polygonPoints){
		for(var x = 0; x<polygonPoints.length; x++){
			if(polygonPoints[x].shapeType == 'POLYGON'){
				var polygonPoint = undefined;
				for(var y = 0; y<polygonPoints[x].shapeData.points.length; y++ ){
					if(polygonPoint){
						polygonPoint += polygonPoints[x].shapeData.points[y].x + ',' + polygonPoints[x].shapeData.points[y].y + ' ';
					}else{
						polygonPoint = polygonPoints[x].shapeData.points[y].x + ',' + polygonPoints[x].shapeData.points[y].y + ' ';
					}
				}
				$scope.injectionAreaGroup.areas[x].shapeData.polygonPoints = polygonPoint;
			}
		}
	};
	
	$scope.loadRequest = function() {
		return {
			status: 			$scope.detail.status,
			drugId:				$scope.detail.drug.id,
			date:				$scope.detail.date,
			hour:				$scope.detail.hour,
			dosage:				$scope.detail.dosage,
			dosageUnit: 		$scope.detail.dosageUnit,
			dosageMethod:		$scope.detail.dosageMethod,
			area:				$scope.detail.area,
			notes:				$scope.detail.notes,
			internalNotes:		$scope.detail.internalNotes,
			injectionAreaId:	$scope.detail.injectionArea ? $scope.detail.injectionArea.id : undefined
		}
	}
	
	$scope.init();
});