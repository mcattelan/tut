'use strict'

app.controller('PatientDossierController', function(
		$rootScope,
		$scope,
		$controller,
		$state,
		$modal,
		$http,
		$sce,
		TherapyService,
		PatientDossierService,
		PatientTherapyService,
		MessageService,
		PatientTherapyDrugService,
		PermissionService,
		PatientService,
		RESTURL) {

	$scope.postInit = $scope.postInit || function() {
		$scope.info = {};
		$scope.detail = {};
		var filters = {
			'PATIENT_ID' : $scope.userId
		};
		
		//valorizzo combo terapie
		PatientTherapyService.search({}, {filters}, function(result) {
			$scope.selects['Therapy'] = result.data;
			$scope.therapyCheck();
		});
		
		//verifica utente loggato se medico
		$scope.hcpUser = $scope.loggedUser.type == "HCP" ? true : false;
		
		//dati dossier
		PatientDossierService.search({},{filters}, function(result) {
			if(result.data.length == 1){
				$scope.detail = result.data[0];
				$scope.detail.patientTherapyId = $scope.detail.patientTherapy !== undefined && $scope.detail.patientTherapy.id !== undefined ? $scope.detail.patientTherapy.id : undefined;
				$scope.therapyCheck();
			}
			$scope.setDossierDefaulValues(); //setto valori di default
		}, function(error) {
			
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	//controlla se preselezionare la terapia o in caso trova ,a terapia più recente per i controlli
	$scope.therapyCheck = function(){
		if($scope.selects['Therapy'].length == 1){
			if($scope.detail.id == undefined || $scope.detail.patientTherapyId == undefined){
				//terapia da preselezionare
				$scope.detail.patientTherapyId = $scope.selects['Therapy'][0].id;
			}
		}else{
			//trovo la terapia più recente
			var i;
			var start = 0;
			for(i = 0; i < $scope.selects['Therapy'].length; i++){
				if($scope.selects['Therapy'][i].start > start){
					$scope.info.mostRecentTherapyId = $scope.selects['Therapy'][i].id;
					start = $scope.selects['Therapy'][i].start;
				}
			}
		}
	}
	
	$scope.onTherapyChange = function(therapy) {
		if(therapy!=undefined){
			$scope.detail.patientTherapyId = therapy.id;
		}else{
			$scope.detail.patientTherapyId = undefined;
		}
	}
	
	$scope.$watch('detail.patientTherapyId', function(newValue, oldValue) {
		$scope.loadTherapyOptions(newValue);
	}, true);
	
	$scope.loadTherapyOptions = function(therapyId){
		var selectedTherapy = undefined;
		if(therapyId){
			selectedTherapy = $scope.selects['Therapy'].filter(function(item) {
				return item.id === therapyId;
			})[0];
		}
		
		if($scope.info){
			$scope.injectionAreas = undefined;
			$scope.info.therapy = selectedTherapy;
			$scope.setTherapyRelatedValues(selectedTherapy);
		}
	}
	
	$scope.setTherapyRelatedValues = function(therapy){
		//Metodo di somministrazione
		$scope.info.expectedDosageMethod = therapy != undefined ? therapy.drugs[0].expectedDosageMethod : undefined;
		if($scope.info.expectedDosageMethod != undefined && $scope.info.expectedDosageMethod !== ""){
			var c = $scope.constants['ExpectedDosageMethod'].filter(function(item) {
			    return item.id === $scope.info.expectedDosageMethod;
			})[0];
			$scope.info.expectedDosageMethod = c.label;
		}else{
			$scope.info.expectedDosageMethod = 'Pompa non presente';
		}
		//Messaggio terapia
		if(therapy != undefined && $scope.info.mostRecentTherapyId != undefined && $scope.info.mostRecentTherapyId != therapy.id){
			$scope.info.therapyMessage = "Verificare la terapia selezionata in quanto non risulta corrispondere a quella pi\u00F9 recente";
			$scope.warnings = {};
			$scope.warnings.warnings = {};
			$scope.warnings.warnings['therapy'] = {};
			$scope.warnings.warnings['therapy'].message = $scope.info.therapyMessage;
		}else{
			$scope.warnings = {};
			$scope.warnings.therapyMessage = undefined;
			$scope.warnings.warnings = {};	
			$scope.warnings.warnings['therapy'] = undefined;
		}
		//Aree somministrazione
		if(therapy){
			var filters = {'THERAPY_ID': therapy.id};
			PatientTherapyDrugService.search({ patientId: $scope.userId }, {filters}, function(result) {
				PatientTherapyDrugService.getInjectionAreaGroup({patientId: $scope.userId, id: result.data[0].id}, {} , function(result) {
					$scope.injectionAreas = result.data.areas;
				}, function(error) {
					$scope.injectionAreas = undefined;
					$scope.detail.injectionAreas = undefined;
				});
			}, function(error) {
				$scope.injectionAreas = undefined;
				$scope.detail.injectionAreas = undefined;
			});	
		}else{
			$scope.injectionAreas = undefined;
			$scope.detail.injectionAreas = undefined;
		}
	}
	
	$scope.setDossierDefaulValues = function(){
		var fields = $scope.constants['PatientDossierPrePopolatedFields'];
		if($scope.detail.malaiseStatus == undefined || $scope.detail.malaiseStatus == ""){
			$scope.detail.malaiseStatus = fields[0].properties.field;
		}
		if($scope.detail.fever == undefined || $scope.detail.fever == ""){
			$scope.detail.fever = fields[1].properties.field;
		}
		if($scope.detail.bloodPressure == undefined || $scope.detail.bloodPressure == ""){
			$scope.detail.bloodPressure = fields[2].properties.field;
		}
		if($scope.detail.antibioticPatient == undefined || $scope.detail.antibioticPatient == ""){
			$scope.detail.antibioticPatient = fields[3].properties.field;
		}
		if($scope.detail.scheduledVaccination == undefined || $scope.detail.scheduledVaccination == ""){
			$scope.detail.scheduledVaccination = fields[4].properties.field;
		}
		if($scope.detail.inconsistentDrugDose == undefined || $scope.detail.inconsistentDrugDose == ""){
			$scope.detail.inconsistentDrugDose = fields[5].properties.field;
		}
		if($scope.detail.criticalVenousAccess == undefined || $scope.detail.criticalVenousAccess == ""){
			$scope.detail.criticalVenousAccess = fields[6].properties.field;
		}
		if($scope.detail.criticalInfusion == undefined || $scope.detail.criticalInfusion == ""){
			$scope.detail.criticalInfusion = fields[7].properties.field;
		}
		if($scope.detail.malfuctionPacPicc == undefined || $scope.detail.malfuctionPacPicc == ""){
			$scope.detail.malfuctionPacPicc = fields[8].properties.field;
		}
		if($scope.detail.malfuctionInfusionPump == undefined || $scope.detail.malfuctionInfusionPump == ""){
			$scope.detail.malfuctionInfusionPump = fields[9].properties.field;
		}
	}

	$scope.downloadPdfPatientDossier = function(id){
		if(angular.isDefined(id)) {
			
			var patientFirstDossier = undefined;
			var name = undefined;
			PatientService.get({ id : $scope.userId }, function(result) {
				if(result.data !== undefined){
					patientFirstDossier = result.data.externalCode1;
				}
				if(patientFirstDossier !== undefined){
					name = 'dossier_paziente_' + patientFirstDossier + '.pdf';
				}else{
					name = 'dossier_paziente.pdf';
				}
			}, function(error) {
				patientFirstDossier = undefined;
			});
			
			$http.post(RESTURL.PATIENT_DOSSIER + '/' +  id + '/export', {} ,{responseType:'arraybuffer'})
			.success(function (response, status, xhr) {
				var file = new Blob([response], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
				$scope.pdf = $sce.trustAsResourceUrl(fileURL);
				var link = angular.element('<a/>');
				link.attr({
					href : fileURL,
					target: '_blank',
					download: name
				})[0].click();
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione pdf non riuscita', '');
			});
		} else {
			MessageService.showError('Dossier paziente non esistente', '');
		}
	}
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_DOSSIER', PatientDossierService);
	
	$scope.save = function() {
		$scope.errors = $scope.validate !== undefined ? $scope.validate() : {};
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if($scope.detail.id != undefined){
				$scope.update();
			}else{
				$scope.insert();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		$scope.warnings = {};
		var request = $scope.prepareInsertRequest !== undefined ? $scope.prepareInsertRequest() : $scope.prepareStandardRequest();
		$scope.service.insert({}, request, function(result) {
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
			$scope.detail = result.data;
			$scope.detail.patientTherapyId = $scope.detail.patientTherapy !== undefined && $scope.detail.patientTherapy.id !== undefined ? $scope.detail.patientTherapy.id : undefined;
			$scope.permissions = result.permissions;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.changesInProgress = false;
			$scope.changesInProgressListener = false;
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.update = function() {
		$scope.warnings = {};// reset
		var request = $scope.prepareUpdateRequest !== undefined ? $scope.prepareUpdateRequest() : $scope.prepareStandardRequest();
		$scope.service.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.detail.patientTherapyId = $scope.detail.patientTherapy !== undefined && $scope.detail.patientTherapy.id !== undefined ? $scope.detail.patientTherapy.id : undefined;
			$scope.warnings = result.responseWarnings;
			$scope.permissions = result.permissions;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.changesInProgress = false;
			$scope.changesInProgressListener = false;
			
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				var title = 'Errore in fase di aggiornamento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.prepareInsertRequest = function() {
		return $scope.prepareRequest();
	}
	
	$scope.prepareUpdateRequest = function() {
		return $scope.prepareRequest();
	}
	
	$scope.prepareRequest = function(){
		var req = {
			therapyId : $scope.detail.patientTherapyId,
			patientId : $scope.userId,
			start : $scope.detail.start,
			dilution : $scope.detail.dilution,
			infusionRate : $scope.detail.infusionRate,
			cfr : $scope.detail.cfr,
			washingLine : $scope.detail.washingLine,
			washingLineRate : $scope.detail.washingLineRate,
			needlesType : $scope.detail.needlesType,
			preTreatementPosology : $scope.detail.preTreatementPosology,
			postTreatementPosology : $scope.detail.postTreatementPosology,
			emergencyTherapy : $scope.detail.emergencyTherapy,
			portACath : $scope.detail.portACath,
			portACathNeedlesType : $scope.detail.portACathNeedlesType,
			piccPort : $scope.detail.piccPort,
			physiologicalPockets : $scope.detail.physiologicalPockets,
			pumpModelId : $scope.detail.pumpModel != undefined ? $scope.detail.pumpModel.id : undefined,
			infusionSetModelId : $scope.detail.infusionSetModel != undefined ? $scope.detail.infusionSetModel.id : undefined,
			minPas : $scope.detail.minPas,
			maxPas : $scope.detail.maxPas,
			minPad : $scope.detail.minPad,
			maxPad : $scope.detail.maxPad,
			minFc : $scope.detail.minFc,
			maxFc : $scope.detail.maxFc,
			minTemperature : $scope.detail.minTemperature,
			maxTemperature : $scope.detail.maxTemperature,
			minSaturation : $scope.detail.minSaturation,
			maxSaturation : $scope.detail.maxSaturation,
			injectionAreas : $scope.detail.injectionAreas,
			patientStatus : $scope.detail.patientStatus,
			diseaseProgress : $scope.detail.diseaseProgress,
			patientNotes : $scope.detail.patientNotes,
			infusionReactions : $scope.detail.infusionReactions,
			other : $scope.detail.other,
			environmentalConditions : $scope.detail.environmentalConditions,
			venousHeritage : $scope.detail.venousHeritage,
			malaiseStatus : $scope.detail.malaiseStatus,
			fever : $scope.detail.fever,
			bloodPressure : $scope.detail.bloodPressure,
			antibioticPatient : $scope.detail.antibioticPatient,
			scheduledVaccination : $scope.detail.scheduledVaccination,
			inconsistentDrugDose : $scope.detail.inconsistentDrugDose,
			criticalVenousAccess : $scope.detail.criticalVenousAccess,
			criticalInfusion : $scope.detail.criticalInfusion,
			malfuctionPacPicc : $scope.detail.malfuctionPacPicc,
			malfuctionInfusionPump : $scope.detail.malfuctionInfusionPump
		}
		return req;
	}
	
});