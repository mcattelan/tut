'use strict'

app.controller('PharmacyListCtrl', function(
		$scope,
		$controller,
		PharmacyService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PHARMACY', PharmacyService);
});