'use strict'
app.controller('MaterialDeliveryListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		MaterialDeliveryService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('MATERIAL_DELIVERY', MaterialDeliveryService);
	
});

app.controller('MaterialDeliveryDetailCtrl', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		MaterialDeliveryService,
		FormUtilService,
		PermissionService,
		MessageService,
		$filter) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.errors = {};
		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if (angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};

		if (!$scope.isNew()) {
			MaterialDeliveryService.get({ id : $scope.id }, function(result) {
				$scope.detail = angular.extend($scope.detail,result.data);
				
				// Se chiamato dall'attività eseguo i controlli per impostare i campi di default
				// in caso siano ancora non definiti
				if (angular.isDefined($scope.activitiForm) && $scope.activitiForm) {
					if (angular.isUndefined($scope.detail.address) || angular.isUndefined($scope.detail.address.county)) {
						$scope.detail.address = angular.copy($scope.activitiForm.address);
					}
					if (angular.isUndefined($scope.detail.organizer)) {
						$scope.detail.organizer = $scope.activitiForm.owner;
					}
					if ($scope.activitiForm.date != undefined) {
						if ($scope.detail.status == 'DRAFT') {
							$scope.detail.requestDate = $scope.activitiForm.date;
						}
					}
				}
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				if($scope.detail==undefined) {
					$scope.detail = {};
				}
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			if ($scope.detail==undefined) {
				$scope.detail = {};
			}
			PermissionService.getAll({ entityType : 'MATERIAL_DELIVERY' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'MATERIAL_DELIVERY' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					if($scope.detail==undefined)
						$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				if($scope.detail==undefined)
					$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		MaterialDeliveryService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			$scope.close();
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		MaterialDeliveryService.update({ id : $scope.id },request,function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			$scope.close();
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					MaterialDeliveryService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.close();
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.send = function() {
		var title = 'Conferma Spedizione/Consegna';
		var text = 'Una volta impostata come confermata non sarà possibile modificare i dati della spedizione/consegna, si desidera procedere?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style,'sm');
		modalInstance.result.then(
		function(confirm) {
			$scope.detail.status = 'CONFIRMED';
			$scope.save();
			if (angular.isDefined($scope.errors) && $scope.errors) {
				$scope.detail.status = 'DRAFT';
			}
		});
	}
	
	$scope.complete = function() {
		var title = 'Completa Spedizione/Consegna';
		var text = 'Una volta impostata come completata non sarà possibile modificare i dati della spedizione/consegna, si desidera procedere?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style,'sm');
		modalInstance.result.then(
		function(confirm) {
			$scope.detail.status = 'COMPLETED';
			$scope.save();
			if (angular.isDefined($scope.errors) && $scope.errors) {
				$scope.detail.status = 'CONFIRMED';
			}
		});
	}
	
	$scope.uncomplete = function() {
		var title = 'Non effettuata Spedizione/Consegna';
		var text = 'Una volta impostata come non effettuata non sarà possibile modificare i dati della spedizione/consegna, si desidera procedere?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style,'sm');
		modalInstance.result.then(
		function(confirm) {
			$scope.detail.status = 'NOT_COMPLETED';
			$scope.save();
			if (angular.isDefined($scope.errors) && $scope.errors) {
				$scope.detail.status = 'CONFIRMED';
			}
		});
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.code', value: $scope.detail.code, required: true, type: 'string' },
					{ id:'detail.requestDate', value: $scope.detail.requestDate, required: true, type: 'string' },
					{ id:'detail.status', value: $scope.detail.status, required: true, type: 'string' },
					{ id:'detail.type', value: $scope.detail.type, required: true, type: 'string' },
					{ id:'detail.organizer', value: $scope.detail.organizer, required: true },
					{ id:'detail.recipient', value: $scope.detail.recipient, required: true },
					{ id:'detail.address', value : $scope.detail.address, required: true }
				]					
		};
		
		// Se l'address è valorizzato controllo l'obbligatorietà di tutti i campi necessari per la spedizione
		if (angular.isDefined($scope.detail.address) && $scope.detail.address) {
			form.formProperties.push({ id:'address.country', value : $scope.detail.address.country, required: true });
			form.formProperties.push({ id:'address.county', value : $scope.detail.address.county, required: true });
			form.formProperties.push({ id:'address.province', value : $scope.detail.address.province, required: true });
			form.formProperties.push({ id:'address.city', value : $scope.detail.address.city, required: true });
			form.formProperties.push({ id:'address.address', value : $scope.detail.address.address, required: true });
		}
		
		var hasMaterials = false;
		var quantityNotAlign = false;
		for (var i=0; i<$scope.detail.materials.length; i++) {
			if ($scope.detail.materials[i].requestedQuantity != undefined) {
				// Controllo che ci sia almeno un materiale in spedizione
				if (!hasMaterials && $scope.detail.materials[i].requestedQuantity > 0) {
					hasMaterials = true;
				}
				// Controllo che la quantità richiesta non sia inferiore alla quantità consegnata
				if (!quantityNotAlign && $scope.detail.materials[i].requestedQuantity < $scope.detail.materials[i].deliveredQuantity) {
					quantityNotAlign = true;
				}
				// Esco se verificate entrambe le condizioni 
				if (quantityNotAlign /*&& hasMaterials*/) {
					break;
				}
			}
		}
		
		// Se lo stato è confermato o completato controllo l'obbligatorietà della data spedizione
		if ($scope.detail.status == 'CONFIRMED' || $scope.detail.status == 'COMPLETED') {
			form.formProperties.push({ id:'detail.shippingDate', value: $scope.detail.shippingDate, required: true, type: 'string' });
		}
		
		// Se lo stato è completato controllo  l'obbligatorietà della data di ricezione e che tutte le quantità consegnate siano
		// un valore numerico >= 0
		var hasMaterialsDelivered = true;
		if ($scope.detail.status == 'COMPLETED') {
			form.formProperties.push({ id:'detail.deliveryDate', value: $scope.detail.deliveryDate, required: true, type: 'string' });
			for (var i=0; i<$scope.detail.materials.length; i++) {
				if (!isNaN(parseInt($scope.detail.materials[i].requestedQuantity)) && $scope.detail.materials[i].requestedQuantity > 0 && 
						(isNaN(parseInt($scope.detail.materials[i].deliveredQuantity)) || $scope.detail.materials[i].deliveredQuantity < 0)) {
					hasMaterialsDelivered = false;
					break;
				}
			}
		}
		errors = FormUtilService.validateForm(form);

		// In caso non sia stato impostato neanche un materiale in spedizione emetto il messaggio di errore
		if (!hasMaterials) {
			errors['detail.materials'] = "E' necessario impostare una quantità maggiore di 0 per almeno uno dei materiali nella lista";
		}
		
		// In caso di materiale consegnato maggiore del materiale richiesto emetto il messaggio di errore
		if (quantityNotAlign && angular.isUndefined(errors['detail.materials'])) {
			errors['detail.materials'] = "La quantità consegnata non può essere superiore alla quantità richiesta";
		}
		
		// In caso di materiali consegnati non impostati emetto il messaggio di errore
		if (!hasMaterialsDelivered) {
			errors['detail.materials'] = "E' necessario impostare le quantità consegnate";
		}
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
		$scope.permissions = permissions;
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.material-deliveries");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.loadRequest = function() {
		var materials = [];
		for (var i=0; i<$scope.detail.materials.length; i++) {
			materials.push({ 
				materialId: $scope.detail.materials[i].material.id,
				id: $scope.detail.materials[i].id,
				requestedQuantity: $scope.detail.materials[i].requestedQuantity,
				deliveredQuantity: $scope.detail.materials[i].deliveredQuantity,
				deliveredSerialNumbers: $scope.detail.materials[i].deliveredSerialNumbers
			})
		}
		return {
			requestDate:		$scope.detail.requestDate,
			shippingDate: 		$scope.detail.shippingDate,
			requestDate:		$scope.detail.requestDate,
			deliveryDate: 		$scope.detail.deliveryDate,
			type: 				$scope.detail.type,
			organizerId: 		$scope.detail.organizer.id,
			recipientId: 		$scope.detail.recipient.id,
			address: 			$scope.detail.address,
			notes: 				$scope.detail.notes,
			internalNotes: 		$scope.detail.internalNotes,
			status:				$scope.detail.status,
			code:				$scope.detail.code,
			materials:			materials
		}
				
	}
	
	$scope.init();
	
});


