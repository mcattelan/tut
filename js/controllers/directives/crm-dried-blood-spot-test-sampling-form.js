'use strict'

app.controller('DriedBloodSpotTestSamplingFormController', function(
		$rootScope,
		$scope,
		$state,
		$modal,
		MessageService,
		DriedBloodSpotTestService) {

	$scope.init = function() {
		$scope.selects = [];
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.detail = $scope.form.entity;
	};
	
	$scope.init();

});