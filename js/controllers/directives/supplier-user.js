'use strict'

app.controller('SupplierUserListCtrl', function(
		$scope,
		$controller,
		SupplierUserService,
		SupplierService) {
	
	$scope.preInit = function() {
		SupplierService.isMedicalServiceSupport(function(result){
			$scope.ismedicalSupportService = result.data != undefined ? result.data : false;
		});
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('SUPPLIER_USER', SupplierUserService);
});