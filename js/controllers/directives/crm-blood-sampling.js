'use strict'
app.controller('BloodSamplingListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		BloodSamplingService) {

	$controller('BaseListCtrl', { $scope: $scope });

	$scope.init('BLOOD_SAMPLING', BloodSamplingService);
});

app.controller('BloodSamplingDetailController', function(
		$scope,
		$state,
		$rootScope,
		$modal,
		BloodSamplingService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.complementaryId)) {
			$scope.initDetails();
		}
	}

	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		//tramite l'id della complementare cerco di capire se l'oggetto blood sampling è stato già salvato oppure no
		BloodSamplingService.getWithDefaults({ complementaryId : $scope.complementaryId }, function(result) {
			$scope.detail = result.data;
			//Sovrascrivo con dati della form che passo alla direttiva
			if(angular.isDefined($scope.activitiForm)) {
				//il patient non lo sovrascrivo perchè non può essere cambiato
				if(angular.isDefined($scope.activitiForm.locationType)) {
					$scope.detail.locationType	= $scope.activitiForm.locationType;
				}
				if(angular.isDefined($scope.activitiForm.address)) {
					$scope.detail.address = $scope.activitiForm.address;
				}
				if(angular.isDefined($scope.activitiForm.date)) {
					$scope.detail.withdrawalDate = $scope.activitiForm.date;
				}
				if(angular.isDefined($scope.activitiForm.owner)) {
					$scope.detail.owner	= $scope.activitiForm.owner;
				}
				if(angular.isDefined($scope.activitiForm.user)) {
					$scope.detail.patient = $scope.activitiForm.user;
				}
				if(angular.isDefined($scope.activitiForm.bloodDrawingCenter)) {
					$scope.detail.bloodDrawingCenter = $scope.activitiForm.bloodDrawingCenter;
				}
			} 

			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		}, function(error) {
			$scope.detail = {};
			$scope.fieldPermissions = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		BloodSamplingService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		BloodSamplingService.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	};

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					BloodSamplingService.delete({ id : id, }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.patient' , value : $scope.detail.patient, type: 'string',  required: true},
					{ id: 'detail.bloodDrawingCenter' , value : $scope.detail.bloodDrawingCenter, type: 'string',  required: true},
					{ id: 'detail.withdrawalDate' , value : $scope.detail.withdrawalDate, type: 'string',  required: true},
					{ id: 'detail.owner' , value : $scope.detail.owner, type: 'string',  required: true},
					{ id: 'detail.locationType' , value : $scope.detail.locationType, type: 'string',  required: true}
					]
		};

		// Se l'address è valorizzato controllo l'obbligatorietà di tutti i campi necessari per la spedizione
		if (angular.isDefined($scope.detail.address) && $scope.detail.address) {
			form.formProperties.push({ id:'address.country', value : $scope.detail.address.country, required: true });
			form.formProperties.push({ id:'address.county', value : $scope.detail.address.county, required: true });
			form.formProperties.push({ id:'address.province', value : $scope.detail.address.province, required: true });
			form.formProperties.push({ id:'address.city', value : $scope.detail.address.city, required: true });
			form.formProperties.push({ id:'address.address', value : $scope.detail.address.address, required: true });
		}
		errors = FormUtilService.validateForm(form);
		if(angular.isDefined($scope.detail.patient) && !angular.isDefined($scope.detail.patient.id)) {
			errors['detail.patient'] = "E' obbligatorio inserire il paziente";
		}
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
		// TODO: da riattivare se, da menù laterale portale NON bisogna modificare  i dati dei prelievi ematici.
//		if($scope.activitiForm == undefined){
//			$scope.canInsert = false;
//			$scope.canDelete = false;
//			$scope.canUpdate = false;
//		}
	}

	$scope.isNew = function() {
		return ($scope.detail.id == undefined || $scope.detail.id == '');
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.loadRequest = function() {
		
		return {
			patientId : 				$scope.detail.patient.id,
			ownerId : 					$scope.detail.owner.id,
			withdrawalDate : 			$scope.detail.withdrawalDate,
			bloodDrawingCenterId : 		$scope.detail.bloodDrawingCenter.id,
			address : 					$scope.detail.address,
			locationType : 				$scope.detail.locationType,
			complementaryActivityId : 	$scope.complementaryId,
		}
	}
	
	$scope.init();

});