'use strict'

app.controller('GatewayHcpListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		$modal,
		MessageService,
		UserService,
		$state,
		GatewayHcpService,
		PortalService,
		TerritoryService,
		GatewayDrugService) {
	
	$scope.ENTITY = 'HCP';
	
	$controller('BaseGatewayListCtrl', { $scope: $scope });

	$scope.initPermission = function(permissions) {	
		$scope.canInsert = false;
		$scope.canDelete = false;
		$scope.canUpdate = false;
		$scope.canExport = false;
		$scope.canInvite = false;	
	}
	
	
	
	$scope.init($scope.ENTITY, GatewayHcpService);
});

'use strict'

app.controller('GatewayInviteHcpController', function(
		$scope,
		$rootScope,
		$controller,
		$modal,
		MessageService,
		UserService,
		$state,
		GatewayHcpService,
		PortalService,
		TerritoryService,
		GatewayDrugService) {
	
	$scope.selectedUsersTemp = [];
	
	$scope.$watch("selectedUsers", function(newValue, oldValue) {
		$rootScope.selectedUsersTemp = newValue;
	}, true);
});