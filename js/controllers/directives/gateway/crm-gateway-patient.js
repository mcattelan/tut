'use strict'

app.controller('GatewayPatientListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		$modal,
		MessageService,
		UserService,
		$state,
		GatewayPatientService,
		PortalService,
		TerritoryService,
		GatewayDrugService) {
	
	$scope.ENTITY = 'PATIENT';
	
	$controller('BaseGatewayListCtrl', { $scope: $scope });

	$scope.initPermission = function(permissions) {	
		$scope.canInsert = false;
		$scope.canDelete = false;
		$scope.canUpdate = false;
		$scope.canExport = false;
		$scope.canInvite = false;	
	}
	
	$scope.populateFilterListsFn = function() {
		if($scope.selects==undefined) 
			$scope.selects = {};
		
		TerritoryService.searchRegions({}, {}, function(result) {
			$scope.selects['Regions'] = result.data;
		}, function(error) {
			$scope.selects['Regions'] = [];
		});
		
		GatewayDrugService.search({},{ filters: { 'PORTAL_ID' : $scope.portalsList }},function(result) {
			$scope.selects['Drugs'] = result.data;

			if(result.failurePortalIds!=undefined && result.failurePortalIds.length>0) {
				var warning = "Non \u00e8 stato possibile recuperare i dati delle terapie/farmaci dal portale "+result.failurePortalIds[0].toUpperCase();
				if(result.failurePortalIds.length>1) {
					warning = "Non \u00e8 stato possibile recuperare i dati delle terapie/farmaci dai portali ";
					for(var i=0; i<result.failurePortalIds.length; i++) {
						warning += " "+result.failurePortalIds[i].toUpperCase();
						if(i<result.failurePortalIds.length-1) {
							warning +=",";
						}
					}
				}
				MessageService.showWarning('Dati non completi',warning);
			}
			
			
		}, function(error) {
			$scope.selects['Drugs'] = [];
		});
	}
	
	$scope.init($scope.ENTITY, GatewayPatientService);
});