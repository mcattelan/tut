'use strict'

app.controller('UserActivityController', function(
		$rootScope,
		$scope,
		$state,
		$modal,
		UserActivityService,
		MessageService,
		FormUtilService,
		PermissionService,
		UserServiceConfService,
		WorkflowService,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isInternalStaff = $rootScope.isInternalStaff();

		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.id)) {
			$scope.initDetail();
		} else {
			$scope.initList();
		}
	}

	$scope.initList = function() {	
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'USER_ACTIVITY' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
			
			$scope.serviceActivityGroups = [];
			if($scope.userId) {
				UserServiceConfService.getServiceActivityGroupsForUser({ 'userId': $scope.userId }, function(result) {
					$scope.serviceActivityGroups = result.data;
				});
			}
			else {
				UserServiceConfService.getServiceActivityGroups({}, function(result) {
					$scope.serviceActivityGroups = result.data;
				});
			}
			
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.$watch('searchFilters.filters.GROUP_CODE', function(v) {
		$scope.activityGroups = [];
		if(v) {
			if($scope.userId) {
				UserActivityService.getActivityGroupsForUser({ 'groupCode': v, 'userId': $scope.userId }, function(result) {
					$scope.activityGroups = result.data;
				});
			}
			else {
				UserActivityService.getActivityGroups({ 'groupCode': v }, function(result) {
					$scope.activityGroups = result.data;
				});
			}
		}
	}, true);

	$scope.initDetail = function(){
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.showOtherData = false;
		
		if(!$scope.isNew()) {
			UserActivityService.get({ id: $scope.id }, function(result) {
				$scope.detail = result.data;
				if(angular.isDefined($scope.detail.message)) { 
					$scope.detail.message = $scope.detail.message.replace(/<\/?.+?>/ig, '');
				}
				$scope.showOtherData = (angular.isDefined($scope.detail.customProperties) && $scope.detail.customProperties.length > 0) ? true : false;
				$scope.showOtherParticipants = (angular.isDefined($scope.detail.otherParticipants) && $scope.detail.otherParticipants.length > 0) ? true : false;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {};

			PermissionService.getAll({ entityType : 'USER_ACTIVITY' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'USER_ACTIVITY' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	}

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		UserActivityService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})){
			if ($scope.isNew()) {
				$scope.update();	
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		UserActivityService.insert({}, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}

	$scope.update = function(){
		var request = $scope.loadRequest();
		UserActivityService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					UserActivityService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.name', value : $scope.detail.name, type: 'string', required : true },				
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		
		if(angular.isDefined($scope.userId)) {
			$scope.searchFilters['filters']['ALL_USER_ACTIVITIES'] = $scope.userId;
		}
		if(angular.isDefined($scope.serviceId)) {
			$scope.searchFilters['filters']['SERVICE_ID'] = $scope.serviceId;
		}
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}
	
	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.diseases");	
		} else {
			$scope.initDetail();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.loadRequest = function() {
		return {
			name: 			$scope.detail.name,
			description: 	$scope.detail.description,
		}
	}
	
	$scope.completeActivity = function(){
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/directive/user-activity/user-activity-no-result-complete.html',
			size: 'md',
		});
	}
	
	$scope.closeConfirmActivity = function(){
		$scope.$modalInstance.close();
	}
	
	$scope.confirmCompleteActivity = function(){
		$scope.errors = $scope.validateActivityNoResult();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})){
			if($scope.detail.assigneeOwnerType == 'LOGGED_USER'){
				if($scope.loggedUser.nickName == undefined){
					MessageService.showError('Utente non registrato al portale. Fase di completamento non possibile');
				}
			}
			var request = $scope.noResultActivityLoadRequest();
			UserActivityService.completeActivityNoResult({ id : $scope.detail.id }, request, function(result){
				if(result.data){
					MessageService.showSuccess('Fase di completamento attivit\u00E0 avviata con successo');
					if($scope.$modalInstance) {
						$scope.$modalInstance.close();
						$scope.$modalInstance.dismiss();
					}
					if($scope.modal) {
						$scope.modal.close();
						$scope.modal.dismiss();
					}
					var businessKey = result.data;
					if(businessKey !== undefined) {
						WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
							function(result) {
								if(result.data !== undefined) {
									$state.go('app.task', { 'id': result.data.id });
								}
								else {
									$state.go('app.my-tasks');	
								}
							},
							function(error) {
								$state.go('app.my-tasks');
							}
						)
					}
					else {
						$state.go('app.my-tasks');
					}
				}else{
					MessageService.showError('Esito attivit\u00E0 non creato');
				}
			});
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.noResultActivityLoadRequest = function() {
		var assigneeOwner = undefined;
		if($scope.detail.assigneeOwnerType == 'OWNER'){
			assigneeOwner = $scope.detail.owner;
		}else{
			assigneeOwner = $scope.loggedUser;
		}
		return {
			assigneeOwnerId:	assigneeOwner.id,
			verifiable: 		$scope.detail.verifiable,
			suggestedDate:		$scope.detail.suggestedDate
		}
	}
	
	$scope.changeActivity = function(){
		var modalInstance = MessageService.simpleConfirm('Modifica attivit\u00E0', 'L\'attivit\u00E0 verr\u00E0 riportata in stato Pianificata e l\'eventuale ordine di servizio, confermato o no, sar\u00E0 annullato.<br />Si desidera procedere?', 'btn-info');
		modalInstance.result.then(
			function(confirm) {
				if(confirm){
					UserActivityService.changeActivityNoResult({ id: $scope.detail.id}, {}, function(result){
						MessageService.showSuccess('Modifica attivit\u00E0 avviata con successo');
						if($scope.modal) {
							$scope.modal.close();
							$scope.modal.dismiss();
						}
						var businessKey = result.data;
						if(businessKey !== undefined) {
							WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
								function(result) {
									if(result.data !== undefined) {
										$state.go('app.task', { 'id': result.data.id });
									}
									else {
										$state.go('app.my-tasks');	
									}
								},
								function(error) {
									$state.go('app.my-tasks');
								}
							)
						}
						else {
							$state.go('app.my-tasks');
						}
					}, function() {
						MessageService.showError("Impossibile procedere con l'operazione");
					});
				}
			}
		)
	}
	
	$scope.validateActivityNoResult = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.assigneeOwnerType', value : $scope.detail.assigneeOwnerType, type: 'string', required : true },				
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.init();
});

app.controller('UserActivityListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		UserActivityService) {

	$scope.preInit = function() {
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		$scope.isLogistic = $rootScope.isLogistic;
	}
	
	$controller('BaseListCtrl', { $scope: $scope });

	$scope.init('USER_ACTIVITY', UserActivityService);
});

app.controller('UserActivityCrossPspController', function(
		$rootScope,
		$scope,
		$state,
		$http,
		$modal,
		$cookies,
		MessageService,
		FormUtilService,
		UserServiceConfService,
		WorkflowService,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isInternalStaff = $rootScope.isInternalStaff();

		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.restUrl)) {
			$scope.initDetail();
		}
	}

	$scope.initDetail = function(){
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.showOtherData = false;
		$http.get($scope.restUrl, { headers: { 'X-AUTH-CLIENTID' : 'CRM-WEB-UI', 'X-AUTH-TOKEN' : $cookies.getObject('italiassistenza_ua')[1]} })
		.success(function (result, status, headers) {
			$scope.detail = result.data;
			if(angular.isDefined($scope.detail.message)) { 
				$scope.detail.message = $scope.detail.message.replace(/<\/?.+?>/ig, '');
			}
			//TODO da rimuovere quando si aggiungeranno i documenti
			$scope.detail.user.type = undefined;
			$scope.showOtherData = (angular.isDefined($scope.detail.customProperties) && $scope.detail.customProperties.length > 0) ? true : false;
			$scope.showOtherParticipants = (angular.isDefined($scope.detail.otherParticipants) && $scope.detail.otherParticipants.length > 0) ? true : false;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		})
		.error(function (result, status, headers) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}
	
	$scope.init();
});