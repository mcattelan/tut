'use strict'

app.controller('SupplierUserListCtrl', function(
		$scope,
		$controller,
		SupplierUserService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('SUPPLIER_USER', SupplierUserService);
});