'use strict'

app.controller('SupplierUserTrainingSessionController', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		QualificationService,
		TerritoryService,
		SupplierUserService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.control = [];
		$scope.errors = {};
		$scope.selects = {};
		
		$scope.selects['Qualification'] = [];
		QualificationService.get(function(result) {
			for(var i=0; i<result.data.length; i++) {
				$scope.selects['Qualification'].push(result.data[i]);
			}
		});
		
		TerritoryService.searchRegions({}, {}, function(result) {
			$scope.selects['Regions'] = result.data;
		}, function(error) {
			$scope.selects['Regions'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if(($scope.supplierUserId === undefined || $scope.supplierUserId == null) && !$scope.popup) {
			$scope.supplierUserId = $stateParams.supplierUserId;
		}
		//non ho il dettaglio di un oggetto do tipo SupplierUserTrainingSession, quindi inizializzo solo la vista in lista
		$scope.initList();
		
	};

	$scope.initList = function() {
		$scope.showList = true;
//		l'oggetto SupplierUserTrainingSession non può avere DataTypeName quindi non devo recuperare i permessi da db
		$scope.initPermission({"UPDATE":false,"EXPORT":false,"VIEW":true,"DELETE":false,"INSERT":false});
		$scope.selection = $scope.selection || false;
		$scope.selected = $scope.selected || [];
		$scope.initPaginationAndFilter();
	};

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;

			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];

		if(!angular.isDefined($scope.searchFilters.filters)){
			$scope.searchFilters['filters'] = {};
		}

		$scope.statsFilters['filters'] = {};
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []

		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		if($scope.objectFilters.trainingSubject && $scope.objectFilters.trainingSubject.id) {
			$scope.searchFilters.filters.TRAINING_SUBJECT_ID = $scope.objectFilters.trainingSubject.id;
			$scope.statsFilters.filters.SUPPLIER_USER_TRAINING_SUBJECT_ID = $scope.objectFilters.trainingSubject.id;
		} else {
			$scope.searchFilters.filters.TRAINING_SUBJECT_ID = undefined;
			$scope.statsFilters.filters.SUPPLIER_USER_TRAINING_SUBJECT_ID = undefined;
		}
		
		if($scope.objectFilters.department) {
			if($scope.objectFilters.department.medicalCenter && $scope.objectFilters.department.medicalCenter.id) {
				$scope.searchFilters.filters.MEDICAL_CENTER_ID = $scope.objectFilters.department.medicalCenter.id;
				$scope.statsFilters.filters.SUPPLIER_USER_MEDICAL_CENTER_ID = $scope.objectFilters.department.medicalCenter.id;
			} else {
				$scope.searchFilters.filters.MEDICAL_CENTER_ID = undefined;
				$scope.statsFilters.filters.SUPPLIER_USER_MEDICAL_CENTER_ID = undefined;
			}
		} else {
			$scope.searchFilters.filters.MEDICAL_CENTER_ID = undefined;
			$scope.statsFilters.filters.SUPPLIER_USER_MEDICAL_CENTER_ID = undefined;
		}
		
		//mapping di tutti i search filters all'interno degli statsFilters
		if(angular.isDefined($scope.searchFilters) && angular.isDefined($scope.searchFilters.filters)) {
			if(angular.isDefined($scope.searchFilters.filters.QUALIFICATION_ID)) {
				$scope.statsFilters.filters.SUPPLIER_USER_QUALIFICATION_ID = $scope.searchFilters.filters.QUALIFICATION_ID; 
			}
			if(angular.isDefined($scope.searchFilters.filters.CODE)) {
				$scope.statsFilters.filters.SUPPLIER_USER_CODE = $scope.searchFilters.filters.CODE
			}
			if(angular.isDefined($scope.searchFilters.filters.MEDICAL_CENTER_ADDRESS_COUNTY)) {
				$scope.statsFilters.filters.SUPPLIER_USER_MEDICAL_CENTER_ADDRESS_COUNTY = $scope.searchFilters.filters.MEDICAL_CENTER_ADDRESS_COUNTY;
			}
			if(angular.isDefined($scope.searchFilters.filters.TRAINING_DATE_FROM)) {
				$scope.statsFilters.filters.SUPPLIER_USER_TRAINING_DATE_FROM = $scope.searchFilters.filters.TRAINING_DATE_FROM;
			}
			if(angular.isDefined($scope.searchFilters.filters.TRAINING_DATE_TO)) {
				$scope.statsFilters.filters.SUPPLIER_USER_TRAINING_DATE_TO = $scope.searchFilters.filters.TRAINING_DATE_TO;
			}
		}

		SupplierUserService.searchUserTrainingSessions({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		for(var key in $scope.control) {
			$scope.control[key](angular.copy($scope.statsFilters));
		}
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.objectFilters = $scope.objectFilters || {};
		$scope.statsFilters = $scope.statsFilters || {};
		if(!angular.isDefined($scope.searchFilters.filters)){
			$scope.searchFilters['filters'] = {};
		}
		
		if(!angular.isDefined($scope.statsFilters.filters)){
			$scope.statsFilters['filters'] = {};
		}
		//$scope.searchFilters.filters.HAS_COMPLETED_ACTIVITIES = true; // TODO: commentato per la DEMO del 23/11/2018
		//$scope.statsFilters.filters.HAS_COMPLETED_ACTIVITIES = true; // TODO: commentato per la DEMO del 23/11/2018
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		$scope.initialStatsFilters = angular.copy($scope.statsFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;

			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.isNew = function(){
		return $scope.id == '';
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
		$scope.statsFilters = {};
	}

	$scope.init();
});

