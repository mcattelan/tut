'use strict'

app.controller('UserMessageListCtrl', function(
		$scope,
		$controller,
		UserMessageService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('USER_MESSAGE', UserMessageService);
});