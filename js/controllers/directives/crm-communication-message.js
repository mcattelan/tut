'use strict'
app.controller('CommunicationMessageListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		CommunicationMessageService) {
	
	$scope.preInit = function() {
	}

	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('COMMUNICATION_MESSAGE', CommunicationMessageService);
	
});