'use strict'
app.controller('PatientTemporaryHomeAddressListController', function(
		$scope,
		$rootScope,
		$controller,
		$http,
		$state,
		RESTURL,
		REGEXP,
		MessageService,
		PatientTemporaryHomeAddressService) {

	$controller('BaseListCtrl', { $scope: $scope });

	$scope.init('PATIENT_TEMPORARY_HOME_ADDRESS', PatientTemporaryHomeAddressService);
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;

			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}

		$scope.list = $scope.list || [];

		if(!angular.isDefined($scope.searchFilters.filters)){
			$scope.searchFilters['filters'] = {};
		}

		if(angular.isDefined($scope.patientFilter)){
			$scope.searchFilters.filters.PATIENT_ID = $scope.patientId;
		}
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []

		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		PatientTemporaryHomeAddressService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
});

app.controller('PatientTemporaryHomeAddressDetailController', function(
		$scope,
		$state,
		$rootScope,
		$modal,
		PatientTemporaryHomeAddressService,
		PatientService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
	}


	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		
		PatientService.get({ id : $scope.patientId}, function(result) {
			$scope.detail.patient = result.data;
			
		}, function(error) {
			$scope.detail = {};
			$scope.fieldPermissions = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		if(!$scope.isNew()){
			PatientTemporaryHomeAddressService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {

			PermissionService.getAll({ entityType : 'PATIENT_TEMPORARY_HOME_ADDRESS' }, function(result) {
				$scope.initPermission(result.data);

				PermissionService.getAllFields({ entityType : 'PATIENT_TEMPORARY_HOME_ADDRESS' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
   
	$scope.confirm = function() {
        var title = 'Conferma indirizzo di domicilio temporaneo';
        var text = 'Conferma indirizzo di domicilio temporaneo, si desidera procedere?';
        var style = 'bg-secondary';
        var modalInstance = MessageService.simpleConfirm(title,text,style);
        modalInstance.result.then(function(confirm) {
        	var request = $scope.loadRequest();
        	PatientTemporaryHomeAddressService.confirm({ id: $scope.id }, request, function(result) {
        		$scope.detail = result.data;
        		$scope.initPermission(result.permissions);
    			$scope.fieldPermissions = result.fieldPermissions;
    			MessageService.showSuccess('Aggiornamento completato con successo');
        	}, function(error) {
        		if(angular.isDefined(error.data.message)){
        			MessageService.showError('Problemi nella conferma dell\'indirizzo di domicilio temporaneo', error.data.message);
        		} else{
        			MessageService.showError('Problemi nella conferma dell\'indirizzo di domicilio temporaneo');
        		}
        	});
        });
    }
    
	$scope.insert = function() {
		var request = $scope.loadRequest();
		PatientTemporaryHomeAddressService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		PatientTemporaryHomeAddressService.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	};

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PatientTemporaryHomeAddressService.delete({ id : id, }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};

	$scope.removeItem = function(index) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.detail.items.splice(index, 1);
				}
		);
	};

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.patient' , value : $scope.detail.patient, type: 'string',  required: true},
					{ id: 'detail.requestedDate' , value : $scope.detail.requestedDate, type: 'string',  required: true},
					{ id: 'detail.startDate' , value : $scope.detail.startDate, type: 'string',  required: true},
					{ id: 'detail.endDate' , value : $scope.detail.endDate, type: 'string',  required: true},
					{ id: 'detail.phoneNumber' , value : $scope.detail.phoneNumber, type: 'string',  required: true}
					]
		};
				
		if(angular.isDefined($scope.detail.endDate) && angular.isDefined($scope.detail.startDate)){
			if($scope.detail.startDate >= $scope.detail.endDate){
				errors['detail.startDate'] = "La data di inizio deve essere inferiore alla data di fine";
			}
			if($scope.detail.endDate <= $scope.detail.startDate){
				errors['detail.endDate'] = "La data di fine deve essere maggiore della data di inizio";
			}
		} 

		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	};

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.loadRequest = function() {
		return {
			patientId : $scope.detail.patient.id,
			pharmacyId :  $scope.detail.pharmacy != undefined ? $scope.detail.pharmacy.id : undefined,
			requestedDate : $scope.detail.requestedDate,
			startDate : $scope.detail.startDate,
			endDate : $scope.detail.endDate,
			address : $scope.detail.address,
			phoneNumber : $scope.detail.phoneNumber,
			internalNotes : $scope.detail.internalNotes
		}
	}

	$scope.init();

});
