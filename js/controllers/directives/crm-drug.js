'use strict'

app.controller('DrugListCtrl', function(
		$scope,
		$controller,
		DrugService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DRUG', DrugService);
});

app.controller('DrugDetailCtrl', function(
		$scope,
		$controller,
		DrugService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('DRUG', DrugService);
});