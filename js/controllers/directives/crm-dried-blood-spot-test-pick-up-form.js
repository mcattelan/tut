'use strict'

app.controller('DriedBloodSpotTestPickUpFormController', function(
		$rootScope,
		$scope,
		$state,
		$modal,
		MessageService,
		DriedBloodSpotTestService,
		PatientTherapyDrugService,
		DocumentService) {

	$scope.init = function() {
		$scope.selects = [];
		
		DriedBloodSpotTestService.getDBSTestToAssignForPickUp({ patientId: $scope.formActivity.user.id }, {}, function(result) {
			for(var i=0; i<result.data.length; i++) {
				if(angular.isUndefined($scope.selects['DriedBloodSpotTest'])) {
					$scope.selects['DriedBloodSpotTest'] = [];
				}
				$scope.selects['DriedBloodSpotTest'].push(result.data[i]);
			}
		}, function(error) {
			MessageService.showError('Errore in reperimento test BDS');
			if(error.status == 404) {
				$state.go('access.not-found');
			} 
		});
		
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.detail = $scope.form.entity;
		$scope.driedBloodSpotTestSelected = undefined;
		$scope.fieldPermissions = {};
		
		if(angular.isDefined($scope.form.entity) && Object.keys($scope.form.entity).length > 0) {
			$scope.driedBloodSpotTestSelected = angular.copy($scope.form.entity);
		}
	};
	
	
	$scope.changeFormEntity = function(selectedDBSTest) {
		if(angular.isDefined(selectedDBSTest)) {
			$scope.detail.id = selectedDBSTest.id;
			$scope.detail.patient = selectedDBSTest.patient;
			$scope.detail.pickUpOwner = selectedDBSTest.pickUpOwner;
			$scope.detail.pickUpDate = selectedDBSTest.pickUpDate;
			$scope.detail.paperCardSerialNumber = selectedDBSTest.paperCardSerialNumber;
			$scope.detail.bloodSamplingDate = selectedDBSTest.bloodSamplingDate;
			$scope.detail.bloodSamplingOwner = selectedDBSTest.bloodSamplingOwner;
			$scope.detail.bloodSamplingLocation = selectedDBSTest.bloodSamplingLocation;
			$scope.detail.address = selectedDBSTest.address;
		}
	}
	
	$scope.init();

});