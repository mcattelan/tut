'use strict'

app.controller('HcpCustomPatientTherapiesController', function(
		$scope,
		$controller,
		$timeout,
		$filter,
		PatientTherapyService,
		PermissionService) {
	
	$scope.init = function() {
		
		/*DeviceTypeService.getAll({},function(result) {
			$scope.deviceTypes = result.data;
		}, function(error) {
			
		});*/
		
		PermissionService.getAllFields({ entityType : 'PATIENT_THERAPY' }, function(result) {
			$scope.therapyFieldPermissions = result.data;
		}, function(error) {
			$scope.therapyFieldPermissions = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		PermissionService.getAllFields({ entityType : 'PATIENT_THERAPY_DRUG' }, function(result) {
			$scope.drugFieldPermissions = result.data;
		}, function(error) {
			$scope.drugFieldPermissions = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		$scope.patientTherapies = [];//terapie del paziente
		$scope.current = {};//terapia corrente
		$scope.index = 0;//indice terapa corrente
		
		var therapySearchFilters = {
			filters: { 'PATIENT_ID' : $scope.patientId },
			sort: ['start']
		}
		PatientTherapyService.search({}, therapySearchFilters, function(result) {
			for(var i = 0; i < result.data.length; i++){
				$scope.patientTherapies[i] = {
						"id":result.data[i].id,
						"patient":$scope.patientId,
						"therapy":result.data[i],
						"title" : $scope.getTherapyTitle(result.data[i])
				};
				$scope.patientTherapies[i].therapy.initialStart = angular.copy($scope.patientTherapies[i].therapy.start);
				
				var timeUnit = $scope.constants['AssumptionFrequencyUnit'].find(obj => {
				  return obj.id === $scope.patientTherapies[i].therapy.drugs[0].assumptionFrequencyUnit
				});
				
				$scope.patientTherapies[i].therapy.drugs[0].assumptionFrequencyLabel = $scope.patientTherapies[i].therapy.drugs[0].assumptionFrequency + ' / ' + timeUnit.label; 
			}			
			if(result.data.length > 0){
				$scope.setCurrentTherapy();
			}
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
		
	$scope.onTherapyChange = function(n) {
		$scope.index += n;
		$scope.current = $scope.patientTherapies[$scope.index];
	}
	
	$scope.existNext = function(){
		return $scope.patientTherapies[$scope.index + 1] != undefined;
	}
	
	$scope.existPrevious = function(){
		return $scope.patientTherapies[$scope.index - 1] != undefined;
	}
	
	$scope.getTherapyTitle = function(t){
		var ret = t.name;
		if(t.start != undefined){
			ret += ' Da ' + $filter('formatDate')(t.start);
			if(t.end != undefined){
				ret += ' A ' + $filter('formatDate')(t.end);
			}
		}
		return ret;
	}
	
	$scope.setCurrentTherapy = function(){
		for (var i = 0; i < $scope.patientTherapies.length; i++) {
			var t = $scope.patientTherapies[i];
			var today = new Date();
			if(t.therapy.start <= today){
				if(t.therapy.end == undefined){
					$scope.index = i;
					$scope.current = t;
				}else if(t.therapy.end >= today){
					$scope.index = i;
					$scope.current = t;
				}
			}
		}
		if($scope.current == {}){
			$scope.current = $scope.patientTherapies[0];
		}
	}
	
	$scope.allowNavigation = function(){
		return $scope.patientTherapies.length > 1;
	}
	
	$scope.init();
});