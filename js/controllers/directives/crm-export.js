'use strict'
app.controller('ExportCtrl', function(
		$scope,
		$rootScope,
		$controller,
		$http,
		$state,
		MessageService,
		DepartmentService,
		QualificationService,
		RESTURL,
		StoredExportService) {
	 
	$scope.init = function(){
		$scope.initPaginationAndFilter();
	}
	
	
	$scope.generateExport = function(xlsExportType) {
		var allRequest = {
				filters : $scope.searchFilters.filters,
				exportTypes : [xlsExportType]
		};
		if(StoredExportService) {
			StoredExportService.generateCheck({}, allRequest, function(result) {
				$http.post(RESTURL.STORED_EXPORT+'/' + 'generate', allRequest, {responseType:'arraybuffer'}).success(function (response) {
					MessageService.showSuccess('L\'Export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile', '', 0);
				}).error(function(data, status, headers, config) {
					MessageService.showError('Generazione xls non riuscita', '');
				});
				$scope.close();
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
				else {
					var title = 'Avvio export non riuscito';
					var message = '';
					if(error.data && error.data.message) {
						message = error.data.message;
					}
					MessageService.showError(title, message);
				}
			});
		}
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.init();
});

app.controller('ExportListCtrl', function(
		$scope,
		$controller,
		StoredExportService) {
	
	$scope.preInit = function() {
		$scope.selects = { 
				'XlsExportType' : $scope.constants['XlsExportType']
		}
		
		if($scope.constants['CustomXlsExportType'] != undefined){
			for(var i=0; i<$scope.constants['CustomXlsExportType'].length; i++) {
				if($scope.selects['XlsExportType'].indexOf($scope.constants['CustomXlsExportType'][i]) < 0) {
					$scope.selects['XlsExportType'].push($scope.constants['CustomXlsExportType'][i]);
				}
			}
		}
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('STORED_EXPORT', StoredExportService);
});