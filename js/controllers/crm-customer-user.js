'use strict'

app.controller('CustomerUserController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		CustomerUserService,
		PermissionService,
		MessageService,
		SMART_TABLE_CONFIG) {


	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};

	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'CUSTOMER_USER' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		
		$scope.contactMethodsType = {};
		for(var i= 0; i < $rootScope.constants['PreferredContactType'].length; i++){
			$scope.contactMethodsType[$rootScope.constants['PreferredContactType'][i].id] = null;
		}
		
		if (!$scope.isNew()) {
			CustomerUserService.get({ id : $scope.id }, $scope.searchFilters, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				
				for(var i = 0; i < $scope.detail.preferredContacts.length; i++) {
					$scope.contactMethodsType[$scope.detail.preferredContacts[i].type] = $scope.detail.preferredContacts[i].preference;
				}
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			PermissionService.getAll({ entityType : 'CUSTOMER_USER' }, function(result) {
				$scope.initPermission(result.data);
				PermissionService.getAllFields({ entityType : 'CUSTOMER_USER' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
			
		}
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		CustomerUserService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.impersonateUser(nickName);
				}
		);
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	};
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.init();
});