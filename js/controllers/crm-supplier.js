'use strict'

app.controller('SupplierController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		SupplierService,
		FormUtilService,
		PermissionService,
		MessageService,
		REGEXP,
		RESTURL,
		$sce,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if($scope.id === undefined || $scope.id == null) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id) && $scope.id !== '') {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'SUPPLIER' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.showListUser = false;
		if (!$scope.isNew()) {
			SupplierService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				$scope.initPaginationAndFilterSupplierUsers();
				$scope.showListUser = true;
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				$scope.initPaginationAndFilterSupplierUsers();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					type: 'COMPANY',
					representative: {}
			};
			
			PermissionService.getAll({ entityType : 'SUPPLIER' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'SUPPLIER' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.initPaginationAndFilterSupplierUsers = function() {
		$scope.paginationSupplierUsers = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFiltersSupplierUsers = $scope.searchFiltersSupplierUsers || {};
		$scope.initialSearchFiltersSupplierUsers = angular.copy($scope.searchFiltersSupplierUsers);
	}
	
	
	$scope.searchUser = function(tableStateSupplierUsers, smCtrlSupplierUsers) {
		$scope.smCtrlSupplierUsers = $scope.smCtrlSupplierUsers || smCtrlSupplierUsers;
		if (tableStateSupplierUsers === undefined) {
			if ($scope.smCtrlSupplierUsers !== undefined) {
				$scope.smCtrlSupplierUsers.tableState().pagination.start = 0;
				$scope.smCtrlSupplierUsers.pipe();
			}
			return;
		}
		if(tableStateSupplierUsers !== undefined) {
			tableStateSupplierUsers.pagination.number = $scope.paginationSupplierUsers.size;
			
			$scope.paginationSupplierUsers.start = tableStateSupplierUsers.pagination.start;
			if(tableStateSupplierUsers.sort != null && tableStateSupplierUsers.sort.predicate !== undefined) {
				$scope.paginationSupplierUsers.sort = tableStateSupplierUsers.sort.reverse ? [ "-" + tableStateSupplierUsers.sort.predicate ] : [ tableStateSupplierUsers.sort.predicate ];
			}
			if(tableStateSupplierUsers.pagination.number !== undefined) {
				$scope.paginationSupplierUsers.size = tableStateSupplierUsers.pagination.number;
			}
		}
		$scope.listUser = $scope.listUser || [];
		
		angular.extend($scope.searchFiltersSupplierUsers, $scope.paginationSupplierUsers);
		delete $scope.searchFiltersSupplierUsers.total;

		$scope.listUser = [];
		PermissionService.getAll({ entityType : 'SUPPLIER_USER' }, function(result) {
			$scope.canInsertUser = result.data.INSERT;
			$scope.canExportUser = result.data.EXPORT;
			
			SupplierService.searchAllUser( {id: $scope.id},$scope.searchFiltersSupplierUsers, function(result) {
				$scope.listUser = result.data;
				$scope.permissionUser = result.permissions;
				$scope.paginationSupplierUsers.start = result.start;
				$scope.paginationSupplierUsers.size = result.size;
				$scope.paginationSupplierUsers.sort = result.sort;
				$scope.paginationSupplierUsers.total = result.total;
				
				if(tableStateSupplierUsers !== undefined) {
					tableStateSupplierUsers.pagination.numberOfPages = Math.ceil($scope.paginationSupplierUsers.total / ($scope.paginationSupplierUsers.size !== undefined && $scope.paginationSupplierUsers.size != null ? $scope.paginationSupplierUsers.size : 1));
				}
				
			}, function(error) {
				$scope.listUser = [];
				$scope.initPaginationAndFilterSupplierUsers();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.listUser = [];
			$scope.initPaginationAndFilterSupplierUsers();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	/*$scope.initListUser = function() {
		$scope.listUser = [];
		PermissionService.getAll({ entityType : 'SUPPLIER_USER' }, function(result) {
			$scope.canInsertUser = result.data.INSERT;
			
			SupplierService.getAllUser( {id: $scope.id}, function(result) {
				$scope.listUser = result.data;
				$scope.permissionUser = result.permissions;
			}, function(error) {
				$scope.listUser = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.listUser = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};*/
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		SupplierService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
			} else {
				$scope.update();
			}
		}	
	};
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		SupplierService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.invite = function(id) {
		var modalInstance = MessageService.activationConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.inviteUser(id);
				}
		);
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.impersonateUser(nickName);
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.name', value : $scope.detail.name, type: 'string', required: true },
					{ id: 'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.vatNumber', value : $scope.detail.vatNumber, type: 'string',  dependRequired: 'detail.fiscalCode', pattern: REGEXP.VAT_NUMBER,  customMessage:'Il campo deve rispettare il formato della partita iva' },
					{ id: 'detail.type' , value : $scope.detail.type, type: 'string',  required: true},
					{ id: 'detail.representativePhoneNumber', value : $scope.detail.representativePhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeCellPhoneNumber', value : $scope.detail.representativeCellPhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeFaxNumber', value : $scope.detail.representativeFaxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeEmail', value : $scope.detail.representativeEmail, type: 'string', pattern: REGEXP.EMAIL,  customMessage: "L'indirizzo mail deve essere valido" }
					]
		};
		if($scope.detail.type === 'PERSON') {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale' });
		}
		else {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.VAT_NUMBER_OR_FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale o della partita iva' });
		}
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.suppliers");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.supplier", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		
		$scope.detail['preferredContacts'] = [];
		for(var key in $scope.contactMethodsType) {
			$scope.detail['preferredContacts'].push({type: key, preference: $scope.contactMethodsType[key]});
		}
		
		return {
			type : 							$scope.detail.type,
			name: 							$scope.detail.name,
			description: 					$scope.detail.description,
			fiscalCode: 					$scope.detail.fiscalCode,
			vatNumber: 						$scope.detail.vatNumber,
			phoneNumber: 					$scope.detail.phoneNumber,
			faxNumber: 						$scope.detail.faxNumber,
			address: 						$scope.detail.address,
			internalNotes: 					$scope.detail.internalNotes,
			representativeFirstName: 		$scope.detail.representativeFirstName, 
			representativeLastName: 		$scope.detail.representativeLastName,
			representativePhoneNumber: 		$scope.detail.representativePhoneNumber,
			representativeFaxNumber: 		$scope.detail.representativeFaxNumber,
			representativeCellPhoneNumber: 	$scope.detail.representativeCellPhoneNumber,
			representativeEmail: 			$scope.detail.representativeEmail,
			preferredContacts:				$scope.detail.preferredContacts
		}
	}

	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.SUPPLIER + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}

	$scope.init();
});