'use strict'

app.controller('CompletedUserActivityController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		UserActivityService,
		UserServiceConfService, 
		NationalityService,
		FormUtilService,
		PermissionService,
		MessageService,
		REGEXP,
		WorkflowService,
		RESTURL,
		SMART_TABLE_CONFIG,
		$sce) {


//Inizializza sia la lista che la scheda
$scope.init = function() {
	$scope.loggedUser = $rootScope.loggedUser;
	

	$scope.errors = {};
	$scope.selects = {
			'Nationalities' : []
	};
	
	NationalityService.getAll({ }, function(result) {
		$scope.selects['Nationalities'] = result.data;
	}, function(error) {
		$scope.selects['Nationalities'] = [];
		if(error.status == 404) {
			$state.go('access.not-found');
		}
	})

	if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
		$scope.id = $stateParams.id;
	}

	if(angular.isDefined($scope.id)) {
		$scope.initDetails();
	}
	else {
		$scope.initList();
	}
};

$scope.initList = function() {
	$scope.showList = false;
	PermissionService.getAll({ entityType : 'USER_ACTIVITY' }, function(result) {
		$scope.initPermission(result.data);
		$scope.selection = $scope.selection || false;
		$scope.selected = $scope.selected || [];
		$scope.initPaginationAndFilter();
		$scope.showList = true;
		
		$scope.serviceActivityGroups = [];
		UserServiceConfService.getServiceActivityGroups({}, function(result) {
			$scope.serviceActivityGroups = result.data;
		});
		
	}, function(error) {
		$scope.list = [];
		if(error.status == 404) {
			$state.go('access.not-found');
		}
	});
};

$scope.$watch('searchFilters.filters.GROUP_CODE', function(v) {
	$scope.activityGroups = [];
	if(v) {
		UserActivityService.getActivityGroups({ 'groupCode': v }, function(result) {
			$scope.activityGroups = result.data;
		});
	}
}, true);

$scope.initDetails = function() {
	$scope.selection = false;
	$scope.selectedIds = [];
	$scope.detail = {};
	$scope.fieldPermissions = {};
	
	$scope.initActivityPaginationAndFilter();

	if (!$scope.isNew()) {
		UserActivityService.get({ id : $scope.id }, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		$scope.getServicesConfs();
		$scope.activityShowList = true;
	}
	else {
		$scope.detail = {};

		PermissionService.getAll({ entityType : 'USER_ACTIVITY' }, function(result) {
			$scope.initPermission(result.data);
			
			PermissionService.getAllFields({ entityType : 'USER_ACTIVITY' }, function(result) {
				$scope.fieldPermissions = result.data;
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
};

$scope.search = function(tableState, smCtrl) {
	$scope.smCtrl = $scope.smCtrl || smCtrl;
	if (tableState === undefined) {
		if ($scope.smCtrl !== undefined) {
			$scope.smCtrl.tableState().pagination.start = 0;
			$scope.smCtrl.pipe();
		}
		return;
	}
	if(tableState !== undefined) {
		tableState.pagination.number = $scope.pagination.size;
		
		$scope.pagination.start = tableState.pagination.start;
		if(tableState.sort != null && tableState.sort.predicate !== undefined) {
			$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
		}
		if(tableState.pagination.number !== undefined) {
			$scope.pagination.size = tableState.pagination.number;
		}
	}
	$scope.list = $scope.list || [];
	
	/*console.log($scope.loggedUser,$scope.internalStaffFilter )
	debugger;*/
	
	
	
	angular.extend($scope.searchFilters, $scope.pagination);
	$scope.searchFilters.filters.ACTIVITY_PATIENT_ID = $scope.patientFilter ? $scope.patientFilter.id : undefined;
	$scope.searchFilters.filters.ACTIVITY_HCP_ID = $scope.hcpFilter ? $scope.hcpFilter.id : undefined;
	$scope.searchFilters.filters.ACTIVITY_SUPPLIER_ID = $scope.supplierFilter ? $scope.supplierFilter.id : undefined;
	$scope.searchFilters.filters.ACTIVITY_INTERNAL_STAFF_ID = $scope.internalStaffFilter ? $scope.internalStaffFilter.id : undefined;
	
	delete $scope.searchFilters.total;
	$scope.selectedIds = []
	for(var i=0; i<$scope.selected.length; i++) {
		$scope.selectedIds[i] = $scope.selected[i].id;
	}

	UserActivityService.search({}, $scope.searchFilters, function(result) {
		$scope.list = result.data;
		$scope.permissions = result.permissions;
		$scope.pagination.start = result.start;
		$scope.pagination.size = result.size;
		$scope.pagination.sort = result.sort;
		$scope.pagination.total = result.total;
		for(var i=0; i<$scope.list.length; i++) {
			if ($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
				$scope.list.splice(i, 1);
			} else {
				$scope.list[i].selected = false;
			}
		}
		if(tableState !== undefined) {
			tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
		}
	}, function(error) {
		$scope.list = [];
		$scope.initPaginationAndFilter();
		if(error.status == 404) {
			$state.go('access.not-found');
		}
	});
}

$scope.save = function() {
	$scope.errors = $scope.validate();
	if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
		if ($scope.isNew()) {
			$scope.insert();
		} else {
			$scope.update();
		}
	} else {
		MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
	}
};

$scope.insert = function() {
	var request = $scope.loadRequest();
	UserActivityService.insert({}, request, function(result) {
		$scope.detail = result.data;		
		$scope.id = $scope.detail.id;
		$scope.fieldPermissions = result.fieldPermissions;
		$scope.initPermission(result.permissions);
		MessageService.showSuccess('Inserimento completato con successo');
		$state.go("app.hcp", {id: $scope.id});
	}, function(error) {
		MessageService.showError('Inserimento non riuscito');
		if (error.status == 404) {
			$state.go('access.not-found');
		}
	});
};

$scope.update = function() {
	var request = $scope.loadRequest();
	UserActivityService.update({ id : $scope.id },request,function(result) {
		$scope.detail = result.data;
		$scope.fieldPermissions = result.fieldPermissions;
		$scope.initPermission(result.permissions);
		MessageService.showSuccess('Aggiornamento completato con successo');
	}, function(error) {
		MessageService.showError('Aggiornamento non riuscito');
		if (error.status == 404) {
			$state.go('access.not-found');
		}
	});
}

$scope.activate = function() {
	UserActivityService.activate({ id : $scope.id }, undefined, function(result) {
		MessageService.showSuccess('Attivazione completata con successo');
		$state.reload();
		
	}, function(error) {
		if (error.status == 404) {
			$state.go('access.not-found');
		} else {
			MessageService.showError('Errore in fase di attivazione', '');
		}
	});
}

$scope.suspend = function() {
	UserActivityService.suspend({ id : $scope.id }, undefined, function(result) {
		MessageService.showSuccess('Sospensione completata con successo');
		$state.reload();
		
	}, function(error) {
		MessageService.showError('Sospensione non riuscita');
		if(error.status == 404) {
			$state.go('access.not-found');
		} 
	});
}

$scope.delete = function(id) {
	var modalInstance = MessageService.deleteConfirm();
	modalInstance.result.then(
			function(confirm) {
				UserActivityService.delete({ id : id }, {}, function(result) {
					$scope.initList();
					MessageService.showSuccess('Cancellazione completata con successo');
					$state.go("app.hcps");
				}, function(error) {
					MessageService.showError('Cancellazione non riuscita');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
	);
}

$scope.invite = function(id) {
	var modalInstance = MessageService.activationConfirm();
	modalInstance.result.then(
			function(confirm) {				
				$scope.inviteUser(id);
			}
	);
}

$scope.impersonate = function(nickName){
	var modalInstance = MessageService.impersonateConfirm();
	modalInstance.result.then(
			function(confirm) {				
				$scope.impersonateUser(nickName);
			}
	);
}

$scope.validate = function() {
	var errors = {};
	var form = {
			formProperties : [
				/*{ id:'detail.role' , value : $scope.detail.role, type: 'string',  required:true},*/
				{ id:'detail.lastName', value: $scope.detail.lastName, required:true, type:'string' },
				{ id:'detail.sex', value: $scope.detail.sex, required:true, type:'string' },
				{ id:'detail.firstName', value: $scope.detail.firstName, required:true, type:'string' },
				{ id:'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
				{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
				{ id:'detail.cellPhoneNumber', value: $scope.detail.cellPhoneNumber, type:'string' , pattern: REGEXP.PHONE_NUMBER },
				{ id:'detail.fiscalCode', value: $scope.detail.fiscalCode, type:'string',pattern: REGEXP.FISCAL_CODE},
				{ id:'detail.privacyEnrollment', value: $scope.detail.privacyEnrollment, type:'string', required:true},
				{ id:'detail.enrollmentDate',  value: $scope.detail.enrollmentDate, type:'string', required:true},
				{ id:'detail.department',  value: $scope.detail.department, type:'string', required:true}
				]
	};
	errors = FormUtilService.validateForm(form);
	
	if($scope.detail.privacyEnrollment !== undefined && $scope.detail.privacyEnrollment == false) {
		errors['detail.privacyEnrollment'] = 'Campo obbligatorio';
	}
	
	console.log(errors);
	return errors;
}

$scope.initPaginationAndFilter = function() {
	$scope.pagination = {
			start: 0,
			size: SMART_TABLE_CONFIG.TABLE_SIZE,
			sort: []
	};
	
	

	$scope.searchFilters = $scope.searchFilters || {"filters":{"STATUS":['COMPLETED','NOT_OCCURRED','TO_VERIFY','CONFIRMED_NO_RESULT']}};
	$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	
	
	$scope.patientFilter = undefined;
	$scope.hcpFilter = undefined;
	$scope.supplierFilter = undefined;
	
	
	if($rootScope.isInternalStaff()){
		$scope.internalStaffFilter = $scope.loggedUser;
		
	}
	/*if($rootScope.isSupplier()){
		$scope.supplierFilter = $scope.loggedUser;
		
	}*/
	else{
		$scope.internalStaffFilter = undefined;
	}
}

$scope.initPermission = function(permissions) {
	if (permissions != undefined && permissions != null) {
		$scope.canInsert = permissions.INSERT;
		$scope.canDelete = permissions.DELETE;
		$scope.canExport = permissions.EXPORT;
		if ($scope.isNew()) {
			$scope.canUpdate =  $scope.canInsert;
		}
		else {
			$scope.canUpdate = permissions.UPDATE;
		}
	} 
	else {
		$scope.canInsert = false;
		$scope.canDelete = false;
		$scope.canUpdate = false;
	}
	$scope.permissions = permissions;
}

$scope.clearFilters = function(){
	delete $scope.patientFilter;
	delete $scope.hcpFilter;
	delete $scope.supplierFilter;
	delete $scope.internalStaffFilter;
	$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
}

$scope.isNew = function() {
	return $scope.id == '';
}







/*
$scope.initActivityPaginationAndFilter = function() {
	$scope.activityPagination = {
			start: 0,
			size: SMART_TABLE_CONFIG.TABLE_SIZE,
			sort: [ '-date' ]
	};

	$scope.activitySearchFilters = $scope.activitySearchFilters || { filters: { USER_ID: $scope.id } };
	$scope.activityInitialSearchFilters = angular.copy($scope.activitySearchFilters);
}

$scope.searchActivities = function(tableState, smCtrl) {
	$scope.activitySmCtrl = $scope.activitySmCtrl || smCtrl;
	if (tableState === undefined) {
		if ($scope.activitySmCtrl !== undefined) {
			$scope.activitySmCtrl.tableState().pagination.start = 0;
			$scope.activitySmCtrl.pipe();
		}
		return;
	}
	if (tableState !== undefined) {
		tableState.pagination.number = $scope.activityPagination.size;
		
		$scope.activityPagination.start = tableState.pagination.start;
		if (tableState.sort != null && tableState.sort.predicate !== undefined) {
			$scope.activityPagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
		}
		if (tableState.pagination.number !== undefined) {
			$scope.activityPagination.size = tableState.pagination.number;
		}
	}
	$scope.activities = $scope.activities || [];
	
	angular.extend($scope.activitySearchFilters, $scope.activityPagination);
	delete $scope.activitySearchFilters.total;
	
	UserActivityService.search({}, $scope.activitySearchFilters, function(result) {
		$scope.activities = result.data;
		
		$scope.activityPermissions = result.permissions;
		$scope.activityPagination.start = result.start;
		$scope.activityPagination.size = result.size;
		$scope.activityPagination.sort = result.sort;
		$scope.activityPagination.total = result.total;
		for (var i=0; i<$scope.activities.length; i++) {
			$scope.activities[i].selected = $scope.selectedIds.indexOf($scope.activities[i].id) >= 0;
		}
		if (tableState !== undefined) {
			tableState.pagination.numberOfPages = Math.ceil($scope.activityPagination.total / ($scope.activityPagination.size !== undefined && $scope.activityPagination.size != null ? $scope.activityPagination.size : 1));
		}
	}, function(error) {
		$scope.activities = [];
		if(error.status == 404) {
			$state.go('access.not-found');
		};
	});
}*/


$scope.exportXls = function(allData) {
	if(angular.isDefined($scope.list) && $scope.list.length > 0) {
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		//delete $scope.searchFilters.total;
		
		var exportFilter = angular.copy($scope.searchFilters);
		if(allData !== undefined && allData != null && allData == true) {
			exportFilter.start = 0;
			delete exportFilter.size;
		} 
	
		if(exportFilter['filters'] == undefined){
			exportFilter['filters'] = {};
		}
		
		if(exportFilter['total'] != undefined){
			delete exportFilter.total
		}
		
	
		$http.post(RESTURL.USER_ACTIVITY + '/search/export-xls', exportFilter, {responseType:'arraybuffer'})
		.success(function (response, status, xhr) {
			var file = new Blob([response], {type: 'application/vnd.ms-excel'});
			var fileURL = URL.createObjectURL(file);
			$scope.pdf = $sce.trustAsResourceUrl(fileURL);
			var link = angular.element('<a/>');
			link.attr({
				href : fileURL,
				target: '_blank',
				download: 'export.xls'
			})[0].click();
		}).error(function(data, status, headers, config) {
			MessageService.showError('Generazione pdf non riuscita', '');
		});
	} else {
		MessageService.showError('Non sono presenti dati da esportare', '');
	}
}

$scope.init();

});

