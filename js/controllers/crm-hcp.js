'use strict'

app.controller('HcpController', function(
		$rootScope,
		$scope,
		$stateParams,
		$http,
		$state,
		$modal,
		HcpService,
		UserServiceConfService, 
		NationalityService,
		EntityCustomPropertyConfService,
		FormUtilService,
		PermissionService,
		MessageService,
		REGEXP,
		WorkflowService,
		RESTURL,
		SMART_TABLE_CONFIG,
		GatewayHcpService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {
				'Nationalities' : []
		};
		
		NationalityService.getAll({ }, function(result) {
			$scope.selects['Nationalities'] = result.data;
		}, function(error) {
			$scope.selects['Nationalities'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}

		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};

	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'HCP' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.initDetails = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.detail = {};
		$scope.detailActivate = {};
		$scope.fieldPermissions = {};
		
		$scope.contactMethodsType = {};
		for(var i= 0; i < $rootScope.constants['PreferredContactType'].length; i++){
			$scope.contactMethodsType[$rootScope.constants['PreferredContactType'][i].id] = null;
		}
		
		$scope.remindTherapyExpiryTypes = {};
		for(var i= 0; i < $rootScope.constants['RemindTherapyExpiryType'].length; i++){
			$scope.remindTherapyExpiryTypes[$rootScope.constants['RemindTherapyExpiryType'][i].id] = false;
		}

		if (!$scope.isNew()) {
			HcpService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				
				for(var i = 0; i < $scope.detail.preferredContacts.length; i++) {
					$scope.contactMethodsType[$scope.detail.preferredContacts[i].type] = $scope.detail.preferredContacts[i].preference;
				}
				
				if(angular.isDefined($scope.detail.remindTherapyExpiryTypes)) {
					for(var i= 0; i < $scope.detail.remindTherapyExpiryTypes.length; i++) {
						$scope.remindTherapyExpiryTypes[$scope.detail.remindTherapyExpiryTypes[i]] = true;
					}
				}
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {};

			PermissionService.getAll({ entityType : 'HCP' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'HCP' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
				EntityCustomPropertyConfService.search({filters :{'DATA_TYPE_NAME':'HCP'}}, function(result) {
					if(angular.isDefined(result.data) && result.data.length > 0) {
						$scope.detail.customProperties = [];
						for(var i = 0; i < result.data.length; i++) {
							var customPropertyConf = result.data[i];
							var customProperty = {
									name: customPropertyConf.name,
									code: customPropertyConf.code,
									list: customPropertyConf.list,
									mandatory: customPropertyConf.mandatory,
									type: customPropertyConf.type,
									choices: customPropertyConf.choices
							}
							$scope.detail.customProperties.push(customProperty);
						}
					}
				}, function(error) {
				})
				
				HcpService.searchApprovals({}, function(result) {
					$scope.detail.approvals = result.data || [];
				}, function(error) {
				});
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		if($scope.objectFilters.department && $scope.objectFilters.department.id) {
			$scope.searchFilters.filters.DEPARTMENT_ID = $scope.objectFilters.department.id;
		}
		else {
			$scope.searchFilters.filters.DEPARTMENT_ID = undefined;
		}
		
		$scope.searchFilters.filters.PATIENT_ID = $scope.objectFilters.patient != undefined ? $scope.objectFilters.patient.id : undefined;
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		HcpService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}	
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		HcpService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			$state.go("app.hcp", {id: $scope.id});
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		HcpService.update({ id : $scope.id },request,function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			$state.go("app.hcp", {id: $scope.id}, {reload: true});
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				var title = 'Errore in fase di aggiornamento';
				var message = 'Alcuni dati non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	
	
	$scope.confirmActivate = function(detailActivate) {
		
		var doBindAccount = 'binded' == detailActivate.accountType;
		var Service;
		//Se ci sono portali correlati fa la chiamata al metodo del gateway-rest
		if($rootScope.relatedPortals!=undefined && $rootScope.relatedPortals.length>0 && doBindAccount) {
			Service = GatewayHcpService;
		}
		else {
			Service = HcpService;
		}
		
		Service.activateNotification({ id : $scope.id }, detailActivate, function(result) {
			MessageService.showSuccess('Attivazione completata con successo');
			$state.reload();
			//$scope.$modalInstance.close();
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				if(error.data && error.data.message) {
					MessageService.showError('Attivazione non riuscita: ' + error.data.message);
				}
				else {
					MessageService.showError('Attivazione non riuscita');
				}
			}
		});
	}
	
	$scope.suspend = function() {
		HcpService.suspend({ id : $scope.id }, undefined, function(result) {
			MessageService.showSuccess('Sospensione completata con successo');
			$state.reload();
			
		}, function(error) {
			MessageService.showError('Sospensione non riuscita');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.terminate = function() {
		HcpService.terminate({ id : $scope.id }, undefined, function(result) {
			MessageService.showSuccess('Uscita dal programma completata con successo');
			$state.reload();
			
		}, function(error) {
			MessageService.showError('Uscita dal Programma non riuscita');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					HcpService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.hcps");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.doInvite = function(id) {
		var modalInstance = MessageService.activationConfirm();
		modalInstance.result.then(
			function(confirm) {				
				$scope.inviteUser(id);
			}
		);
	}
	
	/**
	 * Mostra popup per la scelta degli utenti su altri PSP da legare al corrente
	 */
	$scope.doBindToOtherAccount = function(hcp) {
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/user/bind-to-other-account.html',
			size: 'md'
		});
	}
	
	$scope.invite = function(hcp) {
		
		//Verifico se ci sono portali collegati
		if($rootScope.relatedPortals!=undefined && $rootScope.relatedPortals.length>0) {
			GatewayHcpService.search({},{ 
				filters: {
					'LASTNAME' : hcp.lastName, 
					'FIRSTNAME' : hcp.firstName, 
					'REGISTERED' : true, 
					'STATUS' : ['ACTIVE','WAITING'],
					'PORTAL' : '-'+$rootScope.programProperties['PORTAL_ID']
				}, 
				start: 0, 
				sort: ['lastName','firstName','portalName'] 
			},function(result) {
				$scope.relatedCandidates = result.data;	
				if($scope.relatedCandidates==undefined || $scope.relatedCandidates.length==0) {
					$scope.doInvite(hcp.id);
				}
				else {
					$scope.doBindToOtherAccount(hcp);
				}
			}, function(error) {
				console.log('Error retrieving candidate related hcps '+error);
				MessageService.showError('Errore','Problema nella ricerca di possibili accounts collegati al Medico presenti su altri PSP');
			});
		}
		else {
			$scope.doInvite(hcp.id);
		}
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.impersonateUser(nickName);
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					/*{ id:'detail.role' , value : $scope.detail.role, type: 'string',  required:true},*/
					{ id:'detail.lastName', value: $scope.detail.lastName, required:true, type:'string' },
					{ id:'detail.sex', value: $scope.detail.sex, required:($scope.isHcp ? false : true), type:'string' },
					{ id:'detail.firstName', value: $scope.detail.firstName, required:true, type:'string' },
					{ id:'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.cellPhoneNumber', value: $scope.detail.cellPhoneNumber, type:'string' , pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.fiscalCode', value: $scope.detail.fiscalCode, type:'string',pattern: REGEXP.FISCAL_CODE},
					{ id:'detail.email' , value: $scope.detail.email, type:'email', pattern:REGEXP.EMAIL},
					{ id:'detail.enrollmentDate',  value: $scope.detail.enrollmentDate, type:'string', required:true},
					{ id:'detail.department',  value: $scope.detail.department, type:'string', required:true}
					]
		};
		errors = FormUtilService.validateForm(form);
		if(angular.isDefined($scope.detail.customProperties)){
			for(var i = 0; i < $scope.detail.customProperties.length; i++){
				var prop = $scope.detail.customProperties[i];
				if(prop.mandatory == true && (prop.values == '' || prop.values == undefined || prop.values == null ) ){
					errors['detail.customProperties_' + i] = 'Campo obbligatorio';
				}
			}
		}	
		if($scope.detail.privacyEnrollment !== undefined && $scope.detail.privacyEnrollment == false) {
			errors['detail.privacyEnrollment'] = 'Campo obbligatorio';
		}
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};
		
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		$scope.objectFilters = $scope.objectFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			$scope.canInvite = permissions.INVITE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
			$scope.canInvite = false;
		}
		$scope.permissions = permissions;
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.hcps");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.hcp", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				} else {
					// Già presente nella lista
				}
			} else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

	$scope.loadRequest = function() {
		var departmentId = undefined;
		if ($scope.detail.department != undefined && $scope.detail.department != null) {
			departmentId = $scope.detail.department.id;
		}
		
		$scope.detail['preferredContacts'] = [];
		for(var key in $scope.contactMethodsType) {
			$scope.detail['preferredContacts'].push({type: key, preference: $scope.contactMethodsType[key]});
		}
		
		$scope.detail['remindTherapyExpiryTypes'] = [];
		for(var key in $scope.remindTherapyExpiryTypes) {
			if($scope.remindTherapyExpiryTypes[key] == true) {
				$scope.detail['remindTherapyExpiryTypes'].push(key);
			}
		}
		
		return {
			title : 					$scope.detail.title,
			role : 						$scope.detail.role,
			lastName: 					$scope.detail.lastName,
			firstName: 					$scope.detail.firstName,
			sex: 						$scope.detail.sex,
			birthDate: 					$scope.detail.birthDate,
			nationality: 				$scope.detail.nationality,
			code:						$scope.detail.code,
			fiscalCode: 				$scope.detail.fiscalCode,
			specialization: 			$scope.detail.specialization,
			phoneNumber: 				$scope.detail.phoneNumber,
			faxNumber: 					$scope.detail.faxNumber,
			cellPhoneNumber: 			$scope.detail.cellPhoneNumber,
			email: 						$scope.detail.email,
			address: 					$scope.detail.address,
			enrollmentDate: 			$scope.detail.enrollmentDate,
			privacyEnrollment: 			$scope.detail.privacyEnrollment,
			internalNotes: 				$scope.detail.internalNotes,
			departmentId: 				departmentId,
			receiveModuleDate:			$scope.detail.receiveModuleDate,
			customProperties:			$scope.detail.customProperties,
			preferredContacts:			$scope.detail.preferredContacts,
			skypeId:					$scope.detail.skypeId,
			remindTherapyExpiryTypes: 	$scope.detail.remindTherapyExpiryTypes,
			approvals:					$scope.detail.approvals,
		}
	}
	
	$scope.startMasterActivity = function(masterActivity) {
		UserServiceConfService.startUserMasterActivityManually({ userId : $scope.id, masterActivityId : masterActivity.id }, 
			function(result) {
				MessageService.showSuccess('Avvio attivit\u00E0 completato con successo');
				var businessKey = result.data;
				if(businessKey !== undefined) {
					WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
						function(result) {
							if(result.data !== undefined) {
								$state.go('app.task', { 'id': result.data.id });	
							}
							else {
								$state.go('app.my-tasks');
							}
						},
						function(error) {
							$state.go('app.my-tasks');
						}
					)
				}
				else {
					$state.go('app.my-tasks');
				}
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					MessageService.showError('Errore in fase di avvio attivit\u00E0','Alcuni dati inseriti non sono corretti');
				}
			}
		);
	}
	
	$scope.haveOtherData = function() {
		var res = angular.isDefined($scope.detail.customProperties) && $scope.detail.customProperties.length > 0;
		return res;
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.HCP + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
	$scope.inviteAll = function() {
		$scope.countHcpToInvite();
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/user/invite-all-users.html',
			size: 'md',
		});
	};
	
	$scope.confirmInvite = function(){
		HcpService.inviteActivationAll({}, $scope.searchFilters, function(result) {
			MessageService.showSuccess('Invito di registrazione medico creato con successo');
		}, function (error){
			MessageService.showError('Invito di registrazione medico non creato');
		});
		$scope.$modalInstance.close();
	};
	
	$scope.undoInvite = function(){
		$scope.$modalInstance.dismiss('cancel');
	}
	
	$scope.countHcpToInvite = function() {
		$scope.usersToInvite = 0;
		HcpService.countSearchInvite({}, $scope.searchFilters, function(result) {
			$scope.usersToInvite = result.data;
		});
	}
	
	$scope.init();
});