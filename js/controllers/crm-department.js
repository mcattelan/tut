'use strict'

app.controller('DepartmentController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		DepartmentService,
		MessageService,
		FormUtilService,
		PermissionService,
		SMART_TABLE_CONFIG,
		REGEXP,
		TerritoryService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};
		
		TerritoryService.searchRegions({}, {}, function(result) {
			$scope.selects['Regions'] = result.data;
		}, function(error) {
			$scope.selects['Regions'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});

		if(($scope.medicalCenterId === undefined || $scope.medicalCenterId == null) && !$scope.popup) {
			$scope.medicalCenterId = $stateParams.medicalCenterId;
		}
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'DEPARTMENT' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.detail = {};

		if (!$scope.isNew()) {
			DepartmentService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			PermissionService.getAll({ entityType : 'DEPARTMENT' }, function(result) {
				$scope.initPermission(result.data);
				PermissionService.getAllFields({ entityType : 'DEPARTMENT' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		DepartmentService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
		

	};
	
	$scope.insert = function() {
		var request = this.loadRequest();
		request.medicalCenterId = $scope.medicalCenterId;
		DepartmentService.insert({}, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			if ($scope.modal) {
				$scope.modal.close();
			}
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});

	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		DepartmentService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					DepartmentService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.name', value : $scope.detail.name, type: 'string', required : true },
					{ id:'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.cellPhoneNumber', value : $scope.detail.cellPhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.email', value : $scope.detail.email, type: 'string',  pattern: REGEXP.EMAIL,  customMessage:"L'indirizzo mail deve essere valido" },
					{ id:'address.country', value : $scope.detail.address.country, type: 'string', required : true },
					{ id:'address.county', value : $scope.detail.address.county, type: 'string',  required : true }
					]
		};
		errors = FormUtilService.validateForm(form);
		
		if(errors['address.county'] != undefined){
			
			$('.ui-select-container').each(function(k,v){if($(v).attr('id').lastIndexOf('county') != -1){$(v).addClass('error')}})
		}
		if(errors['address.country'] != undefined){
			
			$('.ui-select-container').each(function(k,v){if($(v).attr('id').lastIndexOf('country') != -1){$(v).addClass('error')}})
		}
		console.log(errors);
		return errors;
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);	
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}
	
	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.undo = function() {
		if ($scope.isNewDepartment()) {
			$state.go("app.medicalcenter-departments");	
		} else {
			$scope.initDetails();
		}
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}
	
	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

	$scope.loadRequest = function() {
		return {
			name:            $scope.detail.name,
			description:     $scope.detail.description,
			refFirstName:    $scope.detail.refFirstName,
			refLastName:     $scope.detail.refLastName,
			qualification:   $scope.detail.qualification,
			phoneNumber:     $scope.detail.phoneNumber,
			faxNumber:       $scope.detail.faxNumber,
			cellPhoneNumber: $scope.detail.cellPhoneNumber,
			email:           $scope.detail.email,
			address:         $scope.detail.address,
			internalNotes:   $scope.detail.internalNotes
		}
	};

	$scope.init();
});