'use strict';
app.controller('DisclaimerController', function(
		$rootScope,
		$scope,
		$http,
		$state,
		$stateParams,
		$filter,
		RESTURL,
		UserService,
		MessageService,
		PatientService,
		PortalService,
		HcpService) {

		console.log($rootScope);
	
		console.log('disclaimer');
		
		$scope.showDisclaimers = function(){
			$scope.show = true;
		}
		
		$scope.init = function(){
			
			$scope.disclaimers = [];
			$scope.detail = {};
			$scope.loggedUser = $rootScope.loggedUser;
			$scope.show = true;
			
			
			if(!angular.isDefined($scope.loggedUser) || $scope.loggedUser == null || angular.equals($scope.loggedUser,{})){
				
				UserService.getLoggedUser({},function(result){
					
					$scope.loggedUser = result.data;
					$rootScope.loggedUser = result.data;
					
					if(!angular.isDefined($rootScope.constants) || $rootScope.constants == null || angular.equals($rootScope.constants,{})){
						
						PortalService.get({},function(result){
							
							$rootScope.constants= result.data;
							$scope.title = $filter('constantLabel')($scope.loggedUser.title,'UserTitle') 
												
							
						},function(error){
							
							$state.go('access.not-found')
							
						})
						
					}
					
					UserService.authorizationKeyForUser({},function(result){
						
						
						$scope.disclaimers = result.data;
						for(var i = 0; i < $scope.disclaimers.length; i++){
							var key = $scope.disclaimers[i].keyId;
							$scope.detail[key] = {};
							
							if(angular.isDefined($scope.loggedUser.authorizationKeys) && angular.isDefined($scope.loggedUser.authorizationKeys[key])){
								
								if($scope.loggedUser.authorizationKeys[key] == true){
									$scope.detail[key]['yes'] = true;
									$scope.show = true;
									$scope.disableSelections = true;
									$scope.detail[key]['no'] = false;
								}
								else if($scope.loggedUser.authorizationKeys[key] == false){
									$scope.detail[key]['yes'] = false;
									$scope.show = false;
									$scope.disableSelections = false;
									$scope.detail[key]['no'] = true;
								}
								else{
									$scope.show = true;
									$scope.disableSelections = false;
									$scope.detail[key]['yes'] = false;
									$scope.detail[key]['no'] = true;
								}
							}
						}
								
							
					},function(error){
						MessageService.showError('Impossibile trovare i disclaimer di autorizzazione richiesti', 'Disclaimer di autorizzazione mancanti');
						$state.go('access.not-found');
					});
					
				},function(error){
					
					$state.go('access.not-found')
					
				})
				
			}
			else{
				
				
				UserService.authorizationKeyForUser({},function(result){
					
					
					$scope.disclaimers = result.data;
					for(var i = 0; i < $scope.disclaimers.length; i++){
						var key = $scope.disclaimers[i].keyId;
						$scope.detail[key] = {};
						
						if(angular.isDefined($scope.loggedUser.authorizationKeys) && angular.isDefined($scope.loggedUser.authorizationKeys[key])){
							
							if($scope.loggedUser.authorizationKeys[key] == true){
								$scope.detail[key]['yes'] = true;
								$scope.show = true;
								$scope.disableSelections = true;
								$scope.detail[key]['no'] = false;
							}
							else if($scope.loggedUser.authorizationKeys[key] == false){
								$scope.detail[key]['yes'] = false;
								$scope.show = false;
								$scope.disableSelections = false;
								$scope.detail[key]['no'] = true;
							}
							else{
								$scope.show = true;
								$scope.disableSelections = false;
								$scope.detail[key]['yes'] = false;
								$scope.detail[key]['no'] = true;
							}
						}
					}
							
						
				},function(error){
					MessageService.showError('Impossibile trovare i disclaimer di autorizzazione richiesti', 'Disclaimer di autorizzazione mancanti');
					$state.go('access.not-found');
				});
			}
			
			
			
			
		}
		
		
		$scope.save = function(){
			
			var req = $scope.loadRequest();
			console.log(req);
			if(angular.isDefined($scope.loggedUser) && $scope.loggedUser != null && !angular.equals($scope.loggedUser,{})){
				if($scope.loggedUser.type == 'PATIENT'){
					PatientService.updateAuthorizationKeys({id:$scope.loggedUser.id},req,function(result){
						$state.go('app.dashboard');
					},function(error){
						MessageService.showError('Errore', 'Impossibile aggiornare le autorizzazioni richieste');
						$state.go('access.not-found')
					})
				}
				else if($scope.loggedUser.type == 'HCP'){
					HcpService.updateAuthorizationKeys({id:$scope.loggedUser.id},req,function(result){
						$state.go('app.dashboard');
					},function(error){
						MessageService.showError('Errore', 'Impossibile aggiornare le autorizzazioni richieste');
						$state.go('access.not-found')
					})
				}
				else{
					//TODO da aggiornare piu avanti
				}
			}
			else{
				
				UserService.getLoggedUser({},function(result){
					
					$scope.loggedUser = result.data;
					
					if($scope.loggedUser.type == 'PATIENT'){
						PatientService.updateAuthorizationKeys({id:$scope.loggedUser.id},req,function(result){
							console.log(result);
							$state.go('app.dashboard');
						},function(error){
							MessageService.showError('Errore', 'Impossibile aggiornare le autorizzazioni richieste');
							$state.go('disclaimer')
						})
					}
					else if($scope.loggedUser.type == 'HCP'){
						HcpService.updateAuthorizationKeys({id:$scope.loggedUser.id},req,function(result){
							$state.go('app.dashboard');
							
						},function(error){
							MessageService.showError('Errore', 'Impossibile aggiornare le autorizzazioni richieste');
							$state.go('disclaimer')
						})
					}
					else{
						//TODO da aggiornare piu avanti
					}
					
				},function(error){
					
					$state.go('access.not-found')
					
				})
				
			}
		
			
		}
		
		$scope.undo = function(){
			
			$scope.detail = undefined;
		}
		
		$scope.setHcpValues = function(){
			
			console.log($scope.detail);
			for(var key in $scope.detail){
				
				if(angular.isDefined($scope.detail[key]['yes']) && $scope.detail[key]['yes'] == true){
					$scope.detail[key]['yes'] = true;
					$scope.detail[key]['no'] = false;
				}
				else if(angular.isDefined($scope.detail[key]['yes']) && $scope.detail[key]['yes'] == false){
					$scope.detail[key]['no'] == true;
					$scope.detail[key]['yes'] = false;
				}
				else{
					//TODO gestione null
				}
			}
		}
		
		$scope.loadRequest = function(){
			
			var request = {};
			for(var key in $scope.detail){
				
				if(angular.isDefined($scope.detail[key]['yes']) && $scope.detail[key]['yes'] == true){
					request[key] = true;
				}
				else if(angular.isDefined($scope.detail[key]['no']) && $scope.detail[key]['no'] == true){
					request[key] = false;
				}
				else{
					//TODO gestione null
				}
			}
			
			return {
				
				'authorizationKeys':request
			};
			
		}
			 
		$scope.init();
		
});

