'use strict'

app.controller('LogisticController', function(
		$rootScope,
		$scope,
		$stateParams,
		$http,
		$state,
		LogisticService,
		FormUtilService,
		PermissionService,
		MessageService,
		REGEXP,
		RESTURL,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if($scope.id === undefined || $scope.id == null) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id) && $scope.id !== '') {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'LOGISTIC' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.showListUser = false;
		if (!$scope.isNew()) {
			LogisticService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				$scope.initPaginationAndFilterLogisticUsers();
				$scope.showListUser = true;
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				$scope.initPaginationAndFilterLogisticUsers();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					type: 'COMPANY',
					representative: {}
			};
			
			PermissionService.getAll({ entityType : 'LOGISTIC' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'LOGISTIC' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.initPaginationAndFilterLogisticUsers = function() {
		$scope.paginationLogisticUsers = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFiltersLogisticUsers = $scope.searchFiltersLogisticUsers || {};
		$scope.initialSearchFiltersLogisticUsers = angular.copy($scope.searchFiltersLogisticUsers);
	}
	
	
	$scope.searchUser = function(tableStateLogisticUsers, smCtrlLogisticUsers) {
		$scope.smCtrlLogisticUsers = $scope.smCtrlLogisticUsers || smCtrlLogisticUsers;
		if(tableStateLogisticUsers === undefined) {
			if($scope.smCtrlLogisticUsers !== undefined) {
				$scope.smCtrlLogisticUsers.tableState().pagination.start = 0;
				$scope.smCtrlLogisticUsers.pipe();
			}
			return;
		}
		if(tableStateLogisticUsers !== undefined) {
			tableStateLogisticUsers.pagination.number = $scope.paginationLogisticUsers.size;
			
			$scope.paginationLogisticUsers.start = tableStateLogisticUsers.pagination.start;
			if(tableStateLogisticUsers.sort != null && tableStateLogisticUsers.sort.predicate !== undefined) {
				$scope.paginationLogisticUsers.sort = tableStateLogisticUsers.sort.reverse ? [ "-" + tableStateLogisticUsers.sort.predicate ] : [ tableStateLogisticUsers.sort.predicate ];
			}
			if(tableStateLogisticUsers.pagination.number !== undefined) {
				$scope.paginationLogisticUsers.size = tableStateLogisticUsers.pagination.number;
			}
		}
		$scope.listUser = $scope.listUser || [];
		
		angular.extend($scope.searchFiltersLogisticUsers, $scope.paginationLogisticUsers);
		delete $scope.searchFiltersLogisticUsers.total;

		$scope.listUser = [];
		PermissionService.getAll({ entityType : 'LOGISTIC_USER' }, function(result) {
			$scope.canInsertUser = result.data.INSERT;
			$scope.canExportUser = result.data.EXPORT;
			
			LogisticService.searchAllUser( {id: $scope.id},$scope.searchFiltersLogisticUsers, function(result) {
				$scope.listUser = result.data;
				$scope.permissionUser = result.permissions;
				$scope.paginationLogisticUsers.start = result.start;
				$scope.paginationLogisticUsers.size = result.size;
				$scope.paginationLogisticUsers.sort = result.sort;
				$scope.paginationLogisticUsers.total = result.total;
				
				if(tableStateLogisticUsers !== undefined) {
					tableStateLogisticUsers.pagination.numberOfPages = Math.ceil($scope.paginationLogisticUsers.total / ($scope.paginationLogisticUsers.size !== undefined && $scope.paginationLogisticUsers.size != null ? $scope.paginationLogisticUsers.size : 1));
				}
				
			}, function(error) {
				$scope.listUser = [];
				$scope.initPaginationAndFilterLogisticUsers();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.listUser = [];
			$scope.initPaginationAndFilterLogisticUsers();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if(tableState === undefined) {
			if($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		LogisticService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
			} else {
				$scope.update();
			}
		}	
	};
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		LogisticService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.impersonateUser(nickName);
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.name', value : $scope.detail.name, type: 'string', required: true },
					{ id: 'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.vatNumber', value : $scope.detail.vatNumber, type: 'string',  dependRequired: 'detail.fiscalCode', pattern: REGEXP.VAT_NUMBER,  customMessage:'Il campo deve rispettare il formato della partita iva' },
					{ id: 'detail.type' , value : $scope.detail.type, type: 'string',  required: true},
					{ id: 'detail.representativePhoneNumber', value : $scope.detail.representativePhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeCellPhoneNumber', value : $scope.detail.representativeCellPhoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeFaxNumber', value : $scope.detail.representativeFaxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id: 'detail.representativeEmail', value : $scope.detail.representativeEmail, type: 'string', pattern: REGEXP.EMAIL,  customMessage: "L'indirizzo mail deve essere valido" }
					]
		};
		if($scope.detail.type === 'PERSON') {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale' });
		}
		else {
			form.formProperties.push({ id: 'detail.fiscalCode', value : $scope.detail.fiscalCode, type: 'string',  dependRequired: 'detail.vatNumber', pattern: REGEXP.VAT_NUMBER_OR_FISCAL_CODE, customMessage:'Il campo deve rispettare il formato del codice fiscale o della partita iva' });
		}
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.initPermission = function(permissions) {
		if(permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			
			if($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if($scope.isNew()) {
			$state.go("app.logistics");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if(!$scope.selection) {
			$state.go("app.logistic", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		
		$scope.detail['preferredContacts'] = [];
		for(var key in $scope.contactMethodsType) {
			$scope.detail['preferredContacts'].push({type: key, preference: $scope.contactMethodsType[key]});
		}
		
		return {
			type: 							$scope.detail.type,
			name: 							$scope.detail.name,
			description: 					$scope.detail.description,
			fiscalCode: 					$scope.detail.fiscalCode,
			vatNumber: 						$scope.detail.vatNumber,
			phoneNumber: 					$scope.detail.phoneNumber,
			faxNumber: 						$scope.detail.faxNumber,
			address: 						$scope.detail.address,
			internalNotes: 					$scope.detail.internalNotes,
			representativeFirstName: 		$scope.detail.representativeFirstName, 
			representativeLastName: 		$scope.detail.representativeLastName,
			representativePhoneNumber: 		$scope.detail.representativePhoneNumber,
			representativeFaxNumber: 		$scope.detail.representativeFaxNumber,
			representativeCellPhoneNumber: 	$scope.detail.representativeCellPhoneNumber,
			representativeEmail: 			$scope.detail.representativeEmail,
			preferredContacts:				$scope.detail.preferredContacts
		}
	}
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if ($scope.list[i].selected) {
				if ($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				} else {
					// Già presente nella lista
				}
			} else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if ($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.LOGISTIC + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}

	$scope.init();
});