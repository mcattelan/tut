'use strict'
app.controller('QuestionnaireController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state, 
		UserService, 
		Base64, 
		$location, 
		RESTURL, 
		$modal,
		$filter,
		FormUtilService,
		MessageService,
		QuestionnaireService,
		PermissionService,
		QualificationService,
		$sce,
		SMART_TABLE_CONFIG) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id)) {
			$scope.initDetail();
		} else {
			$scope.initList();
		}
	}

	$scope.initList = function() {	
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'QUESTIONNAIRE' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetail = function() {
		$scope.usersGroups = [];
		$scope.textBlockMenu = [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'quote', 'paragragh'],
            ['link']
	    ];
			
		$scope.formulaValidated = false;
		$scope.errors = {};
		
		$scope.constants = $rootScope.constants;
		$scope.usersGroups = $scope.constants['UserType'];
		$scope.questionnairePermissionUserTypes = [];
		QualificationService.get({},function(result) {
			
			$scope.qualifications = result.data;
			for(var i=0; i<$scope.usersGroups.length; i++) {
				if($scope.usersGroups[i].id=='SUPPLIER') {
					for(var j=0; j<$scope.qualifications.length; j++) {
						$scope.questionnairePermissionUserTypes.push({ 
							id: $scope.usersGroups[i].id+'|'+$scope.qualifications[j].id,
							userType: $scope.usersGroups[i], 
							qualification: $scope.qualifications[j] ,
							label: $scope.qualifications[j].name
						});
					}
				}
				else
					$scope.questionnairePermissionUserTypes.push({ 
						id: $scope.usersGroups[i].id,
						userType: $scope.usersGroups[i], 
						qualification: undefined,
						label: $scope.usersGroups[i].label
					});
			}
			
			console.log('QuestionnairePermissionUserTypes:');
			console.log($scope.questionnairePermissionUserTypes);
		}, function(error) {
			MessageService.showError('Problema nel recupero delle qualifiche');
		});

		
		$scope.scoreTypes = $scope.constants['ScoreType'];
		
		$scope.showElements = {
			newQuestionConfig : false,
			newRepliesConfig : false,
			scoreConfig: false
		};
		
		$scope.formulaTags = [];
		
		//Flag per aprire e chiudere il "Modifica" su cuascuna domanda
		$scope.questionsEditingMode = [];
		//backup della versione precedente di ciascuna domanda
		$scope.previousQuestionVersions = [];
		
		$scope.detail = { questions: [] };

		if(!$scope.isNew()) {
			QuestionnaireService.get({ id:$scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.initPermission(result.permissions);
				
				if(result.data.scoreType==undefined || result.data.scoreType==null || result.data.scoreType=='') 
					delete $scope.detail.scoreType;
				
				for(var i=0; i<$scope.detail.questions.length; i++) {
					$scope.questionsEditingMode.push(false);
					if($scope.detail.questions[i].type!='SINGLE_CHOICE' && $scope.detail.questions[i].type!='MULTIPLE_CHOICE') {
						if($scope.detail.questions[i].type=='SCORE' || $scope.detail.questions[i].type=='SCORE_SLIDER') {
							
							var min = parseFloat($scope.detail.questions[i].replies[0].text);
							var max = parseFloat($scope.detail.questions[i].replies[$scope.detail.questions[i].replies.length-1].text);
							var step = parseFloat($scope.detail.questions[i].replies[1].text)-parseFloat(min);
							
							$scope.detail.questions[i].score = { start: min, end: max, step: step };
							if($scope.detail.questions[i].type=='SCORE_SLIDER') {
								$scope.detail.questions[i].score.sliderVal = min;
							}
							
						}
						delete $scope.detail.questions[i].replies;
					}
					
					
					$scope.previousQuestionVersions.push($scope.detail.questions);
					//Inizializza tags formula
					$scope.initFormulaTags();
				}
				$scope.detail.disableModifyQuestion = false;
				
				var viewers = [];
				for(var i=0; i<$scope.detail.viewers.length; i++) {
					var key = $scope.detail.viewers[i].userType;
					if($scope.detail.viewers[i].qualification!=undefined)
						key = $scope.detail.viewers[i].userType+'|'+$scope.detail.viewers[i].qualification.id;
					
					viewers.push({ 
						id: key,
						userType: $scope.detail.viewers[i].userType, 
						qualification: $scope.detail.viewers[i].qualification,
						label: ($scope.detail.viewers[i].qualification)==undefined ? $filter('constantLabel')($scope.detail.viewers[i].userType,'UserType') : $scope.detail.viewers[i].qualification.name
					});		
				}
				$scope.detail.viewers = angular.copy(viewers);	
				
				var compilers = [];
				for(var i=0; i<$scope.detail.compilers.length; i++) {
					var key = $scope.detail.compilers[i].userType;
					
					if($scope.detail.compilers[i].qualification!=undefined)
						key = $scope.detail.compilers[i].userType+'|'+$scope.detail.compilers[i].qualification.id;
					
					compilers.push({ 
						id: key,
						userType: $scope.detail.compilers[i].userType, 
						qualification: $scope.detail.compilers[i].qualification,
						label: ($scope.detail.compilers[i].qualification)==undefined ? $filter('constantLabel')($scope.detail.compilers[i].userType,'UserType') : $scope.detail.compilers[i].qualification.name
					});		
				}
				$scope.detail.compilers = angular.copy(compilers);	
				
				var approvers = [];
				for(var i=0; i<$scope.detail.approvers.length; i++) {
					
					var key = $scope.detail.approvers[i].userType;
					
					if($scope.detail.approvers[i].qualification!=undefined)
						key = $scope.detail.approvers[i].userType+'|'+$scope.detail.approvers[i].qualification.id;
					
					
					approvers.push({ 
						id: key,
						userType: $scope.detail.approvers[i].userType, 
						qualification: $scope.detail.approvers[i].qualification,
						label: ($scope.detail.approvers[i].qualification)==undefined ? $filter('constantLabel')($scope.detail.approvers[i].userType,'UserType') : $scope.detail.approvers[i].qualification.name
					});		
				}
				$scope.detail.approvers = angular.copy(approvers);	
				
				var scoreViewers = [];
				for(var i=0; i<$scope.detail.scoreViewers.length; i++) {
					var key = $scope.detail.scoreViewers[i].userType;
					if($scope.detail.viewers[i].scoreViewers!=undefined)
						key = $scope.detail.scoreViewers[i].userType+'|'+$scope.detail.scoreViewers[i].qualification.id;
					
					scoreViewers.push({ 
						id: key,
						userType: $scope.detail.scoreViewers[i].userType, 
						qualification: $scope.detail.scoreViewers[i].qualification,
						label: ($scope.detail.scoreViewers[i].qualification)==undefined ? $filter('constantLabel')($scope.detail.scoreViewers[i].userType,'UserType') : $scope.detail.scoreViewers[i].qualification.name
					});		
				}
				$scope.detail.scoreViewers = angular.copy(scoreViewers);	
				
				
				console.log('Questionario caricato: ');
				console.log($scope.detail);
				
			}, function(error) {
				$scope.detail = { questions: [] };
				$scope.questionsEditingMode = [];
				$scope.previousQuestionVersions=[];
				$scope.detail.disableModifyQuestion = false;
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			$scope.detail = {
				questions: []	
			};

			PermissionService.getAll({ entityType : 'QUESTIONNAIRE' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'QUESTIONNAIRE' }, function(result) {
					$scope.fieldPermissions = result.data;
					$scope.questionsEditingMode = [];
					$scope.previousQuestionVersions=[];
				}, function(error) {
					$scope.detail = { questions: [] };
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
			
		}
	}
	
	
	$scope.search = function(tableState) {
		if(tableState !== undefined) {
			if (angular.equals(tableState, $scope.previousTableState)) return;
			tableState.pagination.number = $scope.pagination.size;
			$scope.previousTableState = angular.copy(tableState);
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		QuestionnaireService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}
	
	$scope.save = function(){
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			$scope.qst = angular.copy($scope.detail);
			delete $scope.qst.disableModifyQuestion;
			
			for(var q=0; q<$scope.qst.questions.length; q++) {

				delete $scope.qst.questions[q].id;
				delete $scope.qst.questions[q].timeCreated;
				delete $scope.qst.questions[q].timeModified;
				delete $scope.qst.questions[q].position;
				delete $scope.qst.questions[q].disabled;
				
				delete $scope.qst.questions[q].formulaTag;
				
				if(($scope.detail.questions[q].type=='MULTIPLE_CHOICE' || $scope.detail.questions[q].type=='SINGLE_CHOICE')) {
					$scope.qst.questions[q].choices = [];
					for(var i=0; i<$scope.detail.questions[q].replies.length; i++) {
						$scope.qst.questions[q].choices.push({ 
							text: $scope.detail.questions[q].replies[i].text, 
							value: $scope.detail.questions[q].replies[i].value,
							enableQuestions: $scope.detail.questions[q].replies[i].enableQuestions,
							requiredQuestions: $scope.detail.questions[q].replies[i].requiredQuestions
						});
					}
				}
				delete $scope.qst.questions[q].replies;
				if($scope.qst.questions[q].type=='SCORE_SLIDER') {
					delete $scope.qst.questions[q].score.sliderVal;
				
				}
			}
			
			//Test della formula se score custom
			if($scope.qst.scoreType!='CUSTOM') {
				delete $scope.qst.scoreFormula;
			}
			$scope.saveQuestionnaire();
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
	
	$scope.saveQuestionnaire = function() {
		if($scope.qst.scoreType=='')
			delete $scope.qst.scoreType; 
		
		if ($scope.isNew()) {
			$scope.insert();	
		} else {
			$scope.update();
		}
	}
	
	$scope.insert = function() {
		var request = $scope.createRequest($scope.qst);
		
		QuestionnaireService.insert({}, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			if ($scope.modal) {
				$scope.modal.close();
			}
			else {
				$timeout( function() {
					$state.go("app.questionnaire", {id: $scope.id});
				},2000);
			}
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}
	
	$scope.update = function(){
		delete $scope.qst.id;
		delete $scope.qst.timeCreated;
		delete $scope.qst.timeModified;
		
		var request = $scope.createRequest($scope.qst);

		QuestionnaireService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			if ($scope.modal) {
				$scope.modal.close();
			}
			else {
				$stateParams.id = result.data.id;
				$scope.init();
			}
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.createRequest = function(qst) {
		var viewers = [];
		for(var i=0; i<qst.viewers.length; i++) {
			var keys = (''+qst.viewers[i].id).split('|');
			viewers.push({ userType: keys[0], qualificationId: keys[1]});
		}
		qst.viewers = angular.copy(viewers);
		
		var compilers = [];
		for(var i=0; i<qst.compilers.length; i++) {
			var keys = (''+qst.compilers[i].id).split('|');
			compilers.push({ userType: keys[0], qualificationId: keys[1]});
		}
		qst.compilers = angular.copy(compilers);
		
		var approvers = [];
		for(var i=0; i<qst.approvers.length; i++) {
			var keys = (''+qst.approvers[i].id).split('|');
			approvers.push({ userType: keys[0], qualificationId: keys[1]});
		}
		qst.approvers = angular.copy(approvers);
		
		var scoreViewers = [];
		for(var i=0; i<qst.scoreViewers.length; i++) {
			var keys = (''+qst.scoreViewers[i].id).split('|');
			scoreViewers.push({ userType: keys[0], qualificationId: keys[1]});
		}
		qst.scoreViewers = angular.copy(scoreViewers);
		
		return qst;
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					QuestionnaireService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.questionnaires");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.validate = function() {
		var form = {};
		var errors = {};
		
		form = { formProperties : [
		         { id:'detail.name',value : $scope.detail.name,type: 'string',required : true },
		         { id:'detail.minRequiredAnswers',value : $scope.detail.minRequiredAnswers, type: 'long' }
		         ]			
		};

		errors = FormUtilService.validateForm(form);
		console.log('errors ');	
		console.log(errors);
		
		for(var k=0; k<$scope.detail.questions.length; k++) {
			if(angular.isUndefined(errors) || angular.equals(errors, {})) {
				errors = $scope.validateQuestion(k);
			}
		}
	
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.questionnaires");	
		} else {
			$scope.initDetail();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.questionnaire", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

	$scope.loadRequest = function() {
		return {
			name: 			$scope.detail.name,
			description: 	$scope.detail.description,
		}
	}

	$scope.init();
	
	/**************************************
	 * Funzioni dettaglio questionario
	 **************************************/
	$scope.closeNewQuestion = function() {
		$scope.showElements.newQuestionConfig = false;
	}
	
	$scope.displayHtml = function(text) {
		return $sce.trustAsHtml(text);
	}
	
	$scope.addQuestion = function(questionType) {
		
		var cloned = false;
		if('CLONE'==questionType) {
			var srcQuestion = $scope.detail.questions[$scope.detail.questions.length-1];
			
			$scope.newQuestion = angular.copy(srcQuestion);
			questionType = $scope.newQuestion.type;
			delete $scope.newQuestion.formulaTag;
			
			cloned = true;
		}
		else {
			$scope.newQuestion = {
				mandatory : false,
				sliderVertical: false,
				replies: [],
				type : questionType,
				score: undefined,
				text: undefined
			}
		}
		$scope.showElements.newQuestionConfig = true;
		
			
		if('SINGLE_CHOICE'==questionType || 'MULTIPLE_CHOICE'==questionType) {
			$scope.showElements.newRepliesConfig = true;
			$scope.showElements.scoreConfig = false;
			if(!cloned) 
				$scope.newQuestion.replies = [ { text: undefined, value: undefined } ];
			else {
				for(var i = 0; i<$scope.newQuestion.replies.lengrh; i++) {
					delete $scope.newQuestion.replies[i].formulaTag;
				}
			}
			
		}
		else if('SCORE'==questionType || 'SCORE_SLIDER'==questionType) {
			$scope.showElements.newRepliesConfig = false;
			$scope.showElements.scoreConfig = true;
			$scope.newQuestion.replies=[];
			
			if(!cloned) {
				$scope.newQuestion.score={
					start: 0,
					end: 10,
					step: 1
				}
			}
			if('SCORE_SLIDER'==questionType) {
				$scope.newQuestion.score.sliderVal = $scope.newQuestion.score.start;
				$scope.newQuestion.sliderScaleValue = 0;
				
			}
		}
		else {
			$scope.showElements.newRepliesConfig = false;
			$scope.showElements.scoreConfig = false;
		}
	}
	
	$scope.setQuestionType = function(type,q) {
		q.type=type;
		if(type!='SINGLE_CHOICE' && type!='MULTIPLE_CHOICE') {
			q.replies = [];
		} 
		if(type!='SCORE' && type!='SCORE_SLIDER') {
			q.score = undefined;
		}
		if(type=='SINGLE_CHOICE' || type=='MULTIPLE_CHOICE') {
			if(q.replies==undefined || q.replies==null || q.replies.length==0) {
				q.replies = [];
				q.replies.push({text: undefined, value: undefined});
			}
		}
		if(type=='SCORE' || type=='SCORE_SLIDER') {
			if(q.score==undefined || q.score==null) {
				q.score = { start: 0, end: 10, step: 1 };
			}
		}
	}
	
	$scope.addReply = function(q_index) {
		if(q_index==undefined || q_index==null) {
			$scope.newQuestion.replies.push( { text: undefined, value: undefined });
		}
		else {
			$scope.detail.questions[q_index].replies.push( { text: undefined, value: undefined });
		}
	}
	
	$scope.removeReply = function(q_index) {
		if(q_index==undefined || q_index==null) {
			$scope.newQuestion.replies.pop();
		}
		else {
			$scope.detail.questions[q_index].replies.pop();
		}
			
	}
	
	$scope.validateQuestion = function(index) {
		//Validazione
		var newQuestion = (index=='new') ? $scope.newQuestion : $scope.detail.questions[index];
		console.log('ValidateQuestion: '+index);
		var iid = 'q_'+index;
		if(index=='new') {
			iid = 'newQuestion';
		}
		var form = {
			formProperties : [
              { id: iid+'.text',value : newQuestion.text,type: 'string',required : true },
              { id: iid+'.mandatory',value : newQuestion.mandatory, type: 'boolean', required: true }
			]
		};
		
		if(newQuestion.type=='SCORE' || newQuestion.type=='SCORE_SLIDER') {
			form.formProperties.push({ id:iid+'.score.start',value : newQuestion.score.start,type: 'double',required : true });
			form.formProperties.push({ id:iid+'.score.end',value : newQuestion.score.end,type: 'double',required : true });
			form.formProperties.push({ id:iid+'.score.step',value : newQuestion.score.step,type: 'double',required : true });
			if (newQuestion.type=='SCORE_SLIDER') {
				if (newQuestion.sliderScaleValue ==undefined || newQuestion.sliderScaleValue == null) {
					newQuestion.sliderScaleValue = 0;
				}
				if (newQuestion.sliderScaleValue > 0) {
					if ((newQuestion.score.end % newQuestion.sliderScaleValue) != 0) {
						$scope.errors[iid+'.sliderScaleValue'] = "Inserire un valore divisibile con il punteggio massimo."
						return $scope.errors;
					}
				}
			}
		}
		if(newQuestion.type=='SINGLE_CHOICE' || newQuestion.type=='MULTIPLE_CHOICE') {
			for(var c=0; c<newQuestion.replies.length; c++ ) {
				
				form.formProperties.push({ id:iid+'.replyText_'+c,value : newQuestion.replies[c].text, type: 'string',required : true });
				
				//form.formProperties.push({ id:iid+'.replyValue_'+c,value : newQuestion.replies[c].value, type: 'string',required : true });
			}
		}	
		if(newQuestion.type=='PAGE_JUMPS') {
			newQuestion.text = 'Salto pagina';
			for (var i=0; i< form.formProperties.length; i++) {
				if (form.formProperties[i].id == iid+'.text') {
					form.formProperties[i].value = newQuestion.text;
					break;
				}
			}
			form.formProperties.push({ id: iid+'.text',value : newQuestion.text,type: 'string',required : true  });
		}
		
		console.log('form:');
		console.log(form);
		console.log('errors:');
		console.log($scope.errors);
		return FormUtilService.validateForm(form);
	}
	
	
	
	$scope.storeQuestion = function() {
	
		$scope.errors = angular.copy($scope.validateQuestion('new'));
		
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {

			$scope.detail.questions.push($scope.newQuestion);
			$scope.questionsEditingMode.push(false);
			$scope.newQuestion = {};
			$scope.showElements = {
				newQuestionConfig : false,
				newRepliesConfig : false,
				scoreConfig: false,
			};
			
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.deleteQuestionDependencies = function(deletedIndex) {
		console.log('deletedindex: '+deletedIndex);
	
		for(var k=0; k<$scope.detail.questions.length; k++) {
			var enQ = $scope.detail.questions[k].enableQuestions;
			console.log('enQ['+k+']:');
			console.log(enQ);
			
			var reqQ = $scope.detail.questions[k].requiredQuestions;
			console.log('reqQ['+k+']:');
			console.log(reqQ);
			
			if(enQ!=undefined && enQ!=null && enQ.length>0) {
				var i = enQ.indexOf(deletedIndex+1);
				if(i>=0) {
					enQ.splice(i,1);	
				}
				//Sposta in su tutti i riferimenti a domande di posizione maggiore di quella cancellata
				console.log('Ho eliminato la domanda '+(1+deletedIndex))
				for(var ii=0; ii<enQ.length; ii++) {
					
					console.log('enQ['+ii+']:'+enQ[ii]);
					if(enQ[ii]>parseInt(1+deletedIndex)) {
						enQ[ii] = enQ[ii]-1;
					}
					console.log('aggiornato enQ['+ii+']:'+enQ[ii]);
				}
			}
			if(reqQ!=undefined && reqQ!=null && reqQ.length>0) {
				var i = reqQ.indexOf(deletedIndex+1);
				if(i>=0) {
					reqQ.splice(i,1);
					
				}
				//Sposta in su tutti i riferimenti a domande di posizione maggiore di quella cancellata
				console.log('Ho eliminato la domanda '+(1+deletedIndex))
				for(var ii=0; ii<reqQ.length; ii++) {
					
					console.log('reqQ['+ii+']:'+reqQ[ii]);
					if(reqQ[ii]>parseInt(1+deletedIndex)) {
						reqQ[ii] = reqQ[ii]-1;
					}
					console.log('aggiornato reqQ['+ii+']:'+reqQ[ii]);
				}
			}
			console.log('enQ dopo eliminazione:');
			console.log(enQ);
			console.log('reqQ dopo eliminazione:');
			console.log(reqQ);
			$scope.detail.questions[k].enableQuestions = enQ;
			$scope.detail.questions[k].requiredQuestions = reqQ;
			
			var replies = $scope.detail.questions[k].replies;
			if(replies!=undefined && replies!=null) {
				for(var kk=0; kk<replies.length; kk++) {
					var renQ = replies[kk].enableQuestions;
					console.log('renQ['+kk+']:');
					console.log(renQ);
					
					var rreqQ = replies[kk].requiredQuestions;
					console.log('rreqQ['+kk+']:');
					console.log(rreqQ);
					
					if(renQ!=undefined && renQ!=null && renQ.length>0) {
						var i = renQ.indexOf(deletedIndex+1);
						if(i>=0) {
							renQ.splice(i,1);
						}
						//Sposta in su tutti i riferimenti a domande di posizione maggiore di quella cancellata
						console.log('Ho eliminato la domanda '+(1+deletedIndex))
						for(var ii=0; ii<renQ.length; ii++) {
							
							console.log('renQ['+ii+']:'+renQ[ii]);
							if(renQ[ii]>parseInt(1+deletedIndex)) {
								renQ[ii] = renQ[ii]-1;
							}
							console.log('aggiornato renQ['+ii+']:'+renQ[ii]);
						}
					}
					if(rreqQ!=undefined && rreqQ!=null && rreqQ.length>0) {
						var i = rreqQ.indexOf(deletedIndex+1);
						if(i>=0) {
							rreqQ.splice(i,1);
						}
						//Sposta in su tutti i riferimenti a domande di posizione maggiore di quella cancellata
						console.log('Ho eliminato la domanda '+(1+deletedIndex))
						for(var ii=0; ii<rreqQ.length; ii++) {
							
							console.log('rreqQ['+ii+']:'+rreqQ[ii]);
							if(rreqQ[ii]>parseInt(1+deletedIndex)) {
								rreqQ[ii] = rreqQ[ii]-1;
							}
							console.log('aggiornato rreqQ['+ii+']:'+rreqQ[ii]);
						}
					}
					console.log('renQ dopo eliminazione:');
					console.log(renQ);
					console.log('rreqQ dopo eliminazione:');
					console.log(rreqQ);
					replies[kk].enableQuestions = renQ;
					replies[kk].requiredQuestions = rreqQ;
				}
				$scope.detail.questions[k].replies = replies;	
			}
		}
	}
	
	$scope.updateQuestionDependencies = function(index,dir) {
		console.log('index: '+index+' dir:'+dir);
		var enQ = $scope.detail.questions[index].enableQuestions;
		if(enQ==undefined || enQ==null) enQ=[];
		console.log('enQ:');
		console.log(enQ);
		for(var p=0; p<enQ.length; p++) {
			console.log('enQ['+p+']:'+enQ[p]);
			if(enQ[p]==index+1) {
				console.log('scambio!');
				enQ[p] = (dir=='up') ? enQ[p]+1 : enQ[p]-1;
			}
			console.log('enQ['+p+']:'+enQ[p]);
		}
		var reqQ = $scope.detail.questions[index].requiredQuestions;
		if(reqQ==undefined || reqQ==null) reqQ = [];
		for(var p=0; p<reqQ.length; p++) {
			
			if(reqQ[p]==index+1) {
				console.log('scambio!');
				reqQ[p] = (dir=='up') ? reqQ[p]+1 : reqQ[p]-1;
			}
		}
		console.log('enQ dopo scambi:');
		console.log(enQ);
		
		
		var replies = $scope.detail.questions[index].replies;
		console.log('replies:');
		console.log(replies);
		if(replies!=undefined && replies!=null) {
			for(var r=0; r<replies.length; r++) {
				console.log('----------- reply '+r+'--------------');
				var r_enQ = replies[r].enableQuestions;
				if(r_enQ!=undefined && r_enQ!=null) {
					for(var p=0; p<r_enQ.length; p++) {
						console.log('r_enQ['+p+']:'+r_enQ[p]);
						if(r_enQ[p]==index+1) {
							console.log('scambio!');
							r_enQ[p] = (dir=='up') ? r_enQ[p]+1 : r_enQ[p]-1;
						}
						console.log('r_enQ['+p+']:'+r_enQ[p]);
					}
					replies[r].enabledQuestions = r_enQ;
				}
				var r_reqQ = replies[r].requiredQuestions;
				if(r_reqQ!=undefined && r_reqQ!=null) {
					for(var p=0; p<r_reqQ.length; p++) {
						console.log('r_reqQ['+p+']:'+r_reqQ[p]);
						if(r_reqQ[p]==index+1) {
							console.log('scambio!');
							r_reqQ[p] = (dir=='up') ? r_reqQ[p]+1 : r_reqQ[p]-1;
						}
						console.log('r_reqQ['+p+']:'+r_reqQ[p]);
					}
					replies[r].requiredQuestions = r_reqQ;
				}
			}
		}
		$scope.detail.questions[index].replies = replies;
		$scope.detail.questions[index].enableQuestions = enQ;
		$scope.detail.questions[index].requiredQuestions = reqQ;
		
		/*
		for(var k=0; k<$scope.questionnaire.questions.length; k++) {
			delete $scope.questionnaire.questions[k].enableQuestions;
			delete $scope.questionnaire.questions[k].requiredQuestions;
			if($scope.questionnaire.questions[k].replies!=undefined && $scope.questionnaire.questions[k].replies!=null) {
				for(var r=0; r<$scope.questionnaire.questions[k].replies.length; r++) {
					delete $scope.questionnaire.questions[k].replies[r].enableQuestions;
					delete $scope.questionnaire.questions[k].replies[r].requiredQuestions;
				}
			}
		}
		*/
	}
	
	$scope.removeQuestion = function(index) {
		$scope.removeQuestionErrors(index);
		$scope.questionnaire.questions.splice(index,1);
		$scope.questionsEditingMode.splice(index,1);
		$scope.deleteQuestionDependencies(index);
	}
	
	$scope.moveQuestion = function(dir,index) {
		

		var copy = angular.copy($scope.detail.questions[index]);
		if('up'==dir) {
			
			$scope.detail.questions[index] = angular.copy($scope.detail.questions[index-1]);
			$scope.detail.questions[index-1] = angular.copy(copy);
			$scope.updateQuestionDependencies(index-1,dir);
			$scope.updateQuestionDependencies(index,'down');
		
		}
		else {
			
			$scope.detail.questions[index] = angular.copy($scope.detail.questions[index+1]);
			$scope.detail.questions[index+1] = angular.copy(copy);
			$scope.updateQuestionDependencies(index+1,dir);
			$scope.updateQuestionDependencies(index,'up');
			
		}
		
		
	}
	
	$scope.modifyQuestion = function(index) {
		$scope.detail.disableModifyQuestion = true;
		$scope.questionsEditingMode[index] = true;
		$scope.previousQuestionVersions[index] = angular.copy($scope.detail.questions[index]);
	}
	
	$scope.removeQuestionErrors = function(index) {
		//Rimuove gli errori di questa domanda
		var keys = Object.keys($scope.errors);
		for(var k=0; k<keys.length; k++) {
			if( keys[k].indexOf('q_'+index)>=0 ) {
				delete $scope.errors[keys[k]];
			}
		}
	}
	
	$scope.restoreQuestion = function(index) {
		$scope.removeQuestionErrors(index);
		$scope.questionsEditingMode[index] = false;
		$scope.detail.questions[index] = angular.copy($scope.previousQuestionVersions[index]);
		$scope.detail.disableModifyQuestion = false;
	}
	
	$scope.updateQuestion = function(index) {
		$scope.errors = angular.copy($scope.validateQuestion(index));
		
		console.log('Add question:'+index);
		console.log($scope.detail.questions[index]);
		
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			$scope.questionsEditingMode[index] = false;
			if($scope.detail.questions[index].type=='SCORE_SLIDER') {
				$scope.redrawSliders++;
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
		$scope.detail.disableModifyQuestion = false;
	}
	
	//funzione per verificare se una domanda è attivata o obbligatoria da qualcun altro
	/*
	$scope.isActivatedBySomebody = function(index) {
		var isActivated = false;
		for(var k=0; k<$scope.questionnaire.questions.length; k++) {
			var enaQ = $scope.questionnaire.questions[k].enableQuestions;
			var reqQ = $scope.questionnaire.questions[k].requiredQuestions;
			isActivated = (enaQ.indexOf(index+1)>=0) || (reqQ.indexOf(index+1)>=0);
			if(!isActivated && $scope.questionnaire.questions[k].replies!=undefined && $scope.questionnaire.questions[k].replies !=null) {
				for(var kk=0; kk<$scope.questionnaire.questions[k].replies.length; kk++) {
					if(!isActivated) {
						var renaQ = $scope.questionnaire.questions[k].replies[kk].enableQuestions;
						var rreqQ = $scope.questionnaire.questions[k].replies[kk].requiredQuestions;
						isActivated = (renaQ.indexOf(index+1)>=0) || (rreqQ.indexOf(index+1)>=0);
					}
				}
			}
		}
		return isActivated;
	}
	*/
	
	//In seguito all'aggiornamento della domanda index, toglie obbligatorietà alle altre da essa attivate
	/*
	$scope.updateQuestionsMandatory = function(index) {
		//Verifica se la domanda attiva altre domande che erano obbligatorie. In tal caso toglie obbligatorietà
		var enaQ = $scope.questionnaire.questions[index].enableQuestions;
		var reqQ = $scope.questionnaire.questions[index].requiredQuestions;
		for(var i=0; i<enaQ.length; i++) {
			$scope.questionnaire.questions[enaQ[i]-1].mandatory = false;
		}
		for(var i=0; i<reqQ.length; i++) {
			$scope.questionnaire.questions[reqQ[i]-1].mandatory = false;
		}
		if($scope.questionnaire.questions[index].replies!=undefined && $scope.questionnaire.questions[index].replies !=null) {
			for(var r=0; r<$scope.questionnaire.questions[index].replies.length; r++) {
				var enaQ = $scope.questionnaire.questions[index].replies[r].enableQuestions;
				var reqQ = $scope.questionnaire.questions[index].replies[r].requiredQuestions;
				for(var i=0; i<enaQ.length; i++) {
					$scope.questionnaire.questions[enaQ[i]-1].mandatory = false;
				}
				for(var i=0; i<reqQ.length; i++) {
					$scope.questionnaire.questions[reqQ[i]-1].mandatory = false;
				}
			}
		}
	}
	*/
	
	$scope.reset = function() {
		$scope.initDetail();
	}

	$scope.initFormulaTags = function() {	
		$scope.formulaTags = [];
	}
	
	
	
	/*************************************/
	
});
