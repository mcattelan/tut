'use strict'

app.controller('ServiceController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state,
		ProgramService,
		ServiceConfService,
		CustomerService) {
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		
		ProgramService.get(function(result) {
			$scope.program = {};
			for(var i=0; i<result.data.length; i++) {
				if(result.data[i].id == 'NAME') $scope.program['name'] = result.data[i].value;
				if(result.data[i].id == 'DESCRIPTION') $scope.program['description'] = result.data[i].value;
				if(result.data[i].id == 'INTERNAL_NOTES') $scope.program['internalNotes'] = result.data[i].value;
				if(result.data[i].id == 'CUSTOMER_ID') {
					CustomerService.get({ id: result.data[i].value }, function(res) {
						$scope.program['customer'] = res.data;
					});
				}
			}
		}, function(error) {
        	$scope.detail = {};
        	if(error.status == 404) {
        		$state.go('access.not-found');
        	}
    	});
		
		if ($scope.id == null || $scope.id == undefined) {
			$scope.id = $stateParams.id;
		}
		
		$scope.errors = {};
		$scope.initDetails();
	}
	
	$scope.initDetails = function() {
		$scope.detail = {};

		ServiceConfService.getService({id: $scope.id}, function(result) {
			$scope.detail = result.data;
		}, function(error) {
        	$scope.detail = {};
        	if(error.status == 404) {
        		$state.go('access.not-found');
        	}
    	});
	}
	
	$scope.showServiceActivityDetail = function(serviceId, id) {
		$state.go('app.service-activity', {serviceId: serviceId, id: id});
	}
		
	$scope.init();
	}
);
