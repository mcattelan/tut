'use strict'

app.controller('ServiceActivityController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state,
		ProgramService,
		ServiceConfService,
		MasterActivityConfService,
		CustomerService) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		
		if ($scope.serviceId == null || $scope.serviceId == undefined) {
			$scope.serviceId = $stateParams.serviceId;
		}
		if ($scope.id == null || $scope.id == undefined) {
			$scope.id = $stateParams.id;
		}

		ProgramService.get(function(result) {
			$scope.program = {};
			for(var i=0; i<result.data.length; i++) {
				if(result.data[i].id == 'NAME') $scope.program['name'] = result.data[i].value;
				if(result.data[i].id == 'DESCRIPTION') $scope.program['description'] = result.data[i].value;
				if(result.data[i].id == 'INTERNAL_NOTES') $scope.program['internalNotes'] = result.data[i].value;
				if(result.data[i].id == 'CUSTOMER_ID') {
					CustomerService.get({ id: result.data[i].value }, function(res) {
						program.customer = res.data;
					});
				}
			}
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});

		ServiceConfService.getService({id: $scope.serviceId}, function(result) {
			$scope.service = result.data;
		}, function(error) {
			$scope.service = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});

		$scope.initDetails();
	}

	$scope.initDetails = function() {
		$scope.canModify = false;
		$scope.isEditable = $scope.canModify;
		$scope.detail = {};
		
		MasterActivityConfService.get({id: $scope.id}, function(result) {
			$scope.detail = result.data;
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.init();
});
