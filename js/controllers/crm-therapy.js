'use strict'

app.controller('TherapyController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		TherapyService,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la therapiesa che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};

		$scope.selects = {
				'Therapy' : $rootScope.therapies
		};
		
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		if(angular.isDefined($scope.id) && $scope.id !== '') {
			$scope.initDetail();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'THERAPY' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetail = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.detail = {};
		$scope.fieldPermissions = {};

		if (!$scope.isNew()) {
			TherapyService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					drugs: []
			};

			PermissionService.getAll({ entityType : 'THERAPY' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'THERAPY' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.search = function(tableState) {
		$scope.therapies = [];
		
		$scope.pagination.start = tableState !== undefined ? tableState.pagination.start : 0;
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		delete $scope.searchFilters.size;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		TherapyService.search({}, $scope.searchFilters, function(result) {
			$scope.therapies = result.data;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.therapies.length; i++) {
				$scope.therapies[i].selected = $scope.selectedIds.indexOf($scope.therapies[i].id) >= 0;
			}
		}, function(error) {
			$scope.therapies = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.therapies.length; i++) {
			$scope.therapies[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.therapies.length; i++) {
			$scope.therapies[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		if($scope.selection) {
			$scope.selectedIds = []
			for(var i=0; i<$scope.selected.length; i++) {
				$scope.selectedIds.push($scope.selected[i].id);
			}
			for(var i=0; i<$scope.therapies.length; i++) {
				if($scope.therapies[i].selected) {
					if($scope.selectedIds.indexOf($scope.therapies[i].id) < 0) {
						$scope.selected.push($scope.therapies[i]);
						$scope.selectedIds.push($scope.therapies[i].id);
					}
					else {
						// Già presente nella lista, aggiorno ruolo e qualifiche
						$scope.selected[$scope.selectedIds.indexOf($scope.therapies[i].id)] = $scope.therapies[i];
					}
				}
				else {
					if($scope.selectedIds.indexOf($scope.therapies[i].id) >= 0) {
						$scope.selected.splice($scope.selectedIds.indexOf($scope.therapies[i].id), 1);
						$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.therapies[i].id), 1);
					}
				}
			}
			//debugger;
			if($scope.modal) {
				$scope.modal.close();
			}
		}
	}
	
	$scope.undoSelection = function() {
		$scope.initList();
		$scope.modal.dismiss();
	}

	$scope.init();
});
