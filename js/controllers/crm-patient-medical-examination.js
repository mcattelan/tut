'use strict'

app.controller('PatientMedicalExaminationController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		SMART_TABLE_CONFIG,
		PatientMedicalExaminationService) {	

	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if($scope.id === undefined || $scope.id == null) {
			$scope.id = $stateParams.id;
		}
		if(angular.isDefined($scope.id) && $scope.id !== '') {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = {
		}
	}

	$scope.initList = function() {
		$scope.initPaginationAndFilter();
		$scope.search();
	};

	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};

		PatientMedicalExaminationService.get({ id : $scope.id, patientId : $scope.patientId }, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.search = function(tableState) {
//		$scope.list = [];
//		$scope.searchFilters = {}

//		$scope.pagination.start = tableState !== undefined ? tableState.pagination.start : 0;
//		angular.extend($scope.searchFilters, $scope.pagination);
//		delete $scope.searchFilters.total;

//		CustomerService.search({}, $scope.searchFilters, function(result) {
//		$scope.list = result.data;
//		$scope.pagination.start = result.start;
//		$scope.pagination.size = result.size;
//		$scope.pagination.sort = result.sort;
//		$scope.pagination.total = result.total;

//		}, function(error) {
//		$scope.list = [];
//		$scope.initPaginationAndFilter();
//		if(error.status == 404) {
//		$state.go('access.not-found');
//		}
//		});
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.save = function() {			
//		CustomerService.update({ id : $scope.id }, function(result) {
//		$scope.detail = result.data;

//		}, function(error) {
//		if(error.status == 404) {
//		$state.go('access.not-found');
//		}
//		else {
//		$scope.initDetails();
//		}
//		});
	};
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.init();
});