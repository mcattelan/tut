'use strict';

app.controller('MainController', function(
		$rootScope,
		$q,
		$scope,
		$http,
		$state,
		$location,
		$timeout,
		$window,
		UserService,
		PortalService,
		QualificationService,
		MessageService,
		deviceDetector,
		WorkflowService,
		TerritoryService,
		UserServiceConfService,
		UserActivityService,
		ProgramService,
		CustomerService) {
	
	$scope.init = function() {

		$rootScope.deviceDetector = deviceDetector;
		$rootScope.redisStatus = true;
		$rootScope.redisStatusPermissions = {};
		$rootScope.resettingStatus = false;
		$rootScope.getRedisStatus();
		$rootScope.programProperties = {};
		$rootScope.getProgramProperties();
		
		// rescheduling
// $window.setInterval(function(){
// console.log("*** CALL REDIS STATUS SERVICE ***");
// $rootScope.getRedisStatus();
// }, 60000);

		var w = angular.element($window);
		$scope.getWindowDimensions = function() {
			return {
				'h' : w.height(),
				'w' : w.width()
			};
		};
		$scope.$watch($scope.getWindowDimensions, function(newValue, oldValue) {
			$scope.notificationsStyle = {'overflow': 'auto','max-height': (newValue.h - 220) +'px'};
		}, true);

		// IE9 check
		$rootScope.isIE9 = window.isIE9;

		$scope.loggedUser = $rootScope.loggedUser;

		$scope.qualifications = [];
		
		$rootScope.relatedPortals = [];
		if($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			
			//Cerca i portali collegati a questo (solo per i programs, l'admin non lo fa)
			PortalService.getRelatedPortals(function(result) {
				$rootScope.relatedPortals = result.data;
			}, function(error) {
				console.log('Cannot retrieve related portals!');
			});
			
			var found = false;
			for(var i=0; i<$rootScope.userPortals.length; i++) {
				if($location.absUrl().indexOf($rootScope.userPortals[i].url) >= 0) {
					found = true;
					break;
				}
			}
			if((!found)) {
				//$state.go('portals');
			}
			
			QualificationService.get(function(result) {
				$scope.qualifications = result.data;
				$rootScope.qualifications = $scope.qualifications;
			});
			
			TerritoryService.searchCountries({ }, function(result) {
				$rootScope.countries = result.data;
            });
			
			/*
			 * TerritoryService.searchRegions({ }, function(result) {
			 * 
			 * }); TerritoryService.searchProvinces({ }, function(result) {
			 * 
			 * }); TerritoryService.searchTowns({ }, function(result) {
			 * 
			 * });
			 */
			
			if($rootScope.isPatient() || $rootScope.isHcp()) {
				$scope.manualActivitiesMenu = [];
				UserServiceConfService.getUserManualMasterActivities({ userId : $scope.loggedUser.id }, undefined, function(result) {
					$scope.manualActivitiesMenu = result.data;
				}, function(error) {
					$scope.manualActivitiesMenu = [];
				});
			}
			
			$rootScope.countTasks();
		} else {
			$state.go('access.logout');
		}
	}
	
	$rootScope.countTasks = function() {
		if($scope.loggedUser) {
			if($rootScope.isInternalStaff()) {
				WorkflowService.countSearchMyTasks({}, {}, function(result) {
					$rootScope.todoTasks = result.data;
				}, function(error) {
					$rootScope.todoTasks = 0;
				});
				WorkflowService.countSearchNotAssignedTasks({}, {}, function(result) {
					$rootScope.queuedTasks = result.data;
				}, function(error) {
					$rootScope.queuedTasks = 0;
				});
				WorkflowService.countSearchAssignedToOthersTasks({}, {}, function(result) {
					$rootScope.assignedToOthersTasks = result.data;
				}, function(error) {
					$rootScope.assignedToOthersTasks = 0;
				});
			}
			else if($rootScope.isSupplier()) {
				$rootScope.queuedTasks = 0;
				$rootScope.assignedToOthersTasks = 0;
				WorkflowService.countSearchMyTasks({}, {}, function(result) {
					$rootScope.todoTasks = result.data;
				}, function(error) {
					$rootScope.todoTasks = 0;
				});
			}
			else if($rootScope.isLogistic()) {
				$rootScope.queuedTasks = 0;
				$rootScope.assignedToOthersTasks = 0;
				WorkflowService.countSearchMyTasks({}, {}, function(result) {
					$rootScope.todoTasks = result.data;
				}, function(error) {
					$rootScope.todoTasks = 0;
				});
			}
		}
	}
	
	$scope.startMasterActivityMenu = function(masterActivity, notifyIA) {
		var dialogTitle='Avvio attivit\u00E0';
		var dialogText="Si desidera avviare l'attivit\u00E0 '" + masterActivity.name + "'?"
		
		if(masterActivity !== undefined && masterActivity.name == 'Chiamami') {
			dialogText = 'Confermi di richiedere un contatto da ItaliAssistenza?';
		}
		if(masterActivity !== undefined && masterActivity.name == 'Richiedi intervento a domicilio') {
			dialogText = 'Confermi di richiedere un intervento domiciliare ad ItaliAssistenza?';
		}

		if($rootScope.userPortals != undefined && $rootScope.userPortals.length == 1 && ($rootScope.userPortals[0].id == 'regalatiunrespiro' || $rootScope.userPortals[0].id == 'adessovola')) {
			dialogTitle = "Richiesta di Contatto";
			dialogText = "Confermi di richiedere un contatto da Italiassistenza? Verrai contattato telefonicamente il prima possibile, entro e non oltre il giorno lavorativo successivo alla richiesta.";
		} else if($rootScope.programProperties && ($rootScope.programProperties.portalId == 'tutor' || $rootScope.programProperties.portalId == 'quiperte')){
			dialogTitle = "Richiesta di Contatto";
			dialogText = "Confermi la richiesta di contatto? Verrai contattato entro e non oltre 1 giorno lavorativo.";
		} else if($rootScope.programProperties && ($rootScope.programProperties.portalId == 'bi4care')){
			dialogTitle = "Richiesta di Contatto";
			dialogText = "Confermi di voler richiedere un contatto da ItaliAssistenza?";
		}
		
		/* MOD MATTEO aggiunto un parametro alla funzione simpleConfirm*/
		var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info','sm');
		//var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
		
		modalInstance.result.then(
			function(confirm) {
				var notify = false;
// if(angular.isDefined(notifyIA) && notifyIA == true){
// notify = true;
// }
				UserServiceConfService.startUserMasterActivityManually({ userId : $scope.loggedUser.id, masterActivityId : masterActivity.id},
					function(result) {
						MessageService.showSuccess('Avvio attivit\u00E0 completato con successo');
						
					}, function(error) {
						if(error && error.data && error.data.message) {
							MessageService.showError('Avvio attivit\u00E0 non riuscito', error.data.message);
						}
						else {
							MessageService.showError('Avvio attivit\u00E0 non riuscito');
						}
						if (error.status == 404) {
							$state.go('access.not-found');
						}
					}
				);
		});
	}

	$scope.unimpersonate = function(impUserId) {
		UserService.unimpersonate({}, function(response) {
			$state.go('access.login');
		}, function(error) {
			MessageService.showError('Impossibile procedere con l\'operazione', 'Errore imprevisto');
		});
	};
	
	$scope.impersonateUser = function(nickName) {
		UserService.impersonate({ 'nickName' : nickName }, function(response) {
			$state.go('access.login');
		}, function(error) {
			MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
		});
	}
	
	$scope.inviteUser = function(id) {
		UserService.activation({ id : id }, {}, function(result) {
			
			MessageService.showSuccess('Operazione avvenuta con successo', '');
		}, function(error) {
			if(error.status == 404) {
				MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
			}
		});
	}

	$rootScope.isCustomer = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[2].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	$rootScope.isHcp = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[3].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	$rootScope.isPatient = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[4].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	$rootScope.isSupplier = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[1].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	$rootScope.isInternalStaff = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[0].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	$rootScope.isLogistic = function(){
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null) {
			var userTypes = $rootScope.constants.UserType
			if(userTypes[6].id == $scope.loggedUser.type){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	$rootScope.extend = function(obj, src) {
		for (var key in src) {
			if (src.hasOwnProperty(key)) obj[key] = src[key];
		}
		return obj;
	};
	
	$rootScope.getRedisStatus = function(){
		PortalService.getRedisStatus({}, function(response) {
			$rootScope.redisStatus = response.data.status;
			$rootScope.resettingStatus = false;
			$rootScope.redisStatusPermissions = response.fieldPermissions;

		}, function(error) {
			$rootScope.rediStatus = false;
		});
	};
	
	$rootScope.setRunningRedisStatus = function(){
		PortalService.setRunningRedisStatus({}, function(response) {
			$rootScope.redisStatus = response.data.status;
			$rootScope.resettingStatus = true;
		}, function(error) {
			$rootScope.rediStatus = false;
		});
	};
	
	$rootScope.getProgramProperties = function() {
		ProgramService.get(function(result) {
			for(var i=0; i<result.data.length; i++) {
				if(result.data[i].id == 'NAME') 
					$rootScope.programProperties['name'] = result.data[i].value;
				if(result.data[i].id == 'DESCRIPTION') 
					$rootScope.programProperties['description'] = result.data[i].value;
				if(result.data[i].id == 'INTERNAL_NOTES')
					$rootScope.programProperties['internalNotes'] = result.data[i].value;
				if(result.data[i].id == 'CUSTOMER_ID') {
					CustomerService.get({ id: result.data[i].value }, function(res) {
						$rootScope.programProperties.customer = res.data;
					});
				}
				if(result.data[i].id == 'PORTAL_ID') {
					$rootScope.programProperties['portalId'] = result.data[i].value;
				}
			}
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
	};
	
	
	$rootScope.isPersonified = function() {
		if ($scope.loggedUser !== undefined && $scope.loggedUser != null && $scope.loggedUser.originalUserId != null && $scope.loggedUser.originalUserId !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	$scope.init();
});
/*
 * aggiunta una funzione da usare per modficare i searchFilters prima della ricerca
 * preSearch()
 */
app.controller('BaseListCtrl', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		$sce,
		MessageService,
		PermissionService,
		RESTURL,
		REGEXP,
		SMART_TABLE_CONFIG,
		UserService,
		PortalService,
		$window) {
	
	$scope.getRelatedPortals = function() {
		//Gestione portali collegati (es. Tutor-QuiperTe)
		$scope.relatedPortals = $rootScope.relatedPortals;
		if($scope.relatedPortals==undefined || $scope.relatedPortals.length==0) {
			//Cerca i portali collegati a questo (solo per i programs, l'admin non lo fa)
			PortalService.getRelatedPortals(function(result) {
				$scope.relatedPortals = result.data;
				$scope.relatedAccessiblePortals = $scope.getRelatedAccessiblePortals();
			}, function(error) {
				console.log('Cannot retrieve related portals!');
			});
		}
		else {
			$scope.relatedAccessiblePortals = $scope.getRelatedAccessiblePortals();
		}
	}	
	
	$scope.getUserPortals = function() {
		$scope.userPortals = $rootScope.userPortals;
		if($scope.userPortals==undefined) {
			UserService.getLoggedUserPortals({}, function(result) {
				$scope.userPortals = result.data;
				$scope.getRelatedPortals();
			}, function(error) {
				console.log('Cannot retrieve logged user portals!');
			});
		}
		else {
			$scope.getRelatedPortals();
		}
	}
	
	$scope.getRelatedAccessiblePortals = function() {
		var portals = [];
		if($scope.relatedPortals!=undefined && $scope.relatedPortals.length>0) {
			if($scope.userPortals!=undefined && $scope.userPortals.length>0) {
				for(var i=0; i<$scope.relatedPortals.length; i++) {
					for(var j=0; j<$scope.userPortals.length; j++) {
						if($scope.userPortals[j].id==$scope.relatedPortals[i]) {
							portals.push($scope.relatedPortals[i]);
						}
					}
				}
			}
		}
		return portals;
	}
	
	$scope.init = function(entityType, service) {
		if(entityType !== undefined && service !== undefined) {
			$scope.entityType = entityType;
			$scope.service = service;
			$scope.showList = false;
			
			$scope.loggedUser = $rootScope.loggedUser;
			$scope.getUserPortals();
			
			$scope.errors = {};
			$scope.selects = {};
		
			PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
				$scope.initPermission(result.data);
				$scope.selection = $scope.selection || false;
				$scope.selected = $scope.selected || $scope.selectedData || [];
				$scope.selectedData = $scope.selected;
				$scope.initPaginationAndFilter();
				
				$scope.showList = true;
				
			}, function(error) {
				$scope.list = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			MessageService.showError('Errore imprevisto', '');
			$state.go('access.not-found');
		}

	};
	
	$scope.search = function(tableState, smCtrl) {
		if(!$scope.showList) return;
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		if($scope.preSearch !== undefined){
			$scope.preSearch();
		}
		
		$scope.service.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for (var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
			
			if($scope.filterStoreKey) {
				try {
					$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
					$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
				}
				catch(e) {
					console.log(e);
				}
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post($scope.service.getUrl() + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.service.delete({ id : id }, {}, function(result) {
						$scope.search();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = $scope.pagination || {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		if($scope.searchFixedFilters) {
			angular.extend($scope.searchFilters.filters, $scope.searchFixedFilters);
		}
		
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
		
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		
		if($scope.filterStoreKey) {
			try {
				var savedFilters = $window.sessionStorage[$scope.filterStoreKey];
				if(savedFilters) {
					savedFilters = angular.fromJson(savedFilters);
				}
				if(savedFilters) {
					angular.extend($scope.searchFilters, savedFilters);
					$scope.pagination.start =  savedFilters.start;
					$scope.pagination.size =  savedFilters.size;
					$scope.pagination.sort =  savedFilters.sort;
				}
				
				var objectFilters = $window.sessionStorage[$scope.filterStoreKey + '-object'];
				if(objectFilters) {
					objectFilters = angular.fromJson(objectFilters);
				}
				if(objectFilters) {
					angular.extend($scope.objectFilters, objectFilters);
				}
			}
			catch(e) {
				console.log(e);
			}
		}
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canExport = permissions.EXPORT;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
		
		if($scope.filterStoreKey) {
			try {
				$window.sessionStorage.removeItem($scope.filterStoreKey);
				$window.sessionStorage.removeItem($scope.filterStoreKey + '-object');
			}
			catch(e) {
				console.log(e);
			}
		}
	}
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.postConfirmSelectionFn !== undefined) {
			$scope.postConfirmSelectionFn();
		}
		if($scope.modal !== undefined) {
			$scope.modal.close();
		}
	}
	
	$scope.onSelect = function(selectedData) { // per retro compatibilità con vecchi html
		if(!$scope.isMultipleSelection) {
			$scope.unselectAll();	
			for(var i = 0; i < $scope.list.length; i++) {
				if($scope.list[i].id == selectedData.id) {
					$scope.list[i].selected = true;
					break;
				} 
			}
			$scope.selectedData = [selectedData];
		}
	}
	
	$scope.undo = function() {
		if($scope.postCancelSelectionFn !== undefined) {
			$scope.postCancelSelectionFn();
		}
		if($scope.modal !== undefined) {
			$scope.modal.close();
		}
	}
	
	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}
	
	if($scope.preInit !== undefined) {
		$scope.selects = {};
		$scope.preInit();
	}
	
	
	
	
	
});

/*
 * Il controller prevede la possibilità di definire le seguenti proprietà o
 * funzioni: - validate, funzione di validazione dati per richiamata prima di
 * ogni insert o update - prepareInsertRequest, funzione di preparazione dati
 * per l'insert - prepareUpdateRequest, funzione di preparazione dati per
 * l'update
 */
app.controller('BaseDetailCtrl', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		$sce,
		MessageService,
		PortalService,
		PermissionService,
// sharingService,
		RESTURL,
		REGEXP) {
	
	$scope.changesInProgress = false;
	$scope.changesInProgressListener = false;
	$scope.$watch('detail', function(newValue, oldValue) {
		if($scope.changesInProgressListener) {
			$scope.changesInProgress = true;
		}
	}, true);
	
	// Inizializza sia la lista che la scheda
	$scope.init = function(entityType, service) {
		if(entityType !== undefined && service !== undefined) {
			$scope.entityType = entityType;
			$scope.service = service;
		
			$scope.loggedUser = $rootScope.loggedUser;
	
			$scope.errors = {};
			$scope.warnings = {};
			$scope.selects = {};
			
			$scope.initDetails();
		}
		else {
			MessageService.showError('Errore imprevisto', '');
			$state.go('access.not-found');
		}
	};
	
	$scope.initDetails = function() {
		$scope.changesInProgress = false;
		$scope.changesInProgressListener = false;
		$scope.fieldPermissions = {};
		
		if ($scope.isNew()) {
			if($scope.newDefaultData !== undefined && $scope.newDefaultData.workflowToken !== undefined && $scope.service.getByWorkflowToken !== undefined) {
				// Provo a cercare per workflowToken
				$scope.service.getByWorkflowToken({ workflowToken : $scope.newDefaultData.workflowToken }, function(result) {
					$scope.id = result.data.id;
					$scope.detail = result.data;
					
					if($scope.detailObject && $scope.detailReturn) {
						$scope.detailReturn[0] = $scope.detail;
					}
					
					PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
						$scope.permissions = result.data;
						if($scope.permissions['INSERT']) {
							PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
								$scope.fieldPermissions = result.data;
								if($scope.postInit !== undefined) {
									var postIinitResult = $scope.postInit();
									if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
										postIinitResult.then(function (response) {
											$timeout(function() {
												$scope.changesInProgressListener = true;
											}, 0, false);
									    }, function (error) {
									    	$scope.detail = undefined;
											if(error.status == 404) {
												$state.go('access.not-found');
											}
									    });
									}
									else {
										$timeout(function() {
											$scope.changesInProgressListener = true;
										}, 0, false);
									}
								}
								else {
									$timeout(function() {
										$scope.changesInProgressListener = true;
									}, 0, false);	
								}
							}, function(error) {
								$scope.detail = undefined;
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});	
						}
						else {
//							$scope.detail = undefined;
//							MessageService.showError('Utente non abilitato', '');
						}
						
					}, function(error) {
						$scope.detail = undefined;
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});	
				}, function(error) {
					PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
						$scope.permissions = result.data;
						if($scope.permissions['INSERT']) {
							PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
								$scope.fieldPermissions = result.data;
								if($scope.newDefaultData) {
									$scope.detail = angular.copy($scope.newDefaultData);
								}
								else {
									$scope.detail = {};
								}
								
								if($scope.detailObject && $scope.detailReturn) {
									$scope.detailReturn[0] = $scope.detail;
								}
								
								if($scope.postInit !== undefined) {
									var postIinitResult = $scope.postInit();
									if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
										postIinitResult.then(function (response) {
											$timeout(function() {
												$scope.changesInProgressListener = true;
											}, 0, false);
									    }, function (error) {
									    	$scope.detail = undefined;
											if(error.status == 404) {
												$state.go('access.not-found');
											}
									    });
									}
									else {
										$timeout(function() {
											$scope.changesInProgressListener = true;
										}, 0, false);
									}
								}
								else {
									$timeout(function() {
										$scope.changesInProgressListener = true;
									}, 0, false);	
								}
							}, function(error) {
								$scope.detail = undefined;
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});	
						}
						else {
							$scope.detail = undefined;
							MessageService.showError('Utente non abilitato', '');
						}
						
					}, function(error) {
						$scope.detail = undefined;
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				});
			}
			else {
				PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
					$scope.permissions = result.data;
//					if($scope.permissions['INSERT']) {
						PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
							$scope.fieldPermissions = result.data;
							if($scope.newDefaultData) {
								$scope.detail = angular.copy($scope.newDefaultData);
							}
							else {
								$scope.detail = {};
							}
							
							if($scope.detailObject && $scope.detailReturn) {
								$scope.detailReturn[0] = $scope.detail;
							}
							
							if($scope.postInit !== undefined) {
								var postIinitResult = $scope.postInit();
								if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
									postIinitResult.then(function (response) {
										$timeout(function() {
											$scope.changesInProgressListener = true;
										}, 0, false);
								    }, function (error) {
								    	$scope.detail = undefined;
										if(error.status == 404) {
											$state.go('access.not-found');
										}
								    });
								}
								else {
									$timeout(function() {
										$scope.changesInProgressListener = true;
									}, 0, false);
								}
							}
							else {
								$timeout(function() {
									$scope.changesInProgressListener = true;
								}, 0, false);	
							}
						}, function(error) {
							$scope.detail = undefined;
							if(error.status == 404) {
								$state.go('access.not-found');
							}
						});	
//					}
//					else {
////						$scope.detail = undefined;
////						MessageService.showError('Utente non abilitato', '');
//					}
					
				}, function(error) {
					$scope.detail = undefined;
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});	
			}
		}
		else {
			$scope.service.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				
				if($scope.detailObject && $scope.detailReturn) {
					$scope.detailReturn[0] = $scope.detail;
				}
				
				$scope.permissions = result.permissions;
				$scope.fieldPermissions = result.fieldPermissions;
// sharingService.broadcast($scope.entityType,$scope.detail);
				if($scope.postInit !== undefined) {
					var postIinitResult = $scope.postInit();
					if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
						postIinitResult.then(function (response) {
							$timeout(function() {
								$scope.changesInProgressListener = true;
							}, 0, false);
					    }, function (error) {
					    	$scope.detail = undefined;
							if(error.status == 404) {
								$state.go('access.not-found');
							}
					    });
					}
					else {
						$timeout(function() {
							$scope.changesInProgressListener = true;
						}, 0, false);
					}
				}
				else {
					$timeout(function() {
						$scope.changesInProgressListener = true;
					}, 0, false);	
				}
			}, function(error) {
				$scope.detail = undefined;
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.save = function() {
		$scope.errors = $scope.validate !== undefined ? $scope.validate() : {};
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		$scope.warnings = {};
		var request = $scope.prepareInsertRequest !== undefined ? $scope.prepareInsertRequest() : $scope.prepareStandardRequest();
		$scope.service.insert({}, request, function(result) {
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
			if(!$scope.isModal) {
				$state.go($state.current, { id: result.data.id }, true);
			}
			else {
				$scope.detail = result.data;		
				$scope.id = $scope.detail.id;
				$scope.permissions = result.permissions;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.changesInProgress = false;
				$scope.changesInProgressListener = false;
				
				if($scope.postInit !== undefined) {
					var postIinitResult = $scope.postInit();
					
					if($scope.detailObject && $scope.detailReturn) {
						$scope.detailReturn[0] = $scope.detail;
					}
					
					if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
						postIinitResult.then(function (response) {
							$timeout(function() {
								$scope.changesInProgressListener = true;
							}, 0, false);
					    }, function (error) {
					    	$scope.detail = undefined;
							if(error.status == 404) {
								$state.go('access.not-found');
							}
					    });
					}
					else {
						$timeout(function() {
							$scope.changesInProgressListener = true;
						}, 0, false);
					}
				}
				else {
					$timeout(function() {
						$scope.changesInProgressListener = true;
					}, 0, false);	
				}
			}
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.update = function() {
		$scope.warnings = {};// reset
		var request = $scope.prepareUpdateRequest !== undefined ? $scope.prepareUpdateRequest() : $scope.prepareStandardRequest();
		$scope.service.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			$scope.warnings = result.responseWarnings;
			$scope.permissions = result.permissions;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.changesInProgress = false;
			$scope.changesInProgressListener = false;
			
			if($scope.preInit != undefined) {
				$scope.preInit();
			}
			
			if($scope.postInit !== undefined) {
				var postIinitResult = $scope.postInit();
				
				if($scope.detailObject && $scope.detailReturn) {
					$scope.detailReturn[0] = $scope.detail;
				}
				
				if(angular.isObject(postIinitResult) && postIinitResult.then instanceof Function) {
					postIinitResult.then(function (response) {
						$timeout(function() {
							$scope.changesInProgressListener = true;
						}, 0, false);
				    }, function (error) {
				    	$scope.detail = undefined;
						if(error.status == 404) {
							$state.go('access.not-found');
						}
				    });
				}
				else {
					$timeout(function() {
						$scope.changesInProgressListener = true;
					}, 0, false);
				}
			}
			else {
				$timeout(function() {
					$scope.changesInProgressListener = true;
				}, 0, false);	
			}
			
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				var title = 'Errore in fase di aggiornamento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.prepareStandardRequest = function(request) {
		var request = angular.copy(request !== undefined ? request : $scope.detail);
		for(var key in request) {
			if(request[key]) {
				if(angular.isArray(request[key])) {
					for(var i=0; i<request[key].length; i++) {
						if(angular.isObject(request[key][i])) {
							if(request[key][i].id !== undefined && request[key][i].id != null) {
								if(request[key + 'Id'] === undefined) {
									request[key + 'Id'] = [];
								}
								request[key + 'Id'].push(request[key][i].id);
							}
							else {
								request[key][i] = $scope.prepareStandardRequest(request[key][i]);
							}
						}
					}
				}
				else if(angular.isObject(request[key])) {
					if(request[key].id !== undefined && request[key].id != null) {
						request[key + 'Id'] = request[key].id;	
					}
					else {
						request[key] = $scope.prepareStandardRequest(request[key]);
					}
				}
			}
		}
		return request;
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.isNew = function() {
		return !$scope.id;
	}
	
	$scope.refresh = function() {
		$scope.initDetails();
	};
	
	if($scope.preInit !== undefined) {
		$scope.preInit();
	}
	
	$scope.isSaveButtonEnabled = function() {
		if($scope.permissions) {
			return $scope.isNew() ? $scope.permissions.INSERT : $scope.permissions.UPDATE;
		}else{
			return false
		}
			
	}
	
	$scope.closeModal = function() {
		$scope.modal.dismiss();
	}
});
/*
 * Base List controller per le chiamate CROSS-PSP
 * $scope.populateFilterListsFn() => callback da definire per caricare i dati delle liste dei filtri
 */
app.controller('BaseGatewayListCtrl', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		$sce,
		MessageService,
		PermissionService,
		RESTURL,
		REGEXP,
		SMART_TABLE_CONFIG,
		UserService,
		PortalService,
		$window,
		$location) {
	
	$scope.getRelatedPortals = function() {
		//Gestione portali collegati (es. Tutor-QuiperTe)
		$scope.relatedPortals = $rootScope.relatedPortals;
		if($scope.relatedPortals==undefined || $scope.relatedPortals.length==0) {
			//Cerca i portali collegati a questo (solo per i programs, l'admin non lo fa)
			PortalService.getRelatedPortals(function(result) {
				$scope.relatedPortals = result.data;
				$scope.relatedAccessiblePortals = $scope.getRelatedAccessiblePortals();
			}, function(error) {
				console.log('Cannot retrieve related portals!');
			});
		}
		else {
			$scope.relatedAccessiblePortals = $scope.getRelatedAccessiblePortals();
		}
	}	
	
	$scope.getUserPortals = function() {
		$scope.userPortals = $rootScope.userPortals;
		if($scope.userPortals==undefined) {
			UserService.getLoggedUserPortals({}, function(result) {
				$scope.userPortals = result.data;
				$scope.getRelatedPortals();
			}, function(error) {
				console.log('Cannot retrieve logged user portals!');
			});
		}
		else {
			$scope.getRelatedPortals();
		}
	}
	
	$scope.getRelatedAccessiblePortals = function() {
		var portals = [];
		if($scope.relatedPortals!=undefined && $scope.relatedPortals.length>0) {
			if($scope.userPortals!=undefined && $scope.userPortals.length>0) {
				for(var i=0; i<$scope.relatedPortals.length; i++) {
					for(var j=0; j<$scope.userPortals.length; j++) {
						if($scope.userPortals[j].id==$scope.relatedPortals[i]) {
							portals.push($scope.relatedPortals[i]);
						}
					}
				}
			}
		}
		return portals;
	}
	
	$scope.init = function(entityType, service) {
		if(entityType !== undefined && service !== undefined) {
			$scope.entityType = entityType;
			$scope.service = service;
			$scope.showList = false;
			
			$scope.loggedUser = $rootScope.loggedUser;
			$scope.getUserPortals();
			
			$scope.errors = {};
			$scope.selects = {};
			
			if($scope.populateFilterListsFn!=undefined) {
				$scope.populateFilterListsFn();
			}
			
			PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
				$scope.initPermission(result.data);
				$scope.selection = $scope.selection || false;
				$scope.selected = $scope.selected || $scope.selectedData || [];
				$scope.selectedData = $scope.selected;
				$scope.initPaginationAndFilter();
				
				$scope.showList = true;
				
			}, function(error) {
				$scope.list = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			MessageService.showError('Errore imprevisto', '');
			$state.go('access.not-found');
		}

	};
	
	$scope.search = function(tableState, smCtrl) {
		if(!$scope.showList) return;
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		if($scope.preSearch !== undefined){
			$scope.preSearch();
		}
		
		$scope.service.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			var warning;
			if(result.failurePortalIds!=undefined && result.failurePortalIds.length>0) {
				warning = "Non \u00e8 stato possibile recuperare i dati dal portale "+result.failurePortalIds[0].toUpperCase()+". Per visualizzare i dati dei Pazienti iscritti a entrambi i PSP \u00e8 necessario accedere al portale "+result.failurePortalIds[0].toUpperCase()+" e accettare i termini e condizioni d'uso e l'informativa sulla privacy. Puoi effettuare ora l'accesso tramite la funzione \"Cambia Portale\" del menu in alto a destra";
				if(result.failurePortalIds.length>1) {
					warning = "Non \u00e8 stato possibile recuperare i dati dai portali ";
					for(var i=0; i<result.failurePortalIds.length; i++) {
						warning += " "+result.failurePortalIds[i].toUpperCase();
						if(i<result.failurePortalIds.length-1) {
							warning +=",";
						}
					}
					warning +=". Per visualizzare i dati dei Pazienti iscritti a tutti i PSP \u00e8 necessario aver effettuato l'accesso tutti i portali e aver accettato i termini e condizioni d'uso e l'informativa sulla privacy. Puoi effettuare ora l'accesso tramite la funzione \"Cambia Portale\" del menu in alto a destra";
				}
				
				MessageService.showWarning('Dati non completi',warning);
			}
			else if($rootScope.loggedUser.originalUserId != null && $rootScope.loggedUser.originalUserId !== undefined) {
				warning = "Impersonificando l\'utente \u00e8 possibile recuperare i dati solamente dal portale corrente";
				MessageService.showWarning('Dati non completi',warning);
			}
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for (var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
			
			if($scope.filterStoreKey) {
				try {
					$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
					$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
				}
				catch(e) {
					console.log(e);
				}
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.initPaginationAndFilter = function() {
		
		//Se indicato, non aggiunge il portale corrente dalla lista dei portali disponibili
		$scope.portalsList = angular.copy($scope.relatedAccessiblePortals) || [];
		if($scope.customOptions==undefined || ($scope.customOptions.hideCurrentPortal==undefined || !$scope.customOptions.hideCurrentPortal)) {
			$scope.portalsList.push($rootScope.programProperties.portalId);
		} 
		
		$scope.pagination = $scope.pagination || {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		if($scope.searchFixedFilters) {
			angular.extend($scope.searchFilters.filters, $scope.searchFixedFilters);
		}
		
		if($scope.searchFilters.filters!=undefined && $scope.searchFilters.filters['PORTAL_ID']==undefined) {
			$scope.searchFilters.filters['PORTAL_ID'] = $scope.portalsList;
		}
		
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
				
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		
		if($scope.filterStoreKey) {
			try {
				var savedFilters = $window.sessionStorage[$scope.filterStoreKey];
				if(savedFilters) {
					savedFilters = angular.fromJson(savedFilters);
				}
				if(savedFilters) {
					angular.extend($scope.searchFilters, savedFilters);
					$scope.pagination.start =  savedFilters.start;
					$scope.pagination.size =  savedFilters.size;
					$scope.pagination.sort =  savedFilters.sort;
				}
				
				var objectFilters = $window.sessionStorage[$scope.filterStoreKey + '-object'];
				if(objectFilters) {
					objectFilters = angular.fromJson(objectFilters);
				}
				if(objectFilters) {
					angular.extend($scope.objectFilters, objectFilters);
				}
			}
			catch(e) {
				console.log(e);
			}
		}
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canExport = permissions.EXPORT;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
		
		if($scope.filterStoreKey) {
			try {
				$window.sessionStorage.removeItem($scope.filterStoreKey);
				$window.sessionStorage.removeItem($scope.filterStoreKey + '-object');
			}
			catch(e) {
				console.log(e);
			}
		}
	}
	
	/*****************************************************************************
	 * Manda al dettaglio
	 * Nelle viste CROSS-PSP quando il PSP non è quello su cui sono loggato deve 
	 * reaprire una nuova tab all'URL indicato
	 *****************************************************************************/
	$scope.goDetail = function(entity,item) {
		
		if($scope.relatedAccessiblePortals.indexOf(item.portalId)>=0) {
			$window.open(item.detailUrl,'_blank');
		}
		else {
			if($scope.modal!==undefined) {
				$scope.modal.close();
			}
			$state.go('app.'+entity, {'id':item.id});
			
		}
	}
	
	/******************************************************************************
	 * Gestione selezione
	 * ATTENZIONE! che è stata modificata rispoetto al controller BaseListCtrl
	 * (sinceramente non so come fa a funzionare quello...)
	 *****************************************************************************/
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.postConfirmSelectionFn !== undefined) {
			$scope.postConfirmSelectionFn();
		}
		if($scope.modal !== undefined) {
			$scope.modal.close();
		}
	}
	
	$scope.onSelect = function(selectedData) { 
		
		for(var i = 0; i < $scope.list.length; i++) {
			if($scope.list[i].id == selectedData.id) {
				$scope.list[i].selected = selectedData.selected;
				if($scope.multipleList)
					break;
			} 
			else if(!$scope.multipleList) {
				$scope.list[i].selected = undefined;
			}
		}
		
		if(!$scope.multipleList) {
			if(!selectedData.selected)
				$scope.selectedData=[];
			else 	
				$scope.selectedData=[selectedData];
		}
		else {
			if(!selectedData.selected)
				$scope.selectedData.splice($scope.selectedData.indexOf(selectedData),1);
			else 	
				$scope.selectedData.push(selectedData);
		}
	}
	

	$scope.undoSelection = function() {
		$scope.unselectAll();
		if($scope.postCancelSelectionFn !== undefined) {
			$scope.postCancelSelectionFn();
		}
		if($scope.modal !== undefined)
			$scope.modal.dismiss();
	}
	/********************************************************/
	
	
	
	if($scope.preInit !== undefined) {
		$scope.selects = {};
		$scope.preInit();
	}
	
});