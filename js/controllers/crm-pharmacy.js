'use strict'

app.controller('PharmacyController', function(
		$rootScope,
		$scope,
		$stateParams,
		$http,
		$state,
		$sce,
		PharmacyService,
		MessageService,
		FormUtilService,
		PermissionService,
		RESTURL,
		REGEXP) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		if (angular.isDefined($scope.id)) {
			$scope.initDetails();
		} else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		
		PermissionService.getAll({ entityType : 'PHARMACY' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.detail = {};
		$scope.fieldPermissions = {};
		
		if (!$scope.isNew()) {
			PharmacyService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					type: 'HOSPITAL'
			};
			
			PermissionService.getAll({ entityType : 'PHARMACY' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'PHARMACY' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		PharmacyService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for (var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.PHARMACY + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		PharmacyService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			if (!$scope.modal) {
				$state.go("app.pharmacy", {id: $scope.id});
			}
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PharmacyService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PharmacyService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.pharmacies");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.type', value : $scope.detail.type, type: 'string', required : true },
					{ id:'detail.name', value : $scope.detail.name, type: 'string', required : true },
					{ id:'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					]
		};
		if(angular.isDefined($scope.detail.emails)) {
			for(var i = 0; i < $scope.detail.emails.length; i++) {
				form.formProperties.push({ id:'detail.email' + i, value : $scope.detail.emails[i].text, type: 'string',  pattern: REGEXP.EMAIL,  customMessage: "L'indirizzo mail '" + $scope.detail.emails[i].text + "' numero " + (i+1) + " della lista non ha il formato corretto."});
			}
		}
		errors = FormUtilService.validateForm(form);
		if(angular.isDefined($scope.detail.emails)) {
			for(var i = 0; i < $scope.detail.emails.length; i++) {
				errors['detail.emails'] = errors['detail.email' + i];
				delete errors['detail.email' + i];
			}
		}
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);		
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.pharmacies");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.pharmacy", {id: id});
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}
	
	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.loadRequest = function() {
		var req = {
			code: 		   		$scope.detail.code,
			name:          		$scope.detail.name,
			type :         		$scope.detail.type,
			phoneNumber:   		$scope.detail.phoneNumber,
			faxNumber:     		$scope.detail.faxNumber,
			emails:         	$scope.detail.emails,
			address:       		$scope.detail.address,
			notes:				$scope.detail.notes,
			internalNotes: 		$scope.detail.internalNotes,
			openingTime: 		$scope.detail.openingTime
		}
		if($rootScope.isInternalStaff()) {
			req.checkDrugAvailability = $scope.detail.checkDrugAvailability;
		}
		return req;
	}
	
	$scope.activate = function() {

		var title = 'Conferma Attivazione Farmacia';
		var text = 'Si conferma di voler procedere all\'attivazione della Farmacia?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style);
		
		modalInstance.result.then(
			function(confirm) {
				PharmacyService.activate({ id : $scope.detail.id }, $scope.loadRequest(), function(result) {
					$scope.detail = result.data;
					MessageService.showSuccess('Processo di attivazione avviato con successo, controllare tra le attivit\u00E0 in coda se \u00E8 presente il task relativo a questa farmacia');
				}, function(error) {
					MessageService.showError('Avvio del processo di attivazione non riuscito');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
					else {
						$scope.init();
					}
				});
			}
		);
	}
	
	$scope.terminate = function() {
		
		var title = 'Conferma Disattivazione Farmacia';
		var text = 'Si conferma di voler procedere alla disattivazione della Farmacia?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style);
		
		modalInstance.result.then(
			function(confirm) {
				PharmacyService.terminate({ id : $scope.detail.id }, {}, function(result) {
					$scope.detail = result.data;
					MessageService.showSuccess('Disattivazione completata con successo');
				}, function(error) {
					MessageService.showError('Disattivazione non riuscita');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		);
	}
	
	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}
	
	$scope.init();
});