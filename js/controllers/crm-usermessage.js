'use strict'

app.controller('UserMessageController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		$modal,
		UserMessageService,
		MessageService,
		FormUtilService,
		PermissionService,
		PatientService,
		REGEXP,
		SMART_TABLE_CONFIG) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}

		if(angular.isDefined($scope.id)) {
			$scope.initDetail();
		}
		else {
			$scope.initList();
		}
	}

	$scope.initList = function() {	
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'USER_MESSAGE' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.initDetail = function(){
		$scope.detail = {};
		$scope.receivers = [];
		$scope.receiversHcp = [];
		$scope.receiversPatient = [];

		if(!$scope.isNew()) {
			UserMessageService.get({ id:$scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				
				for(var i = 0; i<$scope.detail.receivers.length; i++){
					if($scope.detail.receivers[i].receiver.type=='HCP'){
						$scope.receiversHcp.push($scope.detail.receivers[i].receiver);
					}
					else if($scope.detail.receivers[i].receiver.type=='PATIENT'){
						$scope.receiversPatient.push($scope.detail.receivers[i].receiver);
					}
					
				}
				
				$scope.initPermission(result.permissions);
				
			
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else {
			$scope.detail = {};

			PermissionService.getAll({ entityType : 'USER_MESSAGE' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'USER_MESSAGE' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
			
		}
	}

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		UserMessageService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}
	
	$scope.removeReceiver = function(receiverId){
		
		
		var tmpPatient = [];
		var tmpHcp = [];
		
		for(var i = 0; i < $scope.receiversPatient.length; i++){
			if($scope.receiversPatient[i].id != receiverId){
				tmpPatient.push($scope.receiversPatient[i]);
			}
		}
		$scope.receiversPatient = tmpPatient;
		
		for(var i = 0; i < $scope.receiversHcp.length; i++){
			if($scope.receiversHcp[i].id != receiverId){
				tmpHcp.push($scope.receiversHcp[i]);
			}
		}
		$scope.receiversHcp = tmpHcp;
		
	}
	
	$scope.setReceivers = function(){
		
		console.log($scope.receivers);
	}

	$scope.prepareReceiver = function(){
		
		if($scope.detail['receiverIds'] == undefined || $scope.detail['receiverIds'] == null){
			$scope.detail.receiverIds = [];
		}
		
		for(var i = 0; i < $scope.receiversPatient.length; i++){
	
			$scope.detail.receiverIds.push($scope.receiversPatient[i].id);
		}
		for(var i = 0; i < $scope.receiversHcp.length; i++){
			
			$scope.detail.receiverIds.push($scope.receiversHcp[i].id);
		}
	}
	
	$scope.save = function(){
		
		$scope.prepareReceiver();
		
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})){
			if ($scope.isNew()) {
				$scope.insert();	
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
	
	
	$scope.saveAndSend = function(){
		$scope.prepareReceiver();
	
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})){
			if ($scope.isNew()) {
				$scope.insert(true);	
			} else {
				$scope.update(true);
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function(send) {
		var request = $scope.loadRequest();
		$scope.receivers = [];
		UserMessageService.insert({}, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			
			for(var i = 0; i<$scope.detail.receivers.length; i++){
				if($scope.detail.receivers[i].receiver.type=='HCP'){
					$scope.receiversHcp.push($scope.detail.receivers[i].receiver);
				}
				else if($scope.detail.receivers[i].receiver.type=='PATIENT'){
					$scope.receiversPatient.push($scope.detail.receivers[i].receiver);
				}
				
			}
			
			$scope.fieldPermissions = result.fieldPermissions;			
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			
			if(send == true){
				
				UserMessageService.send({ id : $scope.id },function(result) {
					
					if ($scope.modal) {
						$scope.modal.close();
					}
					else {
						$state.go("app.usermessage", {id: $scope.id});
					}
					
				}, function(error) {
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
			}
			else{
				
				if ($scope.modal) {
					$scope.modal.close();
				}
				else {
					$state.go("app.usermessage", {id: $scope.id});
				}
			}
			
		
			
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}

	$scope.update = function(send){
		var request = $scope.loadRequest();
		console.log(request);
		$scope.receivers = [];
		UserMessageService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			for(var i = 0; i<$scope.detail.receivers.length; i++){
				if($scope.detail.receivers[i].receiver.type=='HCP'){
					$scope.receiversHcp.push($scope.detail.receivers[i].receiver);
				}
				else if($scope.detail.receivers[i].receiver.type=='PATIENT'){
					$scope.receiversPatient.push($scope.detail.receivers[i].receiver);
				}
				
			}
			MessageService.showSuccess('Aggiornamento completato con successo');
			
			if(send == true){
				
				UserMessageService.send({ id : $scope.id },function(result) {
					
					$scope.fieldPermissions = result.fieldPermissions;			
					$scope.initPermission(result.permissions);
					
					if ($scope.modal) {
						$scope.modal.close();
					}
					
				}, function(error) {
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
			}
			else{
				if ($scope.modal) {
					$scope.modal.close();
				}
			}
			
			
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					UserMessageService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.diseases");
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.subject', value : $scope.detail.subject, type: 'string', required : true },	
					{ id:'detail.message', value : $scope.detail.message, type: 'string', required : true },	
					]
		};
		errors = FormUtilService.validateForm(form);
		
		if($scope.detail.receiverIds.length == 0){
			errors['detail.receiverIds'] = "Deve esserci alemno un destinatario";
		}
		
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.usermessage");	
		} else {
			$scope.initDetail();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
					
			var mustToBeMarkedAsRead = false;
			for(var i = 0; i < $scope.list.length && !mustToBeMarkedAsRead; i++){
				for(var j = 0; j < $scope.list[i].receivers.length && !mustToBeMarkedAsRead; j++){
					//console.log($scope.list[i].receivers[j].receiver.id,$rootScope.loggedUser.id)
					mustToBeMarkedAsRead = true;
				}
				
				/*if($scope.list[i].sender.id == $rootScope.loggedUser.id && $scope.list[i].status != 'SENT'){
					
				}*/
			}
			
			if(mustToBeMarkedAsRead){
				UserMessageService.read({ id : id },function(result) {
					
					$scope.fieldPermissions = result.fieldPermissions;			
					$scope.initPermission(result.permissions);
					
					$state.go("app.usermessage", {id: id});
					
				}, function(error) {
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
			}
			
			
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Già presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

	$scope.loadRequest = function() {
		return {
			subject: 	$scope.detail.subject,
			message: 	$scope.detail.message,
			receiverIds: $scope.detail.receiverIds
		}
	}

	$scope.init();
});