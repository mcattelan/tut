'use strict'

app.controller('MedicalCenterController', function(
		$rootScope,
		$scope,
		$stateParams,
		$http,
		$state,
		MedicalCenterService,
		DepartmentService,
		EntityCustomPropertyConfService,
		MessageService,
		FormUtilService,
		PermissionService,
		SMART_TABLE_CONFIG,
		REGEXP,
		RESTURL) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if(($scope.departmentId === undefined || $scope.departmentId == null) && !$scope.popup) {
			$scope.departmentId = $stateParams.departmentId;
		}
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'MEDICAL_CENTER' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.detail = {};

		if (!$scope.isNew()) {
			MedicalCenterService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				if (!angular.isDefined($scope.departmentId)) {
					$scope.initListDepartment();
				}		
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					type: 'HOSPITAL'
			};
			
			PermissionService.getAll({ entityType : 'MEDICAL_CENTER' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'MEDICAL_CENTER' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
				EntityCustomPropertyConfService.search({filters :{'DATA_TYPE_NAME':'MEDICAL_CENTER'}}, function(result) {
					if(angular.isDefined(result.data) && result.data.length > 0) {
						$scope.detail.customProperties = [];
						for(var i = 0; i < result.data.length; i++) {
							var customPropertyConf = result.data[i];
							var customProperty = {
									name: customPropertyConf.name,
									code: customPropertyConf.code,
									list: customPropertyConf.list,
									mandatory: customPropertyConf.mandatory,
									type: customPropertyConf.type,
									choices: customPropertyConf.choices
							}
							$scope.detail.customProperties.push(customProperty);
						}
					}
				}, function(error) {
				})
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		MedicalCenterService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}

		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.initListDepartment = function() {
		$scope.listDepartment = [];
		PermissionService.getAll({ entityType : 'DEPARTMENT' }, function(result) {
			$scope.canInsertDepartment = result.data.INSERT;

			MedicalCenterService.getAllDepartment({id: $scope.id}, function(result) {
				$scope.listDepartment = result.data;
				$scope.permissionDepartment = result.permissions;
			}, function(error) {
				$scope.listDepartment = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.listUser = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		MedicalCenterService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});	
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		MedicalCenterService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					MedicalCenterService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						$state.go("app.medicalcenters");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.deleteDepartment = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					DepartmentService.deleteDepartment({ id : id }, {}, function(result) {
						$scope.initListDepartment();
						$state.go("app.medicalcenters");
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.type', value : $scope.detail.type, type: 'string', required : true },
					{ id:'detail.name', value : $scope.detail.name, type: 'string', required : true },
					{ id:'detail.phoneNumber', value : $scope.detail.phoneNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.email', value : $scope.detail.email, type: 'string',  pattern: REGEXP.EMAIL,  customMessage:"L'indirizzo mail deve essere valido" },
					]
		};
		if($scope.detail['customProperties'] != undefined && $scope.detail['customProperties'] != null){
			for(var i = 0; i < $scope.detail.customProperties.length; i++){
				var prop = $scope.detail.customProperties[i];
				if(prop.mandatory == true && (prop.values == '' || prop.values == undefined || prop.values == null ) ){
					errors['detail.customProperties_'+i] = 'Campo obbligatorio';
				}
			}
		}
		errors = FormUtilService.validateForm(form);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);			
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;

			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.medicalcenters");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.medicalcenter", {id: id});
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if($scope.list[i].selected) {
				if($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				}
				else {
					// Gi� presente nella lista
				}
			}
			else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.loadRequest = function() {
		return {
			type :         		$scope.detail.type,
			otherType :    		$scope.detail.otherType,
			name:          		$scope.detail.name,
			phoneNumber:   		$scope.detail.phoneNumber,
			faxNumber:     		$scope.detail.faxNumber,
			email:         		$scope.detail.email,
			address:       		$scope.detail.address,
			internalNotes: 		$scope.detail.internalNotes,
			customProperties:	$scope.detail.customProperties
		}
	}

	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.MEDICALCENTER + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}

	$scope.init();
});