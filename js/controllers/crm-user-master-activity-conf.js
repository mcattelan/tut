'use strict'

app.controller('UserMasterActivityConfController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state,
		UserMasterActivityConfService,
		FormUtilService,
		MessageService) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		if ($scope.id == null || $scope.id == undefined) {
			$scope.id = $stateParams.id;
		}

		$scope.errors = {};
		$scope.initDetails();
	}

	$scope.initDetails = function() {
		$scope.canModify = false;
		$scope.isEditable = $scope.canModify;
		$scope.detail = {};
		
		if (!$scope.isNew()) {
			UserMasterActivityConfService.get({id: $scope.id}, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
			} else {
				$scope.update();
			}	
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		UserMasterActivityConfService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		return {
			
		}
	}

	$scope.init();
});
