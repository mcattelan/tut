app.directive("tutorDiary", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId: '=tutorDiary',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/patient/tutor-diary.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			scope.loggedUser = $rootScope.loggedUser;
			
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-tutor-diary.js']).then( function() {
				var controller = $controller('TutorDiaryController', { $scope: scope });
			});
		}
	}
});

app.directive("patientAssumptionList", function(baseListDirective) {
	baseListDirective[0].scope['patientId'] = "=patientId";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-patient-assumption-list.js'];
		$scope.templateUrl = 'tpl/app/directive/patient/patient-assumption-list.html',
		$scope.controllerName = 'PatientAssumptionListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("tutorAdherence", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			filters: "=",
			refresh: "=?",
			options: "=?",
		},
		templateUrl: 'tpl/app/directive/chart/tutor-adherence.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#pie_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				
				delete $scope.message;
				$scope.datas = [];
				var searchFilters = {
					'chartTypes': ['TUTOR_PATIENTS_ADHERENCE'],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					$scope.datas = [];
					if (result.data[0].series[0].values) {
						for(var x = 0; x < result.data[0].series.length; x++) {
							$scope.datas.push(result.data[0].series[x]);
						}
					}
					if($scope.datas && $scope.datas.length > 0) {
						
					}
					$('#pie_' + $scope.chartType + "_" + $scope.id).show();
					$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
					$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
					$scope.renderer = true;
					
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("hcpCustomPatientTherapies", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId: '=hcpCustomPatientTherapies',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/patient/hcp-custom-patient-therapies.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-hcp-custom-patient-therapies.js']).then( function() {
				var controller = $controller('HcpCustomPatientTherapiesController', { $scope: scope });
			});
		}
	}
});

app.directive("hcpDashboard", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			hcpId: "="
		},
		templateUrl: 'tpl/app/directive/dashboard/dashboard_HCP.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['ui.select','js/controllers/crm-stats.js']).then( function() {
				scope.searchFilters = {
						filters: {
							'HCP_ID': scope.hcpId 
						}
				}
				scope.filters = scope.searchFilters.filters;
				var controller = $controller('StatsController', { $scope: scope });
			});
		}
	}
});
