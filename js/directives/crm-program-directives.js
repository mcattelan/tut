'use strict';
//**********************************************MODALI LISTA ENTITA'***************************************************************//
app.directive("patientLeftoverDrugsDetectionModal", function($modal, $window, $timeout, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId: "=patientId",
			date: "=date",
			complementaryId: "=complementaryId",
			postFn: "=?postFn",
			postFnDismiss: "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			scope.isPatient = $rootScope.isPatient();
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/leftover-drug/leftover-drugs-detection.html',
					controller: 'PatientLeftoverDrugDetectionController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-leftover-drug-detection.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientLeftoverDrugs", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId: '=patientId',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/leftover-drug/leftover-drugs.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			
			$ocLazyLoad.load(['smart-table','js/controllers/directives/crm-patient-leftover-drug.js']).then( function() {
				var controller = $controller('PatientLeftoverDrugController', { $scope: scope });
			});
		}
	}
});

app.directive("userServices", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			userId: '=userId',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/user-service/user-services.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['smart-table','js/controllers/directives/crm-user-service.js']).then( function() {
				var controller = $controller('UserServiceController', { $scope: scope });
			});
		}
	}
});

app.directive("userActivities", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			userId: '=userId',
			serviceId: '=serviceId',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/user-activity/user-activities.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['smart-table','js/controllers/directives/crm-user-activity.js']).then( function() {
				var controller = $controller('UserActivityController', { $scope: scope });
			});
		}
	}
});


//*********************************************************************************************************************************************//

//**********************************************MODALI DETTAGLIO ENTITA'***************************************************************//
app.directive("medicalCenterModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=medicalCenterModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/medicalcenter/medicalcenter.html',
					controller: 'MedicalCenterController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-medicalcenter.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("departmentModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			medicalCenterId : "=medicalCenterId",
			id : "=departmentModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/medicalcenter/department.html',
					controller: 'DepartmentController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-department.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("hcpModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=hcpModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/hcp-modal.html',
					controller: 'HcpController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-hcp.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("activityConfModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=activityConfModal",
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/activityConf-modal.html',
					controller: 'ServiceActivityController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-service-activity.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("userActivityModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=userActivityModal"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/user-activity/user-activity.html',
					controller: 'UserActivityController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-user-activity.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("customerModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=customerModal",
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/customer/customer.html',
					controller: 'CustomerController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-customer.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("userMasterActivityConfModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=userMasterActivityConfModal",
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/program/services/activityConf.html',
					controller: 'UserMasterActivityConfController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-user-master-activity-conf.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientAssumptionModal", function($modal, $window, $timeout, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId : "=?patientId",
			id : "=patientAssumptionModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			scope.isPatient = $rootScope.isPatient();
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/assumption/assumption.html',
					controller: 'PatientAssumptionController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-assumption.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientMedicalExaminationModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId : "=?patientId",
			id : "=patientMedicalExaminationModal",
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/patient/medical-examination-data.html',
					controller: 'PatientMedicalExaminationController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-patient-medical-examination.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientLeftoverDrugModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId : "=?patientId",
			id : "=patientLeftoverDrugModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			scope.isPatient = $rootScope.isPatient();
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/leftover-drug/leftover-drug.html',
					controller: 'PatientLeftoverDrugController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-leftover-drug.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("userServiceModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=userServiceModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/user-service/user-service.html',
					controller: 'UserServiceController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-user-service.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
app.directive("bloodDrawingCenterModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=bloodDrawingCenterModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/blooddrawingcenter/blood-drawing-center.html',
					controller: 'BloodDrawingCenterController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-blood-drawing-center.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
//*********************************************************************************************************************************************//

//**********************************************MODALI RICERCA ENTITA'*************************************************************************//
app.directive("userSearchModal", function($modal, $ocLazyLoad, $state, $rootScope, $timeout) {
	return {
		restrict: 'A',
		scope : {
			selection: "=?selection",
			selected: "=?userSearchModal",
			searchFilters: "=?filters",
			objectFilters: "=?objectFilters",
			multipleList: "=?multipleList",
			preFn: "=?preFn",
			postFn: "=?postFn",
			userType: "=userType",
			userId: "=?"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				switch(scope.userType) {
			    case 'SUPPLIER':
			    	scope.searchFilters = {
			    		filters:{
			    			'SUPPORT_MEDICAL_SERVICE' : false
			    		}
			    	}
			        scope.userController = "SupplierUserListCtrl";
			        scope.userControllerPath = "js/controllers/directives/supplier-user.js";
			        scope.userTemplate = "tpl/app/directive/supplier-users.html";
			        scope.supplierId = scope.userParent;
			        break;
			    case 'CUSTOMER':
			    	scope.userController = "CustomerUserController";
			    	scope.userControllerPath = "js/controllers/crm-customer-user.js";
			    	scope.userTemplate = "tpl/app/customer/users.html";
			    	scope.customerId = scope.userParent;
			        break;
			    case 'HCP':
			    	scope.userController = "HcpController";
			    	scope.userControllerPath = "js/controllers/crm-hcp.js";
			    	scope.userTemplate = "tpl/app/hcp/hcps.html";
			    	break;
			    case 'PATIENT':
			    	scope.userController = "PatientController";
			    	scope.userControllerPath = "js/controllers/crm-patient.js";
			    	scope.userTemplate = "tpl/app/patient/patients.html";
			    	break;
			    case 'PATIENT_USER':
			    	scope.userController = "PatientUserController";
			    	scope.userControllerPath = "js/controllers/crm-patient-user.js";
			    	scope.userTemplate = "tpl/app/patient/users.html";
			    	scope.patientId = scope.userParent;
			    	break;
			    case 'INTERNAL_STAFF':
			    	scope.userController = "InternalStaffUserListCtrl";
			    	scope.userControllerPath = "js/controllers/directives/internal-staff-user.js";
			    	scope.userTemplate = "tpl/app/directive/internal-staff-users.html";
			    	break;
			    case 'LOGISTIC':
			    	scope.userController = "LogisticUserListCtrl";
			    	scope.userControllerPath = "js/controllers/directives/logistic-user.js";
			    	scope.userTemplate = "tpl/app/directive/logistic-users.html";
			    	scope.logisticId = scope.userParent;
			    	break;
			    case 'SUPPLIER_MEDICAL_SUPPORT':
			    	scope.searchFilters = {
			    		filters:{
			    			'SUPPORT_MEDICAL_SERVICE' : true
			    		}
			    	}
			        scope.userController = "SupplierUserListCtrl";
			        scope.userControllerPath = "js/controllers/directives/supplier-user.js";
			        scope.userTemplate = "tpl/app/directive/supplier-users.html";
			        scope.supplierId = scope.userParent;
			        break;
			    default:
			        return;
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: scope.userTemplate,
					controller: scope.userController,
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( [scope.userControllerPath] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						scope.userId = scope.selected.id;
					} else {
						scope.userId = [];
						for(var i = 0; scope.selected.length>i; i++){
							scope.userId.push(scope.selected[i].id);
						}
					}
					if(scope.postFn) {
						$timeout(function() {
					         scope.postFn();
					     });
					}
				}, function () {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("departmentSearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			selection : "=?selection",
			selected : "=?departmentSearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn",
			departmentId : "=?"
			
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/department-search.html',
					controller: 'DepartmentController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-department.js'] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						scope.departmentId = angular.isDefined(scope.selected) ? scope.selected.id : undefined;
					} else {
						scope.departmentId = [];
						for(var i = 0; scope.selected.length>i; i++){
							scope.departmentId.push(scope.selected[i].id);
						}
					}
					if(scope.postFn) {
						scope.postFn()
					}
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("pharmacyModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=pharmacyModal",
			objectFilters: "=?objectFilters",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/pharmacy/pharmacy.html',
					controller: 'PharmacyController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table', 'ngTagsInput']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-pharmacy.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("diseaseSearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			selection : "=?selection",
			selected : "=?diseaseSearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList || true;
				scope.popup = true;
//				scope.id = '';
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/disease-search.html',
					controller: 'DiseaseController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-disease.js'] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					if(scope.postFn) {
						scope.postFn()
					}
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientTherapySearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=patientId",
			selection : "=?selection",
			selectedList : "=?patientTherapySearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selectedList!=undefined) {
						scope.selected = [];
						scope.selected.push(scope.selectedList);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selectedList || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/therapy/therapies.html',
					controller: 'PatientTherapyController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-patient-therapy.js'] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					} else {
						scope.selectedList = scope.selected;
					}
					if(scope.postFn) {
						scope.postFn()
					}
				}, function () {
					if (!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
//*********************************************************************************************************************************************//
app.directive("documentsModal", function($modal, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			entityType: "=entityType",
			entityId: "=documentsModal",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/documents-modal.html',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']);
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("unreadUserMessage", function($modal, $ocLazyLoad, $state, $rootScope, UserMessageService) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			console.log(element);
			UserMessageService.unreadMessagesNumber({}, function(result){
				
				console.log(result,element);
				
				element.after('<b class="badge bg-info pull-right">'+result.data.unreadUserMessage+'</b>');
			},function(error){
				
			});
		}
	}
	

});

app.directive("patientAssumption", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId: '=patientAssumption',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/assumption/assumptions.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-patient-assumption.js']).then( function() {
				var controller = $controller('PatientAssumptionController', { $scope: scope });
			});
		}
	}
});

app.directive("patientAssumptionDetectionModal", function($modal, $window, $timeout, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId: "=patientId",
			date: "=date",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			scope.isPatient = $rootScope.isPatient();
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.searchFilters = { filters: {'DATE_TO' : scope.date,'PATIENT_ID' : scope.patientId}, start: 0, sort: ['-date'] }
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/assumption/assumptions-detection.html',
					controller: 'PatientAssumptionController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-assumption.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
					//scope.$emit('updateDelegate',{ detailIndex: scope.detailIndex, detail: scope.detail})
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("userQuestionnaireList", ["$rootScope","$ocLazyLoad","$controller","UserQuestionnaireService", 
                                        function($rootScope,$ocLazyLoad,$controller,UserQuestionnaireService) {
	return {
		restrict: 'EA',
		scope: {
			userId : '=userQuestionnaireList',
			hideCharts :  '=?hideCharts'
		},
		templateUrl: 'tpl/app/directive/questionnaire/user-questionnaires.html',
		link: function(scope, element, attrs) {
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/user-questionnaire.js']).then( function() {

				scope.isHcp = $rootScope.isHcp();
				
				scope.$watch('userId',function(nv) {
					if(nv!=undefined) {
						var controller = $controller('UserQuestionnaireListCtrl', { $scope: scope });
					}
				});
				
				scope.onModalCloseFn = function() {
					scope.list = [];
					scope.preInit();
					scope.init('USER_QUESTIONNAIRE', UserQuestionnaireService);
				}
			});
		}
	}	
}]);
/**
 * activityId => se si passa questo parametro allora per quanto riguarda i permessi di compilazione viene controllato che il logged user sia l'assegnatario dell'attività. Se è vero
 * allora viene SEMPRE consentito compilare e confermare (si ignorano sia i permessi a db che quelli nel questionario)
 */
app.directive("userQuestionnaireDetailModal", ["$rootScope","$ocLazyLoad","$modal","$sce", function($rootScope,$ocLazyLoad,$modal,$sce) {
	return {
		restrict: 'A',
		scope: {
			id : '=userQuestionnaireDetailModal',
			submissionDate: '=?submissionDate',
			postFn: '=?',
			postFnArgs: '=?',
			postFnDismiss: "=?",
			postFnDismissArgs: "=?",
			activityId: "=?"  //ID dell'attività da cui si sta aprendo il questionario
		},
		link: function(scope, element, attrs) {
			element.css('cursor','pointer');
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/questionnaire/user-questionnaire-modal.html',
					windowClass: 'largeModal',
					size: 'lg',
					controller: 'UserQuestionnaireDetailCtrl',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','smart-table','rzModule']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/directives/user-questionnaire.js'] );
								}
							)
						}
					}
				});
				
				scope.modal.result.then(function(result) {
					console.log('Modal closed: ' + new Date());
					console.log(result);
					if(scope.postFn) {
						if(scope.postFnArgs!=undefined) {
							scope.postFn(scope.postFnArgs);
						}
						else {
							scope.postFn();
						}
					}
				}, function () {
					console.log('Modal dismissed: ' + new Date());
					if(scope.postFnDismiss) {
						if(scope.postFnDismissArgs != undefined) {
							scope.postFnDismiss(scope.postFnDismissArgs);
						}
						else {
							scope.postFnDismiss();
						}
					}
				});
				
				
			});
		}
	}	
}]);
app.directive("programQuestionnaireModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=programQuestionnaireModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		link: function (scope, element, attrs) {
			
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/program/questionnaires/questionnaire.html',
					controller: 'QuestionnaireController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','as.sortable','rzModule']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-questionnaire.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("materialDamagesList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?materialDamagesList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/material/material-damage/material-damages.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'RECIPIENT_ID' : scope.id }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-material-damage.js']).then( function() {
				var controller = $controller('MaterialDamageListCtrl', { $scope: scope });
			});
		}
	}
});

app.directive("bloodDrawingCenterSearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=patientId",
			selection : "=?selection",
			selected : "=?bloodDrawingCenterSearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList || true;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/blooddrawingcenter/blood-drawing-centers.html',
					controller: 'BloodDrawingCenterController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-blood-drawing-center.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					} else {
						scope.selectedList = scope.selected;
					}
					if(scope.postFn) {
						scope.postFn()
					}
				}, function () {
					if (!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("pharmacySearchModal", function($modal, $ocLazyLoad, $state, $rootScope, $timeout) {
	return {
		restrict: 'A',
		scope : {
			id : "=?patientId",
			selection : "=?selection",
			selectedList : "=?pharmacySearchModal",
			searchFilters : "=?filters",
			objectFilters: "=?objectFilters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn",
			pharmacyId: "=?"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				scope.openModal = function() {
					if(!scope.multipleList) {
						if(scope.selectedList!=undefined) {
							scope.selected = [];
							scope.selected.push(scope.selectedList);
						} else {
							scope.selected = []
						}
					} else {
						scope.selected = scope.selectedList || [];
					}
				
					scope.modal = $modal.open({
						scope: scope,
						templateUrl: 'tpl/app/directive/pharmacy/pharmacies.html',
						controller: 'PharmacyController',
						windowClass: 'largeModal',
						size: 'lg',
						resolve: {
							deps: function($ocLazyLoad) {
								return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
										function() {
											return $ocLazyLoad.load( ['js/controllers/crm-pharmacy.js'] );
										}
								)
							}
						}
					});
					
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
					scope.modal.result.then(function() {
						if(!scope.multipleList) {
							scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
							scope.pharmacyId = scope.selectedList.id
						} else {
							scope.selectedList = scope.selected;
							scope.pharmacyId = [];
							for(var i = 0; scope.selected.length>i; i++){
								scope.pharmacyId.push(scope.selected[i].id);
							}
						}
						if(scope.postFn) {
							$timeout(function() {
								scope.postFn();
							
							},10);
						}
					}, function () {
						if (!scope.multipleList) {
							scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						}
						console.log('Modal dismissed: ' + new Date());
					});
				}
				
				if(scope.preFn) {
					scope.preFn().then(function() {
						scope.openModal();
					});
				} else {
					scope.openModal();
				}
			});
		}
	};
});


app.directive("patientPharmaciesList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?patientPharmaciesList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/patient-pharmacy/patient-pharmacies.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'PATIENT_ID' : scope.id }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-patient-pharmacy.js']).then( function() {
				var controller = $controller('PatientPharmacyListCtrl', { $scope: scope });
			});
		}
	}
});

app.directive("pharmaciesList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?pharmaciesList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/pharmacy/pharmacies.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.selection = false;
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/crm-pharmacy.js']).then( function() {
				var controller = $controller('PharmacyController', { $scope: scope });
			});
		}
	}
});

app.directive("patientPharmacySearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId : "=patientId",
			selection : "=?selection",
			selected : "=?patientPharmacySearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?",
			postFn : "=?"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/patient-pharmacy/patient-pharmacies.html',
					controller: 'PatientPharmacyListCtrl',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-pharmacy.js'] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
//						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						if((scope.selected!==undefined && scope.selected.length>0)) {
							scope.selected = scope.selected[0].pharmacy;
						} else {
							scope.selected = undefined;
						}
						scope.selection = scope.selected.pharmacy;
					} else {
						scope.patientId = [];
						for(var i = 0; scope.selected.length>i; i++){
							scope.patientId.push(scope.selected[i].id);
						}
					}
					if(scope.postFn) {
						$timeout(function() {
					         scope.postFn();
					     });
					}
				}, function () {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

//app.directive("patientPharmacySearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
//	return {
//		restrict: 'A',
//		scope : {
//			id : "=patientId",
//			selection : "=?selection",
//			selectedList : "=?patientPharmacySearchModal",
//			searchFilters : "=?filters",
//			multipleList : "=?multipleList",
//			preFn : "=?",
//			postFn : "=?"
//		},
//		link: function (scope, element, attrs) {
//			element.bind("click", function() {
//				scope.closeButton = true;
//				scope.selection = scope.selection || true;
//				scope.multipleList = scope.multipleList === undefined || scope.multipleList;
//				scope.popup = true;
//				scope.constants = $rootScope.constants;
//				
//				if(scope.preFn) {
//					scope.preFn();
//				}
//				
//				if(!scope.multipleList) {
//					if(scope.selected!=undefined) {
//						scope.singleSelected = scope.selected;
//						scope.selected = [];
//						scope.selected.push(scope.singleSelected);
//					} else {
//						scope.selected = []
//					}
//				} else {
//					scope.selected = scope.selected || [];
//				}
//				
//				scope.modal =  $modal.open({
//					scope: scope,
//					templateUrl: 'tpl/app/directive/patient-pharmacy/patient-pharmacies-selected.html',
//					controller: 'PatientPharmacyDetailCtrl',
//					windowClass: 'largeModal',
//					size: 'lg',
//					resolve: {
//						deps: function($ocLazyLoad) {
//							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
//									function() {
//										return $ocLazyLoad.load( ['js/controllers/crm-patient-pharmacy.js'] );
//									}
//							)
//						}
//					}
//				});
//				
//				scope.modal.result.then(function() {
//					if (scope.postFn)
//						scope.postFn();
//					console.log('Modal closed: ' + new Date());
//				}, function () {
//					if (scope.postFnDismiss)
//						scope.postFnDismiss();
//					console.log('Modal dismissed: ' + new Date());
//				});
//			});
//		}
//	};
//});

app.directive("materialDeliveriesList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?materialDeliveriesList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/material/material-delivery/material-deliveries.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'RECIPIENT_ID' : scope.id }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-material-delivery.js']).then( function() {
				var controller = $controller('MaterialDeliveryListCtrl', { $scope: scope });
			});
		}
	}
});

app.directive("materialDeliveryModal", function($modal, $ocLazyLoad, $state, $rootScope, $filter) {
	return {
		restrict: 'A',
		scope : {
			id : "=materialDeliveryModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			activitiForm: '=?activitiForm',
		},
		link: function (scope, element, attrs) {
		
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
//				scope.$watch('activitiForm',function(nv) {
//					if(nv!=undefined) {
//						if(scope.detail == undefined) 
//							scope.detail = {};
//						scope.detail.address = angular.copy(scope.activitiForm.address); 
//						scope.detail.organizer = scope.activitiForm.owner;
//						if(scope.detail.organizer!=undefined) {
//							scope.detail.organizerName = ($filter("constantLabel")(scope.detail.organizer.title,'UserTitle')!=undefined ? $filter("constantLabel")(scope.detail.organizer.title,'UserTitle') : '')+" "+scope.detail.organizer.lastName+" "+scope.detail.organizer.firstName;
//						}
//						
//						if(scope.activitiForm.date != undefined) {
//							if(scope.detail.status == 'DRAFT')
//								scope.detail.requestDate = scope.activitiForm.date;
//						}
//					}
//				},true);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/material/material-delivery/material-delivery.html',
					controller: 'MaterialDeliveryDetailCtrl',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-material-delivery.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					//modal.dismiss('cancel');
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("materialDamageModal", function($modal, $ocLazyLoad, $state, $rootScope, $filter) {
	return {
		restrict: 'A',
		scope : {
			id : "=materialDamageModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			activitiForm: '=?activitiForm',
		},
		link: function (scope, element, attrs) {
		
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
//				scope.$watch('activitiForm',function(nv) {
//					if(nv!=undefined) {
//						if(scope.detail == undefined) 
//							scope.detail = {};
//						scope.detail.address = angular.copy(scope.activitiForm.address); 
//						scope.detail.organizer = scope.activitiForm.owner;
//						if(scope.detail.organizer!=undefined) {
//							scope.detail.organizerName = ($filter("constantLabel")(scope.detail.organizer.title,'UserTitle')!=undefined ? $filter("constantLabel")(scope.detail.organizer.title,'UserTitle') : '')+" "+scope.detail.organizer.lastName+" "+scope.detail.organizer.firstName;
//						}
//						
//						if(scope.activitiForm.date != undefined) {
//							if(scope.detail.status == 'DRAFT')
//								scope.detail.requestDate = scope.activitiForm.date;
//						}
//					}
//				},true);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/material/material-damage/material-damage.html',
					controller: 'MaterialDamageDetailCtrl',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-material-damage.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					//modal.dismiss('cancel');
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
app.directive("materialModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=materialModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.close = function() {
					scope.modal.close();
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/material/material.html',
					controller: 'MaterialController',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-material.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("materialDeliveryItemsModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			detail : "=materialDeliveryItemsModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.close = function() {
					scope.modal.close();
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/material/material-delivery/material-delivery-items-modal.html',
					controller: function($scope,$rootScope) {
						
						$scope.init = function() {
							if ($scope.detail==undefined) {
								$scope.detail.deliveredQuantity = 0;
								$scope.detail.deliveredSerialNumbers = [];
							}
						}	
						
						$scope.$watch('detail.deliveredQuantity',function(nv) {
							if (angular.isDefined(nv)) {
								$scope.detail.deliveredSerialNumbers.length = nv;
							}
						});
						
						$scope.confirm = function() {
							$scope.detail.deliveredQuantity = $scope.detail.deliveredSerialNumbers.length;
							$scope.close($scope.detail);
						}
						
						$scope.init();
					},
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']);
						}
					}
				});
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("communicationMessagesList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?communicationMessagesList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/communication-message/communication-messages.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'RECEIVER_OR_SENDER_ID' : scope.id }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-communication-message.js']).then( function() {
				var controller = $controller('CommunicationMessageListCtrl', { $scope: scope });
			});
		}
	}
});
app.directive("bloodDrawingCenterList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-blood-drawing-center.js'];
		$scope.templateUrl = 'tpl/app/directive/blooddrawingcenter/blood-drawing-center-list.html',
		$scope.controllerName = 'BloodDrawingCenterListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("patientBloodDrawingCentersList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?patientBloodDrawingCentersList",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/patient-blooddrawingcenter/patient-blood-drawing-centers.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'PATIENT_ID' : scope.id }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-patient-blood-drawing-center.js']).then( function() {
				var controller = $controller('PatientBloodDrawingCenterListCtrl', { $scope: scope });
			});
		}
	}
});

//*********************************************************************************************************************************************//

//************************************************************GRAFICI'*************************************************************************//
app.directive("pieChart", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			chartType: "@",
			filters: "=",
			refresh: "=?",
			options: "=?",
			drawCallback: "=?drawCallback"
		},
		templateUrl: 'tpl/app/directive/chart/pie-chart.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#pie_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				
				delete $scope.message;
				$scope.datas = [];
				var searchFilters = {
					'chartTypes': [$scope.chartType],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					if(result.data[0].message) {
						$('#nodata_' + $scope.chartType + "_" + $scope.id).show();
						$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
						$('#pie_' + $scope.chartType + "_" + $scope.id).hide();
						$scope.message = result.data[0].message;
						if($scope.drawCallback) {
							$scope.drawCallback();
						}
					} else {
						$scope.datas = [];
						if (result.data[0].series[0].values) {
							for(var x = 0; x < result.data[0].series.length; x++) {
								for(var i = 0; i < result.data[0].series[x].values.length; i++) {
									if(result.data[0].series[x].values[i].data !== 0) {
										$scope.datas.push(result.data[0].series[x]);
										break;
									}
								}
							}
						}
						if($scope.datas && $scope.datas.length > 0) {
							$scope.render();
						}
					}
				},function(error) {
					console.log(error)
				});
			}
			
			//Uso anche i signals dove non funziona il watch
			$scope.$on('refreshCharts', function(events, filters) {
				$scope.filters = filters;
				$scope.search(angular.copy($scope.filters));
			});
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
			$scope.render = function() {
				$('#pie_' + $scope.chartType + "_" + $scope.id).show();
				$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				var formatter = undefined;
				if(angular.isDefined($scope.options) &&
						angular.isDefined($scope.options.series) &&
						angular.isDefined($scope.options.series.percentage) &&
						$scope.options.series.percentage == true) {
					formatter = function(label, series) {
						return "<div style='font-weight: 900; font-size:12px; text-align:center; padding:2px; color:black; width: 80pt;'>" + Math.round(series.percent * 10) / 10 + "%</div>";
					}
				} else {
					formatter = function(label, series) {
						return "<div style='font-weight: 900; font-size:12px; text-align:center; padding:2px; color:black; width: 80pt;'>" + Math.round(series.data[0][1]) + "</div>";
					}
				}
				
				$scope.defaultOptions = {
					series: { pie: { show: true, radius: 1, stroke: { width: 3 }, label: { show: true, radius: 0.6, formatter: formatter, threshold: 0.01 } }, title: { show: false }, average:{ show: false },  total:{ show: false } },
					legend: { show: true },
					colors:  ['#ff7d4d', '#28d094', '#27b7d1', '#9681db', '#ffd54c', '#c7ed57', '#3ceacd', '#e86f5f'],
					grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#58666e' },   
					tooltip: false,
					tooltipOpts: { content: '%s: %p.0%' },
					hooks: {
						draw: function(plot, canvascontext) {
							if($scope.drawCallback) {
								$scope.drawCallback();
							}
						}
					}
				}
				if($scope.options) {
					$scope.defaultOptions = angular.merge($scope.defaultOptions, $scope.options);
				}
				$scope.renderer = true;
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("barChart", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			chartType: "@",
			filters: "=",
			refresh: "=?",
			key: "@",
			dataset: "=?",
			options: "=?",
			drawCallback: "=?drawCallback"
		},
		templateUrl: 'tpl/app/directive/chart/bar-chart.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.noData = true;
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				$('#bar_' + $scope.chartType + "_" + $scope.id).hide();
				
				delete $scope.message;
				$scope.data = {
						values: [],
						series: []
				};
				var searchFilters = {
						'chartTypes': [$scope.chartType],
						'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					if(result.data[0].message) {
						$scope.data = {
								values: [],
								labels: []
						};
						$scope.message = result.data[0].message; 
						$('#nodata_' + $scope.chartType + "_" + $scope.id).show();
						$('#bar_' + $scope.chartType + "_" + $scope.id).hide();
						$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
						if($scope.drawCallback) {
							$scope.drawCallback();
						}
					} else {
						$scope.data = {
								values: [],
								series: []
						};
						if (result.data[0].series[0].values) {
							for(var i = 0; i < result.data[0].series[0].values.length; i++) {
								$scope.data.values.push([i, result.data[0].series[0].values[i].data]);
								$scope.data.series.push([i, result.data[0].series[0].values[i].label]);
								if(result.data[0].series[0].values[i].data !== 0 && $scope.noData) {
									$scope.noData = false;
								}
							}
							$scope.data.total = result.data[0].series[0].total;
						}
						$scope.render();
					}
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
			$scope.render = function() {
				$('#bar_' + $scope.key + "_" + $scope.id).show();
				$('#spinner_' + $scope.key + "_" + $scope.id).hide();
				$('#nodata_' + $scope.key + "_" + $scope.id).hide();
				$scope.defaultDataset = {
					data: $scope.data.values,
					bars: { show: true, barWidth: 0.9, fillColor: { colors: [{ opacity: 0.4 }, { opacity: 0.9 }], }, align: 'center', lineWidth: 2 }
				}
				if($scope.dataset) {
					$scope.defaultDataset = angular.merge($scope.defaultDataset, $scope.dataset);
				}
								
				$scope.defaultOptions = {
					colors: ['#27b7d1'],
					series: { shadowSize: 2 },  total:{ show: false },
					legend: { show: false, noColumns: 1 },
					xaxis: { font: { color: '#58666e' }, ticks: $scope.data.series, tickLength: 0 },
					yaxis: { font: { color: '#58666e' }, min: 0, tickFormatter: function(val, axis) { return parseInt(val) + ""; }, tickDecimals: 0 },
					grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
					tooltip: false,
					tooltipOpts: { content: '%s of %x.1 is %y.4', defaultTheme: false, shifts: { x: 0, y: 20 } },
					hooks: {
						draw: function(plot, canvascontext) {
							if(plot.getData().length > 0 && plot.getData()[0]['data'] != undefined) {
								$("#" + $(plot.getPlaceholder()).attr('id') + " > .data-point-label").remove()
								$.each(plot.getData()[0].data, function(i, el) {
									if(el[1]) {
										var o = plot.pointOffset({x: el[0], y: el[1]});
										$('<div class="data-point-label">' + el[1] + '</div>').css({
											position: 'absolute',
											left: o.left - 5,
											top: o.top - 20,
											display: 'none'
										}).appendTo(plot.getPlaceholder()).fadeIn('slow');
									}
								});
							}
							if($scope.drawCallback) {
								$scope.drawCallback();
							}
						}
					}
				}
				if($scope.options) {
					$scope.defaultOptions = angular.merge($scope.defaultOptions, $scope.options);
				}
				$scope.renderer = true;
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("barStackedChart", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			chartType: "@",
			filters: "=",
			refresh: "=?",
			key: "@",
			dataset: "=?",
			options: "=?",
			drawCallback: "=?drawCallback"
		},
		templateUrl: 'tpl/app/directive/chart/bar-stacked-chart.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.noData = true;
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.key + "_" + $scope.id).show();
				$('#nodata_' + $scope.key + "_" + $scope.id).hide();
				$('#bar_' + $scope.key + "_" + $scope.id).hide();
				
				delete $scope.message;
				$scope.data = {
						ticks: [],
						values: [],
				};
				var searchFilters = {
					'chartTypes': [$scope.chartType],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					if(result.data[0].message) {
						$scope.data = {
								values: [],
								ticks: []
						};
						$scope.message = result.data[0].message; 
						$('#nodata_' + $scope.key + "_" + $scope.id).show();
						$('#bar_' + $scope.key + "_" + $scope.id).hide();
						$('#spinner_' + $scope.key + "_" + $scope.id).hide();
						if($scope.drawCallback) {
							$scope.drawCallback();
						}
					} else {
						$scope.data = {
								values: [],
								ticks: []
						};
						if (result.data[0].series[0].values) {
							var dataset = {};
							if(angular.isDefined($scope.options) && angular.isDefined($scope.options.series) && $scope.options.series.percentage == true) {
								for(var x = 0; x < result.data[0].series.length; x++) {
									$scope.data.ticks.push([x, result.data[0].series[x].name]);
									for(var i = 0; i < result.data[0].series[x].values.length; i++) {
										if(angular.isUndefined(dataset[""+result.data[0].series[x].values[i].label])) {
											dataset["" + result.data[0].series[x].values[i].label] = [];
										}
										if(result.data[0].series[x].total !== 0){
											dataset[""+result.data[0].series[x].values[i].label].push([x, Math.round((result.data[0].series[x].values[i].data * 100) / result.data[0].series[x].total)]);
										}else{
											dataset[""+result.data[0].series[x].values[i].label].push([x, 0]);
										}
										if(result.data[0].series[x].values[i].data !== 0 && $scope.noData) {
											$scope.noData = false;
										}
									}
								}
							} else {
								for(var x = 0; x < result.data[0].series.length; x++) {
									$scope.data.ticks.push([x, result.data[0].series[x].name]);
									for(var i = 0; i < result.data[0].series[x].values.length; i++) {
										if(angular.isUndefined(dataset[""+result.data[0].series[x].values[i].label])) {
											dataset["" + result.data[0].series[x].values[i].label] = [];
										}
										dataset[""+result.data[0].series[x].values[i].label].push([x, result.data[0].series[x].values[i].data]);
										if(result.data[0].series[x].values[i].data !== 0 && $scope.noData) {
											$scope.noData = false;
										}
									}
								}
							}
							for(var key in dataset) {
								$scope.data.values.push({label: key, data: dataset[key] });	
							}
						}
						$scope.render();
					}
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
			$scope.render = function() {
				$('#bar_' + $scope.key + "_" + $scope.id).show();
				$('#spinner_' + $scope.key + "_" + $scope.id).hide();
				$('#nodata_' + $scope.key + "_" + $scope.id).hide();
				$scope.defaultDataset = $scope.data.values;
				if($scope.dataset) {
					$scope.defaultDataset = angular.merge($scope.defaultDataset, $scope.dataset);
				}
				$scope.defaultOptions = {
						colors: ['#0DA409', '#78EA74', '#F2DD0F', '#ED7708', '#F20A0A'],
						series: { 
							stack: true,
							bars: { 
								show: true, barWidth: 0.9, align: "center", fill: 0.8,
								numbers: {
									show: true, font: '900 12px "Roboto", sans-serif',
									formatter: function (value) { if(value > 0) {return value} else { return "" }},
									xAlign: function(x) { return x; }
								}
							},
						},
						legend: { show: true, location: 'e', placement: 'outside' },    
						xaxis: { font: { color: '#58666e' }, ticks: $scope.data.ticks, tickLength: 0 },
						yaxis: {
							font: { color: '#58666e' }, min: 0,
							tickFormatter: function(val, axis) { return parseInt(val) + ""; },
							tickDecimals: 0
						},
						grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
						tooltip: false,
						tooltipOpts: { content: '%s of %x.1 is %y.4', defaultTheme: false, shifts: { x: 0, y: 20 }},
						hooks: {
							draw: function(a,b) {
								if($scope.drawCallback) {
									$scope.drawCallback();
								}
							}
						}
				}
				if($scope.options) {
					$scope.defaultOptions = angular.merge($scope.defaultOptions, $scope.options);
				}
				$scope.renderer = true;
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("lineChart", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			refresh: "=?",
			distance: "=?",
			chartType: "@",
			filters: "=",
			dataset: "=?",
			options: "=?",
			drawCallback: "=?drawCallback"
		},
		templateUrl: 'tpl/app/directive/chart/line-chart.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.nodata = true;
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#line_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
			
				delete $scope.message;
				$scope.data = {
						values: [],
						labels: []
				};
				var searchFilters = {
					'chartTypes': [$scope.chartType],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					if(result.data[0].message) {
						$scope.data = {
								values: [],
								labels: []
						};
						$('#nodata_' + $scope.chartType + "_" + $scope.id).show();
						$('#line_' + $scope.chartType + "_" + $scope.id).hide();
						$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
						$scope.message = result.data[0].message;
						if($scope.drawCallback) {
							$scope.drawCallback();
						}
					} else {
						$scope.data = {
								values: [],
								labels: []
						};
						if (result.data[0].series[0].values) {
							for(var i = 0; i < result.data[0].series[0].values.length; i++) {
								$scope.data.values.push([i, result.data[0].series[0].values[i].data]);
								$scope.data.labels.push([i, result.data[0].series[0].values[i].label]);
								if(result.data[0].series[0].values[i].data !== 0 && $scope.noData) {
									$scope.noData = false;
								}
							}
							$scope.data.total = result.data[0].series[0].total;
							$scope.render();
						}
					}
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
			$scope.render = function() {
				$('#line_' + $scope.chartType + "_" + $scope.id).show();
				$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				$scope.defaultDataset = {
					data: $scope.data.values,
					points: { show: true, radius: 6 },
					lines: { show: true, tension: 0.45, lineWidth: 3, fill: true }
				};
				if($scope.dataset) {
					$scope.defaultDataset = angular.merge($scope.defaultDataset, $scope.dataset);
				}
				
				$scope.defaultOptions = {
					colors: ['#3ceacd'],
					series: { shadowSize: 3 },
					xaxis: { font: { color: '#58666e' }, position: 'bottom', ticks: $scope.data.labels },
					yaxis: { font: { color: '#58666e' }, tickDecimals: 0 },
					grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
					tooltip: false,
					tooltipOpts: { content: '%x.1 is %y.4', defaultTheme: false, shifts: { x: 0, y: 20 } },
					hooks: {
						draw: function(plot, canvascontext) {
							if(plot.getData().length > 0 && plot.getData()[0]['data'] != undefined) {
								$("#" + $(plot.getPlaceholder()).attr('id') + " > .data-point-label").remove();
								$.each(plot.getData()[0].data, function(i, el){
									var o = plot.pointOffset({x: el[0], y: el[1]});
									$('<div class="data-point-label">' + el[1] + '</div>').css({
										position: 'absolute',
										left: o.left - 5,
										top: o.top - 20,
										display: 'none'
									}).appendTo(plot.getPlaceholder()).fadeIn('slow');
								});
							}
							if($scope.drawCallback) {
								$scope.drawCallback();
							}
						}
					}
				}
				if($scope.options) {
					$scope.defaultOptions = angular.merge($scope.defaultOptions, $scope.options);
				}
				$scope.renderer = true;
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("splineChart", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			refresh: "=?",
			chartType: "@",
			filters: "=",
			dataset: "=?",
			options: "=?",
			drawCallback: "=?drawCallback"
		},
		templateUrl: 'tpl/app/directive/chart/spline-chart.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.noData = true;
				$scope.id = Date.now();
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#spline_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
			
				delete $scope.message;
				$scope.data = {
						series: [{
							values: [],
							labels: []
						}]
				};
				var searchFilters = {
					'chartTypes': [scope.chartType],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					if(result.data[0].message) {
						$scope.data = {
								values: [],
								labels: []
						};
						$('#nodata_' + $scope.chartType + "_" + $scope.id).show();
						$('#spline_' + $scope.chartType + "_" + $scope.id).hide();
						$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
						$scope.message = result.data[0].message;
						if($scope.drawCallback) {
							$scope.drawCallback();
						}
					} else {
						$scope.data = {
								series: [{
									values: [],
									labels: []
								}]
						};
						if (result.data[0].values) {
							for(var i = 0; i < result.data[0].series[0].values.length; i++) {
								$scope.data.values.push([i, result.data[0].series[0].values[i].data]);
								$scope.data.labels.push([i, result.data[0].series[0].values[i].label]);
								if(result.data[0].values[i].data !== 0 && $scope.noData) {
									$scope.noData = false;
								}
							}
							$scope.render();
						}
					}
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.render = function() {
				$('#spline_' + $scope.chartType + "_" + $scope.id).show();
				$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				$scope.defaultDataset = {
					data: $scope.data.values,
					points: { show: true, radius: 6 },
					splines: { show: true, tension: 0.45, lineWidth: 5, fill: 0 }
				};
				if($scope.dataset) {
					$scope.defaultDataset = angular.merge($scope.defaultDataset, $scope.dataset);
				}
				
				$scope.defaultOptions = {
					colors: ['#3ceacd'],
					series: { shadowSize: 3 },
					xaxis: { font: { color: '#58666e' }, position: 'bottom', ticks: $scope.data.labels },
					yaxis: { font: { color: '#58666e' }, tickDecimals: 0 },
					grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
					tooltip: false,
					tooltipOpts: { content: '%x.1 is %y.4', defaultTheme: false, shifts: { x: 0, y: 20 } },
					hooks: {
						draw: function(plot, canvascontext) {
							if(plot.getData().length > 0 && plot.getData()[0]['data'] != undefined) {
								$("#" + $(plot.getPlaceholder()).attr('id') + " > .data-point-label").remove()
								$.each(plot.getData()[0].data, function(i, el){
									var o = plot.pointOffset({x: el[0], y: el[1]});
									$('<div class="data-point-label">' + el[1] + '</div>').css({
										position: 'absolute',
										left: o.left - 5,
										top: o.top - 20,
										display: 'none'
									}).appendTo(plot.getPlaceholder()).fadeIn('slow');
								});
							}
							if($scope.drawCallback) {
								$scope.drawCallback();
							}
						}
					}
				};
				if($scope.options) {
					$scope.defaultOptions = angular.merge($scope.defaultOptions, $scope.options);
				}
				$scope.renderer = true;
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});

//modale comune per selezione filtri e conferma avvio creazione export
app.directive("exportModal", function($modal, $ocLazyLoad, $state, $rootScope, $filter) {
	return {
		restrict: 'A',
		scope : {
			id : "=exportModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			exportTitle: "=exportTitle"
		},
		link: function (scope, element, attrs) {
		
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.exportTitle = scope.exportTitle === undefined || scope.exportTitle
				scope.customCtrlName = scope.customCtrlName === undefined || scope.customCtrlName;
				scope.customCtrlPath = scope.customCtrlPath === undefined || scope.customCtrlPath;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/export/export-modal.html',
					controller: 'ExportCtrl',
					windowClass: 'heightAutoModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-export.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});


app.directive("qualificationsSelect", function($state, $rootScope, QualificationService){
	return {
		restrict: 'A',
		scope: {
			qualificationsSelect: "=qualificationsSelect",
		},
	
		link: function (scope, element, attrs) {
			
			scope.qualificationsSelect = [];
			QualificationService.search({}, {}, function(result){
				scope.qualificationsSelect = result.data;
			},function(error){
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
		}
	};
});

app.directive("serviceConfMasterActivityNameSelect", function($state, $rootScope, UserServiceConfService) {
	return {
		restrict: 'A',
		scope: {
			serviceConfMasterActivityNameSelect: "=serviceConfMasterActivityNameSelect",
		},
	
		link: function (scope, element, attrs) {
			
			scope.serviceConfMasterActivityNameSelect = [];
			UserServiceConfService.getServiceConfMasterActivityName({}, function(result){
				scope.serviceConfMasterActivityNameSelect = result.data;
			},function(error){
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
		}
	};
});

app.directive("monthsSelect", function($state, $rootScope, QualificationService){
	return {
		restrict: 'A',
		scope: {
			monthsSelect: "=monthsSelect",
		},
	
		link: function (scope, element, attrs) {
			scope.months = moment.months();
			for(var i = 0; scope.months.length > i; i++) {
				if(angular.isUndefined(scope.monthsSelect)) {
					scope.monthsSelect = [];
				}
				scope.monthsSelect.push({id: i, label: scope.months[i]});
			}
		}
	};
});

app.directive("yearsSelect", function($state, $rootScope, QualificationService){
	return {
		restrict: 'A',
		scope: {
			years: "=yearsSelect",
		},
	
		link: function (scope, element, attrs) {
						
			scope.years = [];
			var d = new Date();
			for(var i=d.getFullYear(); i >= 2014; i--){
				scope.years.push({id: i, label: i});
			}
		}
	};
});

app.directive("startUserServiceModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=startUserServiceModal",
			userId: "=userId",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/user-service/user-service-start.html',
					controller: function($scope, $rootScope, ServiceConfService, UserServiceConfService, MessageService) {
						
						$scope.init = function() {
							$scope.errors = {};
							$scope.detail = {};
							$scope.detail.startDate = {}
							
							ServiceConfService.getService({ id:$scope.id }, function(result) {
								$scope.detail = result.data;
								$scope.fieldPermissions = result.fieldPermissions;
							}, function(error) {
								$scope.detail = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});
						}	
						
						$scope.startUserServiceManually = function(serviceId) {
							$scope.errors = {};
							if($scope.detail.startDate == undefined) {
								$scope.errors['detail.startDate'] =  "E' necessario inserire una data di avvio del servizio";
							}
							
							if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
								var request = {
										start : $scope.detail.startDate,
								};

								UserServiceConfService.startUserServiceManually({ userId : $scope.userId, serviceId: serviceId }, request, function(result) {
									MessageService.showSuccess('Servizio avviato con successo');
									$state.reload();

								}, function(error) {
									$scope.errors = error;
									if (error.status == 404) {
										$state.go('access.not-found');
									}
									else {
										var title = 'Errore in fase di avvio servizio';
										var message = 'Alcuni dati inseriti non sono corretti';
										if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
											message = $scope.errors.data.message;
										}
										MessageService.showError(title, message);
									}
								});
								$scope.close();
							} else {
								MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
							}
						}
						
						$scope.init();
					},
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
				
			});
		}
	};
});

app.directive("userTrainingSessions", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			participantId : "=participantId",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		templateUrl: 'tpl/app/directive/trainingsession/user-training-sessions.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);

			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-user-training-session.js']).then( function() {
				var controller = $controller('TrainingSessionController', { $scope: scope });
			});
		}
	};
});

app.directive("userTrainingSessionsModal", function($modal, $ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			participantId : "=participantId",
			preFn : "=?preFn",
			postFn : "=?postFn"
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/trainingsession/user-training-sessions.html',
					controller: 'TrainingSessionController',
					windowClass: 'largeModal',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-user-training-session.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function() {
					scope.modal.close();
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});


app.directive("userTrainingSessionModal", function($modal, $ocLazyLoad, $state, $rootScope, $filter) {
	return {
		restrict: 'A',
		scope : {
			participantId : "=participantId",
			id : "=userTrainingSessionModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {

			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/trainingsession/user-training-session.html',
					controller: 'TrainingSessionDetailController',
					windowClass: 'largeModal',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-user-training-session.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function() {
					scope.modal.close();
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
app.directive("trainingSubjectSearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			selection : "=?selection",
			selected : "=?trainingSubjectSearchModal",
			searchFilters : "=?filters",
			objectFilters: "=?objectFilters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn",
			departmentId : "=?"
			
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/training-subject-search.html',
					controller: 'TrainingSubjectController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-training-subject.js'] );
									}
							)
						}
					}
				});
				
				scope.onSelect = function(selected) {
					if(!scope.multipleList) {
						for(var i = 0; i < scope.selected.length; i++) {
							if(scope.selected[i].id !== selected.id) {
								scope.selected[i].selected = false;
							} 
						}
						scope.selected = [selected];
					}
				}
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						scope.trainingSubjectId = scope.selected;
					} else {
						scope.trainingSubjectId = [];
						for(var i = 0; scope.selected.length>i; i++){
							scope.trainingSubjectId.push(scope.selected[i].id);
						}
					}
					if(scope.postFn) {
						scope.postFn()
					}
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugPickUpModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=drugPickUpModal",
			complementaryId : "=?complementaryId",
			activitiForm : "=?activitiForm",
			complementaryType : "=?complementaryType",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-pick-up/drug-pick-up.html',
					controller: 'DrugPickUpDetailController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-pick-up.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugPickUpItemModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			drugPickUp: "=",
			itemIndex: "=",
			id: "=drugPickUpItemModal",
			postFn: "=?postFn",
			postFnDismiss: "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-pick-up/drug-pick-up-item.html',
					controller: 'DrugPickUpItemController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-pick-up-item.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					console.log(scope.drugPickUpItem);
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						console.log(scope.drugPickUpItem);
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugDeliveryModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=drugDeliveryModal",
			complementaryId : "=?complementaryId",
			activitiForm : "=?activitiForm",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-delivery/drug-delivery.html',
					controller: 'DrugDeliveryDetailController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-delivery.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugDeliveryItemModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			drugDelivery: "=",
			itemIndex: "=",
			id: "=drugDeliveryItemModal",
			postFn: "=?postFn",
			postFnDismiss: "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-delivery/drug-delivery-item.html',
					controller: 'DrugDeliveryItemController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-delivery-item.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					console.log(scope.drugDeliveryItem);
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						console.log(scope.drugDeliveryItem);
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugDisposalModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=drugDisposalModal",
			complementaryId : "=?complementaryId",
			activitiForm : "=?activitiForm",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-disposal/drug-disposal.html',
					controller: 'DrugDisposalDetailController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-disposal.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugDisposalItemModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			drugDisposal: "=",
			itemIndex: "=",
			id: "=drugDisposalItemModal",
			postFn: "=?postFn",
			postFnDismiss: "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drug-disposal/drug-disposal-item.html',
					controller: 'DrugDisposalItemController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-disposal-item.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					console.log(scope.drugDisposalItem);
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						console.log(scope.drugDisposalItem);
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("exportPdf", function(DynamicPdfService, $modal, $rootScope, $sce, $cookies) {
	return {
		restrict: 'A',
		scope : {
			exportPdf: "@exportPdf",
			template: "@template",
			filters: "=?",
			parameterMap: "=?",
			ignoreLabels: "=?",
			filename: "@",
			withoutCharts: "=?"
		},
		link: function (scope, element, attrs) {
		
			element.bind("click", function(element) {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: scope.template,
					controller: function($scope, $http, RESTURL) {
						scope.init = function() {
							scope.complete = 0;
							if(scope.withoutCharts) {
								scope.export();
							}
						}
							
						scope.drawCallback = function() {
							scope.complete++;
							scope.charts = $($('.export')[0]).querySelectorAll('[panel-chart]');
							if(scope.complete == scope.charts.length) {
								scope.export();
							}
						}
						
						scope.export = function()  {
							var content = {};
							var canvasList = [];
							if(!scope.withoutCharts) {
								for(var i = 0; i < scope.charts.length; i++) {
									var chart = scope.charts[i]
									if(scope.ignoreLabels) {
										$(chart.getElementsByClassName('data-point-label')).attr('data-html2canvas-ignore', 'true');
										$(chart.getElementsByClassName('flot-text')).attr('data-html2canvas-ignore', 'true');
									}
									html2canvas(chart, {scale: 5}).then(function(canvas) {
							        	canvasList.push(canvas);
							        	// modo per tirarsi su il contesto corretto e il relativo nome
							        	var t = $(scope.charts[canvasList.length - 1]).attr('id');
							        	content[t] = canvas.toDataURL("image/png");
							        	if(canvasList.length == scope.charts.length) {
							        		var request = { "contentMap" : content, "parameterMap": scope.parameterMap, filename: scope.filename };
							        		DynamicPdfService.generatePdf({ type: scope.exportPdf }, request, function(response) {
							        			$scope.pdf = RESTURL.DYNAMIC_PDF + response.data;
							        		}, function(error) {
							        			
							        		});
							        	}
									});
								}
							} else {
								var request = { "contentMap" : {}, "parameterMap": scope.parameterMap, filename: scope.filename };
				        		DynamicPdfService.generatePdf({ type: scope.exportPdf }, request, function(response) {
				        			$scope.pdf = RESTURL.DYNAMIC_PDF + response.data;
				        		}, function(error) {
				        			
				        		});
							}
						}
						
						scope.init();
					}
				});
			});
		}
	};
});

app.directive("patientTemporaryHomeAddressList", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId : '=patientId',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/patient-temporary-home-address/patient-temporary-home-addresses.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'PATIENT_ID' : scope.patientId }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-patient-temporary-home-address.js']).then( function() {
				var controller = $controller('PatientTemporaryHomeAddressListController', { $scope: scope });
			});
		}
	}
});

app.directive("patientTemporaryHomeAddressModal", function($modal, $window, $timeout, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			patientId : "=patientId",
			owner : "=?owner",
			id : "=patientTemporaryHomeAddressModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/patient-temporary-home-address/patient-temporary-home-address.html',
					controller: 'PatientTemporaryHomeAddressDetailController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-patient-temporary-home-address.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});
app.directive("pharmacoVigilanceEventList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?pharmacoVigilanceEventList",
			patientId : "=?patientId",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			filterStoreKey : "@?"
		},
		templateUrl: 'tpl/app/directive/pharmaco-vigilance/events.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			scope.searchFilters = { 
					filters : { 'PATIENT_ID' : scope.patientId }
			};
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/crm-pharmacovigilanceevent.js']).then( function() {
				var controller = $controller('PharmacoVigilanceEventController', { $scope: scope });
			});
		}
	}
});
app.directive("pharmacoVigilanceEventModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=?pharmacoVigilanceEventModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			parentEvent : '=?parentEvent',
			patientId : "=?patientId",
		},
		link: function (scope, element, attrs) {
			if(scope.id == undefined) 
				scope.id='';
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/pharmaco-vigilance/event.html',
					controller: 'PharmacoVigilanceEventController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','angularFileUpload']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-pharmacovigilanceevent.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function() {
					scope.modal.close();
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("bloodSamplingList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-blood-sampling.js' ];
		$scope.templateUrl = 'tpl/app/directive/bloodsampling/blood-sampling-list.html';
		$scope.controllerName = 'BloodSamplingListCtrl';
		$scope.modalSize = 'lg';
	} });
});

app.directive("bloodSamplingModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=bloodSamplingModal",
			complementaryId : "=?complementaryId",
			patientId : "=?patientId",
			bloodDrawingCenter : "=?bloodDrawingCenter",
			activitiForm : "=?activitiForm",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/bloodsampling/blood-sampling.html',
					controller: 'BloodSamplingDetailController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-blood-sampling.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientWeightList", function(baseListDirective) {
	baseListDirective[0].scope['patientId'] = "=?";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-patient-weight.js' ];
		$scope.templateUrl = 'tpl/app/directive/user/patient/weight/patient-weight-list.html';
		$scope.controllerName = 'PatientWeightListCtrl';
		$scope.modalSize = 'lg';
	} });
});

app.directive("patientWeightDetail", function(baseDetailDirective) {
	baseDetailDirective[0].scope['complementaryId'] = "=?complementaryId";
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-patient-weight.js' ];
			$scope.templateUrl = 'tpl/app/directive/user/patient/weight/patient-weight-detail.html';
			$scope.controllerName = 'PatientWeightDetailCtrl';
			$scope.modalSize = 'md';
		}
	});
});

app.directive("otherUserTasksModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=?otherUserTasksModal",
			userActivityId : "=?userActivityId"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.userId = scope.userId;
				scope.userActivityId = scope.userActivityId;
				scope.closeModal = function() {
					if(scope.modal) {
						scope.modal.close();
					}
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/activiti/tasks.html',
					controller: 'OtherUserTaskListController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-task.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("taskDetail", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=?taskDetail",
			popup : "=?popup",
			postFn: "=?postFn",
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = scope.popup;
				if(scope.popup) {
					
					scope.closeModal = function() {
						if(scope.modal) {
							scope.modal.close();
						}
					}
					
					scope.modal =  $modal.open({
						scope: scope,
						templateUrl: 'tpl/app/activiti/task.html',
						controller: 'TaskDetailController',
						windowClass: 'largeModal',
						size: 'lg',
						resolve: {
							deps: function($ocLazyLoad) {
								return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table', 'ngTagsInput']).then(
										function() {
											return $ocLazyLoad.load( ['js/controllers/directives/crm-task.js'] );
										}
								)
							}
						}
					});

					scope.modal.result.then(function() {
						if (scope.postFn) {
							scope.postFn();
						}
						console.log('Modal closed: ' + new Date());
					}, function () {
						console.log('Modal dismissed: ' + new Date());
					});
				} else {
					$state.go('app.task', { id: scope.id });
				}
			});
		}
	};
});

app.directive("drugAdministrationDetail", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/user-activity/complementary/drug-administration.html',
					windowClass: 'largeModal',
					size: 'lg'
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("pharmacyTrainingSessionList", function(baseListDirective) {
	baseListDirective[0].scope['pharmacyId'] = "=pharmacyId";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-pharmacy-training-session.js'];
		$scope.templateUrl = 'tpl/app/directive/pharmacy/trainingsession/training-session-list.html';
		$scope.controllerName = 'PharmacyTrainingSessionListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("pharmacyTrainingSessionDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-pharmacy-training-session.js' ];
			$scope.templateUrl = 'tpl/app/directive/pharmacy/trainingsession/training-session-detail.html';
			$scope.controllerName = 'PharmacyTrainingSessionDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("drugList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-drug.js'];
		$scope.templateUrl = 'tpl/app/directive/drug/drug-list.html';
		$scope.controllerName = 'DrugListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("drugDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-drug.js' ];
			$scope.templateUrl = 'tpl/app/directive/drug/drug-detail.html';
			$scope.controllerName = 'DrugDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("avatarList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['smart-table', 'js/controllers/directives/crm-avatar.js'];
		$scope.templateUrl = 'tpl/app/directive/avatar/avatar-list.html';
		$scope.controllerName = 'AvatarListCtrl';
		$scope.modalSize = 'lg';
	} });
});

app.directive("personalEventDetail", function(baseDetailDirective) {
	baseDetailDirective[0].scope['relatedUserId'] = "=?relatedUserId";
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-personal-event.js' ];
			$scope.templateUrl = 'tpl/app/directive/personalevent/personal-event-detail.html';
			$scope.controllerName = 'PersonalEventDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("userEventList", function(baseListDirective) {
	baseListDirective[0].scope['relatedUserId'] = "=?relatedUserId";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['smart-table', 'js/controllers/directives/crm-user-event.js'];
		$scope.templateUrl = 'tpl/app/directive/userevent/user-event-list.html';
		$scope.controllerName = 'UserEventListCtrl';
		$scope.modalSize = 'lg';
	} });
});

app.directive("userEventDetail", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=userEventDetail",
			type: "=type",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);
				switch(scope.type) {
			    case 'PERSONAL_EVENT':
			        scope.userController = "PersonalEventDetailCtrl";
			        scope.userControllerPath = ["js/controllers/directives/crm-personal-event.js"];
			        scope.userTemplate = "tpl/app/directive/personalevent/personal-event-detail.html";
			        break;
			    case 'USER_ACTIVITY':
			    	 scope.userController = "UserActivityController";
				        scope.userControllerPath = ["js/controllers/directives/crm-user-activity.js"];
				        scope.userTemplate = "tpl/app/directive/user-activity/user-activity.html";
			        break;
			    default:
			        return;
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: scope.userTemplate,
					controller: scope.userController,
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( scope.userControllerPath );
								}
							)
						}
					}
				});

				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugPickUpDeliveryDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-drug-pick-up-delivery.js' ];
			$scope.templateUrl = 'tpl/app/directive/drugpickupdelivery/drug-pick-up-delivery-detail.html';
			$scope.controllerName = 'DrugPickUpDeliveryDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("drugPickUpDeliveryModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=drugPickUpDeliveryModal",
			complementaryId : "=?complementaryId",
			activitiForm : "=?activitiForm",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/drugpickupdelivery/drug-pick-up-delivery-detail.html',
					controller: 'DrugPickUpDeliveryDetailCtrl',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-drug-pick-up-delivery.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("drugPickUpUserComplementaryForm", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			activityForm : "=activityForm",
			complementaryForm: "=complementaryForm",
			status: "@status",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);

				scope.form = angular.copy(scope.complementaryForm);
				scope.formActivity = angular.copy(scope.activityForm);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: "tpl/app/directive/activiti/drugpickupdelivery/" + scope.status + "-drug-pick-up-delivery.html",
					controller: "DrugPickUpDeliveryFormController",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(["js/controllers/directives/crm-drug-pick-up-delivery-form.js"]);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
					angular.merge(scope.complementaryForm, scope.form);
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
				});
			});
		}
	};
});

app.directive("defaultActivityValueList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-default-activity-value.js'];
		$scope.templateUrl = 'tpl/app/directive/defaultactivityvalue/default-activity-value-list.html';
		$scope.controllerName = 'DefaultActivityValueListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("defaultActivityValueDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-default-activity-value.js' ];
			$scope.templateUrl = 'tpl/app/directive/defaultactivityvalue/default-activity-value-detail.html';
			$scope.controllerName = 'DefaultActivityValueDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("logisticSearchModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			selection : "=?selection",
			selectedList : "=?logisticSearchModal",
			searchFilters : "=?filters",
			multipleList : "=?multipleList",
			preFn : "=?preFn",
			postFn : "=?postFn",
			logisticId: "=?"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.selection = scope.selection || true;
				scope.multipleList = scope.multipleList === undefined || scope.multipleList;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selectedList!=undefined) {
						scope.selected = [];
						scope.selected.push(scope.selectedList);
					}
					else {
						scope.selected = []
					}
				}
				else {
					scope.selected = scope.selectedList || [];
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/logistic/logistics.html',
					controller: 'LogisticController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-logistic.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						scope.logisticId = scope.selectedList.id;
					} else {
						scope.selectedList = scope.selected;
						scope.logisticId = [];
						for(var i = 0; scope.selectedList.length>i; i++){
							scope.logisticId.push(scope.selectedList[i].id);
						}
					}
					if(scope.postFn) {
						$timeout(function() {
							scope.postFn();
						});
					}
				}, function () {
					if (!scope.multipleList) {
						scope.selectedList = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("masterActivityConfList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-master-activity-conf.js'];
		$scope.templateUrl = 'tpl/app/directive/masteractivityconf/master-activity-conf-list.html';
		$scope.controllerName = 'MasterActivityConfListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("medicalCenterList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-medical-center.js'];
		$scope.templateUrl = 'tpl/app/directive/medicalcenter/medical-center-list.html';
		$scope.controllerName = 'MedicalCenterListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("patientMaterialList", function(baseListDirective) {
	baseListDirective[0].scope['patientId'] = "=?";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
			$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-patient-material.js'];
			$scope.templateUrl = 'tpl/app/directive/patientmaterial/patient-material-list.html';
			$scope.controllerName = 'PatientMaterialListCtrl';
			$scope.modalSize = 'xl';
		} });
});

app.directive("patientMaterialDetail", function(baseDetailDirective) {
	baseDetailDirective[0].scope['patientId'] = "=?";
	baseDetailDirective[0].scope['detail'] = "=?";
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-patient-material.js' ];
			$scope.templateUrl = 'tpl/app/directive/patientmaterial/patient-material-detail.html';
			$scope.controllerName = 'PatientMaterialDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("patientList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-patient.js'];
		$scope.templateUrl = 'tpl/app/directive/user/patient/patient-list.html';
		$scope.controllerName = 'PatientListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("hcpList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-hcp.js'];
		$scope.templateUrl = 'tpl/app/directive/user/hcp/hcp-list.html';
		$scope.controllerName = 'HcpListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("taskList", function(baseListDirective) {
	baseListDirective[0].scope['type'] = "=";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-task.js'];
		$scope.templateUrl = 'tpl/app/directive/task/task-list.html';
		switch($scope.type) {
	    case 'todo':
	    	$scope.controllerName = 'MyTaskListController';
	    	break;
	    	
	    case 'not-assigned':
	    	$scope.controllerName = 'NotAssignedTaskListController';
	    	break;
	    	
	    case 'assigned-to-others':
	    	$scope.controllerName = 'AssignedToOthersTaskListController';
	    	break;
	    	
	    case 'other-user-task':
	    	$scope.controllerName = 'OtherUserTaskListController';
	    	break;
	    	
	    default:
	    	return;
		}
		$scope.modalSize = 'xl';
	} });
});

app.directive("userActivityList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-user-activity.js'];
		$scope.templateUrl = 'tpl/app/directive/user/activity/user-activity-list.html';
		$scope.controllerName = 'UserActivityListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("supplierList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-supplier.js'];
		$scope.templateUrl = 'tpl/app/directive/supplier/supplier-list.html';
		$scope.controllerName = 'SupplierListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("logisticList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-logistic.js'];
		$scope.templateUrl = 'tpl/app/directive/logistic/logistic-list.html';
		$scope.controllerName = 'LogisticListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("internalStaffUserList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-internal-staff-user.js'];
		$scope.templateUrl = 'tpl/app/directive/internalstaffuser/internal-staff-user-list.html';
		$scope.controllerName = 'InternalStaffUserListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("customerUserList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-customer.js'];
		$scope.templateUrl = 'tpl/app/directive/user/customer/customer-user-list.html';
		$scope.controllerName = 'CustomerUserListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("userMessageList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-user-message.js'];
		$scope.templateUrl = 'tpl/app/directive/user/message/user-message-list.html';
		$scope.controllerName = 'UserMessageListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("exportList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-export.js'];
		$scope.templateUrl = 'tpl/app/directive/export/export-list.html';
		$scope.controllerName = 'ExportListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("pharmacyList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-pharmacy.js'];
		$scope.templateUrl = 'tpl/app/directive/pharmacy/pharmacy-list.html';
		$scope.controllerName = 'PharmacyListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("drugPickUpList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-drug-pick-up.js'];
		$scope.templateUrl = 'tpl/app/directive/drug-pick-up/drug-pick-up-list.html';
		$scope.controllerName = 'DrugPickUpListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("drugDeliveryList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-drug-delivery.js'];
		$scope.templateUrl = 'tpl/app/directive/drug-delivery/drug-delivery-list.html';
		$scope.controllerName = 'DrugDeliveryListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("drugDisposalList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-drug-disposal.js'];
		$scope.templateUrl = 'tpl/app/directive/drug-disposal/drug-disposal-list.html';
		$scope.controllerName = 'DrugDisposalListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("materialDeliveryList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-material-delivery.js'];
		$scope.templateUrl = 'tpl/app/directive/material/material-delivery/material-delivery-list.html';
		$scope.controllerName = 'MaterialDeliveryListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("materialDamageList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-material-damage.js'];
		$scope.templateUrl = 'tpl/app/directive/material/material-damage/material-damage-list.html';
		$scope.controllerName = 'MaterialDamageListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("supplierUserList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-supplier-user.js'];
		$scope.templateUrl = 'tpl/app/directive/user/supplier/supplier-user-list.html';
		$scope.controllerName = 'SupplierUserListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("driedBloodSpotTestList", function(baseListDirective) {
	baseListDirective[0].scope['hidePatientFilter'] = "=?hidePatientFilter";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-dried-blood-spot-test.js'];
		$scope.templateUrl = 'tpl/app/directive/dbst/dried-blood-spot-test-list.html';
		$scope.controllerName = 'DriedBloodSpotTestListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("driedBloodSpotTestDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-dried-blood-spot-test.js' ];
		$scope.templateUrl = 'tpl/app/directive/dbst/dried-blood-spot-test-data.html';
		$scope.controllerName = 'DriedBloodSpotTestDetailCtrl';
		$scope.modalSize = 'lg';
		}
	});
});
//Necessario per aprire il dettaglio del DBST dalle userActivity -> complementari
app.directive("driedBloodSpotTestModal", function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			complementaryId : "=driedBloodSpotTestModal",
			complementaryType : "=complementaryType",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.isModal = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/dbst/dried-blood-spot-test-data.html',
					controller: 'DriedBloodSpotTestModalCtrl',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-dried-blood-spot-test.js'] );
									}
							)
						}
					}
				});
				
				scope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					if(scope.postFnDismiss) {
						scope.postFnDismiss();
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("patientMaterialHandlingUserComplementaryForm", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			activityForm : "=activityForm",
			complementaryForm: "=complementaryForm",
			status: "@status",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);

				scope.form = angular.copy(scope.complementaryForm);
				scope.formActivity = angular.copy(scope.activityForm);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: "tpl/app/directive/activiti/patientmaterialhandling/"  + scope.status + "/" + scope.status + "-patient-material-handling.html",
					controller: "PatientMaterialHandlingFormController",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(["js/controllers/directives/crm-patient-material-handling-form.js"]);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
					scope.complementaryForm.entity.pickUpMaterials = scope.form.pickUpMaterials;
					scope.complementaryForm.entity.newMaterials = scope.form.newMaterials;
					angular.merge(scope.complementaryForm, scope.form);
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
				});
			});
		}
	};
});

app.directive("patientMaterialHandlingList", function(baseListDirective) {
	baseListDirective[0].scope['patientMaterialId'] = "=?";
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-patient-material-handling.js' ];
		$scope.templateUrl = 'tpl/app/directive/patientmaterialhandling/patient-material-handling-list.html';
		$scope.controllerName = 'PatientMaterialHandlingListCtrl';
		$scope.modalSize = 'lg';
	} });
});


app.directive("patientMaterialHandlingDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-patient-material-handling.js' ];
			$scope.templateUrl = 'tpl/app/directive/patientmaterialhandling/patient-material-handling-detail.html';
			$scope.controllerName = 'PatientMaterialHandlingDetailCtrl';
			$scope.modalSize = 'md';
		}
	});
});
app.directive("questionnaireScoreQuestionList", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			questionnaireScoreQuestionList : "=questionnaireScoreQuestionList",
		},
		templateUrl: 'tpl/app/directive/questionnaire/question-list-score.html',
		link: function (scope, element, attrs) {
			
			scope.values = scope.questionnaireScoreQuestionList;
			
			
		}
	}
});

app.directive("userActivateModal", function userActivateModalDirective($modal, $state, $rootScope, NotificationConfService, MessageService, FormUtilService, $timeout, PermissionService) {
	return {
		restrict: 'A',
		scope : {
			user : "=?",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			fieldPermissions : "=?"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.contactMethodsType = {};
				scope.detailActivate = {};
				scope.enabledTypes = [];
				scope.errors = {};
				
				//can-invite="permissions['ACTIVATE'] && user.email!=undefined && user.nickName==undefined"
				PermissionService.getAll({ entityType : scope.user.type }, function(result) {
					scope.canInvite = result.data['INVITE']==true && scope.user.nickName==undefined;
				},function(error) {
					console.log("Errore nel recupero dei permessi");
					scope.canInvite = false;
				});
				
				if(angular.isDefined(scope.user.preferredContacts)) {
					for(var k=0; k<scope.user.preferredContacts.length; k++) {
						scope.contactMethodsType[scope.user.preferredContacts[k].type]=scope.user.preferredContacts[k].preference;
					}
				}
				
				scope.validateActivateNotification = function() {
					var errors = {};
					var form = {
						formProperties : []
					};
					
					if(scope.enabledTypes.indexOf('EMAIL')>=0 && scope.user.email!=undefined && scope.user.email!='') {
						form.formProperties.push({ id:'detailActivate.emailNotification', value: scope.detailActivate.emailNotification, type:'boolean', required: true });
					}
					if(scope.enabledTypes.indexOf('SMS')>=0 && scope.user.cellPhoneNumber!=undefined && scope.user.cellPhoneNumber!='') {
						form.formProperties.push({ id:'detailActivate.smsNotification', value: scope.detailActivate.smsNotification, type:'boolean', required: true });
					}
					
					if(scope.canInvite && scope.user.email!=undefined && scope.user.email!='' && 'binded'!=scope.detailActivate.accountType) {
						form.formProperties.push({ id:'detailActivate.invitedUser', value: scope.detailActivate.invitedUser, type:'boolean', required: true });
					}
					
					errors = FormUtilService.validateForm(form);
					return errors;
				}
				
				scope.close = function(result) {
					$timeout(function() {
						scope.errors = scope.validateActivateNotification();
						
						if(angular.isUndefined(scope.errors) || angular.equals(scope.errors, {})) {
							scope.modal.close(result);
						}
						else {
							var errMsg = 'Indicare i modi di avviso attivazione e invito alla registrazione';
							if('binded'==scope.detailActivate.accountType)
								errMsg = 'Indicare i modi di avviso attivazione';
							MessageService.showError('Errore in fase di salvataggio',errMsg);
						}
					},10);
				}
				
				scope.dismiss = function() {
					scope.modal.dismiss();
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: "tpl/app/directive/user/user-activate.html",
					controller: function($rootScope,$scope,NotificationConfService, MessageService,FormUtilService) {
						
						$scope.activateSearchFilters = {
							filters: {	
								'PORTAL_ID' : $rootScope.relatedPortalNames,
								'REGISTERED' : true
							}
						};
						
						$scope.customOptions = { 'hideCurrentPortal' : true };
						$scope.detailActivate.selectedBindedAccount = undefined;
						
						$scope.unbindAccount = function() {
							$scope.detailActivate.selectedBindedAccount = undefined;
							$scope.detailActivate.invitedUser = false;
						}
						
						$scope.relatedPortals = $rootScope.relatedPortals;
						$scope.detailActivate.accountType = ($scope.detailActivate.accountType==undefined) ? 'unbinded' : $scope.detailActivate.accountType;
						
						
						NotificationConfService.search({},{ filters: {'CODE':'ACTIVATE', 'RECIPIENT_TYPE': $scope.user.type }}, function(result) {
							if(result.data!=undefined) {
								for(var i=0; i<result.data.length; i++) {
									$scope.enabledTypes.push(result.data[i].type);
								}
							}
						}, function(error) {
							MessageService.showError('Errore','Problema nel recupero del tipo di notifiche abilitate');
						});
						
					},
					size: 'md',
				});
				
				
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn(scope.detailActivate);
					}
					
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
				});
			});
		}
	};
});

app.directive("linkList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['smart-table', 'js/controllers/directives/crm-link.js'];
		$scope.templateUrl = 'tpl/app/directive/link/link-list.html';
		$scope.controllerName = 'LinkListController';
		$scope.modalSize = 'lg';
	} });
});

app.directive("driedBloodSpotTestSamplingUserComplementaryForm", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			activityForm : "=activityForm",
			complementaryForm: "=complementaryForm",
			status: "@status",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);

				scope.form = angular.copy(scope.complementaryForm);
				scope.formActivity = angular.copy(scope.activityForm);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: "tpl/app/directive/activiti/driedbloodspottest/" + scope.status + "-dried-blood-spot-test-sampling.html",
					controller: "DriedBloodSpotTestSamplingFormController",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(["js/controllers/directives/crm-dried-blood-spot-test-sampling-form.js"]);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
					angular.merge(scope.complementaryForm, scope.form);
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
				});
			});
		}
	};
});

app.directive("driedBloodSpotTestPickUpUserComplementaryForm", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			activityForm : "=activityForm",
			complementaryForm: "=complementaryForm",
			status: "@status",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);

				scope.form = angular.copy(scope.complementaryForm);
				scope.formActivity = angular.copy(scope.activityForm);
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: "tpl/app/directive/activiti/driedbloodspottest/" + scope.status + "-dried-blood-spot-test-pick-up.html",
					controller: "DriedBloodSpotTestPickUpFormController",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(["js/controllers/directives/crm-dried-blood-spot-test-pick-up-form.js"]);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
					angular.merge(scope.complementaryForm, scope.form);
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
				});
			});
		}
	};
});

app.directive("patientDossier", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			userId: '=userId',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/user/patient/patient-dossier-detail.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['smart-table','js/controllers/directives/crm-patient-dossier.js']).then( function() {
				var controller = $controller('PatientDossierController', { $scope: scope });
			});
		}
	}
});

app.directive("materialList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
			$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/crm-material.js'];
			$scope.templateUrl = 'tpl/app/directive/material/material-list.html';
			$scope.controllerName = 'MaterialListCtrl';
			$scope.modalSize = 'xl';
		} });
});

app.directive("virtualMeetingList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-virtual-meeting.js'];
		$scope.templateUrl = 'tpl/app/directive/virtual-meeting/virtual-meeting-list.html';
		$scope.controllerName = 'VirtualMeetingListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("virtualMeetingDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-virtual-meeting.js' ];
		$scope.templateUrl = 'tpl/app/directive/virtual-meeting/virtual-meeting-data.html';
		$scope.controllerName = 'VirtualMeetingDetailCtrl';
		$scope.modalSize = 'lg';
		}
	});
});

app.directive("organizeVirtualMeeting", function($ocLazyLoad, $modal, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope: {
			postFn : "=?postFn"
		},
		link: function(scope, element, attrs) {
			element.bind("click", function() {
				scope.isModal = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/virtual-meeting/virtual-meeting-organizer.html',
					controller: "VirtualMeetingOrganizerCtrl",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(['js/controllers/directives/crm-virtual-meeting.js']);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					
				});
			});
		}
	}
});

app.directive("requestVirtualMeeting", function($ocLazyLoad, $modal, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope: {
			postFn : "=?postFn"
		},
		link: function(scope, element, attrs) {
			element.bind("click", function() {
				scope.isModal = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/virtual-meeting/virtual-meeting-requester.html',
					controller: "VirtualMeetingRequesterCtrl",
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(['js/controllers/directives/crm-virtual-meeting.js']);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					
				});
			});
		}
	}
});

/* SEZIONE ATTIVITA' PAZIENTI RICHIESTA PER SANOFI(TUT + QPT) DA UTENTE MEDICO 
 * targetUserType: tipo di utenti su cui fare la ricerca delle attività
 * options: opzioni per il calendario
 * 	businessHours => Mostra solo slot dalle 7 alle 20 se true
 * 	hiddenDays => Array con giorni da nascondere [0 = domenica]
 *  allDaySlot => Mostra slot "Tutto il giorno" se true 
 *  
 * */
app.directive("patientActivitiesCalendar",function(RESTURL,$compile,$rootScope,MessageService,
		GatewayUserActivityService,GatewayDrugService,PortalService,UserService,$filter,$timeout,uiCalendarConfig,$modal,$ocLazyLoad) {
	return {
		template: '<div id="calendar-container" class="mTCalendar"><div ng-include="\'tpl/app/directive/calendar-planning/patient-activities-main.html\'"></div></div>',
		replace: true,
		scope : {
			targetUserType : "=",
			options : "=?",
			form : '='
		},
		link: function(scope, el, attrs, ngModel) {
			
			var element = el;
			
			scope.getRelatedPortals = function() {
				//Gestione portali collegati (es. Tutor-QuiperTe)
				scope.relatedPortals = $rootScope.relatedPortals;
				if(scope.relatedPortals==undefined || scope.relatedPortals.length==0) {
					//Cerca i portali collegati a questo (solo per i programs, l'admin non lo fa)
					PortalService.getRelatedPortals(function(result) {
						scope.relatedPortals = result.data;
						scope.relatedAccessiblePortals = scope.getRelatedAccessiblePortals();
						scope.portalsList = angular.copy(scope.relatedAccessiblePortals);
						scope.portalsList.push($rootScope.programProperties.portalId);
					}, function(error) {
						console.log('Cannot retrieve related portals!');
					});
				}
				else {
					scope.relatedAccessiblePortals = scope.getRelatedAccessiblePortals();
					scope.portalsList = angular.copy(scope.relatedAccessiblePortals);
					scope.portalsList.push($rootScope.programProperties.portalId);
				}
			}	
			
			scope.getUserPortals = function() {
				scope.userPortals = $rootScope.userPortals;
				if(scope.userPortals==undefined) {
					UserService.getLoggedUserPortals({}, function(result) {
						scope.userPortals = result.data;
						scope.getRelatedPortals();
					}, function(error) {
						console.log('Cannot retrieve logged user portals!');
					});
				}
				else {
					scope.getRelatedPortals();
				}
			}
			
			scope.getRelatedAccessiblePortals = function() {
				var portals = [];
				if(scope.relatedPortals!=undefined && scope.relatedPortals.length>0) {
					if(scope.userPortals!=undefined && scope.userPortals.length>0) {
						for(var i=0; i<scope.relatedPortals.length; i++) {
							for(var j=0; j<scope.userPortals.length; j++) {
								if(scope.userPortals[j].id==scope.relatedPortals[i]) {
									portals.push(scope.relatedPortals[i]);
								}
							}
						}
					}
				}
				return portals;
			}
			
			scope.getUserPortals();
			//Configurazione dei parametri relativi a fullCalendar
			scope.configCalendar = function() {
				scope.uiConfig = {
					calendar:{
						height: 'auto',
						selectHelper: false,
						eventOverlap: true,
						selectConstraint:{
					      start: '00:01', 
					      end: '23:59', 
					    },
					    defaultView: 'listMonth',
					    displayEventTime : false,
						minTime: scope.options && scope.options.businessHours ? scope.options.businessHours.minTime : undefined,
						maxTime: scope.options && scope.options.businessHours ? scope.options.businessHours.maxTime : undefined,
						selectable: true,
						unselectAuto: true,
						hiddenDays: scope.options.hiddenDays,
						views: {
							listDay: {
								titleFormat: 'DD MMM YYYY',
								displayEventTime : true,
								displayEventEnd: true,
								noEventsMessage: 'Nessun dato da visualizzare',
								listDayFormat: 'dddd',
								listDayAltFormat: false
							},
							listWeek: {
								titleRangeSeparator: '/',
								titleFormat: 'DD MMM YYYY',
								columnFormat: 'ddd D/M',
								displayEventTime : true,
								displayEventEnd: true,
								noEventsMessage: 'Nessun dato da visualizzare',
								listDayFormat: 'dddd',
								listDayAltFormat: 'DD MMMM YYYY'
							},
							listMonth: {
								titleFormat: 'MMMM YYYY',
								displayEventTime : true,
								displayEventEnd: true,
								noEventsMessage: 'Nessun dato da visualizzare',
								listDayFormat: 'dddd',
								listDayAltFormat: 'DD MMMM YYYY'
							}
						},
						select: function(start, end, jsEvent, view) {
							scope.createNewEvent(start, end, jsEvent, view);
						},
						dayClick: function(date, jsEvent, view, resourceObj) {
							
						},
						header:{
							left: 'prev',
							center: 'title',
							right: 'next'
						},
						timeFormat: 'HH:mm',
						lang: 'it',
						locale: 'it',
						allDaySlot: (scope.options.allDaySlot!=undefined) ? scope.options.allDaySlot : false,
						timezone: 'local',
						slotEventOverlap: false,
						dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
						eventClick: scope.eventClick,
						eventRender: function(event, element, view) {
//	 						if(view.name!='listMonth') {
//								if(event.userColors!=undefined && event.userColors!=null) {
//									for(var e=0; e<event.userColors.length; e++) {
//										var margin = 'margin-right: 0px;';
//										if(e==0) {
//											margin = 'margin-right: 2px;';
//										}
//										if(event.userColors[e]!=undefined)
//											element.prepend('<div style="background-color: '+event.userColors[e]+'; float: left; display: inline; '+margin+'">&nbsp;&nbsp;</div>');
//									}
//								}
//	 						}
			 				$compile(element)(scope);
						},
						viewRender: function(view, element) {
							scope.loadEvents();
						},
						eventAfterAllRender: scope.afterRender
					}
				};
			}
			
			scope.clearFilters = function(){
				scope.userEventFilters = {};
			}
			
			scope.loadMasterActivityTypesForRecipient = function() {
				//carico i tipi attività da visualizzare nel menu a tendina del filtro
				//mantengo il filtro su RECIPIENT_TYPE perchè su questa pagina mi interessano solo le attività dei pazienti (che il medico può vedere)
				//tengo anche il filtro per PORTAL_ID perchè può essere che sugli altri portali ci siano tipi attività in più
				if(scope.targetUserType != undefined && angular.isDefined(scope.targetUserType)) {
					scope.userEventFilters['RECIPIENT_TYPE'] = scope.targetUserType;
					if(scope.userEventFilters['PORTAL_ID'] != undefined && scope.userEventFilters['PORTAL_ID'].length == 0){
						scope.userEventFilters['PORTAL_ID'] = undefined;
					}
					var pars = {
						'filters': scope.userEventFilters
					}
					
					GatewayUserActivityService.searchMasterActivityTypesForRecipient({}, pars, function(result) {
						scope.masterActivityTypesForRecipient = result.data;
					}, function(error) {
						MessageService.showError(error);
					});	
				} 
			}
			
 			scope.init = function() {
				$(element).find('.meeting-calendar').fullCalendar( 'removeEventSources' );
				$(element).find('.meeting-calendar').fullCalendar( 'removeEvents' );
				
				
				scope.constants = $rootScope.constants;
				scope.selects = {};
				GatewayDrugService.search({},{ filters: { 'PORTAL_ID' : scope.portalsList }},function(result) {
					scope.selects['Drugs'] = result.data;

					if(result.failurePortalIds!=undefined && result.failurePortalIds.length>0) {
						var warning = "Non \u00e8 stato possibile recuperare i dati delle terapie/farmaci dal portale "+result.failurePortalIds[0].toUpperCase();
						if(result.failurePortalIds.length>1) {
							warning = "Non \u00e8 stato possibile recuperare i dati delle terapie/farmaci dai portali ";
							for(var i=0; i<result.failurePortalIds.length; i++) {
								warning += " "+result.failurePortalIds[i].toUpperCase();
								if(i<result.failurePortalIds.length-1) {
									warning +=",";
								}
							}
						}
						MessageService.showWarning('Dati non completi',warning);
					}
					
					
				}, function(error) {
					scope.selects['Drugs'] = [];
				});
				
 				scope.userActivities = [];
 				
 				scope.userActivitiesLoaded = 0;
 				
 				if(scope.options == undefined || scope.options == null) scope.options = {};
 				
 				if(scope.targetUserType == undefined || scope.targetUserType == null) scope.targetUserType = 'PATIENT';
 				
 				if(scope.userEventFilters == undefined || scope.userEventFilters == null) scope.userEventFilters = {};
 				
 				scope.startuserActivities = undefined;
 				
 				scope.enduserActivities = undefined;
 				
 				if(scope.options.hiddenDays == undefined) {
 					scope.options.hiddenDays = [];
 				}
 				
 				scope.userActivityList = [];
 				scope.eventSources = [];
 				var date = new Date();
 				var d = date.getDate();
 				var m = date.getMonth();
 				var y = date.getFullYear();
 				
 				scope.overlay = undefined;
 				scope.overlayKey = undefined;
 				scope.precision = 400;
 				scope.lastClickTime = 0;
 				
 				scope.masterActivityTypesForRecipient = [];
 				
 				scope.loadMasterActivityTypesForRecipient();
 				
 				scope.configCalendar();
				scope.loadEvents(true);
 			}
 			
 			scope.loadEvents = function(forceReload) {
 				if(forceReload==undefined) forceReload = false;
 				
 				var calendar = $(element).find('.meeting-calendar').fullCalendar('getView');
 				
 				var startTmp, endTmp;
 				if(calendar == undefined)
 					calendar = { start: undefined , end : undefined };
 				if(calendar.start == undefined || calendar.end == undefined) {
					//Usa mese corrente
					calendar.start= moment().startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
					calendar.end= moment().startOf('month').add(1,"months").utc().valueOf();
 				}
 				
 				startTmp = new Date(calendar.start.valueOf()).setHours(0);
 				endTmp = calendar.end.valueOf();
 				
 				if(forceReload || scope.toReload(startTmp, endTmp)){
 					scope.startSearch = startTmp;
 	 				scope.endSearch = endTmp;
 	 				
					scope.userEventFilters['RECIPIENT_TYPE'] = scope.targetUserType;
					scope.userEventFilters['START_TIME_FROM'] = scope.startSearch; 
					scope.userEventFilters['START_TIME_TO'] = scope.endSearch;
					if(scope.userEventFilters['PORTAL_ID'] != undefined && scope.userEventFilters['PORTAL_ID'].length == 0){
						scope.userEventFilters['PORTAL_ID'] = undefined;
					}
					var pars = {
						'filters': scope.userEventFilters
					}
					
					scope.loadMasterActivityTypesForRecipient();
					
					GatewayUserActivityService.search({}, pars, function(result) {
						scope.userActivities = result.data;
						var warning;
						if(result.failurePortalIds!=undefined && result.failurePortalIds.length>0) {
							warning = "Non \u00e8 stato possibile recuperare i dati dal portale "+result.failurePortalIds[0].toUpperCase()+". Per visualizzare i dati dei Pazienti iscritti a entrambi i PSP \u00e8 necessario accedere al portale "+result.failurePortalIds[0].toUpperCase()+" e accettare i termini e condizioni d'uso e l'informativa sulla privacy. Puoi effettuare ora l'accesso tramite la funzione \"Cambia Portale\" del menu in alto a destra";
							if(result.failurePortalIds.length>1) {
								warning = "Non \u00e8 stato possibile recuperare i dati dai portali ";
								for(var i=0; i<result.failurePortalIds.length; i++) {
									warning += " "+result.failurePortalIds[i].toUpperCase();
									if(i<result.failurePortalIds.length-1) {
										warning +=",";
									}
								}
								warning +=". Per visualizzare i dati dei Pazienti iscritti a tutti i PSP \u00e8 necessario aver effettuato l'accesso tutti i portali e aver accettato i termini e condizioni d'uso e l'informativa sulla privacy. Puoi effettuare ora l'accesso tramite la funzione \"Cambia Portale\" del menu in alto a destra";
							}
							MessageService.showWarning('Dati non completi',warning);
						}
						else if($rootScope.loggedUser.originalUserId != null && $rootScope.loggedUser.originalUserId !== undefined) {
							warning = "Impersonificando l\'utente \u00e8 possibile recuperare i dati solamente dal portale corrente";
							MessageService.showWarning('Dati non completi',warning);
						}
						scope.userActivitiesLoaded++;
					}, function(error) {
						MessageService.showError(error);
					});
 				}
 			}
 			
			scope.$watch('userActivitiesLoaded', function(nv,ov) {
				$(element).find('.meeting-calendar').fullCalendar('removeEvents');
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.meetingEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.userActivityList);
				scope.userActivityList = [];
				if(scope.userActivities !== undefined && scope.userActivities != null) {
					for(var i=0; i<scope.userActivities.length; i++) {
						var title = scope.userActivities[i].user.lastName + ' ' + scope.userActivities[i].user.firstName + ' - ' + scope.userActivities[i].name;// $filter('limitTo')(scope.userActivities[i].name, 100) + '...';
						var minEndTime = moment(scope.userActivities[i].start).add(30,'m').utc().valueOf();
						var event = {
							'allDay': false,
							'className': ["b-l b-2x b-success"],
							'key': scope.userActivities[i].id,
							'overlap': false,
							'editable': false,
							'completeTitle': scope.userActivities[i].name,
							'title': title,
							'start': scope.userActivities[i].date,
							'end' : scope.userActivities[i].endDate,
							'userColors': [],
							'object': scope.userActivities[i]
						};
						scope.userActivityList.push(event);
					}
				}
				
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.userActivityList);
				$(element).find('.meeting-calendar').fullCalendar('render');
			},true);
			
			scope.toReload = function(startCalendar, endCalendar){
				//determina se si sta cercando oltre quello che è già stato caricato sul calendario per evitare chiamate inutili
				return (startCalendar < scope.startSearch || endCalendar > scope.endSearch);
			}
			
			scope.eventClick = function( event, jsEvent, view ) {
				var userActivityScope = scope.$new(true);
				userActivityScope.closeButton = true;
				userActivityScope.restUrl = event.object.detailRestUrl;
				userActivityScope.constants = angular.copy($rootScope.constants);
				userActivityScope.popup = true;
				userActivityScope.modal =  $modal.open({
					scope: userActivityScope,
					templateUrl: 'tpl/app/directive/user-activity/user-activity.html',
					controller: 'UserActivityCrossPspController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-user-activity.js'] );
									}
							)
						}
					}
				});
				
				userActivityScope.modal.result.then(function() {
					console.log('Modal closed: ' + new Date());
				}, function () {
					console.log('Modal dismissed: ' + new Date());
				});	
			}
			
			scope.closeTooltip = function() {
				scope.overlay.fadeOut();
				scope.overlayKey = undefined;
			}
			
			scope.changeView = function(view) {
				if(view !== undefined) {
					$(element).find('.meeting-calendar').fullCalendar('changeView', view);
					scope.loadEvents();
				}
			};

			scope.today = function(calendar) {
				$(element).find('.meeting-calendar').fullCalendar('gotoDate',(new Date()));
				$(element).find('.meeting-calendar').fullCalendar('changeView','listDay');
			};
			
			scope.afterRender = function(view) {
				if(angular.element('.meeting-calendar-overlay-click').length>0 && (scope.overlay==undefined || scope.overlay==null)){
					scope.overlay = angular.element('.meeting-calendar-overlay-click');
				}
			};
			
			scope.init();
		}
	};
});
/***************************************************************
 * Directives per Gateway multi-psp (per ora solo medici)
 **************************************************************/
//---- Finestra di invito per PSP correlati ---
app.directive("gatewayInvite", function($ocLazyLoad, $modal, $state, $rootScope, $controller, UserService, GatewayUserService, MessageService) {
	return {
		restrict: 'EA',
		scope: {
			user : "=gatewayInvite",
			userType : "@",
			relatedPortals : "=?"
		},
		link: function(scope, element, attrs) {
			element.bind("click", function() {
				
				scope.relatedPortalNames = (scope.relatedPortals==undefined) ? '' : scope.relatedPortals.join(", ");
				scope.selectedUsers = [];
				
				
				scope.listConfig = {
					'isModal' : false,	
					'isSelection' : true,
					'searchFixedFilters' : {
						'PORTAL_ID' : scope.relatedPortalNames,
						'REGISTERED' : true
					},
					'customOptions' : { 'hideCurrentPortal' : true }
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/gateway/gateway-invite.html',
					controller: 'GatewayInviteHcpController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
							 		return $ocLazyLoad.load( ['js/controllers/directives/gateway/crm-gateway-hcp.js'] );
								}
							)
						}
					}
				});
				
				scope.cancel = function() {
					scope.selectedUsers = [];
					scope.modal.dismiss();
				}
				
				/*********************************
				 * Conferma invito o collegamento con altro utente
				 ***********************************/
				scope.confirmInvite = function() {
					if(scope.selectedUsers!=undefined && scope.selectedUsers.length>0) {
						scope.doBindUsers(scope.user,scope.selectedUsers);
					}
					else {
						if($rootScope.selectedUsersTemp != undefined && $rootScope.selectedUsersTemp.length>0){
							scope.doBindUsers(scope.user,$rootScope.selectedUsersTemp);
						}else{
							scope.doInvite(scope.user);
						}
					}
					
				}
				
				scope.close = function() {
					scope.modal.close();
				}
				
				/**********************************************************************************
				 * Chiama rest che effettua il collegamento tra utenze
				 * @Parameters:
				 * user -> utente che sto invitando
				 * usersToBind -> utenze di altri portali che voglio collegare a questa
				 * vincoli:
				 * - user non deve avere altre utenze collegate nello stesso portale di userToBind
				 * - posso collegare con user solo uno userToBind per ciascun portale 
				 * - l'utenza userToBind deve corrispondere a uno stato non DELETED o EXITED
				 * - l'utenza userToBind non deve essere già collegata con altro account del mio portale
				 * - le utenze usersToBind selezionate devono avere lo stesso nickName
				 **********************************************************************************/
				scope.doBindUsers = function(user,usersToBind) {
					
					var modalInstance = MessageService.simpleConfirm('Conferma collegamento utenze','Confermi di voler utilizzare le credenziali degli utenti selezionati anche per questo portale?','bg-secondary','sm');
					modalInstance.result.then(
						function(confirm) {		
							var req = { 
								'userId': user.id,
								'usersToBind': [],
								'userType' : scope.userType
							}
							for(var u=0; u<usersToBind.length; u++) {
								req['usersToBind'].push( { 'userId' : usersToBind[u].id, 'portalId' : usersToBind[u].portalId, 'nickName' : usersToBind[u].nickName } );
							}
							GatewayUserService.bindUsers({}, req, function(result) {
								MessageService.showSuccess('Operazione avvenuta con successo', '');
								scope.close();
							}, function(error) {
								if(error.status == 404) {
									MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
								}
							});
						}
					);
					
					
				}
				
				scope.doInvite = function(user) {
					var modalInstance = MessageService.activationConfirm();
					modalInstance.result.then(
						function(confirm) {				
							UserService.activation({ id : user.id }, {}, function(result) {
								MessageService.showSuccess('Operazione avvenuta con successo', '');
								scope.close();
							}, function(error) {
								if(error.status == 404) {
									MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
								}
							});
						}
					);
				}
			});
		}
	}
});

app.directive("gatewayHcpList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','smart-table', 'js/controllers/directives/gateway/crm-gateway-hcp.js'];
		$scope.templateUrl = 'tpl/app/directive/gateway/gateway-hcp-list.html';
		$scope.controllerName = 'GatewayHcpListCtrl';
		$scope.modalSize = 'xl';
	} });
});


app.directive("gatewayPatientList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','smart-table', 'js/controllers/directives/gateway/crm-gateway-patient.js'];
		$scope.templateUrl = 'tpl/app/directive/gateway/gateway-patient-list.html';
		$scope.controllerName = 'GatewayPatientListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("gatewayUserSearchModal", function($modal, $ocLazyLoad, $state, $rootScope, $timeout) {
	return {
		restrict: 'A',
		scope : {
			isSelection: "=?isSelection",
			selected: "=?gatewayUserSearchModal",
			searchFilters: "=?filters",
			objectFilters: "=?objectFilters",
			multipleList: "=?multipleList",
			preFn: "=?preFn",
			postFn: "=?postFn",
			userType: "=",
			userId: "=?",
			customOptions: "=?"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.isSelection = scope.isSelection || true;
				scope.multipleList = scope.multipleList || false;
				scope.isModal = true;
				scope.popup = true;
				scope.constants = $rootScope.constants;
				
				if(scope.preFn) {
					scope.preFn();
				}
				
				if(!scope.multipleList) {
					if(scope.selected!=undefined) {
						scope.singleSelected = scope.selected;
						scope.selected = [];
						scope.selected.push(scope.singleSelected);
					} else {
						scope.selected = []
					}
				} else {
					scope.selected = scope.selected || [];
				}
				
				switch(scope.userType) {
			    case 'HCP':
			    	scope.userController = "GatewayHcpListCtrl";
			    	scope.userControllerPath = "js/controllers/directives/gateway/crm-gateway-hcp.js";
			    	scope.userTemplate = "tpl/app/directive/gateway/gateway-hcp-list.html";
			    	break;
			    default:
			        return;
				}
				
				scope.modal = $modal.open({
					scope: scope,
					templateUrl: scope.userTemplate,
					controller: scope.userController,
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','smart-table']).then(
									function() {
										return $ocLazyLoad.load( [scope.userControllerPath] );
									}
							)
						}
					}
				});
					
				scope.modal.result.then(function() {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
						scope.userId = scope.selected.id;
					} else {
						scope.userId = [];
						for(var i = 0; scope.selected.length>i; i++){
							scope.userId.push(scope.selected[i].id);
						}
					}
					if(scope.postFn) {
						$timeout(function() {
					         scope.postFn();
					     });
					}
				}, function () {
					if(!scope.multipleList) {
						scope.selected = (scope.selected!==undefined && scope.selected.length>0) ? scope.selected[0] : undefined;
					}
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive('vbox', function() {
	  return {
	    link: function(scope, element, attrs) {
	      attrs.$observe('vbox', function(value) {
	    	  element.get(0).setAttribute("viewBox", value);
	      })
	    }
	  };
});

app.directive("patientDbsTestActivityList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-patient-dbs-test-activity.js'];
		$scope.templateUrl = 'tpl/app/directive/patientdbstestactivity/patient-dbs-test-activity-list.html';
		$scope.controllerName = 'PatientDbsTestActivityListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("patientDbsTestRequest", function($ocLazyLoad, $modal, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope: {
			postFn : "=?postFn"
		},
		link: function(scope, element, attrs) {
			element.bind("click", function() {
				scope.isModal = true;
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/patientdbstestactivity/patient-dbs-test-request.html',
					controller: "PatientDbsTestActivityRequestCtrl",
					windowClass: "heightAutoModal",
					size: 'md',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load([]).then(
								function() {
									return $ocLazyLoad.load(['js/controllers/directives/crm-patient-dbs-test-activity.js']);
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn) {
						scope.postFn();
					}
				}, function () {
					
				});
			});
		}
	}
});
