'use strict';

app.filter('constantLabel', function($rootScope, $filter) {
	return function(values, type) {
		if(values != undefined && type !== undefined && $rootScope.constants !== undefined && $rootScope.constants[type] !== undefined) {
			var res = [];
			if(angular.isArray(values)) {
				for(var c=0; c<$rootScope.constants[type].length; c++) {
					if(values.indexOf($rootScope.constants[type][c].id) >= 0) {
						res.push($rootScope.constants[type][c].label);
					}
				}
			}
			else {
				for(var c=0; c<$rootScope.constants[type].length; c++) {
					if($rootScope.constants[type][c].id === values) {
						res.push($rootScope.constants[type][c].label);
						break;
					}
				}
			}
			return res.join(', ');
		}
		else {
			return values;
		}
	}
});

app.filter('constantInArray', function($filter){
    return function(constants, filtered){
    	var res = [];
        if(constants) {
        	if(filtered) {
        		for(var i=0; i<constants.length; i++) {
        			if(filtered.indexOf(constants[i].id) >= 0) {
        				res.push(constants[i]);
        			}
        		}
        	}
        }
        return res;
    };
});

app.filter('meetingLocationList', function($filter){
    return function(constants, userType, isEnabledPharmacyLocationType, isEnabledBloodDrawingCenterLocationType){
    	var res = [];
        if(constants) {
        	if(userType) {
        		isEnabledPharmacyLocationType = isEnabledPharmacyLocationType || false;
        		isEnabledBloodDrawingCenterLocationType = isEnabledBloodDrawingCenterLocationType || false;
        		for(var i=0; i<constants.length; i++) {
        			var location = constants[i].id;
        			if('PATIENT_HOME' === location) {
        				if(userType === 'PATIENT') {
        					res.push(constants[i]);
        				}
        			}
        			else if('PHARMACY' === location) {
        				if(userType === 'PATIENT' && isEnabledPharmacyLocationType) {
        					res.push(constants[i]);
        				}
        			}
        			else if('BLOOD_DRAWING_CENTER' === location) {
        				if(userType === 'PATIENT' && isEnabledBloodDrawingCenterLocationType) {
        					res.push(constants[i]);
        				}
        			}
        			else {
        				res.push(constants[i]);
        			}
        		}
        	}
        }
        return res;
    };
});

app.filter('qualificationLabel', function($rootScope, $filter) {
	return function(values) {
		if(values != undefined && $rootScope.qualifications !== undefined) {
			var res = [];
			if(angular.isArray(values)) {
				var ids = [];
				for(var i=0; i<values.length; i++) {
					ids.push(values[i].id);
				}
				for(var c=0; c<$rootScope.qualifications.length; c++) {
					if(ids.indexOf($rootScope.qualifications[c].id) >= 0) {
						res.push($rootScope.qualifications[c].name);
					}
				}
			}
			else {
				for(var c=0; c<$rootScope.qualifications.length; c++) {
					if($rootScope.qualifications[c].id === values.id) {
						res.push($rootScope.qualifications[c].name);
						break;
					}
				}
			}
			return res.join(', ');
		}
		else {
			return values;
		}
	}
});

app.filter('formatDate', function($rootScope, $filter, DATE_TIME_FORMATS) {
	return function(date, format, timezone) {
		moment.locale('it');
		if(format === undefined || format == null || format === '') {
			format = DATE_TIME_FORMATS.DATE;
		}
		if(timezone === undefined || timezone == null || timezone === '') {
			timezone = 'Europe/Rome';
		}
		
		if(date !== undefined && date != null) {
			var timestampUTC = null;
			var momentDate = null;
			if(angular.isDate(date)) {
				timestampUTC = date.getTime();
				momentDate = moment(timestampUTC);
			}
			else if(angular.isNumber(date)) {
				// Considero che sia già in UTC
				timestampUTC = date;
				momentDate = moment(timestampUTC);
			}
			else {
				momentDate = moment.utc(date, format);
			}
			if(momentDate !== undefined && momentDate != null) {
				return momentDate.format(format);	
			}
			else {
				return '';
			}
		}
		else {
			return '';
		}
	};
});

app.filter('formatDateTime', function($rootScope, $filter, DATE_TIME_FORMATS) {
	return function(date, format, timezone) {
		moment.locale('it');
		if(format === undefined || format == null || format === '') {
			format = DATE_TIME_FORMATS.DATETIME;
		}
		if(timezone === undefined || timezone == null || timezone === '') {
			timezone = 'Europe/Rome';
		}
		
		if(date !== undefined && date != null) {
			var timestampUTC = null;
			var momentDate = null;
			if(angular.isDate(date)) {
				timestampUTC = date.getTime();
				momentDate = moment(timestampUTC);
			}
			else if(angular.isNumber(date)) {
				timestampUTC = date;
				momentDate = moment(timestampUTC);
			}
			else if(angular.isObject(date) && date._isAMomentObject) {
				timestampUTC = date;
				momentDate = date;//.utc();
			}
			else {
				if(!isNaN(date)) {
					timestampUTC = parseInt(date, 10);
					momentDate = moment(timestampUTC);
				}
				else {
					momentDate = moment(date);
				}
			}
			if(momentDate !== undefined && momentDate != null) {
				return momentDate.format(format);
			}
			else {
				return '';
			}
		}
		else {
			return '';
		}
	};
});

app.filter('formatTime', function($rootScope, $filter, DATE_TIME_FORMATS) {
	return function(date, format, timezone) {
		moment.locale('it');
		if(format === undefined || format == null || format === '') {
			format = DATE_TIME_FORMATS.TIME;
		}
		if(timezone === undefined || timezone == null || timezone === '') {
			timezone = 'Europe/Rome';
		}
		
		if(date !== undefined && date != null) {
			var minutesFromMidnight = null;
			var momentDate = undefined;
			if(angular.isDate(date)) {
				momentDate = moment(date);
			}
			else if(angular.isNumber(date)) {
				// Considero che sia già il valore dei minuti dalla mezzanotte
				var momentDate = moment();
				momentDate.hours(0);
				momentDate.minutes(0);
				momentDate.seconds(0);
				momentDate.milliseconds(0);
				momentDate.add(date, 'minutes');
			}
			else if(angular.isObject(date) && date._isAMomentObject) {
				// Considero che sia tipo momentjs
				momentDate = date;
			}
			else {
				if(!isNaN(date)) {
					var momentDate = moment();
					momentDate.hours(0);
					momentDate.minutes(0);
					momentDate.seconds(0);
					momentDate.milliseconds(0);
					momentDate.add(parseInt(date, 10), 'minutes');
				}
				else {
					momentDate = undefined;
				}
			}
			if(momentDate !== undefined) {
				momentDate.seconds(0);
				momentDate.milliseconds(0);
				if(momentDate !== undefined && momentDate != null) {
					return momentDate.format(format);
				}
				else {
					return '';
				}
			}
		}
		return '';
	};
});

app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.filter('removeHTMLTags', function() {
	return function(text) {
		return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
	};
});

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('constantAcceptedValuesFilter', function($filter){
    return function(constants, filtered){
    	var res = [];
        if(constants) {
        	if(filtered) {
        		for(var i=0; i<constants.length; i++) {
        			if(filtered.indexOf(constants[i].id) >= 0) {
        				res.push(constants[i]);
        			}
        		}
        	}
        }
        return res;
    };
});

app.filter('truncateNumber', function($filter) {
	return function (value, precision) {
		var multiply = 1;
		if (typeof precision !== 'number' || precision < 0) {
			multiply = 100;
		} else {
			for(var i = 0; i < precision; i++) {
				multiply *= 10;
			}
		}
		return $filter('number')((Math.floor((value * multiply)) / multiply), precision);
	};
});


app.filter('constantActive', function($filter){
	return function(constants, filtered){
		var res = [];
		if(constants) {
			if(filtered) {
				for(var i=0; i<constants.length; i++) {
					if(constants[i].active || filtered.indexOf(constants[i].id) >= 0) {
						res.push(constants[i]);
					}
				}
			}
			else {
				for(var i=0; i<constants.length; i++) {
					if(constants[i].active) {
						res.push(constants[i]);
					}
				}
			}
		}
		return res;
	};
});

app.filter('formatBool', function() {
    return function(input) {
        return input !== undefined && input != null ? (input ? 'Si' : 'No') : '';
    }
});


app.filter('constantExcludeValuesFilter', function($filter){
    return function(constants, filtered){
    	var res = [];
        if(constants) {
        	if(filtered) {
        		for(var i=0; i<constants.length; i++) {
        			if(filtered.indexOf(constants[i].id) >= 0) {
        				continue;
        			} else {
        				res.push(constants[i]);
        			}
        		}
        	}
        }
        return res;
    };
});

app.filter('formatArray', function() {
    return function(input) {
        if(input == undefined || input == null || input.length==0) {
        	return "";
        }
        var res = "";
        for(var i=0; i<input.length; i++) {
        	res += input[i];
        	if(i<input.length-1) {
        		res +=", ";
        	}
        }
        return res;
    }
});

app.filter('formatNumber', function() {
    return function(input) {
    	return input !== undefined && input != null ? parseInt(input, 10) : "";
    }
});

app.filter('nameFromParticipantArray', function() {
    return function(input) {
        if(input == undefined || input == null || input.length==0) {
        	return "";
        }
        var res = "";
        for(var i=0; i<input.length; i++) {
        	if(input[i] != null && input[i] != undefined && input[i].user != undefined){
		    	res += input[i].user.lastName + ' ' + input[i].user.firstName;
		    	if(i<input.length-1) {
		    		res +=", ";
		    	}
        	}
        }
        return res;
    }
});

app.filter('monthNumberToLabel', function() {
    return function(input) {
    	moment.locale('it');
    	var momentDate = undefined;
    	if(input && input > 0 && input <= 12) {
	    	var momentDate = moment();
	    	momentDate.day(1);
	    	momentDate.month(input - 1);
	    }
        return momentDate ? momentDate.format('MMMM') : '';
    }
});