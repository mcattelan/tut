app.filter('trainingSubjectLabel', function($rootScope, $filter) {
	return function(values) {
		if(values != undefined) {
			var res = [];
			for(var c=0; c<values.length; c++) {
				res.push(values[c].title);
			}
			return res.join(', ');
		} else {
			return values;
		}
	}
});