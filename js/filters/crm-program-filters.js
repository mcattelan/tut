app.filter('userQuestionnaireStatusLabel', function($rootScope, $filter) {
	return function(value) {
		if(value != undefined) {
			if(value.permanent) return 'Confermato';
			else if(value.compiled) return 'Compilato';
			else return 'Da compilare';
		}	
		else
			return undefined;
	}
});

app.filter('activityFailedReasons', function($rootScope, $filter){
    return function(constants, forActivity, complementaries){
    	var res = [];
        if(constants) {
        	for(var i = 0; i <= constants.length -1; i++) {
        		var reason = constants[i];
        		if(forActivity && reason.properties.forActivity) {
        			res.push(reason);
        			continue;
        		}
        		if(complementaries && complementaries.length > 0) {
        			for(var x = 0; x <= complementaries.length - 1; x++) {
        				var failedReasons = $filter('constantInArray')($rootScope.constants['ComplementaryActivityType'], complementaries[x].type)[0].properties.failedReasons;
        				if(failedReasons) {
        					if(failedReasons.indexOf(reason.id) >= 0) {
        						res.push(reason);
        						break;
        					}
        				}
        			}
        		}
        	}
        }
        return res;
    };
});