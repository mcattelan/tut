'use strict';

app.constant("RESTURL_TUTOR", (function() {
	var BaseV1 = props['rest.v1'];
	return {
		PATIENT_ENROLLMENT: BaseV1 + '/patient-enrollment'
	}
})());

app.factory('SupplierUserService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var SupplierUserService = $resource(RESTURL.SUPPLIER_USER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER
		},
		update: {
			method: 'PUT',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER
		},
		searchUserTrainingSessions: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER + '/search/training-session'
		}
	});
	SupplierUserService.getUrl = function() {
		return ''+RESTURL.SUPPLIER_USER;
	}
	return SupplierUserService;
}]);

app.factory('PatientDossierService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_DOSSIER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_DOSSIER + '/search'
		},
		countSearch: {
			method: 'POST',
			url: RESTURL.PATIENT_DOSSIER + '/search/count'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_DOSSIER
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_DOSSIER + '/:id'
		}
	});
});

app.factory('PatientEnrollmentService', function($resource, RESTURL_TUTOR) {
	return $resource(RESTURL_TUTOR.PATIENT_ENROLLMENT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		insertAndUpload: {
			method: 'POST',
			url: RESTURL_TUTOR.PATIENT_ENROLLMENT+'/insert-upload'
		},
		insertAndUploadMultiple: {
			method: 'POST',
			url: RESTURL_TUTOR.PATIENT_ENROLLMENT+'/insert-upload-multiple'
		},
		search: {
			method: 'POST',
			url: RESTURL_TUTOR.PATIENT_ENROLLMENT+'/search'
		},
		update: {
			method: 'PUT'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL_TUTOR.PATIENT_ENROLLMENT + '/:id'
		},
		downloadUrl: {
			method: 'GET',
			url: RESTURL_TUTOR.PATIENT_ENROLLMENT + '/:id/url'
		}
		
	});
});

app.factory('LinkService', function($resource, RESTURL) {
	return $resource(RESTURL.LINK + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.LINK+'/search'
		}
	});
});

app.factory('PatientService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PatientService =  $resource(RESTURL.PATIENT + '/:id', { id: '@id', delegateIndex: '@delegateIndex' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.PATIENT
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
		delete: {
			method: 'DELETE'
		},
		searchAllUser: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/user/search'
		},
		getDelegate: {
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		markDelegate: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		deleteDelegate: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		getCaregiver:{
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/caregiver/'			
		},
		markCaregiver: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		deleteCaregiver: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		activate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/terminate'
		},
		updateAuthorizationKeys:{
			method: 'PUT',
			url: RESTURL.PATIENT + '/authorization-keys/:id'
		},
		activateNotification: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate-notification'
		},
		inviteActivationAll:{
			method: 'POST',
			url: RESTURL.PATIENT + '/search/invite-all'
		},
		countSearchInvite: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search/count'
		},
		updateAvatar: {
			method: 'PUT',
			url: RESTURL.PATIENT + '/:id/avatar'
		},
		searchApprovals: {
			method: 'GET',
			url: RESTURL.PATIENT + '/approvals'
		},
		delegateHcpsActive: {
			method: 'PUT',
			url: RESTURL.PATIENT +'/delegate-hcp-active'
		},
		updateRemindAssumptionTypes: {
			method: 'GET',
			url: RESTURL.PATIENT + '/update-remind-assumption-types'
		},
		diaryDetails: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/diary'
		},
		homeInjections: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/diary/injections'
		}
	});
	PatientService.getUrl = function() {
		return ''+RESTURL.PATIENT;
	}
	return PatientService;
}]);