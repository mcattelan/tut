'use strict';

app.factory('MessageService', function($rootScope, toaster, $anchorScroll, $modal) {
	return {
		showSuccess : function(title, text, timeout) {
			toaster.clear();
			toaster.pop(
					{
						type: 'success',
						title: title,
						body: text,
						timeout: angular.isDefined(timeout) ? timeout : 2000
					}
			)
		},
		
		showInfo : function(title, text) {
//			toaster.pop('info', title, text);
			toaster.clear();
			toaster.pop(
					{
						type: 'info',
						title: title,
						body: text,
						timeout: 2000
					}
			)
		},

		showWait : function(title, text) {
//			toaster.pop('wait', title, text);
			toaster.clear();
			toaster.pop(
					{
						type: 'wait',
						title: title,
						body: text,
						timeout: 2000
					}
			)
		},

		showWarning : function(title, text, isHtml) {
//			toaster.pop('warning', title, text);
			toaster.clear();
			if(isHtml !== undefined && isHtml) {
				toaster.pop(
						{
							type: 'warning',
							title: title,
							body: text,
							timeout: 2000,
							bodyOutputType: 'trustedHtml'
						}
				)
			}
			else {
				toaster.pop(
						{
							type: 'warning',
							title: title,
							body: text,
							timeout: 2000
						}
				)
			}
		},

		showError : function(title, text) {
			toaster.clear();
			$anchorScroll('app-content');
//			toaster.pop('error', title, text);
			toaster.pop(
					{
						type: 'error',
						title: title,
						body: text,
						timeout: 0
					}
			)
		},

		hideToaster : function() {
			toaster.clear();
		},
		
		/* MOD MATTEO - modifica paramentri funzione */
		simpleAlert : function(title, text, style, large,size="") {
			var templateUrl =  'tpl/common/alertModal.html';
			var modalInstance = $modal.open({
				keyboard: false,
				templateUrl: templateUrl,
				size:size,
				windowClass: large? 'largeModal' : undefined,
				controller: function ($scope, $modalInstance,$state) {
					$scope.dialogTitle = title;
					$scope.dialogText = text;
					$scope.dialogStyle = style;
					
					$scope.confirm = function() {
						$modalInstance.close(true);
					};
					
					$scope.close = function() {
						$modalInstance.dismiss();
					}
				}

			});
			return modalInstance;
		},
		
		simpleConfirm : function(title, text, style, size="") {
			var templateUrl =  'tpl/common/confirmModal.html';
			var modalInstance = $modal.open({
				keyboard: false,
				templateUrl: templateUrl,
				size: size,
				controller: function ($scope, $modalInstance,$state) {
					$scope.dialogTitle = title;
					$scope.dialogText = text;
					$scope.dialogStyle = style;
						
					$scope.confirm = function() {
						$modalInstance.close(true);
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}

			});
			return modalInstance;
		},
		
		deleteConfirm : function() {
			var title = 'Conferma cancellazione';
			var text = 'La cancellazione \u00E8 un\'operazione irreversibile, si desidera procedere?';
			var style = 'bg-secondary';
			return this.simpleConfirm(title, text, style, 'sm');
		},
		
		
		activationConfirm : function() {
			var title = 'Conferma invito';
			var text = 'L\'invito dell\'utente \u00E8 un\'operazione irreversibile, si desidera procedere?';
			var style = 'bg-secondary';
			return this.simpleConfirm(title, text, style, 'sm');
		},
		
		impersonateConfirm : function() {
			var title = 'Conferma impersonificazione';
			var text = 'Procedere con l\'impersonificazione ?';
			var style = 'bg-secondary';
			return this.simpleConfirm(title, text, style, 'sm');
		},
		
		fullConfirm : function(title, text, fn, params, classHeaderStyle) {
			$rootScope.showLoading = false;
			var templateUrl =  'tpl/commons/confirmModal.html';
			var modalInstance = $modal.open({
				keyboard: false,
				templateUrl: templateUrl,
				controller: function ($scope, $modalInstance,$state) {
					$scope.dialogTitle = title;
					$scope.dialogText = text;
					$scope.dialogStyle = classHeaderStyle;
						
					$scope.confirm = function() {
						$modalInstance.close(true);
						fn(params);
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}

			});
		}
	}
});

app.factory('UploaderService', function($q, $rootScope, $cookies, MessageService, DocumentService, FileUploader, RESTURL, FormUtilService) {
	return {
		initFileUploader : function($scope,entityId,entityType) {
			$scope.categoriesForUpload = [];
			$scope.errors = {};
			
			setTimeout(function () {
				DocumentService.getCategoriesForUpload({ entityType : entityType  }, {}, function(results) {
					$scope.categoriesForUpload = results.data;
				}, function(error) {
					MessageService.showError('Errore','Problema nel recupero delle categorie documenti')
				});
    		}, 100);
			
			//FileUploader
			var authToken = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
			var uploader = $scope.uploader = new FileUploader({
			    url: RESTURL.DOCUMENT + '/upload',
			    headers: { 'X-AUTH-TOKEN' : authToken }
			});
			
			uploader.verifyAndUploadItem = function(item,index) {
				var form = { formProperties : [
					{ id:'item_'+index+'.name',value : item.name, type: 'string',required : true }
					]
				};
				$scope.errors = FormUtilService.validateForm(form);
				if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
					$scope.checkUploadInfo($q, DocumentService, entityId, entityType, item).then(function(uploadInfo) {
						if(uploadInfo.disclaimerMessage) {
							var dialogTitle = "";
							var dialogText = $scope.uploadInfo.disclaimerMessage;
							var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger','sm');
							modalInstance.result.then(
								function(confirm) {
									item.upload();
								});
						}
						else {
							item.upload();
						}
					}, function(error) {
						MessageService.showError('Caricamento non riuscito');
					});
				} else {
					MessageService.showError('Errore in fase di caricamento','Alcuni dati inseriti non sono corretti');
				}
			}
			
			uploader.onBeforeUploadItem = function(item) {
				item.formData.length = 0;
				item.formData.push({entityId: entityId || ''});
				item.formData.push({entityType: entityType});
			    item.formData.push({categoryIds: item.categories});
				item.formData.push({name: item.name});
				item.formData.push({disclaimerId: item.disclaimerId});
			}
			
			uploader.onAfterAddingFile =  function(item) {
				item.name= item.file.name;
			}
			
			uploader.verifyAndUploadAll = function() {
				var form = { formProperties : [] };
				var messages = [];
				var uploadInfos = [];
				for(var i=0; i<uploader.queue.length; i++) {
					var obj = { id:'item_'+i+'.categories',value : $scope.uploader.queue[i].categories, type: 'string',required : true };
					form.formProperties.push(obj);
					if($scope.uploader.queue[i].disclaimerMessage) {
						if(messages.indexOf($scope.uploader.queue[i].disclaimerMessage) < 0) {
							messages.push($scope.uploader.queue[i].disclaimerMessage);
						}
					}
				};
				$scope.errors = FormUtilService.validateForm(form);
				if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
					if(messages.length > 0) {
						var dialogTitle = "";
						var dialogText = messages.join('<br />');
						var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger','sm');
						modalInstance.result.then(
							function(confirm) {
								uploader.uploadAll();
							});
					}
					else {
						uploader.uploadAll();
					}
				}
				else {
					MessageService.showError('Errore in fase di caricamento','Alcuni dati inseriti non sono corretti');
				}
			}
			
			// FILTERS
			uploader.filters.push({
			    name: 'customFilter',
			    fn: function(item , options) {
			        return this.queue.length < 10;
			    }
			});
			
			uploader.onCompleteItem = function(item, response, status, headers) {
				$scope.errors = response.errors;
			};
			
			uploader.onCompleteAll = function() {
				var notUploaded = [];
				var messages = [];
				for(var i=0; i<uploader.queue.length; i++) {
					if(uploader.queue[i].isError) {
						notUploaded.push(uploader.queue[i].name);
					}
				}
				if(notUploaded.length>0) {
					var names = '';
					for(var k=0; k<notUploaded.length; k++) {
						names += notUploaded[k]+", ";
					}
					names = (names!='') ? names.substring(0,names.lastIndexOf(',')): '';
					if(notUploaded.length==1)
						MessageService.showError('Caricamento del file non riuscito','Il file '+names+' non \u00E8 stato caricato');
					else
						MessageService.showError('Caricamento dei files non riuscito','I files '+names+' non sono stati caricati');
				}
				else {
					for(var i=0; i<uploader.queue.length; i++) {
						if($scope.uploader.queue[i].completeMessage) {
							if(messages.indexOf($scope.uploader.queue[i].completeMessage) < 0) {
								messages.push($scope.uploader.queue[i].completeMessage);
							}
						}
					};
					MessageService.showSuccess('Caricamento files completato', messages.join('<br />'));
				}
				
			    $scope.search();
			    uploader.queue = [];
			};
			
			$scope.checkUploadInfo = function($q, DocumentService, entityId, entityType, item) {
				var d = $q.defer();
				
				var uploadRequest = {
					entityId: entityId,
					entityType: entityType,
					categoryIds: item.categories,
					name: item.name
				}
				
				var uploadInfo = DocumentService.getUploadInfo(uploadRequest, function(result) {
					if(result.data) {
						item.disclaimerId = result.data.disclaimerId;
						item.disclaimerMessage = result.data.disclaimerMessage;
						item.completeMessage = result.data.completeMessage;
					}
					
					if(item.disclaimerMessage) {
						var dialogTitle = "";
						var dialogText = item.disclaimerMessage;
						var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger','sm');
						modalInstance.result.then(
							function(confirm) {
								d.resolve(uploadInfo);
							});
					}
					else {
						d.resolve(uploadInfo);
					}
				},
				function(error) {
					d.resolve(undefined);
				});
				return d.promise;
			}
		}
	}
});

app.factory('ProgramUploaderService', function($rootScope, $cookies, MessageService, ProgramDocumentService, FileUploader, RESTURL, FormUtilService) {
	return {
		initFileUploader : function($scope,entityId,entityType,programId) {
			$scope.categoriesForUpload = [];
			$scope.errors = {};
			
			setTimeout(function () {
				ProgramDocumentService.getCategoriesForUpload({ programId: programId, entityType : entityType }, {}, function(results) {
					$scope.categoriesForUpload = results.data;
				}, function(error) {
					MessageService.showError('Errore','Problema nel recupero delle categorie documenti')
				});
    		}, 100);
			
			//FileUploader
			var authToken = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
			var uploader = $scope.uploader = new FileUploader({
			    url: RESTURL.DOCUMENT + '/upload',
			    headers: { 'X-AUTH-TOKEN' : authToken }
			});
			
			uploader.verifyAndUploadItem = function(item,index) {
				var form = { formProperties : [
					{ id:'item_'+index+'.name',value : item.name, type: 'string',required : true }
					] 
				};
				$scope.errors = FormUtilService.validateForm(form);
				if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
					item.upload();
				} else {
					MessageService.showError('Errore in fase di caricamento','Alcuni dati inseriti non sono corretti');
				}
			}
			
			uploader.onBeforeUploadItem = function(item) {
				item.formData.length = 0;
				item.formData.push({entityId: entityId || ''});
				item.formData.push({entityType: entityType});
			    item.formData.push({categoryIds: item.categories});
				item.formData.push({name: item.name});
				if(item.type) {
					item.formData.push({type: item.type});	
				}
			}
			
			uploader.onAfterAddingFile =  function(item) {
				item.name= item.file.name;
			}
			
			uploader.verifyAndUploadAll = function() {
				var form = { formProperties : [] };
				for(var i=0; i<uploader.queue.length; i++) {
					var obj = { id:'item_'+i+'.categories',value : $scope.uploader.queue[i].categories, type: 'string',required : true };
					form.formProperties.push(obj);							
				};
				$scope.errors = FormUtilService.validateForm(form);
				if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
					uploader.uploadAll();
				}
				else {
					MessageService.showError('Errore in fase di caricamento','Alcuni dati inseriti non sono corretti');
				}
				
			}
			
			// FILTERS
			uploader.filters.push({
			    name: 'customFilter',
			    fn: function(item , options) {
			        return this.queue.length < 10;
			    }
			});
			
			uploader.onCompleteItem = function(item, response, status, headers) {
				$scope.errors = response.errors;
			};
			
			uploader.onCompleteAll = function() {
				var notUploaded = [];
				for(var i=0; i<uploader.queue.length; i++) {
					if(uploader.queue[i].isError) {
						notUploaded.push(uploader.queue[i].name);
					}
				}
				if(notUploaded.length>0) {
					var names = '';
					for(var k=0; k<notUploaded.length; k++) {
						names += notUploaded[k]+", ";
					}
					names = (names!='') ? names.substring(0,names.lastIndexOf(',')): '';
					if(notUploaded.length==1)
						MessageService.showError('Caricamento del file non riuscito','Il file '+names+' non \u00E8 stato caricato');
					else
						MessageService.showError('Caricamento dei files non riuscito','I files '+names+' non sono stati caricati');
				}
				else {
					MessageService.showSuccess('Caricamento files completato', '');
				}
				
			    $scope.search();
			    uploader.queue = [];
			};
		}
	}
});

function isEmpty(obj) {
    if (obj == null) return true;
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;
    if (typeof obj !== "object") return true;
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;
}(window,document,angular,undefined);