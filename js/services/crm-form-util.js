'use strict';

app.factory('FormUtilService', function(FormsService, REGEXP, $rootScope, $filter, RESTURL, $http) {
	return {
		
		prepareHtmlTemplateName : function(prop, scope) {
			if(prop !== undefined && prop.type !== undefined) {
				switch(prop.type) {
				case 'Hidden': 
					prop.htmlTemplate = 'hidden';
					break;
				case 'Layout':
					prop.htmlTemplate = 'layout';
					break;
				case 'string': 
					prop.htmlTemplate = 'input-text';
					break;
				case 'long':
					prop.htmlTemplate = 'input-text';
					break;
				case 'enum':
					if(!prop.expandEnum) {
						prop.htmlTemplate = 'select';
					}
					else {
						prop.htmlTemplate = 'input-radio';
					}
					break;
				case 'boolean':
					prop.htmlTemplate = 'boolean';
					break;
				case 'Password':
					prop.htmlTemplate = 'password';
					break;
				case 'TextArea':
					prop.htmlTemplate = 'text-area';
					break;
				case 'date':
					prop.htmlTemplate = 'date';
					break;
				case 'Date':
					prop.htmlTemplate = 'date';
					break;
				case 'DateTime':
					prop.htmlTemplate = 'date-time';
					break;
				case 'Time':
					prop.htmlTemplate = 'time';
					break;
					
				case 'UserActivityPlanning':
					prop.htmlTemplate = '/object/planning/user-activity';
					break;
				case 'UserActivityPlanned':
					prop.htmlTemplate = '/object/planned/user-activity';
					break;
				case 'UserActivityWaiting':
					prop.htmlTemplate = '/object/waiting/user-activity';
					break;
				case 'UserActivityConfirmed':
					prop.htmlTemplate = '/object/confirmed/user-activity';
					break;
				case 'UserActivityOccurred':
					prop.htmlTemplate = '/object/occurred/user-activity';
					break;
				case 'UserActivityVerify':
					prop.htmlTemplate = '/object/verify/user-activity';
					break;			
				case 'UserActivityCorrect':
					prop.htmlTemplate = '/object/correct/user-activity';
					break;
				case 'UserServiceConfChangeStatus':
					prop.htmlTemplate = '/object/user-service-conf-change-status';
					break;
				case 'UserServiceConfSwitch':
					prop.htmlTemplate = '/object/user-service-conf-switch';
					break;
				case 'PharmacyActivation':
					prop.htmlTemplate = '/object/pharmacy-activation';
					break;	
				default:
					if(prop.type.indexOf('Enum') >= 0) {
						if(!prop.expandEnum) {
							prop.htmlTemplate = 'select';
						}
						else {
							prop.htmlTemplate = 'input-radio';
						}
					}
					else if(prop.type.indexOf('List') >= 0 && prop.type.indexOf('MultiList') < 0) {
						if(!prop.expandEnum) {
							prop.htmlTemplate = 'select-ui-one';
						}
						else {
							prop.htmlTemplate = 'input-radio';
						}
					}
					else if(prop.type.indexOf('MultiList') >= 0) {
						if(!prop.expandEnum) {
							prop.htmlTemplate = 'select-ui-multi';
						}
						else {
							prop.htmlTemplate = 'input-checkbox';
						}
					}
					else if(prop.type.indexOf('Checklist') >= 0) {
						prop.htmlTemplate = 'input-checkbox';
					}
					else if(prop.type.indexOf('MapKeysValues') >= 0) {
						prop.htmlTemplate = 'map-keys-values.html';
					}
					else {
						//prop.htmlTemplate = 'old-form';
						prop.htmlTemplate = 'input-text';
					}
					break;
				}
				prop.htmlTemplate = 'tpl/app/activiti/form-type/' + prop.htmlTemplate + '.html';
			}
		},
		
		completeForm : function(form, scope, enable) {
			scope.enableWatch = false;
			if($rootScope.completeVariables !== undefined) {
				$rootScope.completeVariables(form);
			}
			if(enable === undefined || enable == null) {
				enable = true;
			}
			var filters = {};
			var extraOptions = {};
			var toRemove = [];
			var nextWidth = undefined;
			var formTitle = undefined;
			var formDescription = undefined;
			scope.templatePdfEnrollment = 'patient-enrollment';
			scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy';
			scope.labels = {};
			
			form.saveButton = 'Salva';
			form.continueButton = 'Salva e Prosegui';
			form.continueConfirm = { title: 'Salvataggio e chiusura attivit\u00E0', text: 'Continuando l\'attivit\u00E0 verr\u00E0 completata e non sar\u00E0 possibile annullare l\'operazione.<br/>Successivamente verr\u00E0 presentata l\'eventuale attivit\u00E0 successiva.<br />Procedere?' };
			form.cancelButton = undefined;
			form.changeButton = undefined;
			form.canSkipActivity = true;
			form.helpButton = undefined;
			
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				if(prop.value == null) {
					prop.value = undefined;
				}
				if(prop.styleClass == null) {
					prop.styleClass = '';
				}
				if(prop.name == null) {
					prop.name = undefined;
				}
				prop.writable = prop.writable && enable;
				
				if(prop.type === 'ListFilter') {
					if(filters[prop.name] === undefined) {
						filters[prop.name] = {};
					}
					try {
						var filter = angular.fromJson(prop.value);
						if(angular.isObject(filter)) {
							filters[prop.name] = filter;
						}
					}
					catch(e) { }
					toRemove.push(prop);
					continue;
				}
				if(prop.type === 'ExtraOptions') {
					if(extraOptions[prop.name] === undefined) {
						extraOptions[prop.name] = {};
					}
					try {
						var opt = angular.fromJson(prop.value);
						if(angular.isObject(opt)) {
							extraOptions[prop.name] = opt;
						}
					}
					catch(e) {
					}
					toRemove.push(prop);
					continue;
				}
				if(prop.type === 'Labels') {
					if(prop.name !== undefined) {
						if(scope.labels[prop.name] === undefined) {
							scope.labels[prop.name] = {};
						}
						try {
							var label = angular.fromJson(prop.value);
							if(angular.isObject(label)) {
								scope.labels[prop.name] = label;
							}
						}
						catch(e) { }
					}
					toRemove.push(prop);
					continue;
				}
				prop.adapt = true;
				if(prop.type === 'Layout') {
					if(prop.name === 'Pagination') {
						prop.width = 12;
						if(prop.value === undefined || prop.value === '') {
							prop.minWidth = 0;
						}
						else {
							prop.minWidth = 6;
						}
						continue;
					}
					if(prop.name === 'Block') {
						prop.width = 6;
						if(prop.value === undefined || prop.value === '') {
							prop.minWidth = 0;
						}
						else {
							prop.minWidth = 3;
						}
					}
					else if(prop.name === 'Line') {
						prop.width = 12;
						if(prop.value === undefined || prop.value === '') {
							prop.minWidth = 0;
						}
						else {
							prop.minWidth = 7;
						}
					}
					else if(prop.name === 'Html') {
						prop.width = 12;
						if(prop.value === undefined || prop.value === '') {
							prop.minWidth = 0;
						}
						else {
							prop.minWidth = 7;
						}
					}
					else if(prop.name === 'Separator') {
						prop.adapt = false;
						prop.width = 12;
					}
					else if(prop.name === 'Header') {
						prop.adapt = false;
						prop.width = 12;
					}
					else if(prop.name === 'Message') {
						if(prop.value === undefined || prop.value === '') {
							toRemove.push(prop);
						}
						else {
							prop.adapt = false;
							prop.width = 12;
						}
					}
					else if(prop.name === 'NextSize') {
						toRemove.push(prop);
						if(!isNaN(prop.value)) {
							nextWidth = prop.value;	
						}
						continue;
					}
					else if(prop.name === 'FormTitle') {
						toRemove.push(prop);
						formTitle = prop.value;
						continue;
					}
					else if(prop.name === 'FormDescription') {
						toRemove.push(prop);
						formDescription = prop.value;
						continue;
					}
					else if(prop.name === 'TaskDueDate') {
						prop.readable = false;
						continue;
					}
					else if(prop.name === 'CancelButton') {
						toRemove.push(prop);
						if(prop.value !== undefined) {
							form.cancelButton = prop.value;
						}
						continue;
					}
					else if(prop.name === 'ChangeButton') {
						toRemove.push(prop);
						if(prop.value !== undefined) {
							form.changeButton = prop.value;
						}
						continue;
					}
					else if(prop.name === 'ContinueConfirm') {
						toRemove.push(prop);
						if(prop.value !== undefined) {
							var value = angular.fromJson(prop.value);
							if(angular.isString(value)) {
								form.continueConfirm.text = value;
							}
							else if(angular.isObject(value)) {
								if(value.title !== undefined) {
									form.continueConfirm.title = value.title;	
								}
								if(value.text !== undefined) {
									form.continueConfirm.text = value.text;	
								}
							}
						}
						continue;
					}
					else if(prop.name === 'HelpButton') {
						toRemove.push(prop);
						if(prop.value !== undefined) {
							form.helpButton = prop.value;
						}
						continue;
					}
					else {
						toRemove.push(prop);
						continue;
					}
				}
				else if(prop.type === 'date') {
					prop.width = 2;
					prop.type = 'Date';
					if(prop.value !== undefined && prop.value !== '' && angular.isString(prop.value)) {
						prop.value = parseInt(prop.value, 10);
					}
				}
				else if(prop.type === 'Date') {
					prop.width = 2;
					if(prop.value !== undefined && prop.value !== '' && angular.isString(prop.value)) {
						prop.value = parseInt(prop.value, 10);
					}
				}
				else if(prop.type === 'DateTime') {
					prop.width = 3;
					if(prop.value !== undefined && prop.value !== '' && angular.isString(prop.value)) {
						prop.value = parseInt(prop.value, 10);
					}
				}
				else if(prop.type === 'long') {
					prop.width = 2;
				}
				else if(prop.type === 'double' || prop.type === 'float') {
					prop.width = 2;
				}
				else if(prop.type === 'Double') {
					prop.width = 3;
				}
				else if(prop.type === 'Time') {
					prop.width = 2;
				}
				else if(prop.type === 'boolean') {
					prop.width = 3;
					if(prop.value !== undefined && prop.value !== '' && angular.isString(prop.value)) {
						prop.value = (prop.value === 'true');
					}
				}
				else if(prop.type === 'MessageAlertObject') {
					prop.value = angular.fromJson(prop.value);
					this.loadObjectEnumValues(prop, 'DrugFormulationEnum', {});
					this.loadObjectEnumValues(prop, 'SLANonComplianceReasonFilteredEnum', { "USER_GROUP": "Patient" }, 'SLANonComplianceReasonForPatient');
					this.loadObjectEnumValues(prop, 'SLANonComplianceReasonFilteredEnum', { "USER_GROUP": "HealthCareProfessional" }, 'SLANonComplianceReasonForHcp');
				}
				else if(prop.type === 'PhoneNumber') {
					prop.width = 3;
					prop.pattern = REGEXP.PHONE_NUMBER;
					prop.customMessage = 'Formato errato (es. +39021234567)';
				}
				else if(prop.type === 'CellPhoneNumber') {
					prop.width = 3;
					prop.pattern = REGEXP.PHONE_NUMBER;
					prop.customMessage = 'Formato errato (es. +391234567)';
				}
				else if(prop.type === 'Email') {
					prop.width = 6;
					prop.pattern = REGEXP.EMAIL;
					prop.customMessage = 'Formato email non corretto';
				}
				else if(prop.type === 'PostalCode') {
					prop.width = 2;
					prop.pattern = REGEXP.ZIP_CODE;
					prop.customMessage = 'Richiesto numero di 5 cifre (es. 20100)';
				}
				else if(prop.type === 'NickName') {
					prop.width = 6;
					prop.pattern = REGEXP.NICK_NAME;
					prop.customMessage = 'Richiesti almeno 6 caratteri, ammessi numeri, lettere, \'.\', \'-\' e \'_\'';
				}
				else if(prop.type === 'Password') {
					prop.width = 6;
					prop.pattern = REGEXP.PASSWORD;
					prop.customMessage = 'Richiesti almeno 8 caratteri, massimo 16 caratteri e deve contenere almeno una maiuscola, una minuscola e un numero';
				}
				else if(prop.type === 'TextArea') {
					prop.width = 12;
					prop.adapt = false;
				}
				else if(prop.type === 'Html') {
					prop.width = 12;
					prop.adapt = false;
				}
				else if(prop.type === 'Hidden') {
					prop.width = 0;
					prop.adapt = false;
				}
				else if(prop.type === 'UserActivityPlanning') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserActivityPlanned') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserActivityWaiting') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
					form.canSkipActivity = false;
				}
				else if(prop.type === 'UserActivityConfirmed') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserActivityOccurred') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserActivityVerify') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserActivityCorrect') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserServiceConfChangeStatus') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'UserServiceConfSwitch') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else if(prop.type === 'PharmacyActivation') {
					prop.width = 12;
					prop.adapt = false;
					prop.value = angular.fromJson(prop.value);
				}
				else {
					if(prop.type.indexOf('Checklist') > 0) {
						prop.value = angular.fromJson(prop.value);
						prop.width = 12;
						prop.adapt = false;
					}
					else if(prop.type.indexOf('MultiList') > 0) {
						prop.value = angular.fromJson(prop.value);
						prop.width = 6;
					}
					else if(prop.type.indexOf('Object') > 0) {
						prop.value = angular.fromJson(prop.value);
						prop.adapt = false;
					}
					else {
						prop.width = 6;
					}
				}
				if(nextWidth !== undefined) {
					prop.width = nextWidth;
					nextWidth = undefined;
					prop.adapt = false;
				}
				if(prop.adapt && prop.minWidth === undefined) {
					switch(prop.width) {
					case 1:
					case 2:
					case 3:
						prop.minWidth = prop.width;
						break;
					case 4:
						prop.minWidth = 3;
						break;
					case 5:
					case 6:
						prop.minWidth = 4;
						break;
					case 7:
					case 8:
						prop.minWidth = 6;
						break;
					case 9:
					case 10:
						prop.minWidth = 8;
						break;
					case 11:
					case 12:
						prop.minWidth = 10;
						break;
					}
				}
			}
			for(var i=0; i<toRemove.length; i++) {
				form.formProperties.splice(form.formProperties.indexOf(toRemove[i]), 1);
			}
			
			var blockSize = 6;
			var space = blockSize;
			var doubleSpace = blockSize * 2;
			var spaceSmall = blockSize * 2;
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				
				if(extraOptions.hasOwnProperty(prop.id)) {
					angular.extend(prop, extraOptions[prop.id]);
				}
				prop.widthSmall = Math.min(prop.width * 2, 12);
				prop.minWidthSmall = Math.min(prop.minWidth * 2, 12);
				if(prop.width > blockSize) {
					if(doubleSpace < prop.width) {
						if(prop.adapt && doubleSpace >= prop.minWidth) {
							prop.width = doubleSpace;
							space = 0;
							doubleSpace = 0;
						}
						else {
//							form.formProperties[i-1].width = form.formProperties[i-1].width + space;
							space = (blockSize * 2) - prop.width;
							doubleSpace = (blockSize * 2) - prop.width;
						}
					}
					else {
						space = doubleSpace - prop.width;
						doubleSpace = doubleSpace - prop.width;
					}
					if(space <= 0) {
						space = blockSize;
					}
					if(doubleSpace <= 0) {
						doubleSpace = blockSize * 2;
					}
				}
				else {
					if(space < prop.width) {
						if(prop.adapt && space >= prop.minWidth) {
							prop.width = space;
							space = 0;
							doubleSpace = doubleSpace - prop.width;
						}
						else {
							if(doubleSpace <= blockSize) {
								form.formProperties[i-1].width = form.formProperties[i-1].width + space;
								space = blockSize - prop.width;
								doubleSpace = doubleSpace - prop.width;
							}
							else {
								space = space + (blockSize - prop.width);
								doubleSpace = doubleSpace - prop.width;
							}
						}
					}
					else {
						space = space - prop.width;
						doubleSpace = doubleSpace - prop.width;
					}
					if(space <= 0) {
						space = blockSize;
					}
					if(doubleSpace <= 0) {
						doubleSpace = blockSize * 2;
					}
				}
				if(spaceSmall < prop.widthSmall) {
					if(prop.adapt && spaceSmall >= prop.minWidthSmall) {
						prop.widthSmall = spaceSmall;
						spaceSmall = 0;
					}
					else {
						if(prop.widthSmall < (blockSize * 2)) {
							form.formProperties[i-1].widthSmall = form.formProperties[i-1].widthSmall + spaceSmall;
						}
						spaceSmall = (blockSize * 2) - prop.widthSmall;
					}
				}
				else {
					spaceSmall = spaceSmall - prop.widthSmall;
				}
				if(spaceSmall <= 0) {
					spaceSmall = blockSize * 2;
				}
				
				this.registerVisibility(form, prop);
			}
			if(formTitle != undefined) {
				form.title = formTitle;
			}
			if(formDescription != undefined) {
				form.description = formDescription;
			}
			var service = this;
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				
				this.prepareHtmlTemplateName(prop, scope); // A questo punto le varie opzioni di visualizazione (expandEnum...) dovrebbero essere già settate
				
				prop.form = form;
				prop.getFormAsMapForPdf = function() {
					return service.getPropertiesValueMapForPdf(form);
				};
				if(prop.type !== undefined && prop.type.indexOf('Enum') >= 0) {
					if(filters.hasOwnProperty(prop.id)) {
						var almostOneWatch = false;
						for(var key in filters[prop.id]) {
							var filterValue = filters[prop.id][key];
							if(angular.isString(filterValue) && filterValue.indexOf('#') == 0) {
								var property = prop;
								if(angular.element(filterValue) !== undefined) {
									almostOneWatch = true;
									service.registerWatch(scope, filters, form.formProperties, prop, key, service);
								}
							}
							else if(angular.isObject(filterValue)) {
								for(var fvKey in filterValue) {
									var fValue = filterValue[fvKey];
									if(angular.isString(fValue) && fValue.indexOf('#') == 0) {
										var property = prop;
										if(angular.element(fValue) !== undefined) {
											almostOneWatch = true;
											service.registerWatch(scope, filters, form.formProperties, prop, key, service);
										}
									}
								}
							}
						}
						if(!almostOneWatch) {
							var filtersTmp = {};
							angular.copy(filters, filtersTmp);
							this.loadEnumValues(scope, prop, filtersTmp[prop.id]);
						}
					}
					else {
						this.loadEnumValues(scope, prop);
					}
				}
				else if(prop.type !== undefined && prop.type.indexOf('Checklist') >= 0) {
					if(filters.hasOwnProperty(prop.id)) {
						var almostOneWatch = false;
						for(var key in filters[prop.id]) {
							var filterValue = filters[prop.id][key];
							if(angular.isString(filterValue) && filterValue.indexOf('#') == 0) {
								var property = prop;
								if(angular.element(filterValue) !== undefined) {
									almostOneWatch = true;
									service.registerWatch(scope, filters, form.formProperties, prop, key, service);
								}
							}
							else if(angular.isObject(filterValue)) {
								for(var fvKey in filterValue) {
									var fValue = filterValue[fvKey];
									if(angular.isString(fValue) && fValue.indexOf('#') == 0) {
										var property = prop;
										if(angular.element(fValue) !== undefined) {
											almostOneWatch = true;
											service.registerWatch(scope, filters, form.formProperties, prop, key, service);
										}
									}
								}
							}
						}
						if(!almostOneWatch) {
							var filtersTmp = {};
							angular.copy(filters, filtersTmp);
							this.loadChecklistValues(prop, filtersTmp[prop.id]);
						}
					}
					else {
						this.loadChecklistValues(prop);
					}
				}
				else if(prop.type !== undefined && (prop.type.indexOf('Pdf') >= 0 && prop.type.indexOf('List') < 0)) {
					prop.isFormValid = function(propId) {
						var errors = service.validateForm(form, false);
						if(errors === undefined || errors.length == 0) {
							return true;
						}
						else {
							var himself = true;
							for(var key in errors) {
								if(key.indexOf(propId) !== 0) {
									himself = false;
									break;
								}
							}
							return himself;
						}
					}
				}
				else if(prop.type !== undefined && (prop.type.indexOf('List') >= 0 || prop.type.indexOf('Object') >= 0)) {
					if(filters.hasOwnProperty(prop.id)) {
						var almostOneWatch = false;
						for(var key in filters[prop.id]) {
							var filterValue = filters[prop.id][key];
							if(angular.isString(filterValue) && filterValue.indexOf('#') == 0) {
								var property = prop;
								if(angular.element(filterValue) !== undefined) {
									almostOneWatch = true;
									service.registerWatch(scope, filters, form.formProperties, prop, key, service);
								}
							}
							else if(angular.isObject(filterValue)) {
								for(var fvKey in filterValue) {
									var fValue = filterValue[fvKey];
									if(angular.isString(fValue) && fValue.indexOf('#') == 0) {
										var property = prop;
										if(angular.element(fValue) !== undefined) {
											almostOneWatch = true;
											service.registerWatch(scope, filters, form.formProperties, prop, key, service);
										}
									}
								}
							}
						}
						if(!almostOneWatch) {
							var filtersTmp = {};
							angular.copy(filters, filtersTmp);
							this.loadListValues(prop, this.prepareFilters(form.properties, filtersTmp[prop.id], this));
						}
					}
					else {
						this.loadListValues(prop);
					}
					if(prop.type.indexOf('Object') >= 0) {
						this.registerWatchObjectSelected(scope, prop);
					}
				}
				this.registerPropChangesData(scope, prop, form.formProperties);
			}
			this.registerUserTasksWatch(scope, form, this);
			
			scope.enableWatch = true;
		},
		
		registerVisibility : function(form, prop) {
			// Gestione visibilità condizionata
			prop.isVisible = function() {
				if(this.visibleIfEquals!==undefined && this.visibleIfEquals!= null) {
					if(angular.isObject(prop.visibleIfEquals)) {
						for(var p=0; p<form.formProperties.length; p++) {
							var pe = form.formProperties[p];
							for(var key in prop.visibleIfEquals) {
								if(pe.id==key && this.visibleIfEquals[key] !== undefined && this.visibleIfEquals[key] != null && this.visibleIfEquals[key] !== '' && this.visibleIfEquals[key] === pe.value) {
									return true;
								}
							}
						}
						return false;
					}
				}
				return true;
			}
		},
		
		registerWatch : function(scope, filters, properties, prop, key, service) {
			
			scope.$watch(
					function() {
						var filterValue = filters[prop.id][key];
						if(filterValue !== undefined && filterValue != null) {
							if(angular.isString(filterValue)) {
								for(var i=0; i<properties.length; i++) {
									var p = properties[i];
									if('#'+p.id === filters[prop.id][key]) {
										return p.value;
									}
								}
							}
							else if(angular.isObject(filterValue)) {
								for(var i=0; i<properties.length; i++) {
									var p = properties[i];
									for(var fv in filterValue)
									if('#'+p.id === filterValue[fv]) {
										return p.value;
									}
								}
							}
						}
						return undefined;
					},
					function(nv, ov) {
						var filtersTmp = service.prepareFilters(properties, filters[prop.id], service);
						var requireFilter = prop.requireFilter !== undefined && prop.requireFilter != null ? prop.requireFilter : false;
						if(!requireFilter || !angular.equals(filtersTmp, {})) {
							if(prop.type !== undefined && prop.type.indexOf('Enum') >= 0) {
								service.loadEnumValues(scope, prop, filtersTmp);
							}
							else if(prop.type !== undefined && prop.type.indexOf('Checklist') >= 0) {
								service.loadChecklistValues(prop, filtersTmp);
							}
							else if(prop.type !== undefined && (prop.type.indexOf('List') >= 0 || prop.type.indexOf('Object') >= 0)) {
								service.loadListValues(prop, filtersTmp);
							}
						}
					}, true);
		},
		
		loadObjectSelected : function(prop) {
			prop.enumValues = [];
			prop.selectedObjects = [];
			if(prop.value !== undefined && prop.value != null) {
				if(angular.isArray(prop.value)) {
					prop.selectedObjects = [];
					var index = 0;
					for(var j=0; j<prop.value.length; j++) {
						if(prop.value[j].selected) {
							if(prop.onlyOne) {
								prop.selectedObjects = prop.value[j].id;
							}
							else {
								prop.selectedObjects[index] = prop.value[j].id;
								index++;
							}
						}
					}
				}
				else {
					if(prop.selected) {
						prop.selectedObjects = p.value[j].id;
					}
				}
			}
		},
		
		registerWatchObjectSelected : function(scope, prop) {
			if(prop !== undefined && prop != null) {
				prop.savedValue = angular.copy(prop.value);
				this.loadObjectSelected(prop);
				
				scope.$watch(
						function() {
							return prop.selectedObjects;
						},
						function(nv, ov) {
							if(prop.value !== undefined && prop.value != null) {
								if(angular.isArray(prop.value)) {
									for(var i=0; i<prop.value.length; i++) {
										if(prop.selectedObjects !== undefined && prop.selectedObjects != null && prop.selectedObjects.indexOf(prop.value[i].id) >= 0) {
											prop.value[i].selected = true;
										}
										else {
											prop.value[i].selected = false;
										}
									}
								}
								else {
									if(prop.selectedObjects !== undefined && prop.selectedObjects != null && prop.selectedObjects.indexOf(prop.value.id) >= 0) {
										prop.value.selected = true;
									}
									else {
										prop.value.selected = false;
									}
								}
							}
						}, true);
			}
		},
		
		prepareFilters : function(properties, filters, service) {
			var filtersTmp = {};
			angular.copy(filters, filtersTmp);
			if(filtersTmp !== undefined) {
				for(var key in filtersTmp) {
					var val = filtersTmp[key];
					if(val !== undefined && val != null) {
						if(angular.isString(val)) {
							if(val.indexOf('#') == 0) {
								var value = service.getPropertyValueForFilter(properties, val);
								filtersTmp[key] = value;
							}
						}
						else if(angular.isObject(val)) {
							for(var v in val) {
								if(angular.isString(val[v]) && val[v].indexOf('#') == 0) {
									var value = service.getPropertyValueForFilter(properties, val[v]);
									filtersTmp[key][v] = value;
								}
							}
							filtersTmp[key] = angular.toJson(filtersTmp[key]);
						}
					}
				}
			}
			return filtersTmp;
		},
		
		getPropertyValueForFilter : function(properties, id) {
			for(var i=0; i<properties.length; i++) {
				var p = properties[i];
				if('#'+p.id === id) {
					if(p.type.indexOf('Object') < 0) {
						return p.value;
					}
					else {
						if(angular.isArray(p.selectedObjects)) {
							return p.selectedObjects.join(',');
						}
						else {
							return p.selectedObjects;
						}
					}
				}
			}
			return undefined;
		},
		
		registerPropChangesValueObjectData : function(scope, prop, properties) {
			if(prop.hasOwnProperty('valueObject')) {
				scope.$watch(function() {
					return prop.valueObject;
				},
				function(nv, ov) {

					if((nv !== undefined && ov !== undefined && !angular.equals(nv, ov)) || (nv === undefined && ov !== undefined)  || (nv !== undefined && ov === undefined)) {
						if(nv !== undefined && nv != null && nv !== '' && angular.isObject(nv)) {

							if(nv.id !== undefined && nv.id != null) {
								prop.value = nv.id;
							}
							else {
								prop.value = nv.name;
							}
						}
						else {
							prop.value = undefined;
						}
					}
				}, true);
			}
		},
		
		registerPropChangesData : function(scope, prop, properties) {
			
			if((prop.dependRequired!=undefined && prop.dependRequired!= null)
				|| (prop.dependRequiredIfValued!=undefined && prop.dependRequiredIfValued!= null)
				|| (prop.requiredIfEquals!=undefined && prop.requiredIfEquals!= null)
				|| (prop.type === 'PacketTypeEnum' || prop.type === 'PacketTypeFilteredEnum')) {
				
				for(var i=0; i<properties.length; i++) {
					var p = properties[i];
					if(prop.dependRequired!=undefined && prop.dependRequired!= null) {
						if(angular.isArray(prop.dependRequired)) {
							for(var j=0; j<prop.dependRequired.length; j++) {
								if(p.id === prop.dependRequired[j]) {
									if(p.clearErrors === undefined) {
										p.clearErrors = [];
									}
									p.clearErrors.push(prop.id);
								}
							}
						}
						else if(angular.isString(prop.dependRequired)) {
							if(p.id === prop.dependRequired) {
								if(p.clearErrors === undefined) {
									p.clearErrors = [];
								}
								p.clearErrors.push(prop.id);
							}
						}
					}
					if(prop.dependRequiredIfValued!=undefined && prop.dependRequiredIfValued!= null) {
						if(angular.isArray(prop.dependRequiredIfValued)) {
							for(var j=0; j<prop.dependRequiredIfValued.length; j++) {
								if(p.id === prop.dependRequiredIfValued[j]) {
									if(p.clearErrors === undefined) {
										p.clearErrors = [];
									}
									p.clearErrors.push(prop.id);
								}
							}
						}
						else if(angular.isString(prop.dependRequiredIfValued)) {
							if(p.id === prop.dependRequiredIfValued) {
								if(p.clearErrors === undefined) {
									p.clearErrors = [];
								}
								p.clearErrors.push(prop.id);
							}
						}
					}
					if(prop.requiredIfEquals!=undefined && prop.requiredIfEquals!= null) {
						if(angular.isObject(prop.requiredIfEquals)) {
							for(var key in prop.requiredIfEquals) {
								if(p.id === key) {
									if(p.clearErrors === undefined) {
										p.clearErrors = [];
									}
									p.clearErrors.push(prop.id);
								}
							}
						}
					}
				}
			}
			scope.$watch(function() {
				return prop.value;
			},
			function(nv, ov) {
				if(scope.errors !== undefined && scope.errors != null && scope.errors[prop.id] !== undefined) {
					scope.errors[prop.id] = '';
				}
				if((nv !== undefined && ov !== undefined && !angular.equals(nv, ov)) || (nv === undefined && ov !== undefined)  || (nv !== undefined && ov === undefined)) {
					prop.lastModified = new Date();
					if(nv !== undefined && nv != null && nv !== '') {
						
						if(prop.targetField != undefined && prop.targetField != null) {
							var propId = prop.targetField.replace('#','');
							for(var i=0; i<properties.length; i++) {
								if(properties[i].id == propId) {
									properties[i].value = nv;
								}
							}
						}
						
						
						for(var i=0; i<properties.length; i++) {
							var p = properties[i];
							if(prop.reset !== undefined && angular.isArray(prop.reset)) {
								for(var j=0; j<prop.reset.length; j++) {
									if(p.id === prop.reset[j]) {
										p.value = undefined;
										if(scope.errors !== undefined && scope.errors != null && scope.errors[prop.id] !== undefined) {
											scope.errors[p.id] = '';
										}
									}
								}
							}
						}
						if(scope.errors !== undefined && scope.errors != null) {
							if(prop.clearErrors!=undefined && prop.clearErrors!= null) {
								for(var j=0; j<prop.clearErrors.length; j++) {
									scope.errors[prop.clearErrors[j]] = '';
								}
							}
						}
					}
				}
				if(prop.type === 'PacketTypeEnum' || prop.type === 'PacketTypeFilteredEnum') {
					switch(prop.value) {
					case 'LOW':
						scope.templatePdfEnrollment = 'patient-enrollment';
						scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy';
						break;
					case 'MEDIUM':
						scope.templatePdfEnrollment = 'patient-enrollment';
						scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy';
						break;
					case 'FULL':
						scope.templatePdfEnrollment = 'patient-enrollment';
						scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy';
						break;
					case 'HS':
						scope.templatePdfEnrollment = 'patient-enrollment-hs';
						scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy-hs';
						break;
					default:
						scope.templatePdfEnrollment = 'patient-enrollment';
						scope.templatePdfEnrollmentPrivacy = 'patient-enrollment-privacy';
						break;
					}
				}
			}, true);
			
		},
		
		validateForm : function(form, viewLog) {
			console.log('****** validate form *********');
			if(viewLog === undefined) viewLog = true;
			var errors = {};
			if(form==undefined || form == null  || form.formProperties == undefined || form.formProperties == null) return errors;
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				if(prop.required && (prop.value === undefined || prop.value == null || (prop.value + '').trim() == '')) {
					errors[prop.id] = "Campo obbligatorio";
					if(prop.type === 'boolean') {
						errors[prop.id] = "Selezionare Si o No";
					}
					prop.value = undefined;
				}
				if(prop.dependRequired!=undefined && prop.dependRequired!= null) {
					if(angular.isArray(prop.dependRequired)) {
						var otherProps = [];
						var index = 0;
						for(var p=0; p<form.formProperties.length; p++) {
							for(var d=0; d<prop.dependRequired.length; d++) {
								if(form.formProperties[p].id==prop.dependRequired[d]) {
									otherProps[index] = form.formProperties[p];
									index = index + 1;
									break;
								}
							}
						}
						if(otherProps.length > 0) {
							// Il campo è obbligatorio se tutte le dipendenze sono vuote
							var names = [];
							for(var d=0; d<prop.dependRequired.length; d++) {
								if ( (otherProps[d].value === undefined || otherProps[d].value == null || otherProps[d].value === '') && (prop.value === undefined || prop.value == null || prop.value === '') ) {
									if(otherProps[d].name !== undefined && otherProps[d].name != null && otherProps[d].name !== '') {
										names.push(otherProps[d].name);
									}
									else {
										if(otherProps[d].alternativeName !== undefined && otherProps[d].alternativeName != null && otherProps[d].alternativeName !== '') {
											names.push(otherProps[d].alternativeName);
										}
										else {
											errors[prop.id] = 'Campo obbligatorio';
										}
									}
								}
								else {
									// Almeno una delle dipendenze o lui stesso è valorizzato
									names = [];
									break;
								}
							}
							if(names.length > 0) {
								errors[prop.id] = 'Obbligatorio se';
								for(var n=0; n<names.length; n++) {
									if(n == 0) {
										errors[prop.id] = errors[prop.id] + ' ' + names[n];	
									}
									else {
										errors[prop.id] = errors[prop.id] + ' o ' + names[n];
									}
									if(n == (names.length - 1)) {
										if(n == 0) {
											errors[prop.id] = errors[prop.id] + ' non valorizzato';
										}
										else {
											errors[prop.id] = errors[prop.id] + ' non valorizzati';
										}
									}
								}
							}
						}
					}
					else if(angular.isString(prop.dependRequired)) {
						var otherProp = null;
						for(var p=0; p<form.formProperties.length; p++) {
							if(form.formProperties[p].id==prop.dependRequired) {
								otherProp = form.formProperties[p];
								break;
							}
						}
						if(otherProp!=undefined && otherProp!=null) {
							if ( (otherProp.value === undefined || otherProp.value == null || otherProp.value === '') && (prop.value === undefined || prop.value == null || prop.value === '') ) {
								if(otherProp.name !== undefined && otherProp.name != null && otherProp.name !== '') {
									errors[prop.id] = 'Obbligatorio se '+otherProp.name+' non valorizzato';
								}
								else {
									if(otherProp.alternativeName !== undefined && otherProp.alternativeName != null && otherProp.alternativeName !== '') {
										errors[prop.id] = 'Obbligatorio se '+otherProp.alternativeName+' non valorizzato';
									}
									else {
										errors[prop.id] = 'Campo obbligatorio';
									}
								}
							}
						}
					}
					else {
						
					}
				}
				if(prop.dependRequiredIfValued!=undefined && prop.dependRequiredIfValued!= null) {
					if(angular.isArray(prop.dependRequiredIfValued)) {
						var otherProps = [];
						var index = 0;
						for(var p=0; p<form.formProperties.length; p++) {
							for(var d=0; d<prop.dependRequiredIfValued.length; d++) {
								if(form.formProperties[p].id==prop.dependRequiredIfValued[d]) {
									otherProps[index] = form.formProperties[p];
									index = index + 1;
									break;
								}
							}
						}
						if(otherProps.length > 0) {
							var names = [];
							for(var d=0; d<prop.dependRequiredIfValued.length; d++) {
								if ( (otherProps[d].value != undefined && otherProps[d].value != null && otherProps[d].value != '') && (prop.value === undefined || prop.value == null || prop.value === '') ) {
									if(otherProps[d].name !== undefined && otherProps[d].name != null && otherProps[d].name !== '') {
										names.push(otherProps[d].name);
									}
									else {
										if(otherProps[d].alternativeName !== undefined && otherProps[d].alternativeName != null && otherProps[d].alternativeName !== '') {
											names.push(otherProps[d].alternativeName);
										}
										else {
											errors[prop.id] = 'Campo obbligatorio';
										}
									}
								}
							}
							if(names.length > 0) {
								errors[prop.id] = 'Obbligatorio se';
								for(var n=0; n<names.length; n++) {
									if(n == 0) {
										errors[prop.id] = errors[prop.id] + ' ' + names[n];	
									}
									else {
										errors[prop.id] = errors[prop.id] + ' o ' + names[n];
									}
									if(n == (names.length - 1)) {
										if(n == 0) {
											errors[prop.id] = errors[prop.id] + ' valorizzato';
										}
										else {
											errors[prop.id] = errors[prop.id] + ' valorizzati';
										}
									}
								}
							}
						}
					}
					else if(angular.isString(prop.dependRequiredIfValued)) {
						var otherProp = null;
						for(var p=0; p<form.formProperties.length; p++) {
							if(form.formProperties[p].id==prop.dependRequiredIfValued) {
								otherProp = form.formProperties[p];
								break;
							}
						}
						if(otherProp!=undefined && otherProp!=null) {
							if ( (otherProp.value != undefined && otherProp.value != null && otherProp.value != '') && (prop.value === undefined || prop.value == null || prop.value === '') ) {
								if(otherProp.name !== undefined && otherProp.name != null && otherProp.name !== '') {
									errors[prop.id] = 'Obbligatorio se '+otherProp.name+' valorizzato';
								}
								else {
									if(otherProp.alternativeName !== undefined && otherProp.alternativeName != null && otherProp.alternativeName !== '') {
										errors[prop.id] = 'Obbligatorio se '+otherProp.alternativeName+' valorizzato';
									}
									else {
										errors[prop.id] = 'Campo obbligatorio';
									}
								}
							}
						}
					}
					else {
						
					}
				}
				if(prop.requiredIfEquals!=undefined && prop.requiredIfEquals!= null) {
					if(angular.isObject(prop.requiredIfEquals)) {
						if(prop.value === undefined || prop.value == null || prop.value === '') {
							var names = [];
							for(var p=0; p<form.formProperties.length; p++) {
								var pe = form.formProperties[p];
								for(var key in prop.requiredIfEquals) {
									if(pe.id==key && prop.requiredIfEquals[key] !== undefined && prop.requiredIfEquals[key] != null && prop.requiredIfEquals[key] !== '' && prop.requiredIfEquals[key] === pe.value) {
										if(pe.name !== undefined && pe.name != null && pe.name !== '') {
											names.push(pe.name);
										}
										else {
											if(pe.alternativeName !== undefined && pe.alternativeName != null && pe.alternativeName !== '') {
												names.push(pe.alternativeName);
											}
											else {
												errors[prop.id] = 'Campo obbligatorio';
											}
										}
										
									}
								}
							}
							if(names.length > 0) {
								errors[prop.id] = 'Obbligatorio per il valore indicato in ';
								for(var n=0; n<names.length; n++) {
									if(n == 0) {
										errors[prop.id] = errors[prop.id] + ' "' + names[n] + '"';
									}
									else {
										errors[prop.id] = errors[prop.id] + ' o "' + names[n] + '"';
									}
								}
							}
						}
					}
				}
				
				if(prop.equals != undefined && prop.equals!=null) {
					var otherProp = null;
					for(var p=0; p<form.formProperties.length; p++) {
						if(form.formProperties[p].id==prop.equals) {
							otherProp = form.formProperties[p];
							break;
						}
					}
					if(otherProp!=undefined && otherProp!=null) {
						if ( otherProp.value!=prop.value ) {
							errors[prop.id] = 'Il valore di questo campo deve essere uguale a quello di  '+otherProp.name+'';
						}
					}
				}
				if(prop.pattern != undefined && prop.pattern!=null) {
					if(prop.value !== undefined && prop.value != null && prop.value != '') {
						if(!prop.pattern.test(prop.value)) {
							errors[prop.id] = "Formato errato";
							if( (prop.customMessage!=undefined && prop.customMessage!=null) ) {
								errors[prop.id] = prop.customMessage;
							}
						}
					}
				}
				if(prop.value !== undefined && prop.value != null) {
					switch(prop.type) {
					case 'string': 
						if(prop.value.length > 4000) {
							errors[prop.id] = "Valore troppo lungo, massimo 4000 caratteri";
						}
						break;
					case 'string': 
						if(prop.value.length > 4000) {
							errors[prop.id] = "Valore troppo lungo, massimo 4000 caratteri";
						}
						break;
					case 'TextArea': 
						if(prop.value.length > 4000) {
							errors[prop.id] = "Valore troppo lungo, massimo 4000 caratteri";
						}
						break;
					case 'Html': 
						if(prop.value.length > 4000) {
							errors[prop.id] = "Valore troppo lungo, massimo 4000 caratteri";
						}
						break;
					case 'long': 
						if(!angular.isNumber(prop.value) && isNaN(prop.value)) {
							errors[prop.id] = "Richiesto un numero";
						}
						break;
					case 'float':
						if(!angular.isNumber(prop.value) && isNaN(prop.value)) {
							errors[prop.id] = "Richiesto un numero intero o decimale (usare \".\" come separatore)";
						}
						break;
					case 'double':
						if(!angular.isNumber(prop.value) && isNaN(prop.value)) {
							errors[prop.id] = "Richiesto un numero intero o decimale (usare \".\" come separatore)";
						}
						break;
					case 'Integer': 
						if(!angular.isNumber(prop.value) && parseInt(prop.value, 10) === prop.value) {
							errors[prop.id] = "Richiesto un numero intero";
						}
						break;
					case 'date':
						if(!angular.isDate(prop.value)) {
							errors[prop.id] = "Richiesta una data";
							if( (prop.customMessage!=undefined && prop.customMessage!=null) ) {
								errors[prop.id] = prop.customMessage;
							}
						}
						break;
					case 'boolean':
						if((prop.value !== true && prop.value !== false && prop.value !== 'true' && prop.value !== 'false')) {
							errors[prop.id] = "Richiesto un valore booleano";
							if( (prop.customMessage!=undefined && prop.customMessage!=null) ) {
								errors[prop.id] = prop.customMessage;
							}
						}
						break;
					case 'PatientEnrollmentServiceObject':
						for(var o=0; o<prop.value.length; o++) {
							var p = prop.value[o];
							if(p.firstName === undefined || p.firstName == null || p.firstName == '') {
								errors[prop.id + '_' + o + '_firstName'] = 'Campo obbligatorio';
							}
							if(p.lastName === undefined || p.lastName == null || p.lastName == '') {
								errors[prop.id + '_' + o + '_lastName'] = 'Campo obbligatorio';
							}
							if(p.delegateContactType !== 'PATIENT') {
								if(p.delegateContactFirstName === undefined || p.delegateContactFirstName == null || p.delegateContactFirstName == '') {
									errors[prop.id + '_' + o + '_delegateContactFirstName'] = 'Campo obbligatorio';
								}
								if(p.delegateContactLastName === undefined || p.delegateContactLastName == null || p.delegateContactLastName == '') {
									errors[prop.id + '_' + o + '_delegateContactLastName'] = 'Campo obbligatorio';
								}
							}
						}
						break;
					default:
						if(prop.enumValues !== undefined && prop.enumValues.length > 0) {
							var found = false;
							if(angular.isArray(prop.value)) {
								for(var k=0; k<prop.value.length; k++) {
									if(prop.value[k] !== undefined && prop.value[k] != null && prop.value[k] !== '') {
										for(var j=0; j<prop.enumValues.length; j++) {
											if(prop.enumValues[j].id === prop.value[k]) {
												found = true;
												break;
											}
										}
										if(!found) {
											if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
												errors[prop.id] = "Valore non previsto";
											}
										}	
									}
								}
							}
							else {
								for(var j=0; j<prop.enumValues.length; j++) {
									if(prop.enumValues[j].id === prop.value) {
										found = true;
										break;
									}
								}
								if(!found) {
									if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
										errors[prop.id] = "Valore non previsto";
									}
								}
							}
						}
						if(prop.type !== 'Layout' && prop.writable && prop.value.length > 255) {
							errors[prop.id] = "Valore troppo lungo, massimo 255 caratteri";
						}
						break;
					}
				}
				if(prop.type !== undefined && errors[prop.id] === undefined && (prop.type.indexOf('Pdf') >= 0 && prop.type.indexOf('List') < 0)) {
					if(prop.lastModified === undefined) {
						prop.lastModified = new Date();
					}
					for(var c=0; c<form.formProperties.length; c++) {
						if(form.formProperties[c].id.indexOf(prop.id) !== 0 && form.formProperties[c].lastModified !== undefined && prop.lastModified !== undefined && form.formProperties[c].lastModified > prop.lastModified) {
							errors[prop.id] = "E' necessario generare nuovamente il PDF in quanto sono state fatte alcune modifiche ai dati";
							break;
						}
					}
				}
			}
			return errors;
		},
		
		getPropertiesToSubmit : function(form) {
			var properties = [];
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				if(prop.writable) {
					if(angular.isArray(prop.value)) {
//						properties.push({ 'id': prop.id, 'value': '[' + prop.value.join() + ']' });
						properties.push({ 'id': prop.id, 'value': angular.toJson(prop.value) });
					}
					else if(angular.isObject(prop.value)) {
						properties.push({ 'id': prop.id, 'value': angular.toJson(prop.value) });
					}
					else {
						properties.push({ 'id': prop.id, 'value': prop.value });
					}
				}
			}
			return properties;
		},
		
		getPropertiesValueMap : function(form) {
			var properties = {};
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				if(prop.writable) {
					properties[prop.id] = prop.value;
				}
			}
			return properties;
		},
		
		getPropertiesValueMapForPdf : function(form) {
			var properties = {};
			for(var i=0; i<form.formProperties.length; i++) {
				var prop = form.formProperties[i];
				if(prop.enumValues !== undefined && prop.enumValues.length > 0) {
					for(var j=0; j<prop.enumValues.length; j++) {
						properties[prop.id + '_' + prop.enumValues[j].id] = '';
					}
					if(prop.value !== undefined) {
						if(angular.isString(prop.value)) {
							for(var j=0; j<prop.enumValues.length; j++) {
								if(prop.enumValues[j].id === prop.value) {
									properties[prop.id + '_' + prop.enumValues[j].id] = 'X';
								}
							}
						}
						else if(angular.isArray(prop.value)) {
							for(var j=0; j<prop.enumValues.length; j++) {
								for(var k=0; k<prop.value.length; k++) {
									if(prop.enumValues[j].id === prop.value[k]) {
										properties[prop.id + '_' + prop.enumValues[j].id] = 'X';
									}
								}
							}
						}
					}
				}
				else if(prop.type === 'Date') {
					if(prop.value !== undefined) {
						properties[prop.id] = $filter('avFormatDate')(prop.value, 'DD/MM/YYYY');
					}
					else {
						properties[prop.id] = '';
					}
				}
				else if(prop.type === 'DateTime') {
					if(prop.value !== undefined) {
						properties[prop.id] = $filter('avFormatDateTime')(prop.value, 'DD/MM/YYYY HH:mm');
					}
					else {
						properties[prop.id] = '';
					}
				}
				else if(prop.type === 'Time') {
					if(prop.value !== undefined) {
						properties[prop.id] = $filter('avFormatTime')(prop.value, 'HH:mm');
					}
					else {
						properties[prop.id] = '';
					}
				}
				else if(prop.type === 'boolean') {
					properties[prop.id + '_true'] = '';
					properties[prop.id + '_false'] = '';
					if(prop.value !== undefined) {
						if(prop.value) {
							properties[prop.id + '_true'] = 'X';
						}
						else {
							properties[prop.id + '_false'] = 'X';
						}
					}
					else {
						properties[prop.id] = '';
					}
				}
				else if(prop.type === 'PatientEnrollmentServiceObject') {
					if(prop.value !== undefined) {
						var object = angular.copy(prop.value);
						if(angular.isArray(object)) {
							for(var j=0; j<prop['PatientDelegateContactTypeEnum'].enumValues.length; j++) {
								for(var k=0; k<object.length; k++) {
									if(prop['PatientDelegateContactTypeEnum'].enumValues[j].id === object[k].delegateContactType) {
										object[k]['delegateContactType_' + prop['PatientDelegateContactTypeEnum'].enumValues[j].id] = 'X';
									}
									else {
										object[k]['delegateContactType_' + prop['PatientDelegateContactTypeEnum'].enumValues[j].id] = '';
									}
								}
							}
							if(object.length == 1) {
								properties[prop.id] = object[0];
							}
							else {
								properties[prop.id] = object;
							}
						}
						else if(angular.isObject(object)) {
							for(var j=0; j<prop['PatientDelegateContactTypeEnum'].enumValues.length; j++) {
								if(prop['PatientDelegateContactTypeEnum'].enumValues[j].id === prop.value.delegateContactType) {
									object['delegateContactType_' + prop['PatientDelegateContactTypeEnum'].enumValues[j].id] = 'X';
								}
								else {
									object['delegateContactType_' + prop['PatientDelegateContactTypeEnum'].enumValues[j].id] = '';
								}
							}
							properties[prop.id] = object;
						}
					}
					else {
						properties[prop.id] = {};
					}
				}
				else {
					if(prop.value !== undefined) {
						properties[prop.id] = prop.value;
					}
					else {
						properties[prop.id] = '';
					}
				}
			}
			if($rootScope.completePropertiesForPdf !== undefined) {
				$rootScope.completePropertiesForPdf(form.processDefinitionId, properties);
			}
			return properties;
		},

		loadEnumValues : function(scope, prop, filters) {
			if(filters === undefined || filters == null) {
				filters = {};
			}
			var service = this;
			FormsService.loadEnumValues(
				{
					'type': prop.type
				},
				filters,
				function(response) {
					prop.enumValues = response.data;
					service.changeLabels(scope, prop);
				},
				function(error) {
					prop.enumValues = [];
				}
			)
		},
		
		loadObjectEnumValues : function(prop, type, filters, key) {
			if(filters === undefined || filters == null) {
				filters = {};
			}
			if(key === undefined || key == null) {
				key = type;
			}
			var service = this;
			prop[key] = [];
			FormsService.loadEnumValues(
				{
					'type': type
				},
				filters,
				function(response) {
					prop[key].enumValues = response.data;
				},
				function(error) {
					prop[key] = [];
				}
			)
		},
		
		loadChecklistValues : function(prop, filters) {
			if(filters === undefined || filters == null) {
				filters = {};
			}
			FormsService.loadChecklistValues(
				{
					'type': prop.type
				},
				filters,
				function(response) {
					prop.enumValues = response.data;
				},
				function(error) {
					prop.enumValues = [];
				}
			)
		},
		
		loadListValues : function(prop, filters) {
			if(filters === undefined || filters == null) {
				filters = {};
			}
			if(prop.type === 'PharmacyList') return;
			var service = this;
			FormsService.loadListValues(
				{
					'type': prop.type
				},
				filters,
				function(response) {
					prop.enumValues = response.data;
					if(prop.value !== undefined && prop.value != null && prop.value !== '') {
						var found = false;
						if(angular.isArray(prop.value)) {
							for(var k=0; k<prop.value.length; k++) {
								if(prop.value[k] !== undefined && prop.value[k] != null && prop.value[k] !== '') {
									for(var j=0; j<prop.enumValues.length; j++) {
										if(prop.enumValues[j].id === prop.value[k]) {
											found = true;
											break;
										}
									}
									if(!found) {
										if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
											prop.value[k] = undefined;
										}
									}
								}
							}
						}
						else {
							for(var j=0; j<prop.enumValues.length; j++) {
								if(prop.enumValues[j].id === prop.value) {
									found = true;
									break;
								}
							}
							if(!found) {
								if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
									prop.value = undefined;
								}
							}
						}
					}
					if(prop.type === 'NationalityList' && prop.value !== undefined && prop.value != null && prop.value !== '' && prop.enumValues !== undefined && prop.enumValues != null) {
						for(var j=0; j<prop.enumValues.length; j++) {
							if(prop.enumValues[j].id === prop.value) {
								prop.valueObject = prop.enumValues[j];
								break;
							}
						}
						if(prop.valueObject === undefined) {
							prop.valueObject = { "name" : prop.value};
						}
					}
					if(prop.type === 'SpecializationList' && prop.value !== undefined && prop.value != null && prop.value !== '' && prop.enumValues !== undefined && prop.enumValues != null) {
						for(var j=0; j<prop.enumValues.length; j++) {
							if(prop.enumValues[j].id === prop.value) {
								prop.valueObject = prop.enumValues[j];
								break;
							}
						}
						if(prop.valueObject === undefined) {
							prop.valueObject = { "name" : prop.value};
						}
					}
					
					if(prop.type.indexOf('Object') >= 0) {
						prop.value = angular.copy(prop.enumValues);
						if(prop.value !== undefined && prop.value != null) {
							if(prop.savedValue !== undefined && prop.savedValue != null) {
								var found = false;
								if(angular.isArray(prop.savedValue)) {
									for(var i=0; i<prop.value.length; i++) {
										if(!prop.value[i].readonly) {
											for(var k=0; k<prop.savedValue.length; k++) {
												if(prop.value[i].id == prop.savedValue[k].id) {
													var filters = prop.value[i].filters;
													var message = prop.value[i].message;
													prop.value[i] = angular.copy(prop.savedValue[k]);
													prop.value[i].filters = filters;// Lascio i filtri vecchi
													prop.value[i].message = message;// Lascio il messaggio vecchio
												}
											}
										}
									}
								}
								else {
									for(var i=0; i<prop.value.length; i++) {
										if(!prop.value[i].readonly) {
											if(prop.value[i].id == prop.savedValue.id) {
												var filters = prop.value[i].filters;
												var message = prop.value[i].message;
												prop.value[i] = angular.copy(prop.savedValue);
												prop.value[i].filters = filters;// Lascio i filtri vecchi
												prop.value[i].message = message;// Lascio il messaggio vecchio
											}
										}
									}
								}
							}
						}
						service.loadObjectSelected(prop);
					}
				},
				function(error) {
					prop.enumValues = [];
				}
			)
		},
		
		loadObjectListValues : function(prop, type, filters) {
			if(filters === undefined || filters == null) {
				filters = {};
			}
			var service = this;
			prop[type] = [];
			
			FormsService.loadListValues(
				{
					'type': type
				},
				filters,
				function(response) {
					prop[type].enumValues = response.data;
					if(prop.value !== undefined && prop.value != null && prop.value !== '') {
						var found = false;
						if(angular.isArray(prop.value)) {
							for(var k=0; k<prop.value.length; k++) {
								if(prop.value[k] !== undefined && prop.value[k] != null && prop.value[k] !== '') {
									for(var j=0; j<prop[type].enumValues.length; j++) {
										if(prop[type].enumValues[j].id === prop.value[k]) {
											found = true;
											break;
										}
									}
									if(!found) {
										if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
											prop.value[k] = undefined;
										}
									}
								}
							}
						}
						else {
							for(var j=0; j<prop[type].enumValues.length; j++) {
								if(prop[type].enumValues[j].id === prop.value) {
									found = true;
									break;
								}
							}
							if(!found) {
								if(prop.skipCheckListValue === undefined || prop.skipCheckListValue == null || !prop.skipCheckListValue) {
									prop.value = undefined;
								}
							}
						}
					}
					if(prop.type === 'NationalityList' && prop.value !== undefined && prop.value != null && prop.value !== '' && prop[type].enumValues !== undefined && prop[type].enumValues != null) {
						for(var j=0; j<prop[type].enumValues.length; j++) {
							if(prop[type].enumValues[j].id === prop.value) {
								prop.valueObject = prop[type].enumValues[j];
								break;
							}
						}
						if(prop.valueObject === undefined) {
							prop.valueObject = { "name" : prop.value};
						}
					}
					if(prop.type === 'SpecializationList' && prop.value !== undefined && prop.value != null && prop.value !== '' && prop[type].enumValues !== undefined && prop[type].enumValues != null) {
						for(var j=0; j<prop[type].enumValues.length; j++) {
							if(prop[type].enumValues[j].id === prop.value) {
								prop.valueObject = prop[type].enumValues[j];
								break;
							}
						}
						if(prop.valueObject === undefined) {
							prop.valueObject = { "name" : prop.value};
						}
					}
					
					if(prop.type.indexOf('Object') >= 0) {
						prop.value = angular.copy(prop[type].enumValues);
						if(prop.value !== undefined && prop.value != null) {
							if(prop.savedValue !== undefined && prop.savedValue != null) {
								var found = false;
								if(angular.isArray(prop.savedValue)) {
									for(var i=0; i<prop.value.length; i++) {
										if(!prop.value[i].readonly) {
											for(var k=0; k<prop.savedValue.length; k++) {
												if(prop.value[i].id == prop.savedValue[k].id) {
													prop.value[i] = angular.copy(prop.savedValue[k]);
												}
											}
										}
									}
								}
								else {
									for(var i=0; i<prop.value.length; i++) {
										if(!prop.value[i].readonly) {
											if(prop.value[i].id == prop.savedValue.id) {
												prop.value[i] = angular.copy(prop.savedValue);
											}
										}
									}
								}
							}
						}
						service.loadObjectSelected(prop);
					}
				},
				function(error) {
					prop[type].enumValues = [];
				}
			)
			
		},
		
		registerUserTasksWatch : function(scope, form, service) {
			$rootScope.reloadFormTasks = $rootScope.reloadFormTasks !== undefined ? $rootScope.reloadFormTasks++ : 0;
			scope.$watch(function() {
				return $rootScope.reloadFormTasks;
			},
			function(nv, ov) {
				if(nv !== ov) {
					for(var i=0; i<form.formProperties.length; i++) {
						var prop = form.formProperties[i];
						if(prop.type !== undefined && prop.type.indexOf('Model') >= 0) {
							service.loadUserTasks(prop, form.taskId);	
						}
					}
				}
			}, true);
		}
	}
});
