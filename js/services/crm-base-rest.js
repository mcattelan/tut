'use strict';

app.factory('PortalService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.CONSTANT, {}, {
		get: {
			method: 'GET',
			url: RESTURL.CONSTANT
		},
		getRedisStatus: {
			method: 'GET',
			url: RESTURL.REDIS_STATUS
		},
		setRunningRedisStatus: {
			method: 'GET',
			url: RESTURL.REDIS_RUNNING_STATUS
		},
		searchPortalAuthorizationKey: {
			method: 'POST',
			url: RESTURL.PORTAL_AUTHORIZATION_KEY + '/search'
		},
		getAllViewPermissions : {
			method : 'GET',
			url : RESTURL.ALL_VIEW_PERMISSIONS
		},
		getRelatedPortals : {
			method: 'GET',
			url: RESTURL.RELATED_PORTALS
		}
	});
}]);

app.factory('QualificationService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.QUALIFICATION, { }, {
		get: {
			method: 'GET'
		},
		delete: {
			method: 'DELETE'
		},
		search: {
			method: 'POST',
			url: RESTURL.QUALIFICATION + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
		getAll: {
			method: 'GET',
			url: RESTURL.QUALIFICATION
		}
	});
}]);

app.factory('UserService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.USER + '/:nickName', {nickName: '@nickName', userId: '@userId', token: '@token', code: '@code', userType: '@userType'}, {
		getLoggedUser : {
			method : 'GET',
			url: RESTURL.USER + '/logged'
		},
		getLoggedUserPortals : {
			method : 'GET',
			url: RESTURL.USER + '/logged/portals'
		},
		getUser : {
			method : 'GET',
			url: RESTURL.USER + '/:userId'
		},
		update: {
			method: 'PUT' 
		},
		impersonate : {
			method : 'POST',
			url: RESTURL.USER + '/impersonate/:nickName'
		},
		unimpersonate : {
			method : 'POST',
			url: RESTURL.USER + '/unimpersonate'
		},
		activationRequest: {
			method: 'GET',
			url: RESTURL.USER + '/public/activation/:token',
			skipAuth: true
		},
//		activationExistingUserRequest: {
//			method: 'GET',
//			url: RESTURL.PUBLIC + '/activation/existing/:token',
//			skipAuth: true
//		},
		activationConfirm: {
			method: 'POST',
			url: RESTURL.USER + '/public/activation/:token',
			skipAuth: true
		},
		forgotPasswordRequest: {
			method: 'GET',
			url: RESTURL.USER + '/public/forgot-password',
			skipAuth: true
		},
		forgotPasswordConfirm: {
			method: 'POST',
			url: RESTURL.USER + '/public/forgot-password',
			skipAuth: true
		},
		changeForgotPasswordRequest: {
			method: 'GET',
			url: RESTURL.USER + '/public/forgot-password/:token',
			skipAuth: true
		},
		changeForgotPasswordConfirm: {
			method: 'POST',
			url: RESTURL.USER + '/public/forgot-password/:token',
			skipAuth: true
		},
		search: {
			method: 'POST',
			url: RESTURL.USER + '/search'
		},
		activation:{
			method:'GET',
			url: RESTURL.USER + '/send-activation/:id'
		},
		authorizationKeyForUser:{
			method:'GET',
			url: RESTURL.USER + '/authorization-key-for-user'
		},
		getRequiredDisclaimers: {
			method: 'GET',
			url: RESTURL.USER + '/logged/disclaimer/required'
		},
		getUserDisclaimerByCode: {
			method: 'GET',
			url: RESTURL.USER +'/logged/disclaimer/code/:code'
		}
	});
}]);

app.factory('WorkflowService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.WORKFLOW, { processDefinitionKey : '@processDefinitionKey', businessKey: '@businessKey', taskId: '@taskId', historicTaskId: '@historicTaskId', userActivityId: '@userActivityId'}, {
		getProcessDefinition : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/process-definition/:processDefinitionKey'
		},
		getProcessDefinitionStartForm : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/process-definition/:processDefinitionKey/form'
		},
		startProcessDefinition : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/process-definition/:processDefinitionKey/start'
		},
		startProcessDefinitionWithForm : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/process-definition/:processDefinitionKey/start-with-form'
		},
		searchMyTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/todo'
		},
		countSearchMyTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/todo/count'
		},
		searchAssignedToOthersTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/assigned-to-others'
		},
		countSearchAssignedToOthersTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/assigned-to-others/count'
		},
		nextMyTasks : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/task/todo/next/:businessKey'
		},
		searchNotAssignedTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/not-assigned'
		},
		countSearchNotAssignedTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/not-assigned/count'
		},
		searchMyCompletedHistoricTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task-history/completed'
		},
		getTask : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/task/:taskId'
		},
		getCompletedHistoricTask : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/task-history/:historicTaskId'
		},
		getTaskForm : {
			method : 'GET',
			url: RESTURL.WORKFLOW + '/task/:taskId/form'
		},
		saveTaskForm : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/form'
		},
		saveTaskFormAndContinue : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/form/continue'
		},
		deleteTask : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/:messageName/delete'
		},
		assignTaskTo : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/assigns-to'
		},
		assignTaskToMe : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/assigns-to-me'
		},
		takeInCharge : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/:taskId/take-in-charge'
		},
		searchOtherUserTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/other-user-task/user-activity/:userActivityId'
		},
		countSearchOtherUserTasks : {
			method : 'POST',
			url: RESTURL.WORKFLOW + '/task/other-user-task/user-activity/:userActivityId/count'
		}
	});
}]);

app.factory('FormsService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.FORMS, {type: '@type'}, {
		save : {
			method : 'POST',
			url: RESTURL.FORMS + '/save'
		},
		saveAndContinue : {
			method : 'POST'
		},
		loadEnumValues : {
			method : 'POST',
			url: RESTURL.FORMS + '/enum-values/:type'
		},
		loadChecklistValues : {
			method : 'POST',
			url: RESTURL.FORMS + '/checklist-values/:type'
		},
		loadListValues : {
			method : 'POST',
			url: RESTURL.FORMS + '/list-values/:type'
		},
		loadModelValue : {
			method : 'GET',
			url: RESTURL.FORMS + '/model-value/:type/:id'
		},
		cancel: {
			method : 'PUT',
			url: RESTURL.EXECUTIONS + '/:executionId'
		}
	});
}]);

app.factory('PermissionService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.PERMISSION + '/:entityType', {entityType : '@entityType'}, {
		getAll : {
			method : 'GET',
			url: RESTURL.PERMISSION + '/:entityType'
		},
		getAllFields : {
			method : 'GET',
			url: RESTURL.PERMISSION + '/:entityType/fields'
		}
	});
}]);

app.factory('TerritoryService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.TERRITORY, {}, {
		searchCountries : {
			method : 'POST',
			url: RESTURL.TERRITORY + '/search-country'
		},
		searchRegions : {
			method : 'POST',
			url: RESTURL.TERRITORY + '/search-region'
		},
		searchProvinces : {
			method : 'POST',
			url: RESTURL.TERRITORY + '/search-province'
		},
		searchTowns : {
			method : 'POST',
			url: RESTURL.TERRITORY + '/search-town'
		},
		searchArea: {
			method: 'POST',
			url: RESTURL.TERRITORY + '/search-area'
		},
	});
}]);

app.factory('TrainingSubjectService', function($resource, RESTURL) {
	return $resource(RESTURL.TRAINING_SUBJECT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		delete: {
			method: 'DELETE'
		},
		search: {
			method: 'POST',
			url: RESTURL.TRAINING_SUBJECT + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		}
	});
});

app.factory('TrainingSessionService', function($resource, RESTURL) {
	return $resource(RESTURL.TRAINING_SESSION + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		delete: {
			method: 'DELETE'
		},
		search: {
			method: 'POST',
			url: RESTURL.TRAINING_SESSION + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		}
	});
});

app.factory('DocumentService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.DOCUMENT + '/:id', {id: '@id'}, {
		get : {
			method: 'GET',
			url: RESTURL.DOCUMENT +'/:id'
		},
		download : {
			method: 'GET',
			url: RESTURL.DOCUMENT +'/:id/url'
		},
		search : {
			method: 'POST',
			url: RESTURL.DOCUMENT + '/search'
		},
		update : {
			method: 'PUT',
			url: RESTURL.DOCUMENT + '/:id'
		},
		deleteDocument : {
			method: 'DELETE',
			url: RESTURL.DOCUMENT + '/:id'
		},
		getCategories : {
			method : 'GET',
			url: RESTURL.DOCUMENT + '/:entityType/document-categories'
		},
		getCategoriesForUpload : {
			method : 'GET',
			url: RESTURL.DOCUMENT + '/:entityType/document-categories/for-upload'
		},
		getUploadInfo : {
			method : 'POST',
			url: RESTURL.DOCUMENT + '/upload/info'
		},
		getDownloadInfo : {
			method : 'GET',
			url: RESTURL.DOCUMENT + '/:id/download/info'
		}
	});
}]);

app.factory('DocumentCategoryConfService', function($resource, RESTURL) {
	return $resource(RESTURL.DOCUMENT_CATEGORY_CONF + '/:id', {id: '@id'}, {
		get : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY_CONF +'/:id'
		},
		getAll : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY_CONF
		},
		search : {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY_CONF + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY_CONF + '/insert'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DOCUMENT_CATEGORY_CONF +'/:id/update'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('DocumentCategoryPermissionService', function($resource, RESTURL) {
	return $resource(RESTURL.DOCUMENT_CATEGORY_PERMISSION + '/:id', {id: '@id'}, {
		get : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY_PERMISSION +'/:id'
		},
		getAll : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY_PERMISSION
		},
		search : {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY_PERMISSION + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY_PERMISSION
		},
		update: {
			method: 'PUT',
			url: RESTURL.DOCUMENT_CATEGORY_PERMISSION +'/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('UserDisclaimerService',  ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.USER + '/:id', {id: '@id', code: '@code'}, {
		get : {
			method: 'GET',
			url: RESTURL.USER +'/logged/disclaimer/:id'
		},
		update: {
			method: 'PUT',
			url: RESTURL.USER +'/logged/disclaimer/:id'
		}
	});
}]);

app.factory('UserDisclaimerPublicService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.USER_DISCLAIMER_PUBLIC, {code: '@code', userType: '@userType'}, {
		getDisclaimersByCode: {
			method: 'GET',
			url: RESTURL.USER_DISCLAIMER_PUBLIC +'/code/:code'
		},
		getDisclaimersByCodeAndUserType: {
			method: 'GET',
			url: RESTURL.USER_DISCLAIMER_PUBLIC +'/code/:code/user-type/:userType'
		}
	});
}]);
